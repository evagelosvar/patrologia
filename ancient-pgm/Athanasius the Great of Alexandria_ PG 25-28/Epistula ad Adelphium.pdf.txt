Epistula ad Adelphium
ΤΟΥ ΕΝ ΑΓΙΟΙΣ ΠΑΤΡΟΣ ΗΜΩΝ ΑΘΑΝΑΣΙΟΥ, ΑΡΧΙΕΠΙΣΚΟΠΟΥ
ΑΛΕΞΑΝ∆ΡΕΙΑΣ, ΕΠΙΣΤΟΛΗ ΠΡΟΣ Α∆ΕΛΦΙΟΝ ΕΠΙΣΚΟΠΟΝ ΚΑΙ
ΟΜΟΛΟΓΗΤΗΝ, ΚΑΤΑ ΑΡΕΙΑΝΩΝ.
Ἐντυχόντες τοῖς παρὰ τῆς σῆς θεοσεβείας γραφεῖσι, τὴν μὲν σὴν εἰς Χριστὸν
εὐσέβειαν ἀληθῶς ἀπεδεξάμεθα, καὶ τὸν μὲν Θεὸν προηγουμένως ἐδοξάσαμεν,
δεδωκότα σοι τὴν τοιαύτην χάριν, ὥστε καὶ τὴν φρόνησιν ἔχειν ὀρθὴν, καὶ μὴ
ἀγνοεῖν κατὰ τὸ δυνατὸν τὰς μεθοδείας τοῦ διαβόλου· τὴν δὲ τῶν αἱρετικῶν
κακόνοιαν ἐθαυμάσαμεν, θεωροῦντες πῶς τοσοῦτον εἰς ἀσεβείας βάραθρον
πεπτώκασιν, ὡς μηκέτι μηδὲ τὰς αἰσθήσεις σώζειν, ἀλλὰ πανταχόθεν διεφθαρμένην
ἔχειν τὴν διάνοιαν. Τοῦτο δὲ τὸ ἐπιχείρημα διαβόλου μέν ἐστιν ὑποβολὴ, τῶν δὲ
παρανόμων Ἰου δαίων μίμησις. Ὥσπερ γὰρ ἐκεῖνοι, πανταχόθεν ἐλεγχόμενοι,
προφάσεις ἐπενόουν καθ' ἑαυτῶν, ἵνα μόνον τὸν Κύριον ἀρνήσωνται, καὶ τὰ
προφητευθέντα καθ' ἑαυτῶν ἐπισπάσωνται· τὸν αὐτὸν τρόπον καὶ οὗτοι,
θεωροῦντες ἑαυτοὺς πανταχόθεν στηλιτευομένους, καὶ βλέποντες γενομένην
βδελυκτὴν παρὰ πᾶσι τὴν αἵρεσιν αὐτῶν, ἐφευρεταὶ γίνονται κακῶν, ἵνα μὴ,
παυόμενοι ταῖς πρὸς τὴν ἀλήθειαν μάχαις, 26.1073 ἀληθῶς Χριστομάχοι διαμείνωσι.
Πόθεν γὰρ αὐτοῖς ἀνέδυ καὶ τοῦτο τὸ κακόν; Πῶς ὅλως τετολμήκασι τὴν καινὴν
ταύτην δυσφημίαν φθέγξασθαι κατὰ τοῦ Σωτῆρος; ἀλλ', ὡς ἔοικε, πονηρὸν ὁ ἀσεβὴς,
καὶ ἀδό κιμος ὄντως περὶ τὴν πίστιν. Πρότερον γὰρ, ἀρνούμενοι τὴν θεότητα τοῦ
μονογενοῦς Υἱοῦ τοῦ Θεοῦ, ὑπεκρίνοντο κἂν τὴν ἔνσαρκον αὐτοῦ παρουσίαν ἐπι
γινώσκειν· νῦν δὲ, κατ' ὀλίγον ὑποκαταβαίνοντες, ἐξέπεσαν καὶ ταύτης αὐτῶν τῆς
οἰήσεως, καὶ παντα χόθεν ἄθεοι γεγόνασιν, ὥστε μήτε Θεὸν αὐτὸν ἐπιγινώσκειν,
μήθ' ὅτι ἄνθρωπος γέγονε πιστεύειν. Εἰ γὰρ ἐπίστευον, οὐκ ἂν τοιαῦτα ἐφθέγξαντο,
οἷα ἔγραψεν ἡ σὴ θεοσέβεια κατ' αὐτῶν. Σὺ μὲν οὖν, ἀγαπητὲ καὶ ἀληθῶς ποθεινό
τατε, πρέποντα τῇ ἐκκλησιαστικῇ παραδόσει καὶ τῇ εἰς τὸν Κύριον εὐσεβείᾳ
πεποίηκας, ἐλέγξας, καὶ παραινέσας καὶ ἐπιτιμήσας τοῖς τοιούτοις· ἐπειδὴ δὲ,
παροξυνόμενοι παρὰ τοῦ πατρὸς αὐτῶν τοῦ δια βόλου, οὐκ ἔγνωσαν, οὐδὲ συνῆκαν,
κατὰ τὸ γε γραμμένον, ἀλλὰ ἐν σκότει διαπορεύονται· μανθανέτωσαν παρὰ τῆς σῆς
εὐλαβείας, ὡς ἡ τοιαύτη αὐτῶν κακόνοια Οὐαλεντίνου καὶ Μαρκίωνος καὶ
Μανιχαίου ἐστίν· ὧν οἱ μὲν ἀντὶ ἀληθείας δόκησιν εἰσηγήσαντο, οἱ δὲ διαιροῦντες
τὰ ἀδιαίρετα ἠρνήσαντο τὸ, Ὁ Λόγος σὰρξ ἐγένετο, καὶ ἐσκήνω σεν ἐν ἡμῖν. ∆ιὰ τί
οὖν, τὰ τούτων φρονοῦντες, οὐχὶ καὶ τῶν ὀνομάτων αὐτῶν γίνονται κληρονόμοι;
Εὔλογον γὰρ ὧν τὴν κακοδοξίαν ἔχουσι, τούτων ἔχειν καὶ τὰ ὀνόματα, ἵνα λοιπὸν
Οὐαλεντινιανοὶ καὶ Μαρκιωνισταὶ καὶ Μανιχαῖοι καλῶνται. Τάχα κἂν οὕτως διὰ τὴν
τῶν ὀνομάτων δυσωδίαν αἰσχυνόμενοι κατανοῆσαι δυνηθῶσιν, εἰς ὅσον βάθος
ἀσεβείας πε πτώκασι. Καὶ ἦν μὲν δίκαιον μηκέτι τούτοις ἀποκρί νασθαι, κατὰ τὴν
παραίνεσιν τὴν ἀποστολικήν· Αἱρετικὸν ἄνθρωπον μετὰ μίαν καὶ δευτέραν νου
θεσίαν παραιτοῦ, εἰδὼς, ὅτι ἐξέστραπται ὁ τοιοῦ τος, καὶ ἁμαρτάνει ὢν
αὐτοκατάκριτος· μάλιστα ὅτι καὶ ὁ προφήτης περὶ τῶν τοιούτων φησίν· Ὁ μωρὸς
μωρὰ λαλήσει, καὶ ἡ καρδία αὐτοῦ μάταια νοήσει· ἐπειδὴ δὲ κατὰ τὸν ἡγούμενον αὐ
τῶν περιέρχονται καὶ αὐτοὶ, ὡς λέοντες, ζητοῦντες τίνα καταπίωσι τῶν ἀκεραίων·
τούτου ἕνεκεν ἀναγκαῖον γέγονεν ἡμῖν τῇ σῇ εὐλαβείᾳ ἀντιγράψαι, ἵνα, πάλιν διὰ
τῆς σῆς νουθεσίας μαθόντες οἱ ἀδελφοὶ, καταγινώσκωσιν ἐπὶ πλεῖον τῆς ἐκείνων
ματαιολογίας. Οὐ κτίσμα προσκυνοῦμεν· μὴ γένοιτο! Ἐθνι κῶν γὰρ καὶ Ἀρειανῶν ἡ
 
 
 

1
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

τοιαύτη πλάνη· ἀλλὰ τὸν Κύριον τῆς κτίσεως σαρκωθέντα τὸν τοῦ Θεοῦ Λόγον
προσκυνοῦμεν. Εἰ γὰρ καὶ ἡ σὰρξ αὐτὴ καθ' 26.1076 ἑαυτὴν μέρος ἐστὶ τῶν
κτισμάτων, ἀλλὰ Θεοῦ γέγονε σῶμα. Καὶ οὔτε τὸ τοιοῦτον σῶμα καθ' ἑαυτὸ διαι
ροῦντες ἀπὸ τοῦ Λόγου προσκυνοῦμεν· οὔτε τὸν Λόγον προσκυνῆσαι θέλοντες,
μακρύνομεν αὐτὸν ἀπὸ τῆς σαρκός· ἀλλ' εἰδότες, καθὰ προείπομεν, τὸ, Ὁ Λόγος σὰρξ
ἐγένετο, τοῦτον καὶ ἐν σαρκὶ γενόμε νον, ἐπιγινώσκομεν Θεόν. Τίς τοιγαροῦν οὕτως
ἄφρων ἐστὶν, ὡς λέγειν τῷ Κυρίῳ· Ἀπόστα ἀπὸ τοῦ σώματος, ἵνα σε προσκυνήσω; Ἢ
τίς οὕτως ἀσεβής ἐστιν, ὡς μετὰ τῶν ἀφρόνων Ἰουδαίων διὰ τὸ σῶμα λέγειν αὐτῷ·
∆ιὰ τί σὺ, ἄνθρωπος ὢν, ποιεῖς σεαυτὸν Θεόν; Ἀλλ' οὐ τοιοῦτος ὁ λεπρός·
προσεκύνει γὰρ τὸν Θεὸν ἐν σώματι ὄντα, καὶ ἐγί νωσκεν, ὅτι Θεὸς ἦν, λέγων· Κύριε,
ἐὰν θέλῃς, δύνασαί με καθαρίσαι. Καὶ οὔτε διὰ τὴν σάρκα ἐνόμισε κτίσμα τὸν τοῦ
Θεοῦ Λόγον· οὔτε διὰ τὸ εἶναι τὸν Λόγον δημιουργὸν πάσης κτίσεως, ἐξουθένει τὴν
σάρκα, ἣν ἐνδεδυμένος ἦν· ἀλλ' ὡς ἐν κτιστῷ ναῷ τὸν κτίστην τοῦ παντὸς
προσεκύνει, καὶ ἐκαθαρί ζετο. Οὕτως καὶ ἡ αἱμοῤῥοοῦσα γυνὴ, πιστεύσασα καὶ μόνον
ἁψαμένη τοῦ κρασπέδου, ἰάθη· καὶ ἡ μὲν θάλαττα ἐπαφρίζουσα τοῖς κύμασιν ἤκουσε
τοῦ σαρκωθέντος Λόγου, καὶ πέπαυται τοῦ χειμῶνος· ὁ δὲ ἐκ γενετῆς τυφλὸς,
πτύσματι τῆς σαρκὸς τεθερά πευται παρὰ τοῦ Λόγου. Καὶ τό γε μεῖζον καὶ
παραδοξότερον (ἴσως γὰρ τοῦτο καὶ τοὺς ἀσεβεστάτους ἐσκανδάλισε), καὶ ἐπ' αὐτοῦ
τοῦ σταυροῦ κρεμαμένου τοῦ Κυρίου (αὐτοῦ γὰρ ἦν τὸ σῶμα, καὶ ἐν αὐτῷ ἦν ὁ
Λόγος), ὁ μὲν ἥλιος ἐσκοτίσθη, ἡ δὲ γῆ ἐτρόμασεν, αἱ πέτραι ἐσχίσθησαν, καὶ τὸ
καταπέτασμα τοῦ ναοῦ ἐσχίσθη, καὶ πολλὰ σώματα τῶν κεκοιμη μένων ἁγίων
ἀνέστη. Ταῦτα δὲ γέγονε, καὶ οὐδεὶς, ὥσπερ νῦν οἱ Ἀρειανοὶ τολμῶσι, διελογίσατο, εἰ
δεῖ σαρκωθέντι τῷ Λόγῳ πείθεσθαι· ἀλλὰ καὶ ἄνθρωπον βλέποντες, ἐπεγίνωσκον
αὐτὸν ὄντα δημιουργὸν αὐτῶν· καὶ ἀνθρωπίνης φωνῆς ἀκούοντες, οὐ διὰ τὸ
ἀνθρώπι νον ἔλεγον, ὅτι ὁ Λόγος κτίσμα ἐστίν· ἀλλὰ καὶ μᾶλλον ἔτρεμον, καὶ οὐδὲν
ἧττον ἐγίνωσκον, ἢ ὅτι ἐκ ναοῦ ἁγίου ἐφθέγγετο. Πῶς τοίνυν οἱ ἀσεβεῖς οὐ
φοβοῦνται μὴ, καθὼς οὐκ ἐδοκίμασαν τὸν Θεὸν ἔχειν ἐν ἐπιγνώσει, παραδοθῶσιν εἰς
ἀδόκιμον νοῦν, ποιεῖν τὰ μὴ καθήκοντα; Οὐ γὰρ κτίσματι ἡ κτίσις προσ κυνεῖ· οὐδὲ
πάλιν διὰ τὴν σάρκα παρῃτεῖτο τὸν Κύ ριον ἑαυτῆς προσκυνεῖν, ἀλλὰ τὸν ἑαυτῆς
δημιουργὸν ἔβλεπεν ἐν σώματι· καὶ ἐν τῷ ὀνόματι Ἰησοῦ Χριστοῦ πᾶν γόνυ ἔκαμπτε,
καὶ κάμψει δὲ, ἐπουρανίων 26.1077 καὶ ἐπιγείων, καὶ καταχθονίων, καὶ πᾶσα
γλῶσσα ἐξομολογήσεται, κἂν τοῖς Ἀρειανοῖς μὴ δοκῇ, ὅτι Κύριος Ἰησοῦς Χριστὸς εἰς
δόξαν Θεοῦ Πατρός. Οὐ γὰρ ἀδοξίαν ἤνεγκεν ἡ σὰρξ τῷ Λόγῳ· μὴ γένοιτο! ἀλλὰ
μᾶλλον αὕτη δεδόξασται παρ' αὐτοῦ. Οὐδὲ, ἐπειδὴ δούλου μορφὴν ἀνέλαβεν ὁ ἐν
μορφῇ Θεοῦ ὑπάρχων Υἱὸς, ἠλαττώθη τῆς θεότητος· ἀλλὰ μᾶλλον αὐτὸς
ἐλευθερωτὴς πάσης σαρκὸς καὶ πάσης κτίσεως γέγονε. Καὶ εἰ ὁ Θεὸς δὲ ἀπέστειλε τὸν
Υἱὸν αὐτοῦ γεννώμενον ἐκ γυναικὸς, οὐκ αἰσχύνην ἡμῖν ἐπάγει τὸ πρᾶγμα, ἀλλὰ
μᾶλλον εὐδοξίαν καὶ μεγάλην χάριν. Γέγονε γὰρ ἄνθρωπος, ἵν' ἡμᾶς ἐν ἑαυτῷ
θεοποιήσῃ· καὶ γέγονεν ἐκ γυναικὸς, καὶ γεγέννηται ἐκ Παρθέ νου, ἵνα τὴν ἡμῶν
πλανηθεῖσαν γέννησιν εἰς ἑαυτὸν μετενέγκῃ, καὶ γενώμεθα λοιπὸν γένος ἅγιον, καὶ
κοινωνοὶ θείας φύσεως, ὡς ἔγραψεν ὁ μακάριος Πέ τρος· Καὶ τὸ ἀδύνατον δὲ τοῦ
νόμου, ἐν ᾧ ἠσθένει διὰ τῆς σαρκὸς, ὁ Θεὸς τὸν ἑαυτοῦ Υἱὸν πέμψας ἐν ὁμοιώματι
σαρκὸς ἁμαρτίας, καὶ περὶ ἁμαρτίας κατέκρινε τὴν ἁμαρτίαν ἐν τῇ σαρκί. Τὴν τοίνυν
προσληφθεῖσαν ὑπὸ τοῦ Λόγου σάρκα, ἐπὶ τῷ πάντας ἀνθρώπους ἐλευθερῶσαι, καὶ
πάντας ἐκ νεκρῶν ἀναστῆσαι, καὶ ἁμαρτίας λυ τρώσασθαι, πῶς οἱ ταύτην
ἐξουδενοῦντες, ἢ οἱ διὰ ταύτην κατηγοροῦντες τοῦ Υἱοῦ τοῦ Θεοῦ ὡς ποιήματος ἢ
κτίσματος, οὐκ ἀχάριστοι φαίνονται, καὶ παντός εἰσι μίσους ἄξιοι; Μόνον γὰρ οὐχὶ
βοῶσι τῷ Θεῷ λέγοντες· Μὴ πέμψῃς σου τὸν Υἱὸν τὸν μονογενῆ ἐν σαρκί· μὴ
 
 
 

2
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

ποιήσῃς αὐτὸν ἐκ Παρθένου σάρκα λαβεῖν, ἵνα μὴ λυτρώσηται ἡμᾶς τοῦ θανάτου καὶ
τῆς ἁμαρτίας. Οὐ θέλομεν αὐτὸν ἐν σώματι γενέσθαι, ἵνα μὴ τὸν ὑπὲρ ἡμῶν
ἀναδέξηται θάνατον· οὐ βουλόμεθα τὸν Λόγον σάρκα γενέσθαι, ἵνα μὴ ἐν ταύ τῃ
μεσίτης ἡμῶν γένηται τῆς πρὸς σὲ προσόδου καὶ τὰς ἐν οὐρανοῖς οἰκήσωμεν μονάς.
Κλεισθήτωσαν αἱ πύλαι τῶν οὐρανῶν, ἵνα μὴ διὰ τοῦ καταπετάσμα τος τῆς σαρκὸς
αὐτοῦ τὴν ἐν οὐρανοῖς ἡμῖν ὁδὸν ἐγκαινίσῃ ὁ σὸς Λόγος. Αὗται μὲν αἱ ἐκείνων
φωναὶ μετὰ διαβολικῆς τόλμης εἰσὶ προφερόμεναι, δι' ἧς ἐπενόησαν ἑαυτοῖς
κακονοίας· οἱ γὰρ μὴ θέλοντες σάρκα γενόμενον τὸν Λόγον προσκυνεῖν,
ἀχαριστοῦσι τῇ ἐνανθρωπήσει αὐτοῦ· καὶ οἱ διαιροῦντες τὸν Λόγον ἀπὸ τῆς σαρκὸς
οὐδὲ μίαν λύτρωσιν τῆς ἁμαρτίας γενέσθαι, οὐδὲ κατάλυσιν τοῦ θανάτου νομίζουσι.
Ποῦ δὲ ὅλως οἱ ἀσεβεῖς καὶ καθ' ἑαυτὴν εὑρήσουσι τὴν σάρκα, ἣν ἔλαβεν ὁ Σωτὴρ,
ἵνα καὶ τολμῶσι λέγειν· Οὐ προσκυνοῦμεν ἡμεῖς τὸν Κύριον μετὰ τῆς σαρκός· ἀλλὰ
διαιροῦμεν τὸ σῶμα, καὶ μόνῳ τούτῳ λατρεύομεν; Ὁ μὲν οὖν μακάριος Στέφανος ἐν
οὐρανοῖς εἶδε τὸν Κύριον ἐκ δεξιῶν ἑστῶτα· οἱ δὲ ἄγγελοι ἔλεγον τοῖς μαθηταῖς·
Οὕτως ἐλεύσεται ὃν τρόπον ἐθεά 26.1080 σασθε αὐτὸν πορευόμενον εἰς τὸν οὐρανόν.
Καὶ αὐτὸς δὲ ὁ Κύριος εἴρηκε, προσφωνῶν τῷ Πατρί· Θέλω, ὅπου εἰμὶ ἐγὼ, ἵνα καὶ
αὐτοὶ πάντοτε μετ' ἐμοῦ ὦσι. Καὶ ὅλως, εἰ ἀδιαίρετός ἐστιν ἡ σὰρξ ἀπὸ τοῦ Λόγου,
πῶς οὐκ ἀνάγκη τούτους ἀποθέσθαι τὴν πλάνην, καὶ προσκυνεῖν λοιπὸν τὸν Πατέρα
ἐν τῷ ὀνόματι τοῦ Κυρίου ἡμῶν Ἰησοῦ Χριστοῦ· ἢ, μὴ προσκυνοῦντας, μηδὲ
λατρεύοντας τῷ ἐν σαρκὶ γενομένῳ Λόγῳ, πανταχόθεν ἐκβάλλεσθαι, καὶ μηκέτι
Χριστιανοὺς, ἀλλ' ἢ ἐθνικοὺς, ἢ μετὰ τῶν Ἰουδαίων συναριθμεῖσθαι; Ἐκείνων μὲν
οὖν, καθὰ προείπομεν, ἡ τοιαύτη μανία καὶ τόλμα· ἡμῶν δὲ ἡ πίστις ἐστὶν ὀρθὴ, καὶ
ἐκ διδασκαλίας ἀποστολικῆς ὁρμωμένη καὶ παραδόσεως τῶν Πατέρων, βεβαιουμένη
ἔκ τε Νέας καὶ Παλαιᾶς ∆ιαθήκης, τῶν μὲν προφητῶν λεγόντων· Ἀπόστειλον τὸν
Λόγον σου καὶ τὴν ἀλήθειάν σου· καὶ, Ἰδοὺ ἡ Παρθένος ἐν γαστρὶ ἕξει καὶ τέξεται
Υἱὸν, καὶ καλέσουσι τὸ ὄνομα αὐτοῦ Ἐμμανουὴλ, ὅ ἐστι μεθερμηνευόμενον, Μεθ'
ἡμῶν ὁ Θεός. Τί δὲ τοῦτό ἐστιν ἢ Θεὸν ἐν σαρκὶ γεγενῆσθαι; Ἡ δὲ ἀποστολικὴ
παράδοσις διδάσκει, τοῦ μὲν μακαρίου Πέτρου λέγοντος· Χριστοῦ οὖν ὑπὲρ ἡμῶν
παθόντος σαρκί· τοῦ δὲ Παύλου γρά φοντος· Προσδεχόμενοι τὴν μακαρίαν ἐλπίδα,
καὶ ἐπιφάνειαν τῆς δόξης τοῦ μεγάλου Θεοῦ καὶ Σωτῆρος ἡμῶν Ἰησοῦ Χριστοῦ, ὃς
ἔδωκεν ἑαυτὸν ὑπὲρ ἡμῶν, ἵνα λυτρώσηται ἡμᾶς ἀπὸ πάσης ἁμαρτίας, καὶ καθαρίσῃ
ἑαυτῷ λαὸν περιούσιον, καὶ ζηλωτὴν καλῶν ἔργων. Πῶς οὖν δέδωκεν αὐ τὸν ὑπὲρ
ἡμῶν, εἰ μὴ σάρκα φορέσας ἦν; ταύτην γὰρ προσενέγκας, ἑαυτὸν ὑπὲρ ἡμῶν
δέδωκεν, ἵν', ἐν ἐκείνῃ τὸν θάνατον ἀναδεξάμενος, καταργήσῃ τὸν τὸ κράτος ἔχοντα
τοῦ θανάτου διάβολον. ∆ιὸ καὶ πάντοτε εὐχαριστοῦμεν ἐν τῷ ὀνόματι Ἰησοῦ
Χριστοῦ· καὶ οὐκ ἀθετοῦμεν τὴν δι' αὐτοῦ γενομένην χάριν εἰς ἡμᾶς. Ἡ γὰρ
ἔνσαρκος παρουσία τοῦ Σωτῆρος θανάτου λύτρον καὶ κτίσεως πάσης σωτηρία
γέγονεν. Ὥστε, ἀγαπητὲ καὶ ποθεινότατε, οἱ μὲν ἀγαπῶν τες τὸν Κύριον
ὑπομιμνησκέσθωσαν ἐκ τούτων· οἱ δὲ τὸν Ἰούδα τρόπον μιμησάμενοι, καὶ
καταλιπόντες τὸν Κύριον, ἵνα μετὰ τοῦ Καϊάφα γένωνται, με ταπαιδευέσθωσαν ἐν
τούτοις, ἐὰν ἄρα θελήσωσιν, ἐὰν ἄρα αἰσχυνθῶσι. Καὶ γινωσκέτωσαν, ὅτι, τὸν
Κύριον ἐν σαρκὶ προσκυνοῦντες, οὐ κτίσματι προσκυνοῦμεν, ἀλλὰ τὸν κτίστην
ἐνδυσάμενον τὸ κτιστὸν σῶμα, καθὰ προείπομεν. Ἐβουλόμεθα δὲ τὴν σὴν εὐλάβειαν
πυθέσθαι παρ' αὐτῶν τοῦτο· ἡνίκα ὁ Ἰσραὴλ ἐκελεύετο ἀνέρχεσθαι εἰς τὴν
Ἱερουσαλὴμ προσκυνεῖν ἐν τῷ ναῷ Κυρίου, ἔνθα ἡ κιβωτὸς καὶ ὑπεράνω ταύτης τὰ
χερουβὶμ τῆς δόξης κατασκιάζοντα τὸ ἱλαστήριον, καλῶς ἐποίουν ἢ τοὐναντίον; Εἰ
μὲν οὖν φαύλως ἔπραττον, πῶς οἱ τοῦ νόμου τούτου κατολιγωροῦντες ὑπὸ
ἐπιτιμίαν ἐγίνον το; Γέγραπται γάρ· Ὃς ἐὰν ἐξουθενήσῃ καὶ μὴ ἀναβῇ,
 
 
 

3
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

ἐξολοθρευθήσεται ἐκ τοῦ λαοῦ· εἰ δὲ καλῶς 26.1081 ἔπραττον, καὶ ἐν τούτῳ
εὐάρεστοι τῷ Θεῷ ἐγίνοντο· πῶς οὐκ ἀπολωλέναι πολλάκις εἰσὶν ἄξιοι οἱ μιαροὶ καὶ
πάσης αἱρέσεως αἴσχιστοι Ἀρειανοὶ, ὅτι, τὸν πάλαι λαὸν ἀποδεχόμενοι διὰ τὴν πρὸς
τὸν ναὸν τιμὴν, οὐ βούλονται τὸν Κύριον ἐν σαρκὶ, ὡς ἐν ναῷ ὄντα, προσκυνεῖν;
Καίτοι ὁ πάλαι ναὸς ἐκλίθων ἦν καὶ χρυσοῦ κατασκευασθεὶς, ὡς σκιά· ἐλθούσης δὲ
τῆς ἀληθείας, πέπαυται λοιπὸν ὁ τύπος, καὶ οὐκ ἔμεινε, κατὰ τὴν Κυριακὴν φωνὴν,
λίθος ἐπὶ λίθον ἐν αὐτῷ, ὃς οὐ κατελύθη. Καὶ οὔτε, τὸν ναὸν βλέποντες ἐκ λίθων,
ἐνόμιζον καὶ τὸν ἐν αὐτῷ ναῷ λαλοῦντα Κύριον εἶναι κτίσμα, οὔτε, τὸν ναὸν
ἐξουθενοῦντες, ἀπερχόμενοι μακρὰν προσεκύνουν· ἀλλ' εἰς αὐτὸν ἐρχόμενοι
νομίμως ἐλάτρευον τῷ Θεῷ ἀπὸ τοῦ ναοῦ χρηματίζοντι. Τούτου δὲ οὕτως
γενομένου, πῶς τὸ σῶμα τοῦ Κυρίου, τὸ πανάγιον καὶ πάνσεπτον ἀληθῶς, ὑπὸ μὲν
τοῦ ἀρχαγγέλου Γαβριὴλ εὐαγγε λισθὲν, ὑπὸ δὲ τοῦ ἁγίου Πνεύματος πλασθὲν, καὶ
τοῦ Λόγου γεγονὸς ἔνδυμα, οὐ προσκυνητόν; Χεῖρα γοῦν σωματικὴν ἐκτείνας ὁ
Λόγος, ἤγειρε τὴν πυρέσσουσαν· καὶ φωνὴν ἀνθρωπίνην ἀφεὶς, ἤγειρε τὸν Λάζαρον
ἐκ νεκρῶν· καὶ πάλιν τὰς χεῖρας ἐκτείνας ἐπὶ τοῦ σταυροῦ, τὸν μὲν ἄρχοντα τῆς ἐξου
σίας τοῦ ἀέρος, τοῦ νῦν ἐνεργοῦντος ἐν τοῖς υἱοῖς τῆς ἀπειθείας, κατέβαλε, τὴν δὲ
ὁδὸν ἡμῖν ἐν τοῖς οὐρανοῖς καθαρὰν ἐποίει. Οὐκοῦν ὁ τὸν ναὸν ἀτιμάζων, ἀτιμάζει
τὸν ἐν τῷ ναῷ Κύριον· καὶ ὁ διαιρῶν τὸν Λόγον ἀπὸ τοῦ σώματος, ἀθετεῖ τὴν χάριν
τὴν δοθεῖσαν ἡμῖν ἐν αὐ τῷ. Καὶ ἀσεβέστατοι δὲ Ἀρειομανῖται μὴ, ἐπειδὴ τὸ σῶμα
κτιστόν ἐστι, νομιζέτωσαν καὶ τὸν Λόγον εἶναι κτίσμα, μηδὲ διὰ τὸ μὴ εἶναι τὸν
Λόγον κτίσμα, διαβαλλέτωσαν τὸ σῶμα αὐτοῦ. Θαυμάσαι γάρ ἐστιν αὐτῶν τὴν
κακόνοιαν, ὅτι καὶ πάντα κυκῶσι καὶ φύρουσι, καὶ προφάσεις ἐφευρίσκουσιν, ἵνα
μόνον τὸν κτίστην τοῖς κτίσμασι συναριθμήσωσιν. Ἀλλ' ἀκουέτωσαν· Εἰ κτίσμα ἦν ὁ
Λόγος, οὐ προσελάμ βανε τὸ κτιστὸν σῶμα, ἵνα αὐτὸ ζωοποιήσῃ. Ποία γὰρ τοῖς
κτίσμασι παρὰ κτίσματος ἔσται βοήθεια, δεομέ νου καὶ αὐτοῦ σωτηρίας; Ἀλλ' ἐπειδὴ,
κτίστης ὢν ὁ Λόγος, αὐτὸς δημιουργὸς γέγονε τῶν κτισμάτων· διὰ τοῦτο καὶ ἐπὶ
συντελείᾳ τῶν αἰώνων τὸ κτιστὸν αὐτὸς ἐνεδύσατο, ἵνα πάλιν αὐτὸς, ὡς κτίστης,
ἀνακαινίσῃ, καὶ ἀνακτῆσαι τοῦτο δυνηθῇ. Κτίσμα δὲ ὑπὸ κτίσματος οὐκ ἄν ποτε
σωθῇ, ὥσπερ οὐδὲ ὑπὸ κτίσματος ἐκτίσθησαν τὰ κτίσματα, εἰ μὴ 26.1084 κτίστης ἦν ὁ
Λόγος. Ὅθεν μὴ καταψευδέσθωσαν τῶν θείων Γραφῶν, μηδὲ σκανδαλιζέτωσαν τοὺς
ἀκεραίους τῶν ἀδελφῶν· ἀλλ' εἰ μὲν βούλωνται, μεταγινωσκέτωσαν καὶ αὐτοὶ, καὶ
μηκέτι τῇ κτίσει λατρευέτωσαν παρὰ τὸν κτίσαντα τὰ πάντα Θεόν· ἐὰν δὲ ταῖς
ἀσεβείαις αὐτῶν ἐμμένειν ἐθέλωσι, μόνοι τούτων ἐμφο ρείσθωσαν, καὶ τριζέτωσαν
τοὺς ὀδόντας κατὰ τὸν πατέρα αὐτῶν τὸν διάβολον, ὅτι ἡ πίστις τῆς καθολικῆς
Ἐκκλησίας, κτίστην οἶδε τὸν τοῦ Θεοῦ Λό γον καὶ δημιουργὸν τῶν ἁπάντων, καὶ
οἴδαμεν, ὅτι Ἐν ἀρχῇ μὲν ἦν ὁ Λόγος, καὶ ὁ Λόγος ἦν πρὸς τὸν Θεόν· γενόμενον δὲ
αὐτὸν καὶ ἄνθρωπον διὰ τὴν ἡμετέραν σωτηρίαν προσκυνοῦμεν, οὐχ ὡς ἴσον ἐν ἴσῳ
γενόμενον τῷ σώματι, ἀλλ' ὡς ∆εσπότην προσλαβόντα τὴν τοῦ δούλου μορφὴν, καὶ
δημιουργὸν καὶ κτίστην ἐν κτίσματι γινόμενον, ἵν', ἐν αὐτῷ τὰ πάντα ἐλευθερώσας,
τὸν κόσμον προσαγάγῃ τῷ Πατρὶ, καὶ εἰρηνοποιήσῃ τὰ πάντα, τὰ ἐν οὐρανοῖς καὶ τὰ
ἐπὶ τῆς γῆς. Οὕτω γὰρ καὶ τὴν πατρικὴν αὐ-τοῦ θεότητα ἐπιγινώσκομεν, καὶ τὴν
ἔνσαρκον αὐτοῦ παρουσίαν προσκυνοῦμεν, κἂν Ἀρειομανῖται διαῤῥηγνύωσιν
ἑαυτούς. Προσαγόρευε πάντας τοὺς ἀγαπῶντας τὸν Κύριον ἡμῶν Ἰησοῦν Χριστόν.
Ἐῤῥῶσθαί σε καὶ μνημονεύειν ἡμῶν τῷ Κυρίῳ εὐχόμεθα, ἀγαπητὲ καὶ ὡς ἀληθῶς
ποθεινότατε, ἐὰν χρεία ᾖ ἀναγνωσθῆναι ταῦτα Ἱερακᾷ τῷ πρεσβυτέρῳ.

 
 
 

4
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

