In Mamantem martyrem
ΟΜΙΛΙΑ ΚΓʹ.
Εἰς τὸν ἅγιον μάρτυρα Μάμαντα.
Οὐκ ἀγνοῶ τὸ μέγεθος τῶν ὑπὸ τῆς πανηγύ ρεως ἐγκωμίων· ἀλλ' ὥσπερ
τοῦτο ἐπίσταμαι, οὕτω καὶ τῆς ἀσθενείας τῆς ἐμαυτοῦ ἐπαισθάνομαι. Ἡ μὲν γὰρ
ὑπόθεσις ἀπαιτεῖ ἄξιόν τι ῥηθῆναι τῶν συν ελθόντων καὶ τῆς ἐλπίδος ἧς ἔχουσιν ἐφ'
ἡμῖν, καὶ τῆς ὑποθέσεως. Ἐπειδὴ γὰρ ἐπὶ τῇ μεγίστῃ τῶν μαρτύρων ἄγομεν τὴν
μνήμην σήμερον, ὀρθὴ πᾶσα διάνοια καὶ ἐμπαράσκευος ἀκοὴ, εἰπεῖν τι ἄξιον τοῦ
μάρτυρος προσδοκῶσα, καὶ πόθῳ τῷ περὶ αὐτὸν τὴν ἐκκλησίαν ἐπάγει. Καὶ γὰρ
εὐγνώμονες παῖδες με γάλα ἀπαιτοῦσι τὰ τῶν πατέρων ἐγκώμια, καὶ οὐκ ἂν
καταδέξαιντο ἐν μικρότητι τοῦ λέγοντος κινδυ νεύειν τῶν ἐγκωμιαζομένων τὸ
μέγεθος. Ὥστε ὅσῳ μείζων ἡ προθυμία, τοσούτῳ μέγας ὁ κίνδυνος. Τί οὖν
ποιήσομεν; Πῶς δὲ καὶ τὰς ὑμετέρας ἐπιθυμίας ἀποπληρώσομεν, καὶ αὐτοὶ μὴ
ἀπέλθωμεν ἀσυντε λεῖς τῶν παρόντων; Παρακαλέσομεν ἑκάστην ψυχὴν, ἃ ἔχουσα
ἦλθεν ἐν τῇ μνήμῃ, ταῦτα ἀνακαινήσασαν τῇ διανοίᾳ, οἴκοθεν ἐπισιτισαμένην, τοῖς
οἰκείοις ἐφοδίοις ἑαυτὴν εὐφράνασαν ἀπελθεῖν.
Μνήσθητέ μοι τοῦ μάρτυρος, ὅσοι δι' ὀνείρων αὐτοῦ ἀπηλαύσατε· ὅσοι,
περιτυχόντες τῷ τόπῳ τούτῳ, ἐσχήκεσαν αὐτὸν συνεργὸν εἰς προσευχήν· ὅσοις,
ὀνόματι κληθεὶς, ἐπὶ τῶν ἔργων παρέστη· ὅσους ὁδοιπόρους ἐπαν ήγαγεν· ὅσους ἐξ
ἀῤῥωστίας ἀνέστησεν· ὅσοις παῖδας ἀπέδωκεν ἤδη τετελευτηκότας· ὅσοις
προθεσμίας βίου μακροτέρας ἐποίησεν. Πάντα μοι συναγαγόντες, ἐγκώμιον ἐκ
κοινοῦ ἐράνου πονήσατε. Ἀλλήλοις διάδοτε, ἃ οἶδεν ἕκαστος, τῷ μὴ εἰδότι· ἃ μὴ
οἶδε, λαμβανέτω παρὰ τοῦ εἰδότος, καὶ οὕτως ἐκ συνεισ φορᾶς ἀλλήλους
συνεστιάσαντες, ἡμῶν τῇ ἀσθενείᾳ σύγγνωτε. Ταῦτα γὰρ ἐγκώμια μάρτυρος, ὁ
πλοῦτος τῶν 31.592 πνευματικῶν χαρισμάτων. Οὐ γὰρ ἔχομεν αὐτὸν κατὰ τὸν
νόμον τῶν ἔξωθεν ἐγκωμίων ἀποσεμνύνειν· οὐκ ἔχομεν λέγειν πατέρας καὶ
προγόνους περιφα νεῖς. Αἰσχρὸν γὰρ ἀλλοτρίοις κόσμοις κοσμεῖσθαι τὸν τῇ οἰκείᾳ
ἀρετῇ διαφανῆ. Συνηθείας γὰρ νόμοις τὰ τοιαῦτα ἐν τοῖς ἐγκωμίοις
παραλαμβάνουσιν. Ἐπεὶ ὅ γε τῆς ἀληθείας ἴδια ἑκάστου ἀπαιτεῖ τὰ ἐγκώμια. Οὔτε
γὰρ ἵππον ταχὺν ποιεῖ ἡ τοῦ πατρὸς περὶ τὸν δρόμον εὐμοιρία, οὔτε κυνὸς ἐγκώμιον
τὸ ἐκ ταχυτάτων φῦναι. Ἀλλ' ὥσπερ ἄλλων ζώων ἡ ἀρε τὴ ἐν αὐτῷ θεωρεῖται
ἑκάστῳ, οὕτω καὶ ἀνδρὸς ἴδιος ἔπαινος ὁ ἐκ τῶν ὑπαρχόντων αὐτῷ κατορθωμάτων
μαρτυρούμενος. Τί πρὸς τὸν παῖδα ἡ τοῦ πατρὸς πε ριφάνεια; Οὕτως οὐκ ἔλαβεν
ἑτέρωθεν τὸ περιφανὲς ὁ μάρτυς· ἀλλ' αὐτὸς τῷ ἐφεξῆς βίῳ λαμπτῆρα εὐ κλείας
ἀνῆψεν. Ἀπὸ Μάμαντος οἱ λοιποὶ, οὐκ ἀφ' ἑτέρων Μάμας. Παῖδες οἱ παρὰ τούτου
τὴν εὐσέβειαν διδαχθέντες τούτῳ σεμνυνέσθωσαν. Αὐτὸς γὰρ οἴκοθεν βρύει τὴν
ἀρετήν. Οὐκ ἔστιν ὥσπερ χείμαῤῥος ἀλλοτρίᾳ συῤῥοίᾳ σεμνυνόμενος, ἀλλὰ πηγή
ἐστιν ἀπὸ τῶν οἰκείων λαγόνων προχέουσα τὸ κάλλος. Θαυμάσωμεν τὸν ἄνδρα μὴ
ἀλλοτρίῳ κόσμῳ κοσμού μενον, ἀλλὰ τῷ ἰδίῳ ἀποσεμνυνόμενον. Ὁρᾷς τοὺς
λαμπροὺς ἱπποτρόφους; ὁρᾷς τὰ λευκὰ αὐτῶν μνή ματα; πῶς λίθοι εἰσὶ
παρατρεχόμενοι; Μνήμῃ δὲ μάρτυρος καὶ πᾶσα μὲν χώρα κεκίνηται, πᾶσα δὲ πόλις
πρὸς ἑορτὴν μεταπεποίηται. Οὐδὲ οἱ συγγε νεῖς τῶν πρὸς τοὺς τῶν πατέρων
ἀποτρέχουσι τά φους, ἀλλὰ πάντες ἐπὶ τὸν τόπον τῆς εὐσεβείας.
Πατέρα τοῦτον τὸν ἀρχηγὸν τῆς ἀληθείας, ἀλλ' οὐχὶ πατέρας καὶ σωμάτων
ἀρχηγοὺς ὀνομάζουσιν. Ὁρᾷς πῶς ἀρετὴ τιμᾶται, καὶ οὐχὶ πλοῦτος; Οὕτως ἡ
 
 
 

1
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

Ἐκκλησία, δι' ὧν τιμᾷ τοὺς προλαβόντας, προ τρέπεται τοὺς παρόντας. Μή μοι, φησὶ,
σπούδαζε περὶ πλοῦτον· μὴ περὶ σοφίαν τοῦ κόσμου τὴν κατ αργουμένην· μὴ περὶ
δόξαν τὴν ἀπαθοῦσαν· ταῦτα τῷ βίῳ συναφανίζεται· ἀλλ' εὐσεβείας ἐργάτης ἔσο.
Αὕτη γὰρ καὶ εἰς οὐρανόν σε ἀνοίσει, αὕτη καὶ ἀθά νατόν σου τὴν μνήμην καὶ διαρκῆ
τὴν εὔκλειαν πα ρὰ ἀνθρώποις παρασκευάσει. Ὥστε, εἴ τις μέμνηται τοῦ ποιμένος,
μὴ θαυ μαζέτω τὸν πλοῦτον. Συνήλθομεν γὰρ ἐπαινέσαι οὐχὶ πλούσιον ὄντα· μὴ
ἀπέλθῃς πλούσιον θαυμάζων, ἀλλὰ πενίαν μετ' εὐσεβείας. Ποιμὴν οὐδὲν μέγα, οὐδὲ
σοφὸν ἐπιτήδευμα. Οὐκ ἂν, ὀργισθεὶς, ἀντὶ ὀνείδους εἶπες τῷ παροξύναντί σε·
Ποιμὴν εἶ; Ποι μὴν οὐδὲν πλέον τῆς ἐφημέρου τροφῆς κεκτημένος, πήραν
ἀνημμένος, καὶ κορύνην φέρων, καὶ τὰ πρὸς ἡμέραν ἐφόδια, οὐδεμίαν μέριμναν περὶ
τῆς αὔριον ἔχων. Θηρίοις πολέμιος, ἡμερωτάτων ζώων σύννομος, φεύγων ἀγορὰν,
φεύγων δικαστήρια, οὐ γνωρίζων συ κοφάντας, οὐ γνωρίζων ἐμπορίαν, οὐκ εἰδὼς
πλοῦτον, οὐκ ἔχων ἰδίαν σκέπην, ὑπὸ τὴν κοινὴν τοῦ κόσμου διαι 31.593 τώμενος,
ἐν νυκτὶ πρὸς οὐρανὸν ἀναβλέπων, καὶ διὰ τῶν ἀστέρων ἀναγινώσκων τὸ θαῦμα
τοῦ πεποιηκότος. Ποιμήν. Μὴ ἐπαισχυνώμεθα τὴν ἀλήθειαν. Μὴ τοὺς ἔξωθεν
μυθοποιοὺς μιμώμεθα, μὴ περιστέλλωμεν τὴν ἀλήθειαν εὐπρεπείᾳ ῥημάτων. Γυμνὴ
ἡ ἀλήθεια, ἀσυν ηγόρητος, αὐτὴ ἑαυτὴν δεικνῦσα. Πλείονι λόγῳ τὸ ταπεινόν· ἀλλὰ
μᾶλλον θαυμάσεις τῷ ἐγκωμίῳ.
Ποιμὴν καὶ πένης, ταῦτα τῷ Χριστιανῷ τὰ σε μνολογήματα. Ἐὰν τοὺς
ἀρχηγοὺς τοῦ διδασκαλείου τοῦ κατ' εὐσέβειαν ζητήσῃς, ἁλιεῖς τε καὶ τελῶναι· ἐὰν
τοὺς μαθητὰς, σκυτοτόμοι πένητες. Οὐδεὶς πλού σιος, οὐδαμοῦ περιφάνεια. Ταῦτα
πάντα μετὰ τοῦ κόσμου κατήργηται. Ἰδοὺ τοίνυν τίνος ἡμέραν ἑορτάζομεν, ἐπὶ τίνι
φαιδροὶ πάντες ἐσμὲν, ἐπὶ τίνι βίος μεταπεποίηται. Ἐπειδὴ ὑπεμνήσθημεν τοῦ
ποιμένος, μὴ καταφρόνει τῆς προσηγορίας. Ἤκου σας, ὅτι ὁ πρῶτος εὐαρεστήσας
Ἄβελ ποιμὴν ὑπῆρ ξε. Τίς ἐκείνου μιμητής; Μωϋσῆς ὁ μέγας νομοθέ της, ὁ τὴν
πεῖραν Φαραὼ ἀποδράσας, ὁ μισήσας τῶν συννόμων τὸ ἐπίβουλον, οὗτος ἐν τῷ ὄρει
τῷ Χωρὴβ ἐποίμαινεν· δι' ὧν ἐποίμαινε Θεῷ προσωμίλει. Οὐ δι καζόμενος εἶδεν ἐπὶ
βάτου ἄγγελον, ἀλλὰ ποιμαίνων ἐκείνης τῆς ὁμιλίας τῆς οὐρανίου ἠξιώθη. Τίς μετὰ
Μωϋσέα; Ἰακὼβ ὁ πατριάρχης, ἐν τῷ ποιμαίνειν ὑπομονὴν τὴν περὶ τῆς ἀληθείας
ἐπιδεικνύμενος, μικρᾷ εἰκόνι τὸν ὅλον ἑαυτοῦ χαρακτηρίσας βίον. Τίνι παρ έδωκε
τὸν ζῆλον; Τῷ ∆αβίδ. ∆αβὶδ ἐκ ποιμαντι κῆς παρῆλθεν εἰς βασιλείαν. Ἀδελφαὶ γὰρ
ποιμαν τικὴ καὶ βασιλική· παρ' ὅσον ἡ μὲν τῶν ἀλόγων, ἡ δὲ τῶν λογικῶν τὴν
ἐπιστασίαν ἐπεπίστευτο. Οὕτως ὑποβάθρα τῆς μείζονος ἐπιστήμης ἐστὶν αὕτη. ∆ιὰ
τοῦτο ἀμφότερα συλλαμβάνων ὁ Κύριός ἐστι καὶ ποιμὴν καὶ βασιλεὺς, τοὺς ἀλόγους
ποιμαίνων, τοὺς λογικωτέρους ὑπὸ τὴν ἐπιστασίαν τῆς βασιλείας ἄγων. Βούλει
μαθεῖν πηλίκον ἐστὶ ποιμήν; Κύριος ποιμαίνει με. Πῶς δὲ ἀδελφὴ βασιλείας
ποιμαντική; Τίς ἐστιν οὗτος ὁ βασιλεὺς τῆς δόξης; Ὁ ἐν ταῦθα ποιμὴν ἐκεῖ βασιλεύς.
Καὶ μὴ νομίσῃς παρ' ἄλλων μὲν μαρτυρεῖσθαι, αὐτὸν δὲ ἐπαισχύνεσθαι τῇ
προσηγορίᾳ ταύτῃ. Ἀλλὰ κατασιωπήσας τοὺς 31.596 νόθους ποιμένας, ἐφ' ἑαυτὸν
περιίστησι τὴν ἀληθῆ τῆς ποιμαντικῆς μαρτυρίαν· Ἐγώ εἰμι ὁ ποιμὴν ὁ καλός. Ἐγώ
εἰμι, καὶ οὐκ ἠλλοίωμαι.
Πῶς, ὅταν τὰ μεγάλα λέγῃ· Ἐγὼ τῇ χειρί μου ἐστερέωσα τὴν γῆν· ἐγὼ
ἐξέτεινα τὸν οὐρανὸν μόνος· ὡς ὅταν τἄλλα λέγῃ τὰ ἀξιοπρεπῆ καὶ ἄξια περὶ Θεοῦ
λέγεσθαι, οὕτως λέγει· Ὁ ποιμὴν ὁ καλός; Ἀποδιώκει τοὺς νόθους, καὶ τὴν ἀλήθειαν
εἰς ἑαυτὸν περιίστησιν. Ἐγώ εἰμι ὁ ποιμὴν ὁ καλός. Μάθε τίς ἐστιν ὁ ποιμὴν, καὶ τίς
ἐστιν ὁ καλός. Αὐτὸς ἑρμηνεύει. Ὁ ἀληθινὸς ποιμὴν τὴν ψυχὴν αὐ-τοῦ τίθησιν ὑπὲρ
τῶν προβάτων· ὁ δὲ μισθωτὸς, καὶ οὐκ ὢν ποιμὴν, οὗ οὐκ ἔστι τὰ πρόβατα ἴδια, οὐ
μέλει αὐτῷ, ὅταν ἴδῃ λύκον ἐρχόμενον. Ἐνταῦθα ζητεῖ ἡ Ἐκκλησία, εἰ ποιμὴν ὁ
 
 
 

2
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

Κύριος, τίς μισθωτὸς ποιμήν; Μὴ ὁ διάβολος; Καὶ εἰ ὁ διάβολος μισθωτὸς ποιμὴν, τίς
ὁ λύκος; Ἀλλὰ λύκος μὲν ὁ διάβολος, τὸ ἀνήμερον θηρίον, τὸ ἁρ-πακτικὸν, τὸ
ἐπίβουλον, ὁ κοινὸς πάντων ἐχθρός. Ἐχέτω τοίνυν ἰδίαν προσηγορίαν μισθωτὸς
ποιμήν. Μισθωτοὺς ποιμένας τότε μὲν πρὸς τοὺς τότε ἀποστρεφόμενος ὁ Κύριος
ἔλεγεν. Εἰσὶ δὲ καὶ οἱ νῦν, ὡς οὐκ ὄφελον, οἱ τὴν προσηγορίαν τῶν μισθωτῶν
οἰκειούμενοι. Τότε ἀρχιερεῖς ἐνεδείκνυντο καὶ Φαρισαῖοι, καὶ πᾶν ἐκεῖνο τὸ κόμμα
τὸ Ἰουδαϊκόν. Ἐκείνους μισθωτοὺς ἔλεγε ποιμένας, τοὺς οὐ διὰ τὸ ἀληθὲς, ἀλλὰ διὰ
τὸ αὐτῶν χρειῶδες τὴν τοῦ ποιμαίνειν ἐξουσίαν λαβόντας. Οἱ ἐπὶ προφάσει ματαίᾳ
προσευχόμενοι, ἵνα κατεσθίωσι τὸν ἄρτον τῶν χηρῶν καὶ τῶν ὀρφανῶν, οὗτοι
μισθωτοί. Οἱ τὴν χρείαν θεραπεύοντες, οἱ τὸ παρὸν διώκοντες, οὐ πρὸς τὸ μέλλον
ἀτενίζοντες, μισθωτοὶ, καὶ οὐ ποιμένες εἰσί. Καὶ νῦν μισθωτοὶ πολλοὶ δοξαρίου δυστήνου τὴν ἑαυτῶν ζωὴν ἀποδιδόντες, οἱ καὶ νῦν ἐπὶ τοῖς ὑγιαίνουσι τοῦ Κυρίου
ῥήμασι σχίσμα ποιοῦντες. Εἰπόντος γὰρ ταῦτα τοῦ Κυρίου, σχίσμα ἐν αὐτοῖς ἐγένετο.
Οἱ μὲν ἔλεγον, ∆αιμόνιον ἔχει· ἄλλοι, ∆αίμων ὀφθαλμοὺς τυφλοὺς βλέπειν ποιεῖν
οὐ δύναται. Ὁρᾷς πῶς παλαιὸν τὸ πάθος τοῦ σχίσματος; Εὐθὺς γὰρ τὸ πτύον
διακρίνει μὲν τὸ ἄχυρον ἀπὸ τοῦ σίτου· καὶ τὸ μὲν κοῦφον καὶ ἄστατον ἀποσχίζεται
τοῦ τροφίμου, τὸ δὲ πρὸς τροφὴν πνευματικὴν ἐπιτήδειον παραμένει τῶν
γεωργούντων. ∆ιὰ τοῦτο σχίσμα ἐγένετο, καὶ οἱ μὲν οὕτω διεφέροντο, οἱ δὲ οὕτως.
Ἰουδαίοις πρέπει τὸ σχίζεσθαι· ἡ δὲ τοῦ Θεοῦ Ἐκκλησία, τὸν χιτῶνα τὸν ἄῤῥαφον,
τὸν ἐκ τῶν ἄνωθεν ὑφαντὸν, δεξαμένη, τὸν ἄσχιστον ὑπὸ στρατιωτῶν
διαφυλαχθέντα, ἡ Χριστὸν ἐνδυσαμένη, μὴ σχιζέτω τὸ ἱμάτιον. Καὶ γινώσκω τὰ ἐμὰ,
καὶ γινώσκουσί με τὰ ἐμά. Ἥρπασεν ὁ αἱρετικὸς πρὸς τὴν ἰδίαν κατασκευὴν τῆς
βλασφημίας. Ἰδοὺ, φησὶν, εἴρηται, ὅτι Γινώσκουσί με τὰ ἐμὰ, καὶ γινώσκω τὰ ἐμά.
Γινώσκειν ἄρα τί λέγει; τὴν οὐσίαν νοεῖν; 31.597 τὸ μέγεθος ἐκμετρεῖν; Ἐκεῖνα
κατανοεῖσθαι τῆς θεότητος, ἃ σὺ τῇ σεαυτοῦ θρασυστομίᾳ κατεπαγγέλλῃ; Ἢ ἐκ τῶν
κατόπιν οὐ νοεῖς τὸ μέτρον τῆς γνώσεως;
Τί γινώσκομεν Θεοῦ; Τὰ ἐμὰ πρόβατα τῆς φωνῆς μου ἀκούει. Ἴδε πῶς νοεῖται
Θεός· ἐκ τοῦ ἀκούειν ἡμᾶς τῶν ἐντολῶν αὐτοῦ· ἐκ τοῦ ἀκούοντας ποιεῖν. Τοῦτο
γνῶσις Θεοῦ, τήρησις ἐντολῶν Θεοῦ. Πῶς οὐ πολυπραγμοσύνη περὶ οὐσίας Θεοῦ; οὐ
ζήτησις τῶν ὑπερκοσμίων; οὐκ ἐπίνοια τῶν ἀοράτων; Γινώσκει με τὰ ἐμὰ, καὶ
γινώσκω τὰ ἐμά. Ἀρκεῖ σοι εἰδέναι, ὅτι ποιμὴν καλός· ὅτι ἔθηκε τὴν ψυχὴν αὐτοῦ
ὑπὲρ τῶν προβάτων. Οὗτος ὅρος τῆς τοῦ Θεοῦ ἐπιγνώσεως. Πόσος δὲ Θεὸς, καὶ τί τὸ
μέτρον αὐτοῦ, καὶ ποταπὸς τὴν οὐσίαν, τὰ τοιαῦτα ἐπικίνδυνα μὲν τῷ ἐρωτῶντι,
ἄπορα δὲ τῷ ἐρωτωμένῳ· σιωπὴ δὲ, θεραπεία τῶν τοιούτων ἐστί. Τὰ ἐμὰ πρόβατα
τῆς ἐμῆς φωνῆς ἀκούει, εἶπεν, οὐ συζητεῖ. Τουτ-έστιν οὐ παρακούει, οὐ φιλονεικεῖ.
Ἤκουσας Υἱὸν, μή μοι τρόπους γεννήσεως μήτε αὐτὸς τεχνολόγει, μήτε ὑπὸ αἰτίαν
ἀνάγαγε τὰ μὴ αἰτιολογήματα, μὴ διὰ σχίσματος διόριζε τὸ συνημμένον. ∆ιὰ τοῦτο
προλαβὼν ἠσφαλίσατό σε ὁ εὐαγγελιστής. Ἤκουσας πρώην, καὶ δὴ ἀκούεις· Ἐν ἀρχῇ
ἦν ὁ Λόγος, ἵνα μὴ τὸν Υἱὸν γέννημα ἀνθρώπινον νομίσῃς ἐκ τοῦ μὴ ὄντος
παρεληλυθέναι. Λόγον σοι εἶπε διὰ τὸ ἀπαθές. Ἦν σοι εἶπε διὰ τὸ ἄχρονον. Ἀρχήν
σοι εἶπεν, ἵνα συνάψῃ τὸν γεννηθέντα τῷ Πατρί. Εἶδες πρόβατον εὐπειθὲς πῶς
ἀκούει φωνὴν ∆εσπότου. Ἐν ἀρχῇ, καὶ ἦν, καὶ Λόγος. Μηδὲ λέγε τὸ, Πῶς ἦν; καὶ, Εἰ
ἦν, οὐκ ἐγεννήθη, καὶ Εἰ ἐγεννήθη, οὐκ ἦν. Οὐκ ἔστι πρόβατον τὸ τὰ τοιαῦτα λέγον.
Προβάτου δέρμα· ἔσωθεν δὲ λύκος ἐστὶν ὁ φθεγγόμενος. Ἐπίβουλος γνωριζέσθω. Τὰ
ἐμὰ πρόβατα τῆς ἐμῆς φωνῆς ἀκούει. Ἤκουσας Υἱόν; Νόησον τὴν πρὸς Πατέρα
ὁμοιότητα. Ὁμοιότητα λέγω δι' ἀσθένειαν τῶν δυνατωτέρων σωμάτων. Ἐπεὶ τό γε
ἀληθὲς (οὐ φοβοῦμαι γὰρ προσβαίνειν τὴν ἀλήθειαν), οὐκ εἰμὶ ἕτοιμος συκοφάντης.
Ταυτότητα λέγω, φυλάσσων Υἱοῦ καὶ Πατρὸς ἰδιότητα. Ἐν ὑποστάσει Υἱοῦ, μορφὴν
 
 
 

3
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

νόησον πατρικὴν, ἵνα μοι τὸν ἀκριβῆ τῆς εἰκόνος διασώσῃς λόγον, ἵνα μοι νοήσῃς
θεοσεβῶς· Ἐγὼ ἐν τῷ Πατρὶ, καὶ ὁ Πατὴρ ἐν ἐμοί· οὐκ οὐσιῶν σύγχυσιν, ἀλλὰ
χαρακτήρων ταυτότητα. Ἀλλὰ γὰρ ἔοικεν ἐναντιώτατον εἶναι τῶν πραγμάτων,
ἀγαπητοὶ, ὅπου γε καὶ τὴν ἡμετέραν ἀσθένειαν ἐξεβιάσατο τὸ εὐπειθὲς τῆς ὑμετέρας
ἀκοῆς εἰπεῖν τι ἐν μέσοις καὶ φθέγξασθαι, ἵνα ἡ δύναμις 31.600 τοῦ Θεοῦ ἐν τῇ τοῦ
ὀργάνου ἀσθενείᾳ μάλιστα δια-φανῇ.
Τάχα γὰρ διὰ τοῦτο ὑπερεπερίσσευσεν ἡμῶν τὸ ἀσθενὲς, ἵνα ἐπὶ πλεῖον
δοξασθῇ ὁ τὸ ἀσθενὲς ἐνισχύων. Ὁ δὲ περιαγαγὼν ἡμῶν τὴν πανήγυριν ταύτην, καὶ
πέρας δοὺς ταῖς περυσιναῖς εὐχαῖς, καὶ κεφαλὴν δοὺς τῷ ἐπιόντι χρόνῳ (ἡ γὰρ αὐτὴ
ἡμέρα ὁρίζει ἡμῖν τὸν παρελθόντα κύκλον, καὶ κεφαλὴ γίνεται πάλιν τῷ
ἐπερχομένῳ), ὁ τοίνυν ἐπισυνάξας καὶ χαρισάμενος τοῦ μέλλοντος τὴν ἐνέργειαν,
διαφυλάξειεν ἡμᾶς ἐν αὐτῷ ἀβλαβεῖς, ἀνεπηρεάστους, ὑπὸ τοῦ λύκου ἀδιαρπάκτους·
ἄσειστον τὴν Ἐκκλησίαν ταύτην, φρουρουμένην τοῖς μεγάλοις πύργοις τῶν
μαρτύρων διατηρήσειε. Πᾶσαν ἐπιβουλὴν καὶ προσβολὴν αἱρετικῶν μηνιαμάτων
ἀποστρέψειεν· ἐν ἡσυχίᾳ δὲ παράσχοι ἡμῖν διδάσκεσθαι τὰ θεῖα λόγια, καὶ διδάσκειν
τὴν ἐπιχορηγουμένην χάριν τοῦ Πνεύματος· ὅτι αὐτῷ ἡ δόξα καὶ τὸ κράτος σὺν τῷ
ἁγίῳ Πνεύματι νῦν καὶ ἀεὶ, καὶ εἰς τοὺς αἰῶνας τῶν αἰώνων. Ἀμήν.

 
 
 

4
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

