Dialogus cum Nestorio
ΤΟΥ ΑΥΤΟΥ ΑΓΙΟΥ ΚΥΡΙΛΛΟΥ ∆ΙΑΛΕΞΙΣ ΠΡΟΣ ΝΕΣΤΟΡΙΟΝ ΟΤΙ
ΘΕΟΤΟΚΟΣ Η ΑΓΙΑ ΠΑΡΘΕΝΟΣ ΚΑΙ ΟΥ ΧΡΙΣΤΟΤΟΚΟΣ.
76.249
{ΝΕΣΤΟΡΙΟΣ.} Οταν ἡ θεία Γραφὴ διαλέγηται γέννησιν Χριστοῦ ἐκ Μαρίας
τῆς παρθένου [η] θάνατον, οὐδαμοῦ τεθεῖσα φαίνεται Θεοῦ, ἀλλὰ Χριστοῦ η Κυρίου
η ̓Ιησοῦ, ἐπειδὴ τὰ τρία ταῦτα τῶν δύο φύσεων σημαντικὰ, ποτὲ μὲν ταύτης, ποτὲ δὲ
ἐκείνης· οιόν τι λέγω· Οταν τὴν ἐκ Παρθένου γέννησιν ἡμῖν τοῦ Χριστοῦ μηνύει
[ξοδ. μηνύων] ὁ ̓Απόστολος, λέγει· ̓Εξαπέστειλεν ὁ Θεὸς τὸν ἑαυτοῦ Υἱὸν γενόμενον
ἐκ γυναικός· οὐκ ειπε δὲ, ̓Εξαπέστειλε τὸν Θεὸν Λόγον· λαμβάνει δὲ τὸ ονομα τὸ
μηνύον τὰς δύο γενέσεις, ητοι Θεὸν [ξοδ. Θεὸς] καὶ ανθρωπον, ἐπειδὴ διπλοῦς ἐστιν
ὁ Χριστός· Υἱὸν γὰρ Θεοῦ ἐγέννησεν ἡ Παρθένος, καθότι εφησεν· ̔Υμεῖς δὲ θεοί ἐστε
καὶ υἱοὶ ̔Υψίστου πάντες· οθεν Χριστοτόκον, Κυριοτόκον, ἀνθρωποτόκον ἐμάθομεν
ἀπὸ τῆς Γραφῆς λέγειν, Θεοτόκον δὲ οὐδαμῶς ἐδιδάχθημεν λέγειν τὴν ἁγίαν
Παρθένον. {ΚΥΡΙΛΛΟΣ.} ̔Ησαΐας βοᾷ ἐν Πνεύματι λέγων· ̓Ιδοὺ ἡ παρθένος ἐν γαστρὶ
εξει, καὶ τέξεται υἱὸν, καὶ καλέσουσι τὸ ονομα αὐτοῦ ̓Εμμανουὴλ, ο ἐστι 76.252
μεθερμηνευόμενον, Μεθ' ἡμῶν ὁ Θεός· Θεὸς ουν ὁ τεχθεὶς, καν θέλῃς, καν μὴ θέλῃς.
{ΝΕΣΤ.} ̔Ο αγγελος τῷ ̓Ιωσὴφ ειπεν· ̓Αναστὰςπαράλαβε τὸ παιδίον καὶ τὴν μητέρα
αὐτοῦ, καὶ φεῦγε εἰς Αιγυπτον, καὶ ισθι ἐκεῖ εως αν ειπω σοι· οὐκ ειπε δὲ, Παράλαβε
τὸν Θεὸν καὶ τὴν μητέρα αὐτοῦ. {ΚΥΡ.} ̓Αλλ' ὁ ἀρχάγγελος Γαβριὴλ πρὸς τὴν
Παρθένον λέγει· Πνεῦμα αγιον ἐπελεύσεται ἐπὶ σὲ, καὶ δύναμις ̔Υψίστου ἐπισκιάσει
σοι· διὸ καὶ τὸ γεννώμενον ἐκ σοῦ αγιον, υἱὸς ̔Υψίστου κληθήσεται. {ΝΕΣΤ.} ̔Ο
̓Απόστολος εφη· ̔Εαυτὸν ἐκένωσε,μορφὴν δούλου λαβών· οπερ ἐστὶ τὸ ἡμέτερον
ἐνδυσάμενος σῶμα, ενθα προέκοπτε σοφίᾳ καὶ ἡλικίᾳ καὶ χάριτι παρὰ Θεῷ καὶ
ἀνθρώποις, ὡς Κυριακὸς ανθρωπος. {ΚΥΡ.} Πᾶν τὸ γενόμενον ἐκ τῆς σαρκὸς, σάρξ
ἐστι· οὐκ ἀναιρεῖ δὲ τῆς γεννήσεως παράδοξον· ὡς γὰρ ὁ Θεὸς τίκτει θεϊκῶς, ουτω
καὶ ἡ θεοπρεπὴς Παρθένος ετεκεν ἐν σαρκὶ τὸν ἐκ Θεοῦ Θεὸν Λόγον. {ΝΕΣΤ.} Πᾶσα
μήτηρ τὸ ὁμοούσιον αὐτῆς τίκτει· οὐκ εστιν ουν αὐτοῦ μήτηρ, εἰ μὴ αὐτὸς ὁμοούσιος
αὐτῆς ὑπάρχει· πῶς γὰρ αὐτοῦ μήτηρ τοῦ ἀλλοτρίου τῆς οὐσίας αὐτῆς ὑπάρχοντος
[ξοδ. τὸ ἀλλότριον ὑπάρχον]; {ΚΥΡ.} ̔Ημᾶς ἐκ Παρθένου καὶ ἐκ Πνεύματος ἁγίου ενα
Υἱὸν ἐδιδάχθημεν ὁμολογεῖν, ὁμοούσιον τῇ μητρὶ ὡς τῷ Πατρὶ, καθὼς οἱ Πατέρες
εἰρήκασιν. {ΝΕΣΤ.} ∆ιαιρῶ τὰς φύσεις, ἑνῶ τὴν προσκύνησιν, ἐπειδὴ ἀχώριστος τοῦ
φαινομένου Θεός· διὰ τοῦτο οὐ χωρίζω τὴν τιμήν. {ΚΥΡ.} ̔Ο διαιρῶν τὰς φύσεις, δύο
υἱοὺς λέγει, μὴ πιστεύων τῇ Γραφῇ λεγούσῃ, οτι ̔Ο Λόγος σὰρξ ἐγένετο. {ΝΕΣΤ.}
Παῦλος λέγει ἀρχιερέα καὶ ἀπόστολον τῆς ὁμολογίας ἡμῶν γενέσθαι ̓Ιησοῦν, πιστὸν
οντα τῷ ποιήσαντι ἑαυτὸν, θυσίαν ἑαυτὸν προσέφερεν ὑπὲρ ἑαυτοῦ ὡς ὑπὲρ τοῦ
λαοῦ· οθεν ἀρχιερεὺς καθίσταται τὰ πρὸς τὸν Θεόν. {ΚΥΡ.} Παῦλος λέγει· Χριστὸς
απαξ προσενεχθεὶς εἰς τὸ πολλῶν ἀνενεγκεῖν ἁμαρτίας, ἐκ δευτέρου χωρὶς ἁμαρτίας
ὀφθήσεται. Καὶ πάλιν· ̔Αμαρτίαν οὐκ ἐποίησεν, καὶ δόλος οὐχ ὑπῆρχεν ἐν τῷ στόματι
αὐτοῦ· καὶ ἰδοὺ αὐτὸς ἑαυτὸν προσφέρει ὑπὲρ ἡμῶν· ιδε ουν οὐχ ὑπὲρ ἑαυτοῦ
ἀνήνεγκεν αὑτὸν, ἀλλ' ὑπὲρ τοῦ λαοῦ. {ΝΕΣΤ.} Αὐτοῦ λέγοντος· ̔Ο τρώγων μου τὴν
σάρκα, καὶ πίνων μου τὸ αιμα, ζήσεται δι' ἐμοῦ, καθὼς ἐγὼ ζῶ διὰ τὸν ἀποστείλαντά
με Πατέρα. 76.253 Τίνα ουν ἐσθίεις, αἱρετικέ; τὴν θεότητα, η τὴν ἀνθρωπότητα; Οὐκ
ειπε γὰρ ὁ Χριστὸς, ̔Ο τρώγων μου τὴν θεότητα, ἀλλὰ, Τὴν σάρκα. {ΚΥΡ.} ̓Εγὼ μὲν
πίστει μεταλαμβάνω τὴν τοῦ ζωοποιοῦ Λόγου ζωοποιοῦσαν σάρκα· οθεν ὁ Σωτὴρ
εφη· Οἱ ἐσθίοντες ἐκ τούτου τοῦ αρτου, οὐ μὴ γεύσωνται θανάτου εἰς τὸν αἰῶνα· οἱ
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

δὲ ἀναξίως μεταλαμβάνοντες, διὰ τῆς ἀπειθείας κριθήσονται. {ΝΕΣΤ.} Κύριος
ἀπέσταλκέ με καὶ τὸ Πνεῦμα αὐτοῦ· Πνεῦμα Κυρίου ἐπ' ἐμὲ, ου εινεκεν εχρισέ με· καὶ
διὰ τοῦτό φησιν· Εχρισέ σε ὁ Θεὸς, ὁ Θεός σου, ελαιον ἀγαλλιάσεως παρὰ τοὺς
μετόχους σου· νόει ουν τὸν χρισθέντα, ὁμολόγει τὸν χρίσαντα, καὶ προσκύνει τὸν
ναὸν διὰ τὸν ἐν αὐτῷ κατοικοῦντα.
{ΚΥΡ.} ̓Εγὼ ἐκ Πνεύματος ἁγίου καὶ Μαρίας τῆς Παρθένου ενα Υἱὸν ἐκ δύο
ὁμολογῶ· τὸν ενα εἰς δύο οὐ διαιρῶ. {ΝΕΣΤ.} Πῶς ἀγνοεῖν λέγεις τὴν ωραν, ην ὁ
Υἱὸς οὐκ οιδεν, εἰ μὴ μόνος ὁ Πατήρ; Η πῶς εἰδὼς ἀρνεῖται [ξοδ. αρχεται]; Η μὴ
εἰδὼς, πῶς ἐκλήθη σοφία καὶ δύναμις Θεοῦ ὁ τῆς ἀγνοίας μετέχων (ξοδ. τὸν ἀγνοίας
μετέχοντα τόπον); {ΚΥΡ.} Οὐκ ἀγνοεῖ ὁ Υἱὸς τοῦ Θεοῦ τὴν ωραν, τὰ πάντα εἰδὼς καὶ
εχων ἐν ἑαυτῷ τὸν Πατέρα, καθὼς αὐτὸς ειπεν· Οὐδεὶς γινώσκει τὸν Πατέρα, εἰ μὴ ὁ
Υἱός· μόνος τὴν φύσιν τοῦ Πατρὸς γινώσκει τὴν πρὸ πάντων, καὶ τὸ τέλος τοῦ
αἰῶνος ἀγνοεῖ ὁ Υἱὸς τοῦ Θεοῦ; Μὴ γένοιτο· ἀλλ' ἀγνοεῖ μὲν ὁ κατὰ χάριν Υἱὸς, οὐ
μέντοι γε ὁ κατὰ φύσιν· κατὰ χάριν ἡμεῖς ἐσμεν ἀγνοοῦντες τὸ πᾶν, καὶ μηδὲν
ἐπιστάμενοι περὶ τὰ τέλη τοῦ αἰῶνος. {ΝΕΣΤ.} Φυσικῶς πεινῶμεν καὶ διψῶμεν καὶ
καθεύδομεν, οὐ γνώμῃ τοῦτο πάσχοντες, ἀλλ' ἀνάγκῃ φύσεως ὑποκείμενοι· τί ουν;
εὑρεθήσεται Θεὸς ἀναγκαστικοῖς νόμοις ἐξακολουθῶν; Μὴ γένοιτο. {ΚΥΡ.} ̔Ημεῖς μὲν
φυσικῶς πεινῶμεν καὶ διψῶμεν καὶ καθεύδομεν, οὐ γνώμῃ, ἀλλ' ἀνάγκῃ φύσεως
δουλεύοντες· αὐτὸς δὲ οὐκ ἀνάγκῃ, ἀλλὰ γνώμῃ· καὶ γὰρ ἐκ πέντε αρτων
πεντακισχιλίους ἐχόρτασεν, καὶ ἐξ ἑπτὰ πάλιν τετρακισχιλίους· αὐτὸς ειπεν· Οὐδεὶς
λαμβάνει τὴν ψυχήν μου ἀπ' ἐμοῦ, ἀλλ' οταν θέλω λαμβάνω αὐτήν· οὐδεὶς δὲ
ανθρωπος δύναται τοῦτο ποιῆσαι, εἰ μὴ ὁ μόνος Χριστός.
{ΝΕΣΤ.} Λέγεις οτι επαθεν ὁ Υἱός; Επαθεν ουν καὶ ὁ Πατήρ· εἰ δὲ λέγεις οτι ὁ
Πατὴρ οὐκ επαθεν, ἑτεροούσιον αὐτοῦ πεποίηκας τὸν Υἱόν. {ΚΥΡ.} Ουτε τὸν Πατέρα
λέγω παθητὸν, ουτε τὸν Υἱὸν ἀπαθῆ· ἀλλ' ἀπαθὲς μὲν τὸ Θεῖον, οτι καὶ ἀσώματον·
παθητὸς δὲ ὁ Κύριος διὰ τὴν σάρκα. {ΝΕΣΤ.} ̓Εγὼ οὐ λέγω τὸν Λόγον σάρκα
γενόμενον, οὐδὲ παθόντα· ἀπαθὲς γὰρ τὸ Θεῖον, καὶ παθῶν 76.256 ἀνώτερος ὁ
ἀπαθής· παθητὴ δὲ σὰρξ καὶ θνητὴ ἀνθρωπίνη φύσις. {ΚΥΡ.} ̔Ομολογοῦμεν καὶ ἡμεῖς
ἀπαθὲς τὸ Θεῖον, διὰ δὲ τῆς σαρκὸς κατεδέξατο πάθος· οθεν, Χριστοῦ παθόντος σαρκὶ
ὑπὲρ ἡμῶν· τὸ δὲ, Χριστὸς οὐκ ἀλλότριον Θεοῦ ονομα· ὁ γὰρ ἀπόστολος Παῦλός
φησιν· ̓Εξ ων ὁ Χριστὸς, ὁ ων ἐπὶ πάντων Θεὸς εὐλογητὸς εἰς τοὺς αἰῶνας. ̓Αμήν.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

