Fragmenta ex libro ii contra Theodorum Mopsuestenum
ΤΟΥ ΑΥΤΟΥ ΚΥΡΙΛΛΟΥ Ἐκ τοῦ πρὸς τὰ Θεοδώρου δευτέρου λόγου Οὗ ἡ
ἀρχή Οἱ τὴν ἁγίαν καὶ θεόπνευστον γραφὴν καθαρῷ διανοίας
περιαθρήσαντες ὀφθαλμῷ.
αʹ.ΤΟΙΣ μαθηταῖς ἔλεγε "Μὴ καλέσητε διδάσκαλον ἐπὶ τῆς "γῆς· εἷς γάρ ἐστιν
ὑμῶν καθηγητὴς, ὁ Χριστός·" οὐ γὰρ ὅτε τοῦτο τοῖς ἀποστόλοις παρήγγειλε, τοῦ
φαινομένου σώματος τὴν οἰκείαν διώριζε θεότητα, οὐδ' ὅτε Χριστὸν ἑαυτὸν
διεμαρτύρατο εἶναι ψυχῆς καὶ σαρκὸς διωρίζετο οὕτως ἄμφω τυγχάνων Θεός τε καὶ
ἄνθρωπος, δοῦλος ὁρώμενος καὶ Κύριος γνωριζόμενος, τὸ μὲν ὑψηλὸν τῆς θεότητος
τῷ ταπεινῷ τῆς ἐνανθρωπήσεως ὑποκρυπτόμενος φρονήματι, τὸ δέ γε ταπεινὸν τοῦ
ὁρωμένου σώματος τῆς θεότητος ὑπεραίρων ἐνεργείᾳ.
βʹ. Μὴ ἀπατάτωσαν οἱ ἄνθρωποι, μηδὲ ἀπατάσθωσαν ἄνθρωπον ἄνουν
δεχόμενοι τὸν κυριακὸν, ὡς αὐτοὶ λέγουσι, μᾶλλον δὲ τὸν Κύριον ἡμῶν καὶ Θεόν·
οὐδὲ γὰρ τὸν ἄνθρωπον χωρίζομεν τῆς θεότητος, ἀλλ' ἕνα καὶ τὸν αὐτὸν
δογματίζομεν, πρότερον μὲν οὐκ ἄνθρωπον, ἀλλὰ Θεὸν καὶ μόνον Υἱὸν καὶ
προαιώνιον ἀμιγῆ σώματος καὶ τῶν ὅσα σώματος· ἐπὶ τέλει δὲ καὶ ἄνθρωπον
προσληφθέντα ὑπὲρ τῆς ἡμετέρας σωτηρίας, παθητὸν σαρκὶ, ἀπαθῆ θεότητι,
περιγραπτὸν σώματι, ἀπερίγραπτον θεότητι, τὸν αὐτὸν ἐπίγειον καὶ οὐράνιον,
ὁρώμενον καὶ νοούμενον, χωρητὸν καὶ ἀχώρητον, ἵν' ὅλῳ ἀνθρώπῳ τῷ αὐτῷ καὶ
Θεῷ ὅλος ἄνθρωπος ἀναπλασθῇ ὁ πεσὼν ὑπὸ τὴν ἁμαρτίαν. γʹ. Ἐπειδὴ γὰρ
ἐσαρκώθη ζωὴ κατὰ φύσιν ὑπάρχων ὁ Μονογενὴς τοῦ Θεοῦ Λόγος, ἀνέθαλλεν εἰς
ζωὴν ἡ ἀνθρώπου φύσις· γέγονε γὰρ "πρωτεύων ἐν πᾶσιν αὐτός." καὶ ταύτης ἕνεκα
τῆς αἰτίας ὁ ζωοποιὸς τοῦ Θεοῦ Λόγος ἰδίαν ἐποιήσατο σάρκα τὴν θανάτῳ κάτοχον,
ἵνα κρείττονα καὶ θανάτου καὶ φθορᾶς αὐτὴν ἀποφήνας, παραπέμψῃ καὶ εἰς ἡμᾶς
τὴν χάριν. ὥσπερ γὰρ κατεβιβάσθημεν εἰς θάνατον ἐν Ἀδὰμ, οὕτως ἐν Χριστῷ τὴν
θανάτου παρωσάμενοι τυραννίδα, πρὸς ἀφθαρσίαν ἀναμορφούμεθα. 513 δʹ. Τοῦ
αὐτοῦ ἐκ τῶν κατὰ Θεοδώρου λόγου πρώτου. Ὥσπερ γὰρ ἐκ ψυχῆς καὶ σώματος
ἄνθρωπος εἷς, καίτοι τῶν ἑκατέρων προσόντων ἰδιωμάτων πλείστην ὅσην ἐχόντων
τὴν διαφορὰν πρὸς ἄλληλα κατά γε τὸ εἶναι τοιῶσδέ φημι· ψυχὴ γὰρ ἑτέρα παρὰ τὸ
σῶμα· οὕτως ἂν νοοῖτο καὶ ἐπὶ τοῦ πάντων ἡμῶν Σωτῆρος Χριστοῦ.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

