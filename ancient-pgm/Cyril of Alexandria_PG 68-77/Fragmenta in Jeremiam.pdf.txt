Fragmenta in Jeremiam
FRAGMENTA EX CATENIS IN IEREMIAM.
70.1452
Ὄντως εἰς ψεῦδος ἦσαν οἱ βουνοὶ καὶ δύναμις τῶν ὀρέων, κ.τ.λ. ∆ιαμέμνηται
ὀρῶν καὶ βουνῶν, ἐπείπερ ἐν αὐτοῖς κατὰ τοὺς ἀρχαίους ἐκείνους καιροὺς ἱερά τε
καὶ τεμένη δειμάμενοι καὶ βωμοὺς ἀναστήσαντες, τοῖς δαιμονίοις χαριεστάτας
προσεκόμιζον θυσίας. Ἡ δὲ αἰσχύνη κατανάλωσε τοὺς μόχθους τῶν πατέρων ἡμῶν
ἀπὸ νεότητος αὐτῶν. Αἰσχύνην λέγουσι τὴν ἀληθῶς ἀκλεᾶ καὶ ἀδοξοτάτην
εἰδωλολατρείαν, καὶ τὴν ἐπὶ τούτῳ πλάνησιν, ἣ καὶ τοὺς μόχθους τῶν πατέρων
αὐτῶν κατανάλωσε· δεδαπανήκασι γὰρ ἱερουργοῦντες τοῖς δαίμοσι τὰ πρόβατα
αὐτῶν, καὶ τοὺς μόσχους αὐτῶν, καὶ φρονήσεως ζημίαν ὁμοῦ τοῖς οὖσιν ὑπέμειναν.
Οἴ μοι! ὅτι ἐκλείπει ἡ ψυχή μου ἐπὶ τοῖς ἀνηρημένοις.
Περιδράμετε ἐν ταῖς ὁδοῖς Ἱερουσαλήμ. Ὁρᾷς ἐν τούτοις ὠδίνοντα μὲν ἐπὶ
τοῖς ἀνῃρημένοις, καὶ προχείρως μὲν ἐλεεῖν ἐθέλοντα τὴν Ἱερουσαλὴμ, εἰργόμενον
δέ πως ὑφ' ἑαυτοῦ, μονονουχὶ δὲ καὶ ἀνασειράζοντα τὴν θεοπρεπῆ γαληνότητα διὰ
τὸ τῆς ἁμαρτίας ὑπέρογκον· ζητοῦντα δ' οὖν ὅμως τὸ κατοικτείρειν τὰς ἁμαρτίας,
καὶ ἐφ' ἑνὶ θέλοντα καταλύειν τὰ ἐξ ὀργῆς; Ἕνα γὰρ ἄνδρα τοὺς περιθέοντας
ἐπιδεικνύναι πιστὸν ἐν αὐτῇ διετάττετο. Καὶ ἐγὼ εἶπα· Ἴσως πτωχοί εἰσι, διότι οὐκ
ἐδυνήθησαν, κ.τ.λ. Τὸ προὖχον καὶ ἱερώτερον ἐν τοῖς Ἰουδαίοις ταλανίζεται μὲν
εἰκότως, ῥαγδαιοτέραν δὲ τὴν ὀργὴν ἐπήντλησεν αὐτῷ. Καὶ τοῦτο σαφηνιεῖ λέγων
αὐτὸς ὁ Χριστὸς, Οὐαὶ ὑμῖν, Γραμματεῖς καὶ Φαρισαῖοι ὑποκριταὶ, ὅτι περιάγετε τὴν
θάλασσαν καὶ τὴν ξηρὰν, ποιῆσαι ἕνα προσήλυτον, κ.τ.λ. Πρὸς τίνα λαλήσω καὶ
διαμαρτύρομαι; Κατωνείδιζον γὰρ τὸ ῥῆμα Χριστοῦ, τοῖς ἀκροωμένοις περὶ αὐτοῦ
λέγοντες· ∆αιμόνιον ἔχει καὶ μαίνεται· τί αὐτοῦ ἀκούετε; Οὐχὶ ὁρᾷς τί αὐτοὶ ποιοῦσι
ἐν ταῖς πόλεσιν Ἰούδα; 70.1453 Ὁσίᾳ δὴ οὖν τετιμώρηνται ψήφῳ. Ἀναθέντες γὰρ
κτίσει τὸ σέβας, καὶ τοῖς ἔργοις τῶν ἰδίων χειρῶν λατρεύσαντες, αὐτοὶ ταῖς ἰδίαις
ἐπήντλησαν κεφαλαῖς τὰ ἐκ θείας. ὀργῆς. Οὐ γὰρ ἀδίκως ἐκτείνεται δίκτυα
πτερωτοῖς, κατὰ τὸ γεγραμμένον. Ἠσχύνθησαν σοφοί. Σοφοὺς ἔοικεν ἐν τούτοις
ὑποδηλοῦν τοὺς τῶν Ἑλλήνων λογάδας, ποιητάς τε καὶ συγγραφέας· οἳ γλῶτταν μὲν
ἔχουσιν εὐφυᾶ καὶ περιειργασμένην, καὶ ταῖς τῶν λόγων κομψείαις ἐξησκημένην εἰς
τὸ ἐπίχαρι, φρονήσεως δὲ τῆς ἀληθοῦς ἀμέτοχοι τὴν καρδίαν. Ὡς γὰρ ὁ πάνσοφος
γράφει Παῦλος, Ἐματαιώθησαν ἐν τοῖς διαλογισμοῖς αὐτῶν, καὶ ἐσκοτίσθη ἡ
ἀσύνετος αὐτῶν καρδία. Φάσκοντες εἶναι σοφοὶ, ἐμωράνθησαν, καὶ ἑξῆς. Οὐκοῦν
σοφοὶ μὲν, τό γε ἧκον εἰς φωνὴν καὶ γλῶτταν, ἄσοφοι δὲ εἰς τὴν καρδίαν. Ἡ δὲ
σοφία τοῦ κόσμου τούτου μωρία παρὰ τῷ Θεῷ ἐστι, κατὰ τὸ γεγραμμένον. Ἐλαίαν
ὡραίαν εὔσκιον τῷ εἴδει. Ἀχρειοῖ γὰρ ὁλοτρόπως τὴν τοῦ ἀνθρώπου ψυχὴν τὸ
ἀποφοιτῆσαι Θεοῦ καὶ ἀμοιρῆσαι παντελῶς τῆς παρ' αὐτοῦ χάριτος καὶ ἐπικουρίας.
Φοβερὸν γὰρ τὸ ἐμπεσεῖν εἰς χεῖρας Θεοῦ ζῶντος. Πρὸς σὲ ἔθνη. Εἰ γὰρ καὶ γεγόνασιν
ὑβρισταὶ καὶ ἀπειθεῖς, καὶ ἀσυνεσίας ἔμπλεω τὴν διάνοιαν ἔχοντες, ἀλλ' οὖν
σώζονται καὶ δοξάζονται παρὰ Χριστοῦ.
Ἀπαρχὴ δὲ τούτων γεγόνασιν οἱ ἀθλοφόροι μάρτυρες, ὧν τῆς εὐκλείας ἡ
φαιδρότης ὅλην ἀστράπτει τὴν ὑπ' οὐρανόν. Καὶ ἔσται ὡς τὸ ξύλον. Οὐ γὰρ ἐπὶ
πλούτῳ μέγα φρονεῖ, οὔτε μὴν διακένων δοξαρίων ἐκτὸς γεγονὼς τὴν ὀσφὺν
ὑψηλήν (σιξ)· εἰκαιότητος γὰρ ταῦτα, καὶ ἕτερον οὐδέν. Καὶ γάρ ἐστιν ἀσφαλὲς
ἐνέχυρον εἰς σωτηρίαν, τὸ ἐπὶ Θεῷ πεποιθέναι τῷ πάντα ἰσχύοντι· τὸ δὲ ἐφ' ἑτέροις
τισι ποιεῖσθαι τὴν ἐλπίδα, μάταιον καὶ ἀνωφελές. Ἰδοὺ ὡς ὁ πηλός. Μεταπλάττει
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

γὰρ εὐκόλως ἐφ' ᾧπερ ἂν ἕλοιτο τὸ παρ' αὐτοῦ γεγονός. Οὐκ ἰδοὺ οἱ λόγοι; κ.τ.λ.
Πῦρ ὀνομάζει τὸ εὐαγγελικὸν καὶ σωτήριον κήρυγμα, ἢ καὶ αὐτὴν τὴν τοῦ ἁγίου
Πνεύματος μέθεξιν, ὃ καὶ πυρὶ παρεικάζεται. Καὶ γοῦν ὁ πάνσοφος Ἰωάννης ὁ
Βαπτιστὴς περὶ αὐτοῦ τε καὶ τοῦ πάντων ἡμῶν Σωτῆρος Ἰησοῦ Χριστοῦ οὕτω φησίν·
Ἐγὼ ὑμᾶς ὕδατι βαπτίζω εἰς μετανοίαν, κ.τ.λ. Ὀρθῶς οὖν ἔφη ὁ Χριστός· Πῦρ ἦλθον
βαλεῖν ἐπὶ τὴν γῆν, καὶ τί θέλω εἰ ἤδη ἀνήφθη; Καὶ ὑπεστρέψατε Ὁρᾷς ὅπως οὐκ
ἀνέχεται τῶν τύπων ἀθετουμένων, μᾶλλον δὲ αὐτῆς τῆς ἀληθείας ἐξυβρισμένης ὡς
ἐν σκιαῖς ἔτι; Ἀμεταμέλητα γὰρ τὰ χαρίσματα καὶ ἡ κλῆσις τοῦ Θεοῦ, κατὰ τὸ
γεγραμμένον. Τοὺς δὲ ἀνεθέντας ἅπαξ ἀνόπιν ἕλκοντες Ἰουδαῖοι, καὶ τοῖς τῆς
δουλείας αὖθις ὑποφέροντες ζυγοῖς, τὸ 70.1456 ἀμεταμέλητον οὐ τετηρήκασι·
προσκεκρούκασί τε διὰ τοῦτο, τοῦ μυστηρίου τὴν δύναμιν ἀτιμάζοντες
, εἰ καὶ ἦν ἐν τύποις ἔτι.
∆ικαιούμεθα δὲ ὅτι δωρεὰν διὰ χάριτος τῆς ἐν Χριστῷ ζωῆς ἀντάλλαγμα
προσενεγκόντες οὐδὲν, ἀλλ' οὐδὲ τὴν τῆς ἐλευθερίας ἐκπριάμενοι δόξαν, ἡμερότητι
δὲ καὶ φιλανθρωπίᾳ ∆εσποτικῇ τὸ χρῆμα κερδαίνοντες. Ἐὰν ὑμεῖς δῶτε, κ.τ.λ.
Ἐπειδὴ γὰρ ταῖς Αἰγυπτίων ἐπικουρίαις ἐπιθαρσήσαντες ἀντεφέροντο τοῖς
Βαβυλωνίοις, ταύτῃ τοι λελυπημένοι μετὰ τὴν τῶν Ἱεροσολύμων καὶ τῆς Σαμαρείας
ἅλωσιν, ἐτράποντο κατ' αὐτῶν, καὶ ἀμογητὶ νενικήκασιν. Τοῦ ἐπιστρέψαι, κ.τ.λ.
Ὅταν διώκῃ Θεὸς, οὐδεὶς ἀνασώσει τὸν κινδυνεύοντα· ἀλλ' ἐνθάπερ ἂν εἴη τις, ἐκεῖ
περιτεύξεται τῷ θυμῷ. Ἰδοὺ ἐγὼ ἐκδικῶ τὸν Ἀμμών. Κυρίλλου τὸ αὐτὸ καὶ
Ἀπολιναρίου. Ἀμμὼν δὲ οἱ μὲν τὴν Ἀλεξάνδρειαν λέγουσιν, οὕτω πάλαι
καλουμένην, ὡς καὶ ἐν τῷ Ναούμ φησι· Μὴ κρείττων εἶ σὺ τῆς Ἀμμὼν
, τῆς κατοικούσης ἐν ποταμοῖς; Ὕδωρ κύκλῳ αὐτῆς ἧς ἡ εὐπορία θάλασσα, καὶ ὕδωρ
τὰ τείχη αὐτῆς· Αἰθιοπία ἰσχὺς αὐτῆς καὶ Αἴγυπτος, καὶ οὐκ ἔστη πέρας τῆς φυγῆς·
καὶ Λίβυες ἐγένοντο βοηθοὶ αὐτῆς, καὶ αὐτὴ εἰς μετοικίαν πορεύσεται. Ἀλλ' οἱ
παρόντες καιροὶ τῆς Ἀλεξάνδρου πρεσβύτεροι κτίσεως. Ἴσως οὖν πρὸ Ἀλεξάνδρου
πόλις ὑπῆρχεν ἐκεῖ, καὶ παυσαμένην ἀνέκτισε. Τινὲς δέ φασιν ὡς Ἀμμωνᾶν τὸν ∆ία
καλοῦσιν Αἰγύπτιοι, ἀφ' οὗ καὶ Ἀμμωνιακὴ προσαγορεύεται χώρα· ὃν αὐτῆς υἱὸν
καλεῖ ὡς παρ' αὐτῆς θεοποιηθέντα· χειροποίητον γὰρ τὸ ἄγαλμα, ὃ δὴ, φησὶν, οἱ
Βαβυλώνιοι παραλήψονται, καὶ τοὺς ἄλλους αὐτῶν οὓς ἐπάγει θεούς. Τὸ δὲ ἐπὶ
Φαραὼ καὶ Αἴγυπτον, κατὰ κοινοῦ ἔχει τὸ ἐκδικῶ. Καὶ ἀγγέλους εἰς ἔθνη ἀπέστειλε.
Περιοχὴν δὲ εἶπεν οὓς Ἱερεμίας ἀγγέλους, καὶ Σύμμαχος ἀγγελίαν ἐξέδωκεν· νοοῖτο
δ' ἂν καὶ πολιορκία. Καλεῖ δὲ κατὰ τῆς Ἰδουμαίας ἔθνη πολλὰ τὸ θεῖον νεῦμα, καὶ
τὴν ἐξ αὐτοῦ κίνησιν ἀγγέλους εἶπε. Κινηθέντες γὰρ ἐκ Θεοῦ πρὸς πόλεμον,
ἀλλήλους ἠρέθιζον, ὥς φησιν Ἀβδιού· συνεισέβαλον γὰρ τοῖς ἐξ Ἰσραὴλ εἰς
Ἰδουμαίαν καὶ τὰ περίοικα τῶν ἐθνῶν. Πρὸς Ἰδουμαίαν δὲ μεταστρέψας, φησίν·
Ἰδοὺ μικρὸν ἔδωκά σε ἐν ἔθνεσιν, ἀλλ' ὅμως εὐκαταφρόνητος γενόμενος ἐφρόνεις
εἰς μέγα ἐπὶ ταῖς δυσχερείαις, ἀγνοῶν ὡς ἁλώσῃ κἂν ὑψώσῃς ὡς ἀετὸς τὴν
κατοίκησιν. Τῷ δὲ, καὶ ἔσται Ἰδουμαία εἰς ἄβατον, καὶ τοῖς ἐφεξῆς, συνωδὰ πάλιν
ἔφησεν Ἀβδιού· Καὶ ἔσται ὁ οἶκος Ἡσαῦ εἰς καλάμην, καὶ ἐκκαυθήσεται εἰς αὐτοὺς
, καὶ οὐκ ἔσται πυρφόρος ἐν τῷ οἴκῳ Ἡσαῦ.
Ἡ δὲ παροιμία καὶ παρὰ τοῖς ἔξω κρατεῖ· Οὐδὲ πυρφόρος ἐλείφθη, ἐπὶ τῶν
ἄρδην ἀπολλυμένων. Προηγεῖτο γάρ τις πῦρ ἔχων, καὶ περικαθαίρων πρὸ τῆς
συμβολῆς τὰ στρατεύματα, καὶ νόμος ἦν μηδένα κατὰ τούτου χεῖρα φέρειν πολέμιον.
70.1457 Ἰατρεύσαμεν τὴν Βαβυλῶνα. Βαβυλῶνα τὴν Ἱερουσαλὴμ ἐν τούτοις
κατονομάζουσιν, ὡς τὰ ἐκείνης μιμεῖσθαι σπουδάζουσαν, καὶ οὐδὲν ἀποδέουσαν τῆς
τῶν ἐθνῶν χώρας, τό γε ἧκον εἰς ἕξιν τελείαν, καὶ τὸ μὴ ἀνέχεσθαι τιμᾷν τὸν νόμον,
ἤτοι τῆς προφητικῆς παιδαγωγίας τὸ ἐπωφελές.
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

