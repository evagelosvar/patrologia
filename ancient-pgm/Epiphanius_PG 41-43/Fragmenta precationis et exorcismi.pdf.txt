Fragmenta precationis et exorcismi
Ἐπικαλούμεθά σε, Θεὲ παντοκράτορ Κύριε Ἰησοῦ Χριστὲ οὐράνιε βασιλεῦ·
ἐπικαλούμεθά σε τὸν ποιήσαντα τὸν οὐρανὸν καὶ τὴν γῆν. Ἐξορκίζω σε, πνεῦμα
ἀκάθαρτον, κατὰ τοῦ λόγου Θεοῦ τοῦ ποιήσαντος τὸν οὐρανὸν καὶ τὴν γῆν.
φοβήθητι τὸν ἀναβαλλόμενον φῶς ὡς ἱμάτιον· τὸν μόνον ἔχοντα ἀθανασίαν καὶ
φῶς ἀπρόσιτον· ᾧ ἡ δόξα καὶ τὸ κράτος εἰς τοὺς αἰῶνας. Ἀμήν.

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

