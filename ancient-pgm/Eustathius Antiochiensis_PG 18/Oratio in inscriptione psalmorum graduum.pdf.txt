Oratio in inscriptione psalmorum graduum (fragmenta)
Ο ΛΟΓΟΣ ΕΙΣ ΤΑΣ ΕΠΙΓΡΑΦΑΣ ΤΩΝ ΑΝΑΒΑΘΜΩΝ
33 ∆όξαν δὲ ἐπίκτητον ὁ Πατὴρ οὐκ ἐπιδέχεται, τέλειος, ἄπειρος,
ἀπερινόητος ὢν, ἀπροσδεὴς κάλλους καὶ παντοίας εὐπρεπείας. Ἀλλ' οὐδὲ ὁ Λόγος
αὐτοῦ, Θεὸς ὢν ὁ γεννηθεὶς ἐξ αὐτοῦ, δι' οὗ γεγόνασιν ἄγγελοι καὶ οὐρανοὶ καὶ γῆς
ἄπειρα μεγέθη καὶ πᾶσαι συλλήβδην τῶν γενητῶν ὕλαι τε καὶ συστάσεις· ἀλλ' ὁ
ἄνθρωπος τοῦ Χριστοῦ, ἐκ νεκρῶν ἐγειρόμενος, ὑψοῦται καὶ δοξάζεται, τῶν
ἐναντίων αἰσχύνην ἄντικρυς ἀραμένων. 34 Οἱ δὲ φθόνῳ τὸ μῖσος ἀράμενοι κατ'
αὐτοῦ καὶ πεφραγμένοι πολεμίᾳ παρατάξει σκορπίζονται, τοῦ Λόγου τε καὶ Θεοῦ τὸν
ἑαυτοῦ ναὸν ἀξιοπρεπῶς ἀναστήσαντος.

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

