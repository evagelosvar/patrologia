Ad Graecos ex communibus notionibus
ΓΡΗΓΟΡΙΟΥ ΕΠΙΣΚΟΠΟΥ ΝΥΣΣΗΣ
Πῶς τρία πρόσωπα λέγοντες ἐν τῇ θεότητι οὔ φαμεν τρεῖς θεούς πρὸς
τοὺς Ἕλληνας ἀπὸ τῶν κοινῶν ἐννοιῶν
Εἰ τὸ θεὸς ὄνομα προσώπου δηλωτικὸν ὑπῆρχεν, τρία πρόσωπα λέγοντες ἐξ
ἀνάγκης τρεῖς ἂν ἐλέγομεν θεούς· εἰ δὲ τὸ θεὸς ὄνομα οὐσίας σημαντικόν ἐστιν, μίαν
οὐσίαν ὁμολογοῦντες τῆς ἁγίας τριάδος ἕνα θεὸν εἰκότως δογματί ζομεν, ἐπειδὴ
μιᾶς οὐσίας ἓν ὄνομα τὸ θεός ἐστιν. διὸ καὶ ἀκολούθως τῇ τε οὐσίᾳ καὶ τῷ ὀνόματι
εἷς ἐστι θεὸς καὶ οὐ τρεῖς. οὐδὲ γὰρ θεὸν καὶ θεὸν καὶ θεόν φαμεν, ὥσπερ λέγομεν
πατέρα καὶ υἱὸν καὶ ἅγιον πνεῦμα, ἐπεὶ τοῖς ὀνόμασι τοῖς τῶν προσώπων
σημαντικοῖς συμπλέκομεν τὸν καὶ σύνδεσμον διὰ τὸ μὴ ταὐτὰ εἶναι τὰ πρόσωπα,
ἑτεροῖα δὲ μᾶλλον καὶ διαφέροντα ἀλλήλων κατ' αὐτὴν τὴν τῶν ὀνο μάτων
σημασίαν, τῷ δὲ θεὸς ὀνόματι δηλωτικῷ τῆς οὐσίας ὄντι ἔκ τινος ἰδιώματος
προσόντος αὐτῇ οὐ συνάπτομεν τὸν καὶ σύνδεσμον ὥστε λέγειν ἡμᾶς θεὸν καὶ θεὸν
καὶ θεόν, ἐπείπερ ἡ αὐτή ἐστιν οὐσία, ἧς ἐστι τὰ πρόσωπα καὶ ἣν σημαίνει τὸ θεὸς
ὄνομα· διὸ καὶ ὁ αὐτὸς θεός· τῷ δὲ αὐτῷ καὶ ἐπὶ δηλώσει τοῦ αὐτοῦ ὁ καὶ σύνδεσμος
οὐ συμπλέκεταί 3,1.20 ποτε.
Εἰ δὲ λέγομεν πατέρα θεὸν καὶ υἱὸν θεὸν καὶ πνεῦμα ἅγιον θεὸν ἢ θεὸν
πατέρα καὶ θεὸν υἱὸν καὶ θεὸν πνεῦμα ἅγιον, τὸν καὶ σύνδεσμον κατ' ἔννοιαν τοῖς
τῶν προσώπων ὀνόμασι συνάπτομεν, οἷον πατρί, υἱῷ, ἁγίῳ πνεύματι, ἵνα ᾖ πατὴρ
καὶ υἱὸς καὶ ἅγιον πνεῦμα, τουτέστι πρόσωπον καὶ πρόσωπον καὶ πρόσωπον, διὸ καὶ
τρία πρόσωπα. τὸ δὲ θεὸς ὄνομα ἀπολύτως καὶ ὡσαύτως κατηγορεῖται ἑκάστου τῶν
προσώπων ἄνευ τοῦ καὶ συνδέσμου, ὥστε μὴ δύνασθαι ἡμᾶς λέγειν θεὸν καὶ θεὸν
καὶ θεόν, ἀλλὰ νοεῖν τὸ ὄνομα <δεύτερον καὶ> τρίτον μὲν λεγόμενον τῇ φωνῇ διὰ τὰ
ὑποκείμενα πρόσωπα, προσβαλλόμενον δὲ τῇ δευτερώσει καὶ τῇ τριτώσει ἄνευ τοῦ
καὶ συνδέσμου διὰ τὸ μὴ ἕτερον καὶ ἕτερον εἶναι θεόν. οὐ γάρ, καθὸ τὴν ἑτερότητα
σῴζει πατὴρ πρὸς υἱόν, κατὰ τοῦτο θεὸς ὁ πατήρ· οὕτω γὰρ οὐκ ἂν θεὸς ὁ υἱός· εἰ
γάρ, ἐπειδὴ πατὴρ ὁ πατήρ, διὰ τοῦτο καὶ θεὸς ὁ πατήρ, ἐπειδὴ μὴ πατὴρ ὁ υἱός, οὐ
θεὸς ὁ υἱός· εἰ δὲ θεὸς ὁ υἱός, οὐκ, ἐπειδὴ υἱός. ὁμοίως καὶ ὁ πατὴρ οὐκ, ἐπειδὴ
πατήρ, θεός, ἀλλ' ἐπειδὴ οὐσία τοιάδε, ἧς ἐστι πατὴρ καὶ υἱὸς καὶ δι' ἣν πατὴρ θεὸς
καὶ υἱὸς θεὸς καὶ πνεῦμα ἅγιον θεός. μὴ διαιρουμένης δὲ τῆς οὐσίας ἐν ἑκάστῳ τῶν
προσώπων ὥστε καὶ τρεῖς εἶναι οὐσίας κατὰ τὰ πρόσωπα, δῆλον ὅτι οὐδὲ τὸ ὄνομα
διαιρεθήσεται, ὅπερ σημαίνει τὴν οὐσίαν, τουτέστι τὸ θεός, εἰς τὸ εἶναι τρεῖς θεούς.
ἀλλ' ὥσπερ οὐσία ὁ πατήρ, οὐσία ὁ υἱός, οὐσία τὸ ἅγιον πνεῦμα καὶ οὐ τρεῖς οὐσίαι,
οὕτω καὶ θεὸς ὁ πατήρ, θεὸς ὁ υἱός, θεὸς τὸ πνεῦμα τὸ ἅγιον καὶ οὐ τρεῖς θεοί. εἷς
γὰρ θεὸς καὶ ὁ αὐτός, ἐπεὶ καὶ μία οὐσία καὶ ἡ αὐτή, εἰ 3,1.21 καὶ λέγεται ἕκαστον
τῶν προσώπων καὶ ἐνούσιον καὶ θεός. ἢ γὰρ ἀνάγκη τρεῖς λέγειν οὐσίας πατρὸς καὶ
υἱοῦ καὶ ἁγίου πνεύματος, ἐπειδὴ οὐσία τῶν προσώπων ἕκαστον, ὅπερ ἐστὶν
ἀλογώτατον, ἐπείπερ οὐδὲ Πέτρον καὶ Παῦλον καὶ Βαρνάβαν φαμὲν τρεῖς οὐσίας·
μία γὰρ καὶ ἡ αὐτὴ τῶν τοιούτων προσώπων ἡ οὐσία· ἢ μίαν λέγοντες οὐσίαν, ἧς
ἐστι πατὴρ καὶ υἱὸς καὶ ἅγιον πνεῦμα, καίπερ ἐνούσιον εἰδότες τῶν προσώπων
ἕκαστον, ἕνα δικαίως καὶ ἀκολούθως φαμὲν θεόν, εἰ καὶ τῶν προσώπων ἕκαστον
θεὸν εἶναι πιστεύομεν, διὰ τὸ κοινὸν τῆς οὐσίας.
\ Ωσπερ γὰρ διὰ τὸ διαφέρειν τὸν πατέρα τοῦ τε υἱοῦ καὶ τοῦ ἁγίου πνεύματος
τρία φαμὲν πρόσωπα πατρὸς καὶ υἱοῦ καὶ ἁγίου πνεύματος, οὕτως, ἐπειδὴ μὴ
διαφέρει πατὴρ υἱοῦ τε καὶ ἁγίου πνεύ ματος κατὰ τὴν οὐσίαν, μίαν εἶναι λέγομεν
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

τὴν οὐσίαν πατρὸς καὶ υἱοῦ καὶ ἁγίου πνεύματος. εἰ γάρ, ἔνθα διαφορά, τριὰς διὰ τὴν
διαφοράν, ἔνθα ταυτότης, μονὰς διὰ τὴν ταυτότητα· ἔστι δὲ ταυτότης τῶν
προσώπων κατὰ τὴν οὐσίαν· μονὰς ἄρα αὐτῶν κατὰ τὴν οὐσίαν. εἰ δὲ κατὰ τὴν
οὐσίαν μονὰς τῆς ἁγίας τριάδος, δῆλον ὅτι καὶ κατὰ τὸ θεὸς ὄνομα. δηλωτικὸν γὰρ
τοῦτο τῆς οὐσίας οὐ τὸ τί αὐτῆς παριστῶν (δῆλον ὅτι ἐπείπερ ἀπερινόητον καὶ
ἀκατάληπτον τὸ τῆς θείας οὐσίας), ἀλλ' ἀπό τινος ἰδιώματος προσόντος 3,1.22 αὐτῇ
λαμβανόμενον παραδηλοῖ αὐτήν, καθάπερ τὸ χρεμε τιστικὸν καὶ τὸ γελαστικὸν
ἰδιώματα ὄντα φύσεων λεγό μενα σημαίνει τὰς φύσεις, ὧνπέρ ἐστιν ἰδιώματα. ἔστι
τοίνυν ἰδίωμα τῆς ἀϊδίου οὐσίας, ἧς ἐστι πατὴρ καὶ υἱὸς καὶ ἅγιον πνεῦμα, τὸ πάντα
ἐποπτεύειν καὶ θεωρεῖν καὶ γινώσκειν, οὐ μόνον τὰ ἔργῳ γινόμενα, ἀλλὰ καὶ τὰ ἐν
νῷ λαμβανόμενα, ὅπερ μόνης ἐστὶν ἐκείνης τῆς οὐσίας, ἅτε δὴ καὶ αἰτίας πάντων
ὑπαρχούσης τῆς τὰ πάντα ποιησάσης καὶ πάντων ὡς ἰδίων ποιημάτων δεσποζούσης,
τὰ δὲ κατ' ἀνθρώπους ἅπαντα συμφέροντί τινι καὶ ἀρρήτῳ λόγῳ πρυτανευούσης.
ἐντεῦθεν εἰλημμένον τὸ θεὸς ὄνομα κυρίως λεγόμενον σημαίνει τὴν οὐσίαν
ἐκείνην, ἥτις ἀληθῶς δεσπόζει τῶν ἁπάντων ὡς πάντων δημιουργός. μιᾶς
τοιγαροῦν ὑπαρ χούσης τῆς οὐσίας, ἧς ἐστι πατὴρ καὶ υἱὸς καὶ ἅγιον πνεῦμα, καὶ
ἑνὸς τοῦ παραδηλοῦντος αὐτὴν ὀνόματος (φημὶ δή, τοῦ θεός) εἷς θεὸς ἔσται κυρίως
καὶ ἀκολούθως τῷ λόγῳ τῆς οὐσίας, μηδενὸς λόγου καταναγκάζοντος ἡμᾶς τρεῖς
λέγειν θεούς, ὥσπερ οὖν οὐδὲ τρεῖς οὐσίας. εἰ γὰρ ἐπὶ Πέτρου καὶ Παύλου καὶ
Βαρνάβα τρεῖς οὐσίας οὔ φαμεν διὰ τὸ μιᾶς αὐτοὺς εἶναι, πόσῳ μᾶλλον ἐπὶ πατρὸς
καὶ υἱοῦ καὶ ἁγίου πνεύματος τοῦτο δικαίως οὐ ποιήσομεν· εἰ γὰρ τὴν οὐσίαν οὐ
διαιρετέον εἰς τρεῖς διὰ τὰ πρόσωπα, δῆλον ὅτι οὐδὲ τὸν θεόν, ἐπεὶ μὴ πρόσωπον
δηλοῖ τὸ θεός, ἀλλὰ τὴν οὐσίαν. εἰ γὰρ πρόσωπον ἐδήλου τὸ θεός, ἓν καὶ μόνον
3,1.23 τῶν προσώπων ἐλέγετο θεός, ὅπερ ἐσημαίνετο τῷ τοιῷδε ὀνόματι, ὥσπερ οὖν
καὶ πατὴρ μόνος ὁ πατὴρ λέγεται διὰ τὸ προσώπου δηλωτικὸν εἶναι τοῦτο τὸ ὄνομα.
Εἰ δὲ φαίη τις, ὅτι Πέτρον καὶ Παῦλον καὶ Βαρνάβαν φαμὲν τρεῖς οὐσίας
μερικάς [δῆλον ὅτι τοῦτ' ἐστιν ἰδικάς]· τοῦτο γὰρ κυριώτερον εἰπεῖν, γνώτω ὅτι
μερικὴν οὐσίαν, τουτέστιν ἰδικήν, λέγοντες οὐδὲν ἕτερον σημᾶναι βουλόμεθα ἢ
ἄτομον, ὅπερ ἐστὶ πρόσωπον. διὸ δὴ καὶ τρεῖς εἰ λέγοι μεν μερικὰς οὐσίας, τουτέστιν
ἰδικάς, οὐδὲν ἄλλο φαμὲν ἢ τρία πρόσωπα· προσώποις δὲ οὐχ ἕπεται τὸ θεός,
καθάπερ δέδεικται. οὐδ' ἄρα οὐδὲ τῇ μερικῇ, ὅπερ ἐστὶν ἰδικῇ, οὐσίᾳ· ταὐτὸν γάρ
ἐστιν ἰδικὴ οὐσία τῷ προσώπῳ ἐπὶ τῶν ἀτόμων λεγομένη. τί οὖν λεκτέον πρὸς
ἐκεῖνο, ὅτι Πέτρον καὶ Παῦλον καὶ Βαρνάβαν τρεῖς φαμὲν ἀνθρώπους; εἰ γὰρ ταῦτα
πρόσωπα, πρόσωπα δὲ τῷ σημαντικῷ ὀνόματι τῆς κοινῆς οὐσίας οὐ σημαίνεται,
ὁμοίως οὐδὲ ἡ μερικὴ λεγομένη ἤτοι ἰδικὴ οὐσία, ἐπειδὴ ταὐτὸν αὕτη τῷ προσ
ώπῳτίνος χάριν τρεῖς ἀνθρώπους φαμὲν αὐτοὺς μιᾶς οὐσίας ὑπάρχοντας, ἧς ἐστι τὸ
ἄνθρωπος δηλωτικόν, εἰ μήτε διὰ τὰ πρόσωπα μήτε διὰ τὸ λέγεσθαι μερικὴν ἤγουν
ἰδικὴν οὐσίαν ἐκφωνοῦμεν τοῦτο; φαμέν, ὅτι καταχρηστικῶς καὶ οὐ κυρίως τοῦτο
λέγομεν διά τινα συνήθειαν ἐξ ἀναγκαίων αἰτιῶν κρατήσασαν, αἵτινες οὐ
θεωροῦνται ἐπὶ τῆς ἁγίας τριάδος, ἵνα καὶ ἐπ' αὐτῆς τοῦτο αὐτὸ ποιῶμεν. εἰσὶ 3,1.24
δὲ αἱ αἰτίαι αὗται· ὁ ὅρος ὁ τοῦ ἀνθρώπου οὐκ ἀεὶ ἐν τοῖς αὐτοῖς ἀτόμοις ἤγουν
προσώποις θεωρεῖται· τῶν μὲν γὰρ προτέρων τελευτώντων ἕτερα ἀντ' αὐτῶν
συνίσταται καὶ πάλιν τῶν αὐτῶν πολλάκις μενόντων ἄλλα τινὰ ἐπιγίνεται, ὡς ποτὲ
μὲν ἐν τούτοις, ποτὲ δὲ ἐν ἐκείνοις, καὶ ποτὲ μὲν ἐν πλείοσιν, ποτὲ δὲ ἐν ὀλιγωτέροις
θεωρεῖσθαι τὸν τῆς φύσεως ἤγουν τοῦ ἀνθρώπου ὅρον. διὰ ταύτην οὖν τὴν αἰτίαν
τῆς τε προσθήκης καὶ τῆς ἀφαιρέσεως τῆς τε ἀποβιώσεως καὶ γεννήσεως τῶν
ἀτόμων, ἐν οἷς θεωρεῖται ὁ τοῦ ἀνθρώπου ὅρος, ἀναγκαζόμεθα καὶ πολλοὺς λέγειν
ἀνθρώπους καὶ ὀλίγους τῇ τροπῇ καὶ ἀλλοιώσει τῶν προσώπων ἐκκρου σθείσης τῆς
κοινῆς συνηθείας καὶ παρ' αὐτὸν τὸν τῆς οὐσίας λόγον, ὥστε συναριθμεῖν τοῖς
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

προσώποις τρόπον τινὰ καὶ οὐσίας. ἐπὶ δὲ τῆς ἁγίας τριάδος οὐδὲν τοιοῦτον
συμβαίνει ποτέ· δεῖ γὰρ τὰ αὐτὰ πρόσωπα καὶ οὐχ ἕτερα καὶ ἕτερα λέγεσθαι ἀεὶ κατὰ
τὸ αὐτὸ καὶ ὡσαύτως ἔχοντα μήτε προσ θήκην τινὰ δεχόμενα τὴν εἰς τετράδα μήτε
μείωσιν τὴν εἰς δυάδα· (οὔτε γὰρ γεννᾶται ἢ ἐκπορεύεται ἐκ τοῦ πατρὸς ἢ ἐξ ἑνὸς
τῶν προσώπων πρόσωπον ἕτερον, ὥστε καὶ τετράδα εἶναί ποτε τὴν τριάδα· οὔτε
τελευτᾷ ποτε ἓν τῶν τριῶν τούτων προσώπων κἂν ὡσεὶ ῥοπῇ ὀφθαλμοῦ, ὥστε
δυάδα τὴν τριάδα γενέσθαι κἂν τῇ ἐνθυμήσει·) προσθήκης δὲ καὶ μειώσεως τροπῆς
τε καὶ ἀλλοιώσεως μηδεμιᾶς γινομένης τοῖς τρισὶ προσώποις πατρὸς καὶ υἱοῦ καὶ
ἁγίου πνεύματος οὐδὲν τὸ παρακροῦον τὴν ἡμετέραν διάνοιαν πρὸς τοῖς τρισὶ
προσώποις καὶ τρεῖς λέγειν θεούς. πάλιν τὰ τοῦ ἀνθρώπου 3,1.25 πρόσωπα πάντα
οὐκ ἀπὸ τοῦ αὐτοῦ προσώπου κατὰ τὸ προσεχὲς ἔχει τὸ εἶναι, ἀλλὰ τὰ μὲν ἐκ τούτου,
τὰ δὲ ἐξ ἐκείνου ὡς πολλὰ καὶ διάφορα εἶναι πρὸς τοῖς αἰτιατοῖς καὶ τὰ αἴτια. ἐπὶ δὲ
τῆς ἁγίας τριάδος οὐχ οὕτως· ἓν γὰρ πρόσωπον καὶ τὸ αὐτό, τοῦ πατρός, ἐξ οὗπερ ὁ
υἱὸς γεν νᾶται καὶ τὸ πνεῦμα τὸ ἅγιον ἐκπορεύεται. διὸ δὴ καὶ κυρίως τὸν ἕνα αἴτιον
μετὰ τῶν αὐτοῦ αἰτιατῶν ἕνα θεόν φαμεν τεθαρρηκότως, ἐπειδὴ καὶ συνυπάρχει
αὐτοῖς. οὔτε γὰρ χρόνῳ διῄρηται ἀλλήλων τὰ πρόσωπα τῆς θεότητος οὔτε τόπῳ, οὐ
βουλῇ, οὐκ ἐπιτηδεύματι, οὐκ ἐνεργείᾳ, οὐ πάθει, οὐδενὶ τῶν τοιούτων, οἷάπερ
θεωρεῖται ἐπὶ τῶν ἀνθρώπων· ἢ μόνον, ὅτι ὁ πατὴρ πατήρ ἐστι καὶ οὐχ υἱὸς καὶ ὁ
υἱὸς υἱός ἐστι καὶ οὐ πατήρ, ὁμοίως καὶ τὸ πνεῦμα τὸ ἅγιον οὔτε πατὴρ οὔτε υἱός.
διόπερ οὐδεμία ἀνάγκη παρακρούει ἡμᾶς τρεῖς θεοὺς εἰπεῖν τὰ τρία πρόσωπα, ὥσπερ
ἐφ' ἡμῶν πολλοὺς ἀνθρώπους φαμὲν τὰ πολλὰ πρόσωπα διὰ τὰς εἰρημένας αἰτίας.
ὅτι δὲ διὰ τὰς εἰρημένας αἰτίας καὶ οὐ κατὰ λόγον ἀναγκαῖόν φαμεν τὰ πολλὰ
πρόσωπα τοῦ ἀνθρώπου πολλοὺς ἀνθρώπους, ἐντεῦθεν γένοιτ' ἂν δῆλον· τὸ αὐτὸ
κατὰ τὸ αὐτὸ ἓν καὶ πολλὰ οὐ δύναται εἶναι· ἔστι δὲ Πέτρος καὶ Παῦλος καὶ
Βαρνάβας ὁμολογουμένως κατὰ τὸ ἄνθρωπος εἷς ἄν θρωπος· κατὰ τὸ αὐτὸ ἄρα,
τουτέστι κατὰ τὸ ἄνθρωπος, πολλοὶ οὐ δύνανται εἶναι. λέγονται δὲ πολλοὶ
ἄνθρωποι καταχρηστικῶς δηλονότι καὶ οὐ κυρίως· τὸ δὲ καταχρηστι κῶς λεγόμενον
διαφθείρειν οὐκ ἄξιον οὐδὲ ἱκανὸν παρὰ 3,1.26 τοῖς εὖ φρονοῦσι τὸ κυρίως τε ὂν
καὶ λεγόμενον. οὐκ ἄρα λεκτέον ἐπὶ τῶν τριῶν προσώπων τῆς θείας οὐσίας τρεῖς
θεούς, ἐπεὶ κατὰ τὸ θεὸς εἷς ἐστι θεὸς καὶ ὁ αὐτὸς διὰ τὴν ταυτότητα τῆς οὐσίας, ἧς
ἐστι τὸ θεὸς σημαντικὸν κατὰ τὸν εἰρημένον τρόπον.
Εἰ δὲ λέγοι τις, ὅτιπερ ἡ γραφὴ συναριθμεῖ τρεῖς ἄνδρας λέγουσα, κατὰ
περιουσίαν ἐκ τῶν ἡμετέρων ἡμᾶς ἐλέγξαι πειρώμενος, οὐκ ὀρθὸς οὐδὲ ὅσιος
πέφανται τῆς γραφῆς ἀκροατὴς ὁ τοιοῦτος· οὗτος γὰρ ἂν οὐδὲ ἐκίνει πρὸς ἡμᾶς
λόγους περὶ τοῦ εἰ δεῖ λέγειν τρεῖς θεοὺς τὰ τῆς θείας οὐσίας τρία πρόσωπα συνορῶν
μάλιστα, ὅτι πατέρα καὶ υἱὸν καὶ ἅγιον πνεῦμα διδοῦσα ἡ γραφὴ καὶ θεὸν λόγον,
θεὸν μὴ λόγον (τουτέστι θεὸν πατέρα), θεὸν ἅγιον πνεῦμα θεὸν παραδιδοῦσα
καθόλου παραιτεῖται τρεῖς θεοὺς εἰπεῖν, ἀσέ βειαν ἡγουμένη τὴν πολυθεΐαν καὶ ἕνα
θεὸν διόλου κηρύτ τουσα μήτε τὰ πρόσωπα συμφύρουσα μήτε τὴν θεότητα
διαιροῦσα, φυλάττουσα δὲ μᾶλλον ταυτότητα θεότητος ἐν ἰδιότητι ὑποστάσεων
ἤγουν προσώπων τριῶν. εἰ τοίνυν ὀρθὸς ἦν ὁ τοιοῦτος καὶ τοῦ δικαίου λόγον
ἐποιεῖτο, ταῦτα κατανοῶν ἐκ τῆς γραφῆς τὰ ἴδια κυροῦν οὐκ ἔσπευδεν, ἐπυνθάνετο
δὲ μᾶλλον τὴν αἰτίαν μαθεῖν αἰτούμενος, δι' ἣν τρεῖς ἄνδρας λέγει ἡ γραφὴ καίπερ
ἕνα ἄνθρωπον τὴν σύμπασαν φύσιν γινώσκουσα κατὰ τὸ Ἄνθρωπος ὡσεὶ 3,1.27
χόρτος αἱ ἡμέραι αὐτοῦ ἑνικῶς τὸ κοινὸν τῆς φύσεως ἐκφω νήσασα· τὸ προσὸν γὰρ
τῷ κοινῷ τῆς φύσεως ἀποφαινομένη καθ' ἑνὸς αὐτὸ εἶπε διὰ τὸ ἔνα γινώσκειν
ἄνθρωπον τὸν σύμπαντα ἄνθρωπον καὶ οὐ πολλούς· ἤκουε γὰρ φιλομαθῶς ἐρωτῶν,
ὅτι καὶ ἡ γραφὴ ὡς τροφὸς ἀγαθὴ ἴδια βρέφη γινώσκουσα τοὺς ἀνθρώπους ἔστιν ὅτε
συμψελλίζει αὐτοῖς ὡσαύτως χρωμένη τισὶ τῶν ὀνομάτων οὐ παρατιτρώσκουσα τὸ
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

3

τέλειον δηλονότι καὶ τοὺς τροφῆς μεταλαμβάνειν δυνα μένους στερεᾶς ἀδικοῦσα·
οὐ γὰρ δόγματα τὰ ψελλίσματα ὁριζομένη τὰ τέλεια καταβλάπτει, ἀλλὰ
συγκαταβαίνουσα τοῖς νηπίοις δι' εὐσπλαγχνίαν καὶ μιμουμένη τὰ ἐκείνων οὕτως
ἀνάγει πρὸς ἑαυτὴν εἰς τελειότητα ἡλικίας ἄγουσα. ὁρίζεται δὲ ὅμως καὶ δογματίζει
τὰ τέλεια κατὰ τὸ ἑαυτῇ πρέπον καὶ τοὺς μαθητευομένους προσῆκον διδάσκεσθαι.
ἀμέλει ὅτι ὦτα καὶ ὀφθαλμοὺς καὶ στόμα καὶ τὰ λοιπὰ δὴ μόρια σωματικὰ λέγουσα
ἔχειν τὸν θεὸν οὐ δόγμα τὸ τοιοῦτο παραδίδωσι σύνθετον ἐκ διαφόρων μελῶν
ὁριζομένη τὸ θεῖον, ἀλλὰ κατὰ τὸν εἰρημένον τρόπον ἐκ μεταφορᾶς τῶν ἡμετέρων
λαμβάνουσα τὰ τοιαῦτα πρὸς ἀναγωγήν, ὡς εἶπον, τῶν μὴ ἀμέσως ἐπὶ τὰ ἀσώματα
χωρεῖν δυναμένων στερεαῖς τισι καὶ τρανοτάταις ταῖς λέξεσι τὰ δόγματα ἐκτίθεται,
πνεῦμα λέγουσα τὸν θεὸν εἶναι καὶ πανταχοῦ, ἔνθα τις πορευθείη, παρεῖναι τὸ
ἁπλοῦν αὐτοῦ καὶ ἀπερίγραφον τούτους σοφῶς 3,1.28 ἐκπαιδεύουσα. οὕτως καὶ
τρεῖς ἄνδρας λέγουσα διὰ συν ήθειαν, ἵνα μὴ ξενίζῃ τὸ κοινὸν καὶ ἐν χρήσει τῶν
πολλῶν ὑπάρχον, καὶ ἕνα φησὶ δι' ἀκρίβειαν, ἵνα μὴ παρασαλεύσῃ τὸ τέλειον καὶ ἐν
τῇ φύσει τῶν πραγμάτων θεωρούμενον· καὶ τὸ μὲν ἡγούμεθα συγκατάβασιν ἐπὶ
χρησίμῳ καὶ συμ φέροντι τῶν νηπιωδεστέρων γεγενημένην, τὸ δὲ ὁριζόμεθα δόγμα
ἐπὶ βεβαιώσει καὶ παραδόσει τῆς τελειότητος ἐκτι θέμενον.
Ἀλλά τινες συζητητικοὶ ὑπάρχοντες καὶ διεγηγερ μένοι πρὸς τὸ μηδὲν ἐᾶν
ἀναντίρρητον ἐπὶ τῷ καὶ τοὺς λέγοντας καὶ τοὺς ἀκροωμένους, ὡς ἔγωγε φαίην ἄν,
φιλοπόνως γυμνάσαι παρορῶντες τὸ παρ' ἡμῶν λελεγμένον καὶ τὸ μὴ δεδομένον ὡς
ὁμολογούμενον λαμβάνοντες παρα λογισμῷ τε χρώμενοι τὸ δοκοῦν κατασκευάζουσι
καί φασιν, ὅτι ὥσπερ λέγομεν ὑπόστασις ὑποστάσεως, ᾗ ὑπόστασις, τουτέστι καθὸ
ὑπόστασις, οὐδὲν διαφέρει, καὶ οὐ παρὰ τοῦτο μία ὑπόστασις αἱ πᾶσαι ὑποστάσεις·
καὶ πάλιν οὐσία οὐσίας, ᾗ οὐσία, οὐδὲν διαφέρει, καὶ οὐ παρὰ τοῦτο μία οὐσία αἱ
πᾶσαι οὐσίαι, οὕτως ἂν εἴποιμεν θεὸς θεοῦ, ᾗ θεός, οὐδὲν διαφέρει, καὶ οὐ διὰ τοῦτο
εἷς θεὸς αἱ τρεῖς ὑποστάσεις, καθ' ὧν τὸ θεὸς κατηγορεῖται· καὶ πάλιν ἄνθρωπος
ἀνθρώπου, ἧ ἄνθρωπος, οὐδὲν διαφέρει λέγοντες οὐκ ἀναιροῦμεν τὸ τρεῖς
ἀνθρώπους εἶναι Πέτρον καὶ Παῦλον καὶ Βαρνάβαν· διαφέρει γὰρ οὐσία οὐσίας οὐ
καθὸ οὐσία, ἀλλὰ καθὸ τοιάδε οὐσία καὶ ὑπόστασις ὑποστάσεως καθὸ τοιάδε
ὑπόστασις· 3,1.29 ὡσαύτως καὶ ἄνθρωπος ἀνθρώπου, ᾗ τοιόσδε ἄνθρωπος, καὶ
πάλιν θεὸς θεοῦ, ᾗ τοιόσδε θεός· τὸ δὲ τοιόσδε ἢ τοιόσδε ἐπὶ δύο ἢ καὶ πλειόνων
εἴωθε λέγεσθαι. ἀλλὰ ταῦτα μέν, ὡς εἴπομεν, ἐκεῖνοι λέγουσιν· ἡμεῖς δὲ δείξομεν
σόφισμα τὸ πᾶν εἶναι καὶ οὐδὲν ἕτερον τὸ λελεγμένον, οὐδαμῶς ἄλλως
περιγινόμενοι ἀλλ' ἢ αὐτοῖς τοῖς εἰρημένοις κεχρημένοι καὶ δεικνύντες μὴ δεῖν τὸν
τοιόνδε θεὸν καὶ τοιόνδε θεὸν ἢ τοιόνδε ἄνθρωπον καὶ τοιόνδε λέγειν· ἀλλ' εἰ ἄρα
τοιάνδε ὑπόστασιν θεοῦ καὶ τοιάνδε ὑπόστασιν ἀνθρώπου· πολλὰς γὰρ ὑπο στάσεις
τοῦ ἑνὸς ἀνθρώπου καὶ τρεῖς ὑποστάσεις τοῦ ἑνὸς θεοῦ φαμεν δικαίως. τὸ μὲν οὖν
τοιόσδε λεγόμενον διακρῖναι βούλεται τί τινος κοινωνοῦντος κατ' ἐκεῖνο τοὔνομα,
ᾧ τὸ τοιόσδε προστίθεται· οἷον ζῷον τοιόνδε φαμὲν τὸν ἄνθρωπον διακρῖναι
βουλόμενοι τοῦ ἵππου, φέρε εἰπεῖν, κοινωνοῦντος αὐτῷ κατὰ τοὔνομα τοῦ ζῴου,
διαφέροντος δὲ τῷ λογικῷ καὶ τῷ ἀλόγῳ. διακρίνεται δέ τί τινος ἢ οὐσίᾳ ἢ
ὑποστάσει ἢ οὐσίᾳ καὶ ὑποστάσει· καὶ οὐσίᾳ μὲν διακέκριται ὁ ἄνθρωπος τοῦ ἵππου,
ὑποστάσει δὲ Παῦλος Πέτρου, οὐσίᾳ δὲ καὶ ὑποστάσει ἥδε ἡ ὑπόστασις τοῦ ἀνθρώπου
τῆσδε τῆς ὑποστάσεως τοῦ ἵππου. ἀλλὰ φανερουμένου τοῦ λόγου τῶν κατ' οὐσίαν
ἁπλῶς καὶ τῶν καθ' ὑπόστασιν καὶ οὐ κατ' οὐσίαν διαφερόντων πρόδηλος ἔσται καὶ
ὁ περὶ τῶν κατ' οὐσίαν ἅμα καὶ ὑπόστασιν διακεκριμένων. περὶ τούτων οὖν ἡμῖν
ἐξεταζέσθω ὁ λόγος· ὅτι μὲν γὰρ καὶ τὰ διαφέροντα κατ' οὐσίαν λέγονται δύο ἢ τρεῖς
οὐσίαι καὶ τὰ διαφέροντα καθ' ὑπόστασιν ὡσαύτως λέγονται δύο καὶ τρεῖς
ὑποστάσεις, 3,1.30 ὁμολογοῦσι καὶ αὐτοὶ καὶ ἡμεῖς· διαφερόμεθα δέ, ὅτι ἐπὶ μὲν
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

4

Πέτρου καὶ Παύλου φασὶ δεῖν <δύο> λέγειν ἀνθρώπους, ἡμεῖς δὲ οὐ κυρίως μέντοι
καὶ κατὰ τὸν ἐπιστημονικὸν λόγον· οὐδὲ εἷς γὰρ νῦν ἡμῖν λόγος περὶ κοινῆς καὶ
καταχρηστικῆς χρήσεως· αὕτη γὰρ οὔτε πρὸς ἀναίρεσίν τινος οὔτε πρὸς σύ στασιν
ἰσχύει. σαφηνιζέσθω τοιγαροῦν πρότερον ἡμῖν, τὸν ἄνθρωπον καὶ τὸν ἵππον ἢ τὸν
ἵππον καὶ τὸν κύνα τίνος χάριν διαφέρειν λέγομεν καθὸ τοιάσδε οὐσίας. ἢ πρόδηλον
ὅτι ἐπειδὴ διαφέρουσιν ἀλλήλων κατὰ τὰ χαρακτηρίζειν οὐσίας εἰωθότα, οἷον
λογικῷ καὶ ἀλόγῳ, χρεμετιστικῷ, ὑλακτικῷ καὶ εἴ τινι τοιούτῳ. τῷ γὰρ λέγειν
οὐσίαν τοιάνδε οὐδὲν ἕτερον λέγομεν ἢ ὕπαρξιν ζωῆς μετέχουσαν πρὸς
ἀντιδιαστολὴν τῆς μὴ τοιαύτης, ὕπαρξιν λογικεύεσθαι πε φυκυῖαν πρὸς διάκρισιν
ἀλογίᾳ διαφερούσης, ὕπαρξιν τὸ χρεμετιστικὸν ἔχουσαν χαρακτηριστικόν, καὶ εἴ τι
τοιοῦτον. ἀντὶ γὰρ τοιούτων διαφορῶν καὶ ἰδίων προστίθεται τῇ οὐσίᾳ ἢ καὶ παντὶ
γένει πρὸς διάκρισιν τῶν ὑπ' αὐτὸ εἰδῶν τὸ τοιάδε καὶ τοιόνδε οἶον τοιάδε οὐσία
ἀντὶ τοῦ αἰσθητικὴ ἢ ἀναίσθητος, τοιόνδε ζῷον ἀντὶ τοῦ λογικὸν ἢ ἄλογον. καὶ
πάλιν λέγομεν· διαφέρει Παῦλος Πέτρου, καθὸ τοιάδε ὑπόστασις ἑκάστῳ αὐτῶν,
ἐπειδὴ διαφέρουσιν ἀλλήλων κατά τινα τῶν ὑπόστασιν καὶ οὐκ οὐσίαν συνιστᾶν
πεφυκότων, οἷον φαλακρότητι, μακρότητι, πατρότητι, υἱότητι καὶ εἴ τινι 3,1.31
τοιούτῳ· πρόδηλον γάρ, ὡς οὐ ταὐτὸν εἶδος καὶ ἄτομον, τουτέστιν οὐσία καὶ
ὑπόστασις· λέγων γάρ τις ἄτομον, τουτέστιν ὑπόστασιν, εὐθὺς τὴν διάνοιαν τοῦ
ἀκροωμένου παραπέμπει πρὸς τὸ ζητῆσαι οὖλον, γλαυκόφθαλμον, υἱόν, πατέρα καὶ
εἴ τι ὅμοιον· λέγων δὲ εἶδος, τουτέστιν οὐσίαν, πρὸς τὸ γνῶναι δηλονότι, ζῷον
λογικόν, θνητόν, νοῦ καὶ ἐπιστήμης δεκτικόν, ζῷον ἄλογον, θνητόν, χρεμετιστικὸν
καὶ τὰ τοιαῦτα. εἰ δὲ μὴ ταὐτὸν οὐσία καὶ ἄτομον, ὅπερ ἐστὶν ὑπόστασις, οὐδὲ ταὐτὰ
τὰ χαρακτηρίζοντα ταύτην τε κἀκείνην. εἰ δὲ καὶ ταῦτα οὐ ταὐτά, οὐδὲ τοῖς αὐτοῖς
ὀνόμασι δυνατὸν συνάπτεσθαι· ἀλλὰ τὰ μὲν τοῖς κατ' οὐσίας ἢ οὐσιῶν κειμένοις, τὰ
δὲ τοῖς κατὰ ἄτομον λεγομένοις. τρία τοίνυν ἐστὶν ὀνόματα, περὶ ὧν ἡ ζήτησις·
οὐσία, ἄτομον, ἄνθρωπος. καὶ τῇ μὲν οὐσίᾳ συνάπτομεν τὸ τοιάδε πρὸς διάκρισιν, ὡς
εἶπον, τῶν ὑπ' αὐτὴν εἰδῶν κατ' οὐσίαν ἀλλήλων διαφερόντων· τῇ δὲ ὑποστάσει
πάλιν ὁμοίως τὸ τοιάδε συζεύγνυμεν πρὸς διαίρεσιν προσώπων τῶν ἀλλήλοις
κοινωνούντων τούτου τοῦ ὀνόματος, τουτέστι τῆς ὑποστάσεως, καὶ διαφερόντων
ἀλλήλων οὐ τοῖς οὐσίαν χαρακτηρίζουσιν, ἀλλὰ τοῖς λεγομένοις συμβεβηκόσιν. τίνι
οὖν βούλονται τρόπῳ συνάψαι τῷ ἄνθρωπος ὀνόματι τὸ τοιόσδε; (ἐκ γὰρ τῶν
κοινῶς ὁμολογουμένων τὰ ἀμφιβαλλόμενα δέχεται τὴν λύσιν.) ὡς τῇ οὐσίᾳ; ἔσται
οὖν τὰ ὑπ' αὐτὸν οὐσιώδει διαφορᾷ διακεκριμένα ἀλλήλων; ὅπερ οὐκ ἔστιν· οὐδὲν
γὰρ διαφέρει κατ' οὐσίαν Παῦλος Πέτρου, καθ' ὧν κατηγορεῖται τὸ ἄνθρωπος. ἀλλ'
ὡς τῇ ὑποστάσει; προσώπου ἄρα δηλωτικὸν τὸ ἄν 3,1.32 θρωπος καὶ οὐκ οὐσίας,
ὅπερ ἄτοπον λέγειν· τὸ κοινὸν γὰρ τῆς οὐσίας σημαίνει τὸ ἄνθρωπος καὶ οὐκ ἰδικὸν
πρόσωπον, Παύλου, φέρε εἰπεῖν, ἢ Βαρνάβα. οὐκ ἄρα οὐδενὶ τρόπῳ τὸ τοιόσδε
συνάπτεται τῷ ἄνθρωπος κατά γε τὸν ἐπιστημονικὸν λόγον. εἰ δὲ ἡ κοινὴ χρῆσις
ἀπορεῖ τούτου καὶ κατακέχρηται τοῖς τῆς οὐσίας ὀνόμασι εἰς προσώπου δήλωσιν,
οὐδὲν πρὸς τὸν ἀκριβῆ κανόνα τῆς λογικῆς ἐπιστήμης. ἀλλὰ τί κατατρέχω τῆς
συνήθους καταχρήσεως ἀπορούσης εἰς τὰ τοιαῦτα λανθάνων, ὅτι καὶ οἱ
ἐπιστημονικοὶ τῶν λόγων λόγοις προσφόροις φράσαι τὸ νοηθὲν πολλάκις οὐκ
εὐποροῦντες ἑτέροις καὶ αὐτοὶ καταχρηστικοῖς χρῶνται ὀνόμασι πρὸς παράστασιν
τοῦ λεγομένου; πλὴν ἐκεῖνο σαφὲς ἡμῖν ἔστω, ὅτιπερ, εἰ ἐλέγομεν ἐπὶ Πέτρου καὶ
Παύλου ἄνθρωπος ἀνθρώπου, ᾗ ἄνθρωπος, οὐδὲν διαφέρει, ἀλλ' ᾗ τοιόσδε
ἄνθρωπος, καὶ οὐσία οὐσίας, ᾗ οὐσία, οὐδὲν διαφέρει, ἀλλ' ᾗ τοιάδε οὐσία, λέγειν ἐπ'
αὐτῶν ἠδυνάμεθα. εἰ δὲ τοῦτο λέγειν μὴ δυνατόν, ἐπεὶ μία καὶ ἡ αὐτὴ οὐσία Πέτρου
καὶ Παύλου, οὐδὲ ἄρα οὐδὲ ἐκεῖνο, ἐπείπερ οὐσίας δηλωτικὸν τὸ ἄνθρωπος ὄνομα· εἰ
δὲ τὸ τοιόσδε καὶ τοιόσδε οὐκ ἀκόλουθον συνάψαι τῷ ἄνθρωπος ὀνόματι, οὐδ' ἄρα
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

5

οὐδὲ δύο ἢ τρεῖς κυρίως λέγομεν ἀνθρώπους. καὶ εἰ ἐπὶ τοῦ ἀνθρώπου ταῦτα
δέδεικται, πόσῳ μᾶλλον ἐπὶ τῆς ἀϊδίου καὶ θείας οὐσίας κυριώτερον ἁρμόσει τὸ μὴ
τοιόνδε θεὸν καὶ τοιόνδε λέγεσθαι τῶν ὑποστάσεων ἑκάστην μηδὲ θεὸν καὶ θεὸν καὶ
θεὸν ἐκφωνεῖσθαι τὸν πατέρα καὶ τὸν υἱὸν καὶ τὸ ἅγιον πνεῦμα μηδὲ τρεῖς θεούς,
κἂν τῇ ἐνθυμήσει, δογματίζεσθαι. 3,1.33 συνέστηκεν ἄρα τῷ πρὸς ἡμῶν καὶ δικαίῳ
καὶ ἀκολούθῳ καὶ ἐπιστημονικωτάτῳ λόγῳ, ὡς ἕνα θεόν φαμεν τὸν τῶν ἁπάντων
δημιουργόν, εἰ καὶ ἐν τρισὶ προσώποις ἤγουν ὑποστάσεσι θεωρεῖται πατρὸς καὶ υἱοῦ
καὶ ἁγίου πνεύματος.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

6

