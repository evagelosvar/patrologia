De pythonissa ad Theodosium episcopum
ΓΡΗΓΟΡΙΟΥ ΕΠΙΣΚΟΠΟΥ ΝΥΣΣΗΣ ΕΠΙΣΤΟΛΗ ∆ΙΑ ΤΗΝ ΕΓΓΑΣΤΡΙΜΥΘΟΝ
ΠΡΟΣ ΘΕΟ∆ΟΣΙΟΝ ΕΠΙΣΚΟΠΟΝ
Ὁ εἰπὼν τοῖς ἑαυτοῦ μαθηταῖς Ζητεῖτε καὶ εὑρήσετε, δώσει πάντως καὶ τὴν
πρὸς τὸ εὑρεῖν δύναμιν τοῖς φιλομαθῶς κατὰ τὴν ἐντολὴν τοῦ κυρίου
διερευνωμένοις καὶ ἀναζητοῦσι τὰ κεκρυμμένα μυστήρια· ἀψευδὴς γὰρ ὁ
ἐπαγγειλάμενος, ὑπὲρ τὴν αἴτησιν ἐπιδαψιλευόμενος τῇ μεγαλοδωρεᾷ τῶν
χαρισμάτων. οὐκοῦν Πρόσεχε τῇ ἀναγνώσει, τέκνον Τιμόθεε· πρέπειν γὰρ οἶμαι τῇ
τοῦ μεγάλου Παύλου φωνῇ πρὸς τὴν σὴν ἀγαθότητα χρήσασθαι καὶ ∆ώῃ σοι κύριος
σύνεσιν ἐν πᾶσιν, ὥστε σε πλουτισθῆναι Ἐν παντὶ λόγῳ καὶ πάσῃ γνώσει. νῦν δὲ
περὶ ὧν ἐπέταξας· ὅσαπερ ἂν ὁ κύριος ὑποβάλῃ, δι' ὀλίγων ὑπηρετήσασθαί σου τῇ
προθυμίᾳ καλῶς ἔχειν ἐδοκίμασα, ὡς ἂν διὰ τούτων μάθοις ὅτι χρὴ δι' ἀγάπης
ἀλλήλοις δουλεύειν ἡμᾶς ἐν τῷ ποιεῖν τὰ ἀλλήλων θελήματα.
Πρῶτον τοίνυν ἐπειδὴ καὶ τῶν λοιπῶν ἦν κεφαλαίων προ τεταγμένον τὸ
περὶ τοῦ Σαμουὴλ ἐπιζητούμενον νόημα, διὰ 102 βραχέων ὡς ἔστι δυνατὸν θεοῦ
διδόντος ἐκθήσομαι. ἤρεσέ τισι τῶν πρὸ ἡμῶν ἀληθῆ νομίσαι τῆς γοητίδος ἐκείνης
τὴν ἐπὶ τοῦ Σαμουὴλ ψυχαγωγίαν καί τινα λόγον τοιοῦτον εἰς συν ηγορίαν τῆς
ὑπολήψεως αὐτῶν ταύτης παρέσχοντο, ὅτι λυπου μένου τοῦ Σαμουὴλ ἐπὶ τῇ
ἀποβολῇ τοῦ Σαοὺλ καὶ πάντοτε τοῦτο τῷ κυρίῳ προσφέροντος τὸ "ὅ τι θέλεις, τὴν
ἐκ τῶν ἐγγαστριμύθων γινομένην γοητείαν ἐπὶ ἀπάτῃ τῶν ἀνθρώ πων ἐξεκάθαρεν
ἐκ τοῦ λαοῦ Σαούλ," καὶ διὰ τοῦτο δυσανασχετοῦντος τοῦ προφήτου ἐπὶ τῷ μὴ
θελῆσαι τὸν κύριον διαλλαγῆναι τῷ ἀποβλήτῳ, συγχωρῆσαί φασι τὸν θεὸν
ἀναχθῆναι τὴν τοῦ προφήτου ψυχὴν διὰ τῆς τοιαύτης μαγγανείας, ἵνα ἴδῃ ὁ
Σαμουήλ, ὅτι ψευδῆ ὑπὲρ αὐτοῦ τῷ θεῷ προετείνετο, λέγων αὐτὸν πολέμιον εἶναι
ταῖς ἐγγαστριμύθοις τὸν τηνικαῦτα διὰ τῆς αὐτῶν γοητείας τὴν ἄνοδον τῆς ψυχῆς
αὐτοῦ μαντεύσαντα. ἐγὼ δὲ πρὸς τὸ εὐαγ γελικὸν χάσμα βλέπων, ὃ διὰ μέσου τῶν
κακῶν τε καὶ ἀγαθῶν ἐστηρίχθαι φησὶν ὁ πατριάρχης, μᾶλλον δὲ ὁ τοῦ πατριάρχου
κύριος, ὡς ἀμήχανον εἶναι τοῖς τε κατακρίτοις ἐπὶ 103 τὴν τῶν δικαίων ἄνεσιν
διαβῆναι καὶ τοῖς ἁγίοις πρὸς τὸν τῶν πονηρῶν χορὸν διαπερᾶσαι, οὐ δέχομαι
ἀληθεῖς εἶναι τὰς τοιαύτας ὑπολήψεις, διδαχθεὶς μόνον ἀληθὲς εἶναι πιστεύειν τὸ
εὐαγγέλιον. ἐπειδὴ τοίνυν μέγας ἐν ἁγίοις ὁ Σαμουήλ, πονηρὸν δὲ κτῆμα ἡ γοητεία,
οὐ πείθομαι, ὅτι ἐν τοσούτῳ τῆς ἰδίας ἀναπαύσεως ὁ Σαμουὴλ καταστὰς τὸ
ἀδιόδευτον ἐκεῖνο χάσμα πρὸς τοὺς ἀσεβοῦντας διήρχετο οὔτε ἑκὼν οὔτε ἄκων.
ἄκων μὲν οὖν οὐκ ἂν ἔτι ὑπέμεινε τῷ μὴ δύνασθαι τὸ δαιμόνιον διαβῆναι τὸ χάσμα
καὶ ἐν τῷ χορῷ τῶν ὁσίων γενόμενον μετακινῆσαι τὸν ἅγιον, ἑκὼν δὲ οὐκ ἂν τοῦτο
ἐποίησε τῷ μήτε βούλεσθαι ἐπιμιχθῆναι τοῖς κακοῖς μήτε δύνασθαι· τῷ γὰρ ἐν
ἀγαθοῖς ὄντι ἀβούλητον ἀπὸ τῶν ἐν οἷς ἐστιν ἡ πρὸς τὰ ἐναντία μετάστασις· εἰ δέ τις
καὶ βούλεσθαι δοίη, ἀλλ' ἡ τοῦ χάσματος φύσις οὐκ ἐπιτρέπει τὴν πάροδον. Τί οὖν
ἐστίν, ὃ περὶ τούτων ἡμεῖς λογιζόμεθα; ἐχθρὸς τῆς ἀνθρωπίνης φύσεώς ἐστιν ὁ
κοινὸς πάντων πολέμιος, ᾧ πᾶσά ἐστιν ἐπίνοια καὶ σπουδὴ εἰς αὐτὰ τὰ καίρια
βλάπτειν τὸν ἄνθρωπον. τίς δὲ τοιαύτη καιρία κατὰ τῶν ἀνθρώπων ἄλλη πληγὴ ὡς
τὸ ἀποβληθῆναι ἀπὸ τοῦ θεοῦ τοῦ ζωοποιοῦντος καὶ πρὸς τὴν ἀπώλειαν τοῦ
θανάτου ἑκουσίως αὐτομολῆσαι; ἐπειδὴ τοίνυν σπουδή τίς ἐστι κατὰ τὸν βίον τοῖς
φιλο σωμάτοις γνῶσίν τινα τῶν μελλόντων ἔχειν, δι' ἧς ἐλπί ζουσιν ἢ τῶν κακῶν
ἀποφυγὴν ἢ πρὸς τὰ καταθύμια χειραγωγίαν, τούτου χάριν, ὡς ἂν μὴ πρὸς τὸν θεὸν
οἱ ἄνθρω ποι βλέποιεν, πολλοὺς τρόπους τῆς τοῦ μέλλοντος γνώσεως ἡ ἀπατηλὴ
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

τῶν δαιμόνων ἐτεχνάσατο φύσις, οἰωνοσκοπίας, συμβολομαντείας, χρηστήρια,
ἡπατοσκοπίας, νεκυίας, ἐν θουσιασμούς, κατοκωχάς, ἐμπνεύσεις καὶ ἄλλα τοιαῦτα
104 πολλά. καὶ ὅπερ ἄν τις εἶδος προγνώσεως ἔκ τινος ἀπάτης ἀληθὲς εἶναι νομίσῃ,
ἐν ἐκείνῳ τὸ ἀπατηλὸν δαιμόνιον παραφαίνεται εἰς δικαίωσιν τῆς ἠπατημένης
ὑπολήψεως τοῦ πεπλανημένου. οὕτως καὶ τὴν τοῦ ἀετοῦ πτῆσιν πρὸς τὴν τοῦ
παρατετηρηκότος ἐλπίδα συνδραμεῖν παρασκευάζει ὁ δαίμων καὶ τοῦ ἥπατος τὸν
παλμὸν καὶ τὴν ἐκ τοῦ φυσώδους γινομένην τῶν μηνίγγων παραφορὰν καὶ τῶν
ὀμμάτων παραστροφήν· καὶ ἕκαστον κατὰ τὴν σημειωθεῖσαν ἐκ τῆς ἀπάτης
παρατήρησιν ἡ τοῦ δαίμονος πανουργία προδείκνυσι τοῖς ἀπατωμένοις, ὥστε
ἀποστάντας τοὺς ἀνθρώπους τοῦ θεοῦ προσέχειν τῇ θεραπείᾳ τῶν δαιμόνων, δι' ὧν
ἐνεργεῖσθαι τὰ τοιαῦτα πιστεύουσιν.
Ἓν τοίνυν ἀπάτης εἶδος ἦν καὶ τὸ τῶν ἐγγαστριμύθων, ὧν ἡ μαγγανεία
ἐπιστεύετο δύνασθαι τὰς τῶν κατοιχομένων ψυχὰς πάλιν πρὸς τὸν ἄνω βίον
ἐφέλκεσθαι. τοῦ τοίνυν Σαοὺλ ἐν ἀπογνώσει τῆς σωτηρίας γεγονότος διὰ τὸ
πανστρατιᾷ συγκεκινῆσθαι κατ' αὐτοῦ πᾶν τὸ ἀλλόφυλον καὶ ἐπὶ ταύτην ἐλθόντος
τὴν ἐπίνοιαν, ὥστε τὸν Σαμουὴλ αὐτῷ τρόπον ὑποθέσθαι τινὰ σωτηρίας, τὸ
παραμένον τῇ ἐγγαστριμύθῳ δαιμόνιον, δι' οὖ ἠπατᾶτο ἐκ συνηθείας τὸ γύναιον, εἰς
διαφόρους μορφὰς σκιοειδῶς ἐν τοῖς ὀφθαλμοῖς τοῦ γυναίου ἐσχηματίζετο, οὐδενὸς
τῷ Σαοὺλ προφαινομένου ὧν καθεώρα τὸ γύναιον. ὡς γὰρ ἥψατο τῆς γοητείας καὶ
ἤδη τῷ γυναίῳ ἦν ἐν ὀφθαλμοῖς τὰ φαντάσματα, πίστιν τοῦ ἀληθῆ τὰ φαινόμενα
εἶναι ταύτην ὁ δαίμων ἐμηχανήσατο τὸ πρῶτον αὐτὸν 105 κεκρυμμένον ἐν τῷ εἴδει
τοῦ προσχήματος διασαφῆσαι, δι' οὖ μᾶλλον ὁ Σαοὺλ κατεπλάγη, ὡς οὐδὲν ἔτι τῆς
γυναικὸς σφαλησομένης τῷ τὸ ἰδιωτικὸν σχῆμα τὴν γοητικὴν δύναμιν μὴ ἀγνοῆσαι.
εἰπούσης τοίνυν αὐτῆς Θεοὺς ἑωρακέναι ἀναβαίνοντας καὶ ἄνθρωπον ὄρθιον ἐν
διπλοΐδι, πῶς στήσουσι τὸ καθ' ἱστορίαν οἱ δοῦλοι τοῦ γράμματος; εἰ γὰρ ἀληθῶς ὁ
Σαμουήλ ἐστιν ὁ ὀφθείς, οὐκοῦν κατ' ἀλήθειαν καὶ θεοί εἰσιν οἱ ὀφθέντες παρὰ τῆς
φαρμακίδος· θεοὺς δὲ ἡ γραφὴ τὰ δαιμόνια λέγει· Πάντες γὰρ οἱ θεοὶ τῶν ἐθνῶν
δαιμόνια. ἆρ' οὖν μετὰ τῶν δαιμόνων καὶ ἡ ψυχὴ τοῦ Σαμουήλ; μὴ γένοιτο. ἀλλὰ τὸ
πάντοτε ὑπακοῦον τῇ φαρμακίδι δαιμόνιον παρέλαβε καὶ ἄλλα πνεύματα πρὸς
ἀπάτην αὐτῆς τε τῆς γυναικὸς καὶ τοῦ δι' ἐκείνης ἀπατωμένου Σαοὺλ καὶ τὰ μὲν
δαιμόνια θεοὺς νομίζεσθαι παρὰ τῆς ἐγγαστριμύθου ἐποίησεν, αὐτὸ δὲ πρὸς τὸ
ἐπιζητούμενον εἶδος ἐσχηματίσθη καὶ τὰς ἐκείνου φωνὰς ὑπεκρίνατο καί, ὅπερ εἰκὸς
ἦν ἐκ τῶν φαινο μένων λογίσασθαι, τὴν ἐκ τοῦ ἀκολούθου ἐκβήσεσθαι προσ
δοκηθεῖσαν ἀπόφασιν ὡς ἐν εἴδει προφητείας ἐφθέγξατο. ἤλεγ ξε δὲ καὶ ὡς οὐκ
ἠβούλετο ἑαυτὸν ὁ δαίμων εἰπὼν τὴν ἀλήθειαν ὅτι Αὔριον σὺ καὶ Ἰωνάθαν μετ'
ἐμοῦ· εἰ γὰρ ἀληθῶς ἦν Σαμουήλ, πῶς ἐνεδέχετο τὸν ἐν πάσῃ κακίᾳ κατεγνωσμένον
μετ' ἐκείνου γενέσθαι; ἀλλὰ δῆλον, ὅτι ἀντὶ τοῦ Σαμουὴλ ὀφθὲν τὸ πονηρὸν ἐκεῖνο
δαιμόνιον μεθ' ἑαυτοῦ ἔσεσθαι τὸν Σαοὺλ εἰπὸν οὐκ ἐψεύσατο. εἰ δὲ λέγει ἡ γραφὴ
106 ὅτι Καὶ εἶπεν ὁ Σαμουήλ, μὴ ταρασσέτω τὸν ἐπιστήμονα ὁ τοιοῦτος λόγος, ἀλλὰ
προσκεῖσθαι νομιζέτω, ὅτι ὁ νομισθεὶς εἶναι Σαμουήλ. εὑρίσκομεν γὰρ τὴν γραφικὴν
συνήθειαν πολλαχοῦ τὸ δοκοῦν ἀντὶ τοῦ ὄντος διεξιοῦσαν, ὡς ἐπὶ τοῦ Βαλαάμ, νῦν
μὲν λέγοντος αὐτοῦ ὅτι Ἀκούσομαι τί λαλήσει ἐν ἐμοὶ ὁ θεός, μετὰ ταῦτα δὲ ὅτι
γνοὺς [δὲ] ὁ Βαλαάμ, ὅτι ἀρεστὸν ἦν τῷ θεῷ μὴ καταρᾶσθαι τοὺς Ἰσραηλίτας, οὐκέτι
Κατὰ τὸ εἰωθὸς ἀπῆλθεν εἰς συνάντησιν τοῖς οἰωνοῖς· ὁ γὰρ ἀνεπίσκεπτος κἀκεῖ τὸν
ἀληθινὸν θεὸν νομίσει διαλέγεσθαι τῷ Βαλαάμ, ἡ μέντοι ἐπαγωγὴ δείκνυσιν, ὅτι τὸν
ὑπὸ τοῦ Βαλαὰμ νομιζόμενον θεὸν οὕτως ὠνόμασεν ἡ γραφή, οὐχὶ τὸν ὄντως ὄντα
θεόν. οὐκοῦν καὶ ἐνταῦθα ὁ δόξας εἶναι Σαμουὴλ τοὺς τοῦ ἀληθινοῦ Σαμουὴλ
ὑπεκρίνατο λόγους, εὐφυῶς τοῦ δαίμονος ἐκ τῶν εἰκότων μιμουμένου τὴν
προφητείαν. Τὸ δὲ κατὰ τὸν Ἠλίαν πλείονος μὲν χρῄζει τῆς θεωρίας, οὐ μὴν ἐν τῷ
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

προτεθέντι ζητήματι. καὶ γὰρ Ἐκ τοῦ χειμάρρου ὕδωρ πίνειν προσταχθεὶς κατὰ τὸ
λεληθὸς παρὰ τοῦ θεοῦ συνεβουλεύετο, ὥστε τὴν παρὰ τοῦ προφήτου
ἐκφωνηθεῖσαν κατὰ τῶν Ἰσραηλιτῶν τῆς ἀνομβρίας ἀπόφασιν δι' αὐτοῦ πάλιν
ἀναλυθῆναι· ᾧ γὰρ ἐδόθη ἐκ μόνου τοῦ χειμάρρου πίνειν, τοῦ δὴ κατὰ τὸ εἰκὸς ἐν
τοῖς αὐχμοῖς ἀπο ξηραινομένου, ἄλλης τῷ προφήτῃ μὴ εὑρισκομένης πρὸς τὴν
δίψαν παραμυθίας, διὰ τὸ ἀπειρῆσθαι αὐτῷ ἀλλαχόθεν πιεῖν ἐπάναγκες ἦν αἰτῆσαι
τὸν ὄμβρον, ἵνα μὴ ἐπιλείπῃ τὸ ὕδωρ τὸν χείμαρρον. γίνεται δὲ τῷ προφήτῃ παρὰ
τῶν κοράκων ἡ τῶν πρὸς τὴν ζωὴν ἀναγκαίων διακονία, δεικνύν τος διὰ τούτων
τοῦ θεοῦ τῷ προφήτῃ, ὅτι πολλοί εἰσιν οἱ 107 παραμεμενηκότες τῇ τοῦ ἀληθινοῦ
θεοῦ λατρείᾳ, ὅθεν ἐχο ρηγεῖτο διὰ τῶν κοράκων ἡ τροφὴ τῷ προφήτῃ· οὐ γὰρ ἂν
τῶν μεμιασμένων αὐτῷ προσῆγεν ἄρτων ἢ τὸ εἰδωλόθυτον κρέας, ὥστε καὶ διὰ
τούτων ἐναχθῆναι τὸν Ἠλίαν καθυφεῖναί τι τοῦ κατὰ τῶν ἠσεβηκότων θυμοῦ,
μαθόντα διὰ τῶν γινομένων, ὅτι πολλοί εἰσιν οἱ πρὸς τὸν θεὸν ὁρῶντες, οὓς οὐκ
ἔστι δίκαιον συγκαταδικασθῆναι τοῖς ὑπαιτίοις. εἰ δὲ πρωῒ μὲν ὁ ἄρτος, ἐν δὲ τῇ
ἑσπέρᾳ τὸ κρέας αὐτῷ διακονεῖται, ἴσως δι' αἰνίγματος πρὸς τὸν ἐνάρετον βίον
σπουδὴν τὸ γινόμενον ἔχει, ὅτι χρὴ ἀρχομένοις μὲν τῆς κατ' ἀρετὴν ζωῆς, ἧς
σύμβολόν ἐστιν ἡ πρωΐα, τὸν εὔληπτον παρέχειν λόγον, τοῖς δὲ τελειουμένοις τὸν
τελειότερον κατὰ τὴν τοῦ Παύλου φωνήν, ὅς φησιν ὅτι Τελείων δέ ἐστιν ἡ στερεὰ
τροφή, τῶν διὰ τὴν ἕξιν τὰ αἰσθητήρια γεγυμνασμένα ἐχόντων.
Τὸ δὲ κάλυμμα τοῦ Μωυσέως οὐκ ἀγνοήσεις, πρὸς ὅ τι βλέπει, τῇ πρὸς
Κορινθίους ἐπιστολῇ τοῦ Παύλου καθομιλήσας. Ὅσα δὲ περὶ τῶν θυσιῶν
ἐπεζήτησας, καλῶς ποιήσεις ὅλον τὸ Λευϊτικὸν φιλοπονώτερον ἐξετάσας καὶ μετὰ
πλείονος τῆς προσεδρίας καθολικῶς τὸν ἐν τούτοις θεωρήσεις νόμον· οὕτω γὰρ
συγκατανοηθήσεται τῷ ὅλῳ τὸ μέρος· ἐφ' ἑαυτοῦ γὰρ τοῦτο μόνον οὐκ ἂν
διευκρινηθείη πρὸ τῆς τοῦ παντὸς θεωρίας. Ἐν δὲ τοῖς περὶ τῆς ἀντικειμένης
δυνάμεως ἐπηπορημένοις πρόδηλός ἐστιν ἡ λύσις, ὅτι οὐχ ἁπλῶς ἄγγελος, ἀλλ' ἐν
ἀρχαγγέλοις ἦν τεταγμένος ὁ ἀποστάτης γενόμενος. δῆλον οὖν, 108 ὅτι τῇ ἀρχῇ
συνενδείκνυται καὶ τὸ ὑποχείριον τάγμα, ὥστε λελύσθαι τὴν ζήτησιν περὶ τοῦ πῶς
εἷς ἦν καὶ μετὰ πλήθους ἐστίν· τῆς γὰρ ὑποχειρίου στρατιᾶς αὐτῷ συναποστάσης
σεσαφήνισται τὸ ζητούμενον. Τὸ δὲ τελευταῖον τῶν ἐπιζητηθέντων κεφάλαιον
(λέγω δὴ τὸ πῶς παραγίνεται τὸ πνεῦμα πρὸ τοῦ βαπτίσματος) πλατυτέρας
ἐξετάσεώς τε καὶ θεωρίας δεόμενον ἰδίῳ περιγράψαντες λόγῳ, θεοῦ διδόντος,
ἀποστελοῦμέν σου τῇ τιμιότητι.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

3

