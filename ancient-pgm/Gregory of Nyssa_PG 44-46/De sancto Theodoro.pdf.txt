De sancto Theodoro
ΤΟΥ ΑΥΤΟΥ ΕΓΚΩΜΙΟΝ ΕΙΣ ΤΟΝ ΜΕΓΑΝ ΜΑΡΤΥΡΑ ΘΕΟ∆ΩΡΟΝ.
Ὑμεῖς ὁ τοῦ Χριστοῦ λαὸς, ἡ ἁγία ποίμνη, τὸ βασίλειον ἱεράτευμα, οἱ
πανταχόθεν ἀστικοί τε καὶ χωριτικοὶ συῤῥεύσαντες δῆμοι, πόθεν λαβόντες τὸ
σύνθημα τῆς ὁδοῦ, πρὸς τὸν ἱερὸν τοῦτον ἐδημαγωγή θητε τόπον; Τίς ὑμῖν τῆς
ἐνθάδε σπουδαίαν οὕτω καὶ ἐμπρόθεσμον ἀνάγκην ἐπέθηκεν; καὶ ταῦτα ὥρᾳ
χειμῶνος, ἡνίκα καὶ πόλεμος ἠρεμεῖ, καὶ στρατιώ της τὴν πανοπλίαν ἀποσκευάζεται,
καὶ πλωτὴρ ὑπὲρ καπνοῦ τίθησι τὸ πηδάλιον, καὶ γεωγρὸς ἡσυχάζει τοὺς ἀροτῆρας
βοῦς θεραπεύων ἐπὶ τῆς φάτνης; Ἢ πρόδηλον, ὡς ἐσάλπισε μὲν ἐκ τῶν
στρατιωτικῶν καταλόγων ὁ ἅγιος μάρτυς· κινήσας δὲ πολλοὺς ἐκ διαφόρων
πατρίδων πρὸς τὴν οἰκείαν ἀνάπαυσιν καὶ 46.737 ἑστίαν ἐκάλεσεν, οὐκ εἰς
πολεμικὴν εὐτρεπίζων πα ρασκευὴν, ἀλλὰ πρὸς τὴν γλυκεῖαν καὶ μάλιστα δὴ
Χριστιανοῖς πρέπουσαν συνάγων εἰρήνην;
Οὗτος γὰρ, ὡς πιστεύομεν, καὶ τοῦ παρελθόντος ἐνιαυτοῦ τὴν βαρβαρικὴν
ζάλην ἐκοίμισε, καὶ τὸν φρικώδη τῶν ἀγρίων Σκυθῶν ἔστησε πόλεμον, δεινὸν
αὐτοῖς ἐπισεί σας καὶ φοβερὸν ἤδη βλεπομένοις καὶ πλησιάσασιν, οὐ κράνος
τρίλοφον, οὐδὲ ξίφος εὖ τεθηγμένον, καὶ πρὸς τὸν ἥλιον ἀποστίλβον, ἀλλὰ τὸν
ἀλεξίκακον καὶ παντοδύναμον σταυρὸν τοῦ Χριστοῦ, ὑπὲρ οὗ καὶ αὐ τὸς παθὼν, τὴν
δόξαν ταύτην ἐκτήσατο. Καί μοι λοιπὸν τὸν νοῦν ἐπιστήσαντες διασκέψασθε, οἱ τῆς
κα θαρᾶς ταύτης θρησκείας ὑπηρέται καὶ φιλομάρτυρες, ἡλίκον χρῆμα δίκαιος, καὶ
ὅσων ἀξιοῦται τῶν ἀμοι βῶν (τῶν ἐγκοσμίων τε, λέγω, καὶ τῶν παρ' ἡμῖν· τῶν γὰρ
ἀοράτων οὐδεὶς ἱκανὸς λογίσασθαι τὴν με γαλοπρέπειαν)· καὶ διορίσαντες τὸν τῆς
εὐσεβείας καρπὸν, ζηλώσατε τῶν οὕτω προτιμωμένων τὴν γνώ μην. Ἐπιθυμήσατε
δὲ τῶν γερῶν ἃ Χριστὸς πρὸς ἀξίαν διανέμει τοῖς ἀθληταῖς. Καὶ τέως, εἰ δοκεῖ, τῆς
ἀπολαύσεως τῶν μελλόντων ἀγαθῶν σχολαζούσης, ἣν ἐλπὶς ἀγαθὴ ταμιεύεται τοῖς
δικαίοις, ἡνίκα ἂν ὁ τῶν ἡμετέρων βίων δικαστὴς ἐπιφοιτήσῃ, τὴν παρ οῦσαν
ἴδωμεν τῶν ἁγίων κατάστασιν, ὅπως καλλίστη ἐστὶ καὶ μεγαλοπρεπής. Ψυχὴ μὲν
γὰρ ἀνελθοῦσα περὶ τὸν ἴδιον κλῆρον ἐμφιλοχωρεῖ, καὶ ἀσωμάτως τοῖς ὁμοίοις
συνδιαιτᾶται· σῶμα δὲ τὸ σεμνὸν καὶ ἀκηλίδωτον ἐκείνης ὄργανον, οὐδαμοῦ τοῖς
ἰδίοις πά θεσι βλάψαν τῆς ἐνοικησάσης τὴν ἀφθαρσίαν, μετὰ πολλῆς τιμῆς καὶ
θεραπείας περισταλὲν, σεμνῶς ἐν ἱερῷ τόπῳ κατάκειται, ὥσπερ τι κειμήλιον πολυτί
μητον τῷ καιρῷ τῆς παλιγγενεσίας τηρούμενον, πολὺ τὸ ἀσύγκριτον ἔχον πρὸς τὰ
ἄλλα τῶν σωμάτων, ἃ κοινῷ καὶ τῷ τυχόντι διελύθη θανάτῳ, καὶ ταῦτα ἐν ὁμοίᾳ
ὕλῃ τῆς φύσεως. Τὰ μὲν γὰρ ἄλλα τῶν λειψά νων, καὶ βδελυκτὰ τοῖς πολλοῖς ἐστι,
καὶ οὐδεὶς ἡδέως παρέρχεται τάφον, ἢ καὶ ἀνεῳγότι τυχὼν ἐκ τοῦ πα ραδόξου,
ἐπιβαλὼν δὲ τὴν ὄψιν τῇ ἀμορφίᾳ τῶν ἐγ κειμένων, πάσης ἀηδίας πληρωθεὶς, καὶ
βαρέα κατα στενάξας τῆς ἀνθρωπότητος παρατρέχει. Ἐλθὼν δὲ εἴς τι χωρίον ὅμοιον
τούτῳ, ἔνθα σήμερον ὁ ἡμέτερος σύλλογος, ὅπου μνήμη δικαίου καὶ ἅγιον
λείψανον· πρῶτον μὲν τῇ μεγαλοπρεπείᾳ τῶν ὁρωμένων ψυχ αγωγεῖται, οἶκον
βλέπων ὡς Θεοῦ ναὸν, ἐξησκημένον λαμπρῶς τῷ μεγέθει τῆς οἰκοδομῆς, καὶ τῷ τῆς
ἐπικοσμήσεως κάλλει, ἔνθα καὶ τέκτων εἰς ζώων φαντασίαν τὸ ξύλον ἐμόρφωσε, καὶ
λιθοξόος εἰς ἀργύρου λειότητα τὰς πλάκας ἀπέξεσεν. Ἐπέχρωσε δὲ καὶ ζωγράφος τὰ
ἄνθη τῆς τέχνης ἐν εἰκόνι διαγρα ψάμενος, τὰς ἀριστείας τοῦ μάρτυρος, τὰς ἐνστά
σεις, τὰς ἀλγηδόνας, τὰς θηριώδεις τῶν τυράννων μορφὰς, τὰς ἐπηρείας, τὴν
φλογοτρόφον ἐκείνην κά μινον, τὴν μακαριωτάτην τελείωσιν τοῦ ἀθλητοῦ, τοῦ
ἀγωνοθέτου Χριστοῦ τῆς ἀνθρωπίνης μορφῆς τὸ ἐκτύπωμα, πάντα ἡμῶν ὡς ἐν
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

βιβλίῳ τινὶ γλωττο φόρῳ διὰ χρωμάτων τεχνουργησάμενος, σαφῶς δι ηγόρευσε
τοὺς ἀγῶνας τοῦ μάρτυρος, καὶ ὡς λειμῶνα λαμπρὸν τὸν νεὼν κατηγλάϊσεν· οἶδε
γὰρ καὶ γραφὴ σιωπῶσα ἐν τοίχῳ λαλεῖν, καὶ τὰ μέγιστα ὠφελεῖν· 46.740 καὶ ὁ τῶν
ψηφίδων συνθέτης, ἱστορίας ἄξιον ἐποίησε τὸ πατούμενον ἔδαφος. Καὶ τοῖς
αἰσθητοῖς οὕτω φιλοτεχνήμασιν ἐνευπα θήσας τὴν ὄψιν, ἐπιθυμεῖ λοιπὸν καὶ αὐτῇ
πλησιάσαι τῇ θήκῃ· ἁγιασμὸν καὶ εὐλογίαν τὴν ἐπαφὴν εἶναι πιστεύων. Εἰ δὲ καὶ
κόνιν τις δοίη φέρειν τὴν ἐπι κειμένην τῇ ἐπιφανείᾳ τῆς ἀναπαύσεως, δῶρον ὁ χοῦς
λαμβάνεται, καὶ ὡς κειμήλιον ἡ γῆ θησαυρίζεται.
Τὸ γὰρ αὐτοῦ τοῦ λειψάνου προσάψασθαι, εἴ ποτέ τις ἐπιτυχία τοιαύτη
παράσχοι τὴν ἐξουσίαν, ὅπως ἐστὶ πολυπόθητον, καὶ εὐχῆς τῆς ἀνωτάτω τὸ δῶρον,
ἴσασιν οἱ πεπειραμένοι, καὶ τῆς τοιαύτης ἐπιθυμίας ἐμφορηθέντες. Ὡς σῶμα γὰρ
αὐτὸ ζῶν καὶ ἀνθοῦν οἱ βλέποντες κατασπάζονται, τοῖς ὀφ θαλμοῖς, τῷ στόματι, ταῖς
ἀκοαῖς, πάσαις προσ άγοντες ταῖς αἰσθήσεσιν, εἶτα τὸ τῆς εὐλαβείας καὶ τὸ τοῦ
πάθους ἐπιχέοντες δάκρυον, ὡς ὁλοκλήρῳ καὶ φαινομένῳ τῷ μάρτυρι τὴν τοῦ
πρεσβεύειν ἱκεσίαν προσάγουσιν, ὡς δορυφόρον τοῦ Θεοῦ παρακαλοῦντες, ὡς
λαμβάνοντα τὰς δωρεὰς ὅταν ἐθέλῃ ἐπικαλούμε νοι. Ἐκ τούτων πάντων, ὁ εὐσεβὴς
λαὸς, καταμάθετε, ὅτι Τίμιος ἐναντίον Κυρίου ὁ θάνατος τῶν ὁσίων αὐτοῦ. Ἓν μὲν
γὰρ καὶ τὸ αὐτὸ σῶμα πάντων ἀνθρώ πων, ἐξ ἑνὸς φυράματος ἔχον τὴν σύστασιν·
ἀλλὰ τὸ μὲν ἁπλῶς ἀποθανὸν, ῥίπτεται ὡς τὸ τυχόν· τὸ δὲ τῷ πάθει τοῦ μαρτυρίου
χαριτωθὲν, οὕτως ἐστὶν ἐράσμιον καὶ ἀμφισβητήσιμον, ὡς ὁ προλαβὼν λόγος
ἐδίδαξεν. ∆ιὰ τοῦτο πιστεύσωμεν ἐκ τῶν φαινομένων τοῖς ἀοράτοις, ἀπὸ τῆς ἐν τῷ
κόσμῳ πείρας τῇ τῶν μελλόντων ἐπαγγελίᾳ. Πολλοὶ γὰρ οἱ τὴν γαστέρα, καὶ τὴν
κενοδοξίαν, καὶ τὸν ἐνθάδε συρφετὸν πάντων ἐπίπροσθεν ἄγοντες, οὐδὲν ἡγοῦνται
τὸ μέλλον· τῷ τέλει δὲ τοῦ βίου συμπεραιοῦσθαι τὰ πάντα νομίζου σιν. Ἀλλ' ὁ
φρονῶν οὕτως, ἐκ τῶν μικρῶν τὰ με γάλα κατάμαθε, ἐκ τῶν σκιῶν τὰ ἀρχέτυπα
νόησον. Τίς τῶν βασιλέων τοιαύτην τιμᾶται τιμήν; τίς τῶν καθ' ὑπερβολὴν ἐν
ἀνθρώποις φανέντων τοιαύτῃ μνήμῃ δοξάζεται; τίς τῶν στρατηγῶν τῶν πόλεις
τειχήρεις ἑλόντων, καὶ ἔθνη δουλωσαμένων μυρία, οὕτως ἐστὶν ἀοίδιμος, ὡς ὁ
στρατιώτης οὗτος, ὁ πέ νης, ὁ νεόλεκτος, ὃν Παῦλος ὥπλισεν, ὃν ἄγγελοι πρὸς τὸν
ἀγῶνα ἤλειψαν, καὶ νικήσαντα Χριστὸς ἐστεφάνωσεν; Μᾶλλον δὲ ἐπειδὴ ἐγγὺς
ἐγενόμην τῷ λόγῳ τῶν ἀγώνων τοῦ μάρτυρος, φέρε, τὰ κοινὰ κα ταλιπόντες, τὸν
ἐξαίρετον τοῦ ἁγίου λόγον ποιήσωμεν· φίλον γὰρ ἑκάστῳ τὸ ἴδιον. Πατρὶς τοίνυν τῷ
γενναίῳ, ἡ πρὸς ἥλιον ἀνίσχουσα χώρα· εὐγενὴς γὰρ καὶ οὗτος κατὰ τὸν Ἰὼβ τῶν
ἀφ' ἡλίου ἀνατολῶν, καὶ τῆς πατρίδος ἐκείνῳ κοινωνῶν, οὐκ ἀπελείφθη τῆς τοῦ
ἤθους μιμήσεως. Νῦν δὲ πάσῃ τῇ οἰκουμένῃ ἐστὶ μάρτυς κοινὸς τῆς ὑφ' ἡλίῳ
πολίτης· ληφθεὶς δὲ ἐκεῖθεν πρὸς ὁπλιτικοὺς κατα 46.741 λόγους, οὕτω μετὰ τοῦ
ἰδίου τάγματος πρὸς τὴν ἡμετέραν διέβη χώραν, τῆς χειμερινῆς ἀναπαύσεως τοῖς
στρατιώταις ἐνθάδε παρὰ τῶν κρατούντων δια ταχθείσης. Πολέμου δὲ κινηθέντος
ἀθρόον, οὐκ ἐξ ἐφόδου βαρβαρικῆς, ἀλλ' ἐκ νόμου Σατανικοῦ, καὶ δόγματος
θεομάχου (πᾶς γὰρ Χριστιανὸς ἠλαύνετο τῷ δυσσεβεῖ γράμματι, καὶ πρὸς θάνατον
ἤγετο)· τότε δὴ, τότε ὁ τρισμακάριος οὗτος, γνώριμος ὢν ἐπ' εὐ σεβεία, καὶ τὴν
πίστιν τὴν εἰς Χριστὸν πανταχοῦ περιφέρων, μόνον οὐκ ἐπὶ τοῦ μετώπου τὴν ὁμολο
γίαν γραψάμενος, οὐκέτι νεόλεκτος ἦν τὴν ἀνδρείαν, οὐδὲ ἄπειρος πολέμου καὶ
μάχης· ἀλλὰ γενναῖος τὴν ψυχὴν, ἰσχυρὰν ποιησάμενος πρὸς τοὺς κινδύνους τὴν
ἔνστασιν, οὐχ ὑφειμένος, οὐ δειλιῶν, οὐ λόγον ἀπαῤῥησίαστον προβαλλόμενος. Ὡς
γὰρ ἐκάθισε τὸ πονηρὸν ἐκεῖνο δικαστήριον, ὅ τε ἡγεμὼν καὶ ὁ τα ξίαρχος εἰς ταὐτὸ
συνελθόντες, ὡς Ἡρώδης ποτὲ καὶ Πιλάτος, τὸν δοῦλον τοῦ σταυρωθέντος, εἰς
κρίσιν ὁμοίαν τοῦ ∆εσπότου κατέστησαν. «Καὶ λέγε,» φησὶ, «πόθεν σοι θρασύτητος
καὶ τόλμης ἐγγινομένης, εἰς τὸν βασιλικὸν ἐξυβρίζεις νόμον, οὐχ ὑποκύπτεις δὲ
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

τρέμων τοῖς τοῦ ∆εσπότου προστάγμασιν, οὐδὲ προσ κυνεῖς κατὰ τὸ δοκοῦν τοῖς
κρατοῦσιν;» Οἱ γὰρ ἀμφὶ Μαξιμιανὸν τότε τῆς βασιλείας ἡγοῦντο. Ὃς στεῤῥῷ τῷ
προσώπῳ καὶ ἀκαταπλήκτῳ τῇ γνώμῃ, εὔστοχον τὴν ἀπόκρισιν τοῖς λεχθεῖσιν
ἐπέθηκεν· «Θεοὺς μὲν οὐκ οἶδα (οὐδὲ γὰρ εἰσὶ κατὰ τὴν ἀλή θειαν)· δαίμονας δὲ
ὑμεῖς ἀπατεῶνας πλανᾶσθε τῇ τοῦ Θεοῦ τιμῶντες προσηγορίᾳ· ἐμοὶ δὲ Θεὸς ὁ Χρι
στὸς, ὁ τοῦ Θεοῦ μονογενὴς Υἱός. Ὑπὲρ τῆς εὐσεβείας τοίνυν καὶ τῆς ὁμολογίας τῆς
εἰς ἐκεῖνον, καὶ ὁ τιτρώσκων τεμνέτω, καὶ ὁ μαστίζων ξαινέτω, καὶ ὁ καίων
προσαγέτω τὴν φλόγα, καὶ ὁ ταῖς φωναῖς μου ταύταις ἀχθόμενος ἐξαιρέτω τὴν
γλῶσσαν· καθ' ἕκαστον γὰρ μέλος, τὸ σῶμα τῷ κτίσαντι χρεωστεῖ τὴν ὑπομονήν.»
Ἡττήθησαν τῶν λόγων οἱ τύραννοι· καὶ τὴν πρώτην προσβολὴν τοῦ ἀριστέως οὐκ
ἤνεγ καν, νεανίσκον βλέποντες πρὸς τὸ πάθος σφριγῶντα, καὶ ὡς ἡδύ τι πόμα τὴν
τελευτὴν ἐφελκόμενον. Ὡς δὲ ἐκεῖνοι μικρὸν ἐπεῖχον ἠπορημένοι, καὶ βουλευό
μενοι τὸ πρακτέον· εἷς τις τῶν ἐν τέλει στρατιώτης κομψὸς εἶναι οἰόμενος,
ἐπιχλευάζων ὁμοῦ τὴν ἀπό κρισιν τὴν τοῦ μάρτυρος, «Ἔστι γὰρ,» ἔφη, «Υἱὸς, ὦ
Θεόδωρε, τῷ σῷ Θεῷ; καὶ γεννᾷ ἐκεῖνος, ὡς ἄν θρωπος ἐμπαθῶς;» «Ἐμπαθῶς μὲν ὁ
ἐμὸς,» ἔφη, «Θεὸς οὐκ ἐγέννησεν· ἀλλὰ καὶ Υἱὸν ὁμολογῶ, καὶ θεοπρεπῆ λέγω τὴν
γέννησιν· σὺ, ὦ νηπιώδη τὸν λογισμὸν καὶ ἄθλιε, οὐκ ἐρυθριᾷς οὐδὲ ἐγκαλύπτῃ καὶ
θήλειαν ὁμολογῶν θεὰν, καὶ ὡς μητέρα δώδεκα παίδων τὴν αὐτὴν προσκυνῶν,
πολύτοκόν τινα δαί μονα κατὰ τοὺς λαγωοὺς ἢ τὰς ὕας εὐκόλως καὶ ἐγ κυϊσκομένην
καὶ ἀποτίκτουσαν;»
Οὕτως δὲ τοῦ ἁγίου διπλοῦν ἀντιστρέψαντος τῷ εἰδωλολάτρῃ τὸν
καταγέλωτα, σχῆμα πλασάμενοι φιλανθρωπίας οἱ τύραννοι, «Ὀλίγος,» φασὶ, «πρὸς
διάσκεψιν ἐνδοθήτω τῷ μαινομένῳ καιρός· ἴσως κατὰ 46.744 σχολὴν δοὺς ἑαυτῷ
γνώμην, μετάθηται πρὸς τὸ βέλτιον.» Μανίαν γὰρ οἱ ἔκφρονες τὴν σωφροσύνην
ἐκάλουν· ἔκστασιν δὲ καὶ παραφορὰν, τὴν εὐλάβειαν· ὥσπερ ὅταν οἱ μεθύοντες, τὸ
ἴδιον πάθος τοῖς νήφου σιν ὀνειδίζωσιν. Ἀλλ' ὁ εὐσεβὴς ἄνθρωπος καὶ τοῦ Χριστοῦ
στρατιώτης, εἰς ἀνδρικὴν πρᾶξιν τῇ δοθείσῃ σχολῇ κατεχρήσατο. Ποίᾳ ταύτῃ; Καιρὸς
ὑμῖν μετ' εὐφροσύνης ὑποδέξασθαι τὸ διήγημα· Τῇ μυθευομένῃ μητρὶ τῶν θεῶν,
ναὸς ἦν ἐπὶ τῆς μετροπόλεως Ἀμα σείας, ὃν οἱ τότε πλανώμενοι, αὐτοῦ που περὶ τὰς
ὄχθας τοῦ ποταμοῦ τῇ ματαιότητι κατεσκεύασαν. Τοῦτον ὁ γενναῖος, ἐν τῷ τῆς
δοθείσης ἀδείας καιρῷ ἐπιτηρήσας εὔκαιρον ὥραν, καὶ αὖραν ἐπίφορον, ἐμπρήσας
κατέφλεξεν, ἔργῳ τοῖς ἀλιτηρίοις δοὺς τὴν ἀπόκρισιν, ἣν πάντως ἀνέμενον μετὰ
τὴν διάσκεψιν. Ἐπιδήλου δὲ τοῦ πράγματος ταχέως ἅπασι γενομέ νου (καὶ γὰρ ἐν τῷ
μέσῳ τῆς πόλεως φανερώτατον ἐξήφθη τὸ πῦρ), οὐκ ἐπεκρύψατο τὴν ἐγχείρησιν,
οὐδὲ ἐσπούδασε τὸ λαθεῖν· ἀλλ' ἔνδηλος ἦν, καὶ σφόδρα γε τῷ κατορθώματι
γαυριῶν, καὶ ὑπερχαίρων τῇ ταραχῇ τῶν ἀθέων, ἣν ἐταράττοντο, τῷ ναῷ περιαλ
γοῦντες καὶ τῷ ξοάνῳ. Καὶ μέντοι κατεμηνύθη τοῖς ἄρχουσιν, ὡς αὐτουργήσας τὸν
ἐμπρησμόν· καὶ πάλιν κριτήριον φοβερώτερον τοῦ προτέρου· ὥσπερ οὖν καὶ εἰκὸς,
ἐπισυμβάντος τηλικούτου τοῦ παροξύναντος. Καὶ οἱ μὲν ἐπὶ τὸν δικαστικὸν
ἀνέβησαν θρό νον. Θεόδωρος δὲ εὐπαῤῥησίαστος ἐν τοῖς μέσοις ἄρ χοντος θάρσος
ἔχων ἐν τῇ στάσει τοῦ κρινομένου ἐρω τώμενος, καὶ τῷ τάχει τῆς ὁμολογίας τὴν
ἐρώτησιν ἐπικόπτων. Ἐπειδὴ δὲ ἀκατάπληκτος ἦν, καὶ πρὸς οὐδὲν ἐφείδετο τῶν
ἀπειλουμένων δεινῶν, εἰς τοὐναν τίον μετεβλήθησαν τρόπον· καὶ φιλανθρώπως
διαλε γόμενοι, ἐπαγγελίαις ὑποσύρειν ἐπειρῶντο τὸν δίκαιον. «Καὶ γίνωσκε,» φασὶν,
«ὡς εἰ βουληθείης εὐπειθῶς τὴν συμβουλὴν τὴν ἡμετέραν προσδέξασθαι, εὐθύς σε
καταστήσομεν ἐξ ἀφανῶν γνώριμον, ἐξ ἀδόξων ἔντιμον· καί σοι τῆς ἀρχιερωσύνης
ἐπαγγελλόμεθα τὴν ἀξίαν.» Ὡς δὲ τοῦ τῆς ἀρχιερωσύνης ἀξιώματος ἤκουσεν,
ἐγγελάσας μακρὸν ὁ τρισμακάριος εἶπεν· «Ἐγὼ καὶ τοὺς ἱερέας τῶν εἰδώλων
ἀθλίους κρίνων, καὶ ὡς ματαίας πράξεως ὑπηρέτας, οἰκτείρω· τοὺς δὲ ἀρχιερέας ἐπὶ
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

3

πλεῖον ἐλεῶ καὶ βδελύσσομαι· ἐπὶ γὰρ τῶν χειρόνων, ὁ μείζων καὶ πρωτοτακτῶν
ἀθλιώ τερος· ὡς ἐν τοῖς ἀδίκοις, ὁ ἀδικώτερος· ὡς ἐν τοῖς φονεῦσιν, ὁ μᾶλλον ὠμός·
ὡς ἐν τοῖς ἀκολάστοις, ὁ ἀσελγέστερος· καὶ διὰ τοῦτο ἐντεῦθεν ἤδη διὰ τῶν
ὀλεθρίων ἐπαγγελιῶν καταβάλετε· λελήθατε γάρ μοι, ὦ οὗτοι, τὴν κορυφὴν τῶν
κακῶν ὑποσχόμενοι· τῷ δὲ εὐσεβῶς καὶ ὀρθῶς ζῇν προαιρουμένῳ, παραῤῥι πτεῖσθαι
καλὸν ἐν τῷ οἴκῳ τοῦ Θεοῦ μᾶλλον, ἢ οἰκεῖν ἐν σκηνώμασιν ἁμαρτωλῶν. Ἐγὼ καὶ
τοὺς βασιλέας τούτους, ὧν τὸν ἄνομον συνεχῶς ὑπαναγινώσκετε νόμον, ἐλεῶ, ὅτι
αὔταρκες ἐν ἀνθρώποις ἀξίωμα τὸ κράτος τῆς βασιλείας ἔχοντες, τὴν τοῦ ἀρχιερέως
ἑαυτοῖς προσηγορίαν ἀνέθεσαν, καὶ τὴν πενθήρη καὶ σκοτεινὴν πορφύραν ἐκεῖθεν
ἀμπίσχονται, κατὰ μίμη σιν τῶν κακοδαιμόνων ἀρχιερέων, ἀξιώματι φαιδρῷ
σκυθρωπὸν περιφέροντες ἔνδυμα· ἔστι δὲ ὅτε καὶ μιαρῷ βωμῷ πλησιάζοντες, ἀντὶ
βασιλέων γίνονται 46.745 μάγειροι ὄρνεις θύοντες, καὶ βοσκημάτων ἀθλίων
σπλάγχνα διερευνώμενοι, καὶ τῷ λύθρῳ τοῦ αἵματος, ὡς κρεωπῶλαί τινες τὴν
ἐσθῆτα μολύνοντες.»
Μετὰ ταύτας τὰς φωνὰς τοῦ δικαίου, οὐκέτι οὐδὲ τὴν πλαστὴν
φιλανθρωπίαν καὶ ἐσχηματισμένην οἱ ἄρχοντες ἐπεδείκνυντο· ἀλλὰ καὶ
ἀσεβέστατον πρὸς τοὺς Θεοὺς καλοῦντες, καὶ ἔτι τῶν βασιλέων ὑβριστὴν καὶ
βλάσφημον, πρῶτον μὲν ἐπὶ τοῦ βασανιστηρίου ξύλου κρεμάσαντες, τὸ σῶμα
κατέξαινον· ὁ δὲ, τῶν δημίων σφοδρῶς ἐνεργούντων, καρτερὸς ἦν καὶ ἀνέν δοτος,
καὶ στῖχον ἐκ τῆς Ψαλμωδίας ταῖς βασάνοις ἐπῇδεν· Εὐλογήσω τὸν Κύριον ἐν παντὶ
καιρῷ, διὰ παντὸς ἡ αἴνεσις αὐτοῦ ἐν τῷ στόματί μου. Καὶ οἱ μὲν τῶν σαρκῶν
ἀπέξεον· ὁ δὲ ἔψαλλεν, ὡς ἄλλου τινὸς ὑφισταμένου τὴν τιμωρίαν. ∆ιεδέξατο τὴν
κόλασιν ταύτην τὸ δεσμωτήριον, καὶ πάλιν ἐκεῖ θαῦμα ἐτελεῖτο περὶ τὸν ἅγιον, καὶ
νύκτωρ φωνὴ πλήθους ψαλλόντων ἠκούετο, καὶ λαμπάδων ἐπιφαινουσῶν, ὡς ἐν
παννυχίδι αὐγὴ ἑωρᾶτο τοῖς ἔξωθεν, ὡς καὶ τὸν δεσμοφύλακα πρὸς τὴν παράδοξον
ὄψιν καὶ ἀκοὴν ταραχθέντα, εἰσπηδῆσαι εἰς τὸν οἰκίσκον, καὶ μηδένα εὑρεῖν πλὴν
τοῦ μάρτυρος ἡσυχάζοντος, καὶ τῶν ἄλλων δεσμωτῶν καθευδόντων. Ἐπειδὴ δὲ
πολλῶν γινομένων ἤκμαζε πρὸς τὴν ὁμολογίαν καὶ τὴν εὐσέβειαν, ἦλθεν ἐπ' αὐτὸν
ἡ κατακρίνουσα ψῆφος, καὶ πυρὶ τελειωθῆναι κελευσθεὶς, αὐτὸς μὲν ἀπῆλθε τὴν
καλὴν καὶ μακαρίαν πρὸς Θεὸν πορείαν· ἡμῖν δὲ τὴν μνήμην τοῦ ἀγῶνος
διδασκάλιον κατέλιπε, λαοὺς ἀθροίζων, ἐκκλησίαν παιδεύων, δαίμονας ἀπελαύνων,
ἀγγέλους εἰρηνικοὺς κατάγων, ζητῶν ὑπὲρ ἡμῶν παρὰ Θεοῦ τὰ συμφέροντα,
ἰατρεῖον νόσων ποικίλων τὸν τόπον τοῦτον ἀπεργασάμενος, λιμένα τῶν
χειμαζομένων ταῖς θλίψεσι, πενήτων εὐθηνουμένων ταμιεῖον, ὁδοιπόρων ἀνεκτὸν
καταγώγιον, πανήγυριν τῶν ἑορταζόντων ἄληκτον.
Εἰ γὰρ καὶ ἐνιαυσιαίοις ἑορταῖς τὴν ἡμέραν ταύτην τελοῦμεν, ἀλλ' οὐδέποτε
λήγει τῶν κατὰ σπουδὴν ἀφικνουμένων τὸ πλῆθος, τῶν μυρμήκων δὲ σώζει τὴν
ὁμοιότητα ἡ ἐπὶ τάδε φέρουσα λεωφόρος, τῶν μὲν ἀνιόντων, τῶν δὲ ὑποχωρούντων
τοῖς ἐρχομένοις. Ἡμεῖς μὲν οὖν, ὦ μακάριε, τὸν τοῦ ἐνιαυτοῦ κύκλον καταλαβόντες
φιλανθρωπίᾳ τοῦ κτίσαντος, ἠθροίσαμέν σοι τὴν πανήγυριν, τὸν ἱερὸν τῶν
φιλομαρτύρων σύλλογον, τόν τε κοινὸν προσκυνοῦντες ∆εσπότην, καὶ τὴν ἐπινίκιον
πληρώσαντες τῶν σῶν ἀγώνων ὑπόμνησιν· σὺ δὲ, δεῦρο δὴ πρὸς ἡμᾶς, ὅπου ποτ' ἂν
ᾖς, τῆς ἑορτῆς ἔφορος (καλέσαντα γάρ σε ἀντικαλοῦμεν)· καὶ εἴτε τῷ ὑψηλῷ
αἰθέριον διαιτᾷ, εἴτε τινὰ ἐπουράνιον ἀψίδα περιπολεῖς, ἢ χοροῖς 46.748 ἀγγέλων
συντεταγμένος τῷ ∆εσπότῃ παρέστηκας, ἢ μετὰ δυνάμεων καὶ ἐξουσιῶν, ὡς δοῦλος
πιστὸς προσκυνεῖς· μικρὸν τὰ αὐτόθι παραιτησάμενος, ἧκε πρὸς τοὺς τιμῶντάς σε
ἀόρατος φίλος· ἱστόρησον τὰ τελούμενα, ἵνα τὴν εἰς Θεὸν εὐχαριστίαν διπλασιάσῃς,
τὸν ἀντὶ πάθους ἑνὸς, καὶ μιᾶς εὐσεβοῦς ὁμολογίας, τοσαύτας σοι τὰς ἀμοιβὰς
χαρισάμενον· εὐφρανθείης δὲ τῷ αἵματι καὶ τῇ τοῦ πυρὸς ἀλγηδόνι. Ὅσους γὰρ εἶχες
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

4

τότε λαοὺς τῆς τιμωρίας θεατὰς, τοσούτους νῦν τῆς τιμῆς ἔχεις ὑπηρέτας. Χρῄζομεν
πολλῶν εὐεργεσιῶν· πρέσβευσον ὑπὲρ τῆς πατρίδος πρὸς τὸν κοινὸν βασιλέα· πατρὶς
γὰρ μάρτυρος, τοῦ πάθους ἡ χώρα· πολῖται δὲ καὶ συγγενεῖς, οἱ περιστείλαντες καὶ
ἔχοντες καὶ τιμῶντες. Ὑφορώμεθα θλίψεις, προσδοκῶμεν κινδύνους, οὐ μακρὰν οἱ
ἀλιτήριοι Σκῦθαι τὸν καθ' ἡμῶν ὠδίνοντες πόλεμον· ὡς στρατιώτης ὑπερμάχησον·
ὡς μάρτυς ὑπὲρ τῶν ὁμοδούλων χρῆσαι τῇ παῤῥησίᾳ. Εἰ καὶ ὑπερέβης τὸν βίον, ἀλλ'
οἶδας τὰ πάθη καὶ τὰς χρείας τῆς ἀνθρωπότητος· αἴτησον εἰρήνην, ἵνα αἱ πανηγύρεις
αὗται μὴ λήξωσιν, ἵνα μὴ κωμάσῃ κατὰ ναῶν ἢ θυσιαστηρίων λυσσῶν καὶ ἄθεσμος
βάρβαρος, ἵνα μὴ πατήσῃ τὰ ἅγια βέβηλος.
Ἡμεῖς γὰρ καὶ ὑπὲρ ὧν ἀπαθεῖς ἐφυλάχθημεν, σοὶ λογιζόμεθα τὴν
εὐεργεσίαν· αἰτοῦμεν δὲ καὶ τοῦ μέλλοντος τὴν ἀσφάλειαν. Ἂν χρεία γένηται καὶ
πλείονος δυσωπίας, ἄθροισον τὸν χορὸν τῶν σῶν ἀδελφῶν τῶν μαρτύρων, καὶ μετὰ
πάντων δεήθητι· πολλῶν δικαίων εὐχαὶ, λαῶν καὶ δήμων ἁμαρτίας λυσάτωσαν·
ὑπόμνησον Πέτρον, διέγειρον Παῦλον, Ἰωάννην ὁμοίως τὸν θεολόγον καὶ
φιλούμενον μαθητήν· ἵν' ὑπὲρ τῶν Ἐκκλησιῶν ἃς συνεστήσαντο μεριμνήσωσιν,
ὑπὲρ ὧν τὰς ἁλύσεις ἐφόρεσαν, ὑπὲρ ὧν τοὺς κινδύνους καὶ τοὺς θανάτους ἤνεγκαν·
μὴ εἰδωλολατρεία ἄρῃ καθ' ἡμῶν κεφαλὴν, μὴ αἱρέσεις ὡς ἄκανθαι τῷ ἀμπελῶνι
ὑποβλαστήσωσι, μὴ ζιζάνιον ἐγερθὲν πνίξῃ τὸν σῖτον· μή τις πέτρα γένηται καθ'
ἡμῶν λειπομένη τῆς πιότητος τῆς ἀληθινῆς δρόσου, καὶ ἄριζον ἀποδείξῃ τῆς
εὐκαρπίας τοῦ λόγου τὴν δύναμιν· ἀλλὰ τῇ δυνάμει τῆς σῆς πρεσβείας καὶ τῶν σὺν
σοὶ, θαυμαστὲ, καὶ ὑπερλάμπον ἐν τοῖς μάρτυσι λήϊον ἀποδειχθείη τὸ τῶν
Χριστιανῶν πολίτευμα μέχρι τέλους μένον, ἐν τῇ λιπαρᾷ καὶ εὐκάρπῳ τῆς ἐν Χριστῷ
πίστεως ἀρούρᾳ ἀεὶ καρποφοροῦν τὴν αἰώνιον ζωὸν, τὴν ἐν Χριστῷ Ἰησοῦ τῷ Κυρίῳ
ἡμῶν. Μεθ' οὗ τῷ Πατρὶ ἅμα τῷ ἁγίῳ Πνεύματι, δόξα, κράτος, τιμὴ, νῦν καὶ ἀεὶ καὶ
εἰς τοὺς αἰῶνας τῶν αἰώνων. Ἀμήν.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

5

