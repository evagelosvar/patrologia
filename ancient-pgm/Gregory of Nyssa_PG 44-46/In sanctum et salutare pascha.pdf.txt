In sanctum et salutare pascha
ΓΡΗΓΟΡΙΟΥ ΕΠΙΣΚΟΠΟΥ ΝΥΣΣΗΣ ΕΙΣ ΤΟ ΑΓΙΟΝ ΚΑΙ ΣΩΤΗΡΙΟΝ ΠΑΣΧΑ
Ἡ μὲν ἀληθινὴ τοῦ σαββάτου κατάπαυσις ἡ τὴν εὐλογίαν τοῦ θεοῦ
δεξαμένη, ἐν ᾗ κατέπαυσεν ἀπὸ τῶν ἑαυτοῦ ἔργων ὁ κύριος ὑπὲρ τῆς τοῦ κόσμου
σωτηρίας τῇ ἀπραξίᾳ τοῦ θανάτου ἐνσαββατίσας, ἤδη πέρας ἔχει καὶ τὴν ἰδίαν ἔδειξε
χάριν καὶ ὀφθαλμοῖς καὶ ἀκοαῖς καὶ καρδίᾳ διὰ πάντων τούτων τῆς ἑορτῆς ἐν ἡμῖν
τελεσθείσης, οἷς εἴδομεν, οἷς ἠκούσαμεν, οἷς τῇ καρδίᾳ τὴν εὐφροσύνην
ὑπεδεξάμεθα. τὸ μὲν γὰρ ὁρώμενον τοῖς ὀφθαλμοῖς φῶς ἦν τῇ τοῦ πυρὸς νεφέλῃ διὰ
τῶν λαμπάδων ἡμῖν ἐν τῇ νυκτὶ δᾳδουχούμενον, ὁ δὲ παννύχιον ταῖς ἀκοαῖς
προσηχῶν λόγος ἐν ψαλμοῖς καὶ ὕμνοις καὶ ᾠδαῖς πνευματικαῖς οἷόν τι ῥεῦμα χαρᾶς
δι' ἀκοῆς εἰς τὴν ψυχὴν εἰσρέων πλήρεις ἡμᾶς τῶν ἀγαθῶν ἐλπίδων ἐποίησεν, ἡ δὲ
καρδία τῶν λεγομένων τε καὶ βλεπομένων φαιδρυνομένη τὴν ἄφραστον ἐτυποῦτο
μακα ριότητα διὰ τῶν φαινομένων χειραγωγουμένη πρὸς τὸ ἀόρατον, ὥστε τῶν
ἀγαθῶν ἐκείνων, Ἃ οὔτε ὀφθαλμὸς εἶδεν οὔτε οὖς ἤκουσεν οὔτε ἐπὶ καρδίαν
ἀνθρώπου ἀνέβη, εἰκόνα εἶναι τὰ τῆς καταπαύσεως ταύτης ἀγαθὰ δι' ἑαυτῶν τὴν
ἀνεκφώνητον ἐλπίδα τῶν ἀποκειμένων πιστούμενα. ἐπειδὴ τοίνυν ἡ φωτεινὴ νὺξ
αὕτη τὰς ἐκ τῶν λαμπάδων αὐγὰς ταῖς ὀρθριναῖς ἀκτῖσι τοῦ ἡλίου συμμίξασα μίαν
κατὰ τὸ συνεχὲς ἡμέραν ἐποίησε μὴ διαμερισθεῖσαν τῇ παρενθήκῃ τοῦ σκότους,
9.310 νοήσωμεν, ἀδελφοί, τὴν προφήτειαν τὴν λέγουσαν ὅτι Αὕτη ἐστὶν ἡ ἡμέρα, ἣν
ἐποίησεν ὁ κύριος, ἐν ᾗ ἔργον ἐστὶν οὐ βαρύ τι καὶ δυσκατόρθωτον, ἀλλὰ χαρὰ καὶ
εὐφροσύνη καὶ ἀγαλλίαμα οὕτως εἰπόντος τοῦ λόγου ὅτι Ἀγαλλιασώμεθα καὶ
εὐφρανθῶμεν ἐν αὐτῇ. ὢ καλῶν προσταγμάτων, ὢ γλυκείας νομοθεσίας. τίς
ὑπερτίθεται πρὸς τὸ ὑπακοῦσαι τοιούτοις προστάγμασι; τίς δὲ οὐχὶ ζημίαν ἡγεῖται
καὶ τὴν ἐν ὀλίγῳ τῶν προσταγμάτων ἀναβολήν; εὐφροσύνη τὸ ἔργον καὶ ἀγαλλίαμά
ἐστι τὸ ἐπίταγμα, δι' ὧν ἀναλύεται ἡ ἐπὶ τῇ ἁμαρτίᾳ κατάκρισις καὶ εἰς χαρὰν τὰ
λυπηρὰ μεθαρμόζεται. τοῦτό ἐστι τὸ τῆς σοφίας ἀπόφθεγμα, ὅτι ἐν ἡμέρᾳ
εὐφροσύνης ἀμνηστία κακῶν. λήθην ἐποίησε τῆς πρώτης καθ' ἡμῶν ἀποφάσεως ἡ
ἡμέρα αὕτη, μᾶλλον δὲ ἀφανισμόν, οὐχὶ λήθην· ἐξήλειψε γὰρ καθόλου πᾶν
μνημόσυνον τῆς καθ' ἡμῶν κατακρίσεως. τότε ἐν λύπαις ὁ τοκετός, νῦν χωρὶς
ὠδίνων ἡ γέννησις· τότε ἐκ σαρκὸς ἐγεννήθημεν σάρκες, νῦν τὸ γεννηθὲν πνεῦμά
ἐστιν ἐκ πνεύματος· τότε υἱοὶ ἀνθρώπων, νῦν τέκνα θεοῦ ἐγεννήθημεν· τότε εἰς γῆν
ἐξ οὐρανῶν ἀπελύθημεν, νῦν ὁ ἐπουράνιος καὶ ἡμᾶς οὐρανίους ἐποίησε· τότε διὰ τῆς
ἁμαρτίας ὁ θάνατος ἐβασίλευε, νῦν ἀντιμεταλαμβάνει διὰ τῆς ζωῆς ἡ δικαιοσύνη τὸ
κράτος· εἷς ἤνοιξε τότε τοῦ θανάτου τὴν εἴσοδον, δι' ἑνὸς καὶ νῦν ἡ ζωὴ
ἀντεισάγεται· τότε διὰ τοῦ θανάτου τῆς ζωῆς ἐξεπέσομεν, νῦν ὑπὸ τῆς ζωῆς
ἀναιρεῖται ὁ θάνατος· τότε ὑπ' αἰσχύνης τῇ συκῇ ὑπεκρύβημεν, νῦν διὰ δόξης τὸ
ξύλον τῆς ζωῆς προσεγγίζομεν· τότε διὰ τῆς παρακοῆς τοῦ παραδείσου ἐξῳκίσθημεν,
νῦν διὰ πίστεως τοῦ παραδείσου εἴσω γινόμεθα. πάλιν ὁ καρπὸς τῆς ζωῆς εἰς τρυφὴν
πρόκειται κατ' ἐξουσίαν ἡμῖν· πάλιν ἡ τοῦ παραδείσου πηγὴ ἡ τετραχῆ διὰ τῶν
εὐαγγελικῶν ποταμῶν 9.311 μεριζομένη ἅπαν ποτίζει τὸ πρόσωπον τῆς ἐκκλησίας,
ὡς καὶ μεθύειν ἡμῶν τὰς αὔλακας τῶν ψυχῶν, ἃς ὁ σπείρων τὸν λόγον τῷ ἀρότρῳ
τῆς διδασκαλίας ἀνέτεμε, καὶ πληθύειν τῆς ἀρετῆς τὰ γεννήματα.
Τί οὖν ἔτι τοῖς τοιούτοις προσήκει ποιεῖν; τί ἄλλο ἢ μιμεῖσθαι τὰ ὄρη τὰ
προφητικὰ καὶ τοὺς βουνοὺς τοῖς σκιρτήμασι. Τὰ ὄρη, γάρ φησιν, ἐσκίρτησαν ὡσεὶ
κριοὶ καὶ οἱ βουνοὶ ὡς ἀρνία προβάτων. οὐκοῦν δεῦτε, ἀγαλλιασώμεθα τῷ κυρίῳ τῷ
καθελόντι τοῦ πολεμίου τὴν δύναμιν καὶ τὸ μέγα τρόπαιον τοῦ σταυροῦ ὑπὲρ ἡμῶν
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

ἀναστήσαντι ἐν τῷ πτώματι τοῦ ἀντικειμένου. ἀλαλάζωμεν, ἀλαλαγμὸς δέ ἐστιν
ἐπινίκιος εὐφημία παρὰ τῶν κεκρατηκότων ἐγειρομένη κατὰ τῶν ἡττημένων. ἐπεὶ
οὖν πέπτωκεν ἡ τοῦ πολεμίου παράταξις καὶ αὐτὸς ἐκεῖνος ὁ τὸ κράτος ἔχων τῆς
πονηρᾶς τῶν δαιμόνων στρατιᾶς οἴχεται καὶ ἠφάνισται καὶ εἰς τὸ μὴ ὂν ἤδη
μετακεχώρηκεν, εἴπωμεν ὅτι Θεὸς μέγας κύριος καὶ Βασιλεὺς μέγας ἐπὶ πᾶσαν τὴν
γῆν ὁ Εὐλογήσας τὸν στέφανον τοῦ ἐνιαυτοῦ τῆς χρηστότητος αὐτοῦ καὶ συναγαγὼν
ἡμᾶς εἰς τὴν πνευματικὴν ταύτην χοροστασίαν, ἐν Χριστῷ Ἰησοῦ τῷ κυρίῳ ἡμῶν, ᾧ
ἡ δόξα εἰς τοὺς αἰῶνας, ἀμήν.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

