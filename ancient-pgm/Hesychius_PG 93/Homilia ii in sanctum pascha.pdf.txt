Homilia ii in sanctum pascha (homilia 4) (e cod. Sinaitico gr. 492)
Τοῦ αὐτοῦ, εἰς τὸ ἅγιον πάσχα
1 Σάλπιγξ ἡμῖν ἱερὰ καὶ βασιλικὴ τὸ πνευματικὸν τοῦτο συνεκρότησεν
θέατρον, σάλπιγξ ἣν ἐπλήρωσεν Βηθλεὲμ καὶ ἡ Σιὼν ἐπύρωσεν, ἐν ᾗ ὁ Σταυρὸς
σφῦρα καὶ ἄκμων ἡ Ἀνάστασις. Ἧς οὐκ οἶδα πῶς ἐξείπω τὸ κάλλος, πῶς ἐκφράσω τὸ
φῶς, πῶς μηνύσω τὴν ἐν αὐτῷ χαράν, πῶς τὴν ἐν αὐτῇ βασιλείαν διηγήσωμαι. Ποίᾳ
δὲ αὐτὴν ψηλαφήσω χειρί;
2 Ποίοις λόγοις ἀσπάσομαι τάφον γεννῶντα ζωήν, μνῆμα φθορᾶς <μὲν>
ἐλεύθερον, ἀφθαρσίας δὲ πρόξενον, παστάδα τριή μερον τὸν νυμφίον κοιμήσασαν,
νυμφῶνα τὴν νύμφην ἄφθορον μετὰ γάμον ἐγείραντα; Νεκρὸν ἡ θήκη φυλαττομένη
καὶ θεὸν ἡ γῆ σαλευομένη· νεκρὸν μὲν γὰρ αὐτὸν τὸ σῶμα βοᾷ, τὸ δὲ θαῦμα θεόν·
νεκρὸν ἡ ταφή, θεὸν ἡ ἀνάστασις· νεκρὸν τῶν γυναικῶν καὶ τὰ δάκρυα, καὶ θεὸν
τῶν ἀγγέλων τὰ ῥήματα. Ὡς νεκρὸν αὐτὸν Ἰωσὴφ ἐκήδευσεν, ἀλλ' ὁ κηδευθεὶς ὡς
ἄνθρωπος οὗτος ὡς θεὸς τὸν θάνατον ἐσκύ λευσεν. Πάλιν ὡς νεκρὸν οἱ στρατιῶται
ἐφρούρησαν καὶ ὡς θεὸν οἱ πυλωροὶ τοῦ ᾅδου θεωρήσαντες ἔπτηξαν.
3 Τὸν αὐτὸν δὲ τοῦτον κἀκεῖνον ἐρεῖς, οὐκ ἄλλον καὶ ἄλλον, οὐδὲ ἄλλον ἐν
ἄλλῳ, οὐδὲ ἄλλον δι' ἄλλου· εἷς γὰρ ὢν ὁ σαρκω θεὶς Λόγος εἰς ἓν ταῦτα κἀκεῖνα ὡς
ἠβουλήθη ἀρρήτῳ λόγῳ συνήγαγεν· καὶ τὴν μὲν σάρκα διακονῆσαι τοῖς πάθεσι
δέδωκεν, τῇ θεότητι δὲ πρὸς τὰ σημεῖα καὶ τὰ θαύματα κέχρηται, ἀλλ' ὥσπερ οὐ
θεμιτὸν τὸν Λόγον ἐκ τῆς σαρκὸς χωρίζεσθαι, οὕτως ἀνάγκη τὰ παθήματα
συμπεπλέχθαι τοῖς θαύμασιν. Ὁ γὰρ ὡς νεκρὸς εἰς ᾅδου καταβάς, οὗτος ὡς θεὸς τοὺς
νεκροὺς ἠλευθέρωσεν, ἐπεί πως τάφῳ διακονοῦσιν ἄγγελοι, πὼς δὲ λευχείμονες ὡς
ἐπὶ νυμφίῳ ταῖς γυναιξὶν ἐμφανίζονται, πὼς δὲ πρὸς αὐτὰς ἔλεγον· »Ἰησοῦν ζητεῖτε
τὸν Να ζαρηνὸν τὸν ἐσταυρωμένον· οὐκ ἔστιν ὧδε· ἠγέρθη γὰρ καθὼς εἶπεν.
Τουτέστιν οὐρανὸς αὐτῷ τόπος, ἐκεῖ τὰ μύρα πέμψατε. Ἠγέρθη, οὐχ ἡμεῖς αὐτὸν
ἠγείραμεν. Ὑμῶν ἕνεκα τὸν λίθον ἐκυ λίσαμεν· πρὶν γὰρ ἡμᾶς καταβῆναι, ὁ τάφος
ἐκεκέ νωτο. Ἠγέρθη, καθὼς εἶπεν αὐτός.»
4 Ὃ ἄγγελος εἶπεν, οὐ χωρεῖ οὐδὲ προφήτης ἑρμηνεῦσαι· Ὡσηὲ τὸν καιρὸν
τῆς ἀναστάσεως λέγει, Ἠσαΐας οἶδεν, ἀλλὰ τὸ πῶς οὐκ ἐπίσταται. Ταῦτα μὲν γὰρ
Ὡσηὲ προφητείας τὰ ῥή ματα· «Πορευσώμεθα καὶ ἐπιστρέψωμεν πρὸς κύριον τὸν
θεὸν ἡμῶν, ὅτι αὐτὸς» πέπαικεν «καὶ ἰάσεται ἡμᾶς· πατάξει καὶ μο τώσει ἡμᾶς μετὰ
δύο ἡμέρας· ἐν τῇ ἡμέρᾳ τῇ τρίτῃ, ἀναστησό μεθα καὶ ζησόμεθα ἐνώπιον αὐτοῦ.»
Ἄκουε πάλιν ποῖα Ἠσαΐας ἐσάλπισεν· «Ἠισχύνθη ὁ Λίβανος, ἕλη ἐγένετο ὁ Σαρών.
Φανερὰ ἔσται ἡ Γαλιλαία καὶ ὁ Κάρμηλος. Νῦν ἀναστή σομαι, λέγει κύριος, νῦν
δοξασθήσομαι, νῦν ὑψωθήσομαι. Νῦν ὄψεσθε, νῦν αἰσχυνθήσεσθε.» Πρὸς γὰρ τοὺς
Ἰουδαίους τὸν λόγον ἀπετείνετο· νῦν ἀναστήσομαι ὅταν ἐγείρω τὸν Ἀδὰμ ὃν ἡ
παράβασις ἔρριψεν, νῦν δοξασθήσομαι ὅταν τοῖς ἔθνεσιν τῶν παθῶν συστήσω τὴν
ἀπάθειαν, νῦν ὑψω θήσομαι τὴν ὑμετέραν ἀπαρχὴν εἰς οὐρανοὺς ὑψῶν καὶ εἰς τὴν
καθέδραν τῶν χερουβὶν ἐπαίρων «τὴν τοῦ δούλου μορ φὴν» ἥνπερ ἐξ ὑμῶν
κέκτημαι, νῦν ὄψεσθε παυομέ νους τοὺς τύπους, ἀνθοῦσαν τὴν ἀλήθειαν, νῦν
αἰσχυνθή σεσθε συκοφαντοῦντες ῥήμασιν καὶ ἡττώμενοι πράγμασιν, ὅτι τῷ θεῷ ἡ
δόξα, τῷ Πατρὶ καὶ τῷ Υἱῷ καὶ τῷ ἁγίῳ Πνεύ ματι, νῦν καὶ ἀεὶ καὶ εἰς τοὺς αἰῶνας
τῶν αἰώνων. Ἀμήν.

1
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

