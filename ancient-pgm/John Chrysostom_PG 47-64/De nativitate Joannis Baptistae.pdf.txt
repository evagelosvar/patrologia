De nativitate Joannis Baptistae
Τοῦ ἐν ἁγίοις πατρὸς ἡμῶν Ἰωάννου ἀρχιεπισκόπου Κωνσταντινουπόλεως
τοῦ Χρυσοστόμου εἰς τὸ γενέθλιον τοῦ ἁγίου Ἰωάννου τοῦ προδρόμου καὶ
βαπτιστοῦ.
Ἀπαραίτητος σήμερον τῆς διδασκαλίας ὁ λόγος, ἔχει γὰρ Ἰωάννην τὸν
βαπτιστὴν ἡ ἐξήγησις, τὴν ἀσίγητον τῆς ἐρήμου φωνήν· ἐγὼ γάρ φησι φωνὴ βοῶντος ἐν
τῇ ἐρήμῳ. Ὅπου δὲ φωνῆς ἄγεται μνήμη, ἀναγκαίως ἡ τῶν λόγων τιμὴ κεχρεώστηται.
Ἀλλὰ ποίαν ἡμεῖς τῷ δικαίῳ τούτῳ τιμὴν ἀποτίσωμεν; Κυκλούμεθα πανταχόθεν τοῖς
αὐτοῦ κατορθώμασιν· εὐαγγελίζεται γὰρ ἔτι κυοφορούμενος, θαυματουργεῖ παραυτίκα
τικτόμενος, προκόπτει ἐν ταῖς ἐρήμοις ἀσκούμενος, τρέφεται τῷ πνεύματι προαγόμενος,
μεσιτεύει νόμῳ καὶ χάριτι, κηρύττει μετανοίας τὸ βάπτισμα, βαπτίζει τὸν δεσπότην
παραγενόμενον, θεωρεῖ βαπτίζων καὶ τῆς τριάδος τὴν χάριν, μαρτυρεῖ τῇ ἀληθείᾳ βοῶν,
ὑποδείκνυσι τὸν προφητευθέντα δεσπότην, ὁμολογεῖ τὸν ἑαυτοῦ ἰσχυρότερον, ὁδηγεῖ
πρὸς αὐτὸν τοὺς πιστεύοντας, ἐκεῖνον διδόναι κηρύττει τὸ τέλος, προσάγει λαοὺς τοὺς
διψῶντας αὐτὸν ἐπιγινώσκειν, δεσπότην τρέχων μηνύει, αὐτῷ προστρέχειν ἐπείγει,
ἐκδιδάσκει τὸ συμφέρον τοὺς ὄχλους, νουθετεῖ καὶ βαπτιζομένους τελώνας, ἐλέγχει καὶ
τὸν Ἡρώδην παρανομοῦντα, δέχεται καὶ μακαριζόμενον τέλος. Ποίοις ταῦτα
θαυμάσωμεν λόγοις; Ποίαν ἐν τούτοις κινήσωμεν γλῶτταν; Τί τούτων παραλίπωμεν, τί
καὶ εἰπεῖν περὶ τούτων ἰσχύσωμεν; Ποία τούτους ἐλπὶς διεγείρει; Μέγα γὰρ τῶν
ἐγκωμίων τὸ πλάτος, μέγα τῶν κατορθωμάτων τὸ πέλαγος. Ἐκπλήττει καὶ μόνη τοὺς
λέγοντας τῶν πραγμάτων ἡ μνήμη, δειλίαν ἐντίθησι τῇ ψυχῇ τοῦ θαυμάζοντος. Ἀλλ'
ἔλθῃ καὶ νῦν Ζαχαρίας ὁ πατὴρ τοῦ δικαίου, ὁ καὶ τὴν σιωπὴν κατακριθεὶς καὶ διὰ
τοῦτον φθεγξάμενος. Ἐκεῖνος ἡμῖν τὰ κατ' αὐτὸν διηγήσεται· εἴπῃ πῶς ἐφθέγξατο
κατασχεθείσης τῆς γλώττης, πῶς ἐλάλησε χαλινωθέντος τοῦ στόματος, πῶς Ἰωάννης
γεννᾶται καὶ ἡ τοῦ ἀγγέλου ἀπόφασις λύεται, πῶς ἡ τιμωρία ἐπάγεται καὶ ἡ θεραπεία
παρέχεται. Καί, εἰ δοκεῖ, ἐπ' αὐτὴν λοιπὸν ἔλθωμεν τοῦ θαύματος τὴν ἐξήγησιν ἔχουσαν
τοῦ δικαίου τὴν σύλληψιν. Ὁ γὰρ εὐαγγελιστὴς σύντομον αὐτοῦ τε τοῦ Ζαχαρίου καὶ
Ἐλισάβετ τῆς γυναικὸς αὐτοῦ εἰσάγων τὸ κήρυγμα, πρότερον μὲν τὰς προσηγορίας
αὐτῶν καὶ τὴν φύσιν καὶ τὴν ἀξίαν σημαίνει, εἶτα πολιτείας ἔπαινον καὶ τρόπον ἐπάγει,
ἐν ᾧ καὶ τῆς σιωπῆς τὴν κατάκρισιν δείκνυσι καὶ τῆς εὐγλωττίας τὴν χάριν γνωρίζει.
Καὶ ἄκουε τῶν εὐαγγελικῶν ῥημάτων· Οὐκ ἦν φησιν αὐτοῖς τέκνον. Νῦν οὐ
μόνον ἐγὼ τὴν τούτων ἀτεκνίαν ὁρῶ, ἀλλὰ καὶ τὴν ἀρχαίαν εἰκόνα τῶν δικαίων
γινώσκω, Ἀβραὰμ λέγω καὶ Σάρρας. Ἀλλὰ μανθάνω καὶ στείρωσιν τίκτουσαν, ἑτέραν
παιδοποιΐαν χάριτος καὶ φύσεως, ξένον τεκνογονίας μυστήριον. Οὗτος γὰρ καὶ τῆς
ἐπαγγελίας ὁ τρόπος ταύτης· Ὤφθη φησὶν ἄγγελος ἑστὼς ἐκ δεξιῶν τοῦ θυσιαστηρίου
τοῦ θυμιάματος. Ἀξιόπιστος τῆς ὀπτασίας ὁ τόπος, ἀλήθειαν δεικνὺς καὶ οὐ πλάνην τὴν
θέαν. Ἀλλ' ἐταράχθη φησὶ Ζαχαρίας ἰδὼν καὶ φόβος ἐπέπεσεν ἐπ' αὐτόν. Ὦ δειλία, τὸ
τῆς ἀπιστίας προοίμιον, τὸ τῆς γνώμης σαθρόν, τῆς ψυχῆς τὸ εὐόλισθον. Ὅπερ αὐτὸς ὁ
ἄγγελος διορθούμενος ἔφη· Μὴ φοβοῦ, Ζαχαρία, διότι εἰσηκούσθη ἡ δέησίς σου.
Ἐκβάλλει τὸν φόβον καὶ ἀντεισάγει τὸν πόθον· εἰπὼν γὰρ τὸ μὴ φοβοῦ οὐκ ἐσιώπησεν,
ἀλλὰ καὶ τῆς εὐχῆς τὸ κέρδος ἐμήνυσεν. Εἰσηκούσθη φησὶν ἡ δέησίς σου. Καὶ ποία ἡ
δέησις; Ἡ τῆς λευιτικῆς φυλῆς αὐτοῦ αὔξησις. Ἡ γυνή σου γάρ φησιν Ἐλισάβετ
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

γεννήσει υἱὸν καὶ καλέσεις τὸ ὄνομα αὐτοῦ Ἰωάννην. Καλὸν τῆς ἐπαγγελίας τὸ κτῆμα,
μέγα τῆς ὀπτασίας τὸ δῶρον. Ἀληθῶς Ἀβραμιαῖον τὸ καύχημα· ὡς γὰρ ἐκείνῳ
προείρηται τῆς Σάρρας ἡ σύλληψις, οὕτως καὶ τούτῳ προκατήγγελται τῆς Ἐλισάβετ ἡ
κύησις. Ἀλλ' ἡ μὲν πρόρρησις ἴση, οὐκ ἴση δὲ τῶν πραγμάτων ἡ τάξις· τοῦ γὰρ Ἀβραὰμ
μηδὲν ἐκ τῆς ἐπαγγελίας πεπονθότος δεινόν, ἄφωνος μετὰ τὸν εὐαγγελισμὸν Ζαχαρίας
εὑρίσκεται μὴ τῷ ἀγγέλῳ πιστεύσας καὶ σιωπὴν τὴν τιμωρίαν δεξάμενος.
Ἰδοὺ γὰρ ἔσῃ φησὶ σιωπῶν καὶ μὴ δυνάμενος λαλῆσαι ἄχρι ἧς ἡμέρας γένηται
ταῦτα, ἀνθ' ὧν οὐκ ἐπίστευσας τοῖς λόγοις μου. Ἀνένδοτος ἡ δίκη τῷ πλημμελήσαντι,
ἀνυπέρθετος ἡ τῆς τιμωρίας ὀργή. Οὐκ ἔδει γὰρ ἀληθῶς ἀπιστηθῆναι τὸν ἄγγελον, οὐ τὸ
θυσιαστήριον εὐκαταφρόνητον ἔχειν, οὐ τὸν καιρὸν τοῦ θυμιάματος καιρὸν ἀπάτης
λογίσασθαι, – ὁ γὰρ ἄγγελος αὐτῷ θυμιῶντι ὤφθη –, οὐ τῆς τοῦ Ἀβραὰμ ἱστορίας
ἐπιλελῆσθαι. Καὶ τότε στεῖρα καὶ πότε γῆρας παιδοποιεῖ, εὔπειστα πανταχοῦ τὰ
λεγόμενα ἐδείχθη· εἶχε γὰρ τῆς τοιαύτης τεκνογονίας τὸ θαῦμα οὐ μόνον στεῖραν ἀλλὰ
καὶ παρθένον τίκτειν παραδόξως. Καὶ ἦν τὸ ἄπιστον εἰς πίστιν προγυμναζόμενον, τὸ
ἀδύνατον ὡς δυνατὸν ἐθιζόμενον· ἔνθεν γὰρ καὶ ἐπὶ τῆς στείρας νῦν ἀπιστία κολάζεται,
ἵνα ἡ ἐπὶ τῆς παρθένου θαυματουργία μὴ ἀμφιβάλληται. Ἔσῃ φησὶ σιωπῶν καὶ μὴ
δυνάμενος λαλῆσαι. Ὢ τῆς φιλανθρώπου τιμωρίας διόρθωσιν μᾶλλον ἐργασαμένης τὴν
κόλασιν. Αὐτὴν γὰρ ὁ ἄγγελος τὴν σφαλεῖσαν φωνὴν σωφρονίζει, αὐτὴν χαλινοῖ τὴν
τολμήσασαν γλῶτταν. Ἦν γάρ φησι διανεύων καὶ ἔμεινε κωφός. Καλῶς καὶ τὸ ἔμεινεν ὁ
εὐαγγελιστὴς συνεγράψατο· περιέμενε γὰρ ἡ σιωπὴ τὴν τικτομένην φωνήν, ὁ Ζαχαρίας
τὸν Ἰωάννην, ὁ πρεσβύτης τὸν παῖδα, ὁ ἱερεὺς τὸν προφήτην. Ἀμέλει τὸ βρέφος
τίκτεται καὶ ἡ φωνὴ τοῦ πατρὸς ἐπανίσταται. Ἐρωτώμενος γάρ φησιν ὁ Ζαχαρίας τί ἂν
θέλῃ τὸ παιδίον καλεῖσθαι, ᾔτησε πινακίδιον καὶ ἔγραψε λέγων· Ἰωάννης ἐστὶ τὸ ὄνομα
αὐτοῦ. Ὁ μὴ πιστεύσας τοῦ ἀγγέλου τοῖς λόγοις νῦν ἠναγκάσθη τὴν ὀπτασίαν ἐγγράφως
κηρύττειν· ὁ μὴ δεξάμενος τότε ἀκοῇ τὰ ῥηθέντα νῦν νομοθετεῖ καὶ τῇ χειρὶ τὰ
γινόμενα. Ὅθεν φησὶν ἐθαύμασαν πάντες· ἀνεῴχθη γὰρ τὸ στόμα αὐτοῦ παραχρῆμα καὶ
ἡ γλῶσσα καὶ ἐλάλει εὐλογῶν τὸν θεόν. Ὢ τοῦ ξένου θαύματος. Ὄνομα γράφεται
τέκνου καὶ στόμα τοῦ πατρὸς κωφὸν διανοίγεται, Ἰωάννης καλεῖται καὶ τοῦ καλοῦντος
ἡ γλῶσσα ῥυθμίζεται. Ὁρᾷς ὡς οὐ διημάρτομεν ἀπαραίτητον εἰπόντες καὶ ἡμεῖς τὴν
παροῦσαν ἐξήγησιν. Ἰδοὺ αὐτὸ τοῦ δικαίου τὸ ὄνομα καὶ στόμα σιωπῶν διανοίγει καὶ
γλῶσσαν δυσκίνητον διεγείρει. Οὕτως γὰρ καὶ δείκνυται βοῶσα ἡ τοῦ Ἰωάννου φωνή,
ὅτι καὶ αὐτὸ τὸ ὄνομα φωνῆς ἐνέργεια δείκνυται. Ἐγώ φησι φωνὴ βοῶντος ἐν τῇ
ἐρήμῳ· ἑτοιμάσατε τὴν ὁδὸν κυρίου, εὐθείας ποιεῖτε τὰς τρίβους αὐτοῦ. Ὢ τοῦ
θαύματος. Λόγος ἐπιδημεῖ καὶ φωνὴ προκηρύττει, δεσπότης ἔρχεται καὶ δοῦλος
προαποστέλλεται, βασιλεὺς παραγίνεται καὶ στρατιώτης προετοιμάζεται. Ἀλλὰ τί νῦν
προλαβὼν τὸ τῆς ἐρήμου κήρυγμα λέγω ἔχων ἑτέραν ξένην τινὰ καὶ μεγάλην τῶν
προοιμίων φωνήν; Ἔργα ἐστὶ πρῶτα ῥημάτων ἀνώτερα. Ἔτι γὰρ οἰκῶν ὁ βαπτιστὴς ἐν
τῇ γαστρὶ τῆς μητρός, ἔτι τοῖς τῆς φύσεως δεσμοῖς κατεχόμενος, ἔτι τοῖς μητρικοῖς
σπαργάνοις ἐνειλημένος, σκιρτήματι τὴν τοῦ δεσπότου βοᾷ παρουσίαν. Ἐγένετο γάρ
φησιν ὡς ἤκουσε τὸν ἀσπασμὸν τῆς Μαρίας ἡ Ἐλισάβετ, ἐσκίρτησε τὸ βρέφος ἐν τῇ
κοιλίᾳ αὐτῆς. Βλέπε τοῦ μυστηρίου τὸ ξένον· οὔπω γεννᾶται τὸ βρέφος καὶ ἐν τῇ κοιλίᾳ
αὐτῆς τῷ σκιρτήματι φθέγγεται, οὔπω βοᾶν συγχωρεῖται καὶ δι' ἔργων ἀκούεται, οὔπω
μανθάνει τὸν βίον καὶ τὸν θεὸν προκηρύττει, οὔπω γινώσκει τὴν κτίσιν καὶ τὸν
δημιουργὸν προμηνύει, οὔπω βλέπει τὸ φῶς καὶ γνωρίζει τὸν ἥλιον, οὔπω τίκτεται καὶ
προτρέχειν ἐπείγεται.
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

Οὐ γὰρ φέρει παρόντος τοῦ δεσπότου κατέχεσθαι, οὐ περιμένει τοῦ τόκου τοὺς
ὅρους, οὐ τὸν καιρὸν τοῦ κηρύγματος, ἀλλὰ ῥῆξαι φιλονεικεῖ καὶ τὸ τῆς γαστρὸς
δεσμωτήριον, προμηνύσαι σπουδάζει τὸν δεσπότην ἐρχόμενον. «Ὁ λύων» φησὶ «τοὺς
δεσμοὺς παρεγένετο, καὶ τί ἐγὼ δεδεμένος κατέχομαι; Ἦλθεν ὁ τῷ λόγῳ κατασκευάζων
τὰ σύμπαντα, καὶ τί ἔτι ἐγὼ περιμένω τοὺς νόμους τῆς φύσεως;» «Ἐξέλθω» φησί,
«προδράμω, κηρύξω, βοήσω· Ἴδε ὁ ἀμνὸς τοῦ θεοῦ ὁ αἴρων τὴν ἁμαρτίαν τοῦ κόσμου».
Ἀλλὰ τί τῷ θερμῷ ζήλῳ τοῦ βρέφους καὶ ὁ ἡμέτερος σήμερον συνεκτείνεται λόγος;
Ἐπίσχωμεν δέ, εἰ δοκεῖ, νῦν τέως καὶ τὴν γλῶσσαν ἀρκούντως μετὰ τοῦ βρέφους
σκιρτήσασαν. Καλὸν γὰρ συμμετρεῖν καὶ τῷ καιρῷ τὴν ἐξήγησιν καὶ ἐντρυφᾶν μὲν ἤδη
τοῖς ὁπωσοῦν εἰρημένοις, περιμένειν δὲ σὺν θεῷ καὶ τὴν τῶν καθεξῆς θεωρίαν. Οὔτε
γὰρ τοὺς ὅρους τοῦ τόκου ὁ βαπτιστὴς ἐβιάσατο τῷ σκιρτήματι, ἀλλ' ἔμεινε καὶ αὐτὸς
τὸν προσήκοντα χρόνον, εἰδὼς ὅτι καὶ τοῦτον δι' ἡμᾶς μετ' αὐτὸν ὁ δεσπότης πληρώσει·
ᾧ πρέπει ἡ δόξα εἰς τοὺς αἰῶνας τῶν αἰώνων. Ἀμήν.

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

3

