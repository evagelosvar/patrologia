De sancta Thecla martyre
Σῶσόν με ἐκ πάντων τῶν διωκόντων με καὶ ῥῦσαί με, μήποτε ἁρπάσῃ ὡς
λέων τὴν ψυχήν μου, μὴ ὄντος λυτρουμένου μηδὲ σῴζοντος." Καὶ ταχεῖα ἡ τῆς
κόρης βοήθεια· ἀφανοῦς γὰρ γενομένης ἐξαίφνης, ὁ μὲν μνηστὴρ ἀπῄει ἓν μόνον
κερδάνας ἀσελγείας ἱπποδρομίαν. Τῷ νυμφίῳ δὲ ἡ νύμφη παρειστήκει που
ψάλλουσα· "∆ικαίως ἡ βοήθειά μου παρὰ τοῦ θεοῦ τοῦ σῴζοντος τοὺς εὐθεῖς τῇ
καρδίᾳ." Μάθωμεν δὴ καὶ ἡμεῖς τοὺς κατὰ τῶν σαρκικῶν τῆς ἐγκρατείας ἀγῶνας,
ἐπιστρατεύσωμεν ἡδοναῖς ὡς ἡ μάρτυς, ὑπὲρ σαρκὸς κατὰ σαρκὸς ὁπλισώμεθα, τῇ
τῆς ἀναστάσεως τὰ μέλη προευτρεπίσωμεν δόξῃ, μὴ κρατώμεθα τέρψεσι τὰ τῆς
γεέννης ὀδυνηρὰ προξενούσαις, φεύγωμεν τὸ ἡδύ, ἵνα μὴ τὸ ὄντως ἡδὺ ἀφ' ἡμῶν
δραπετεύσῃ. Ἡ χήρα τῷ τεθνεῶτι συνοίκῳ τηρείτω τὴν εὔνοιαν ὡς πάλιν εἰς ζωὴν
φανουμένῳ. Ἡ γάμῳ συζῶσα πρὸς ἀλλοτρίας ἡδονὰς νενεκρώσθω. Ἡ σηπομένη
πορνείαις ἡδονῇ τὴν φλόγα γαμικῇ δροσιζέτω. Ἡ παρθένος ὡραιότερον νυμφίον τοῦ
Χριστοῦ μὴ ζητείτω. Ἕκαστος τὴν κατὰ τάξιν οἰκείαν καθαρότητα σῴζων
προξενείτω τὴν τοῦ δεσπότου Χριστοῦ τῇ ψυχῇ θεωρίαν· "Μακάριοι" γάρ, φησίν, "οἱ
καθαροὶ τῇ καρδίᾳ, ὅτι αὐτοὶ τὸν θεὸν ὄψονται"· αὐτῷ ἡ δόξα εἰς τοὺς αἰῶνας τῶν
αἰώνων. Ἀμήν.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

