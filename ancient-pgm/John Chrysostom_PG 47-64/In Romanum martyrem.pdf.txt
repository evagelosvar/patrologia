In Romanum martyrem
ΕΙΣ ΤΟΝ ΑΓΙΟΝ ΜΑΡΤΥΡΑ ΡΩΜΑΝΟΝ. Λόγος βʹ.
50.611
αʹ. Παλαῖστραι μὲν σώμασιν ἀνδρείαν χαρίζονται, καὶ τέχνης ἀθλητικῆς
ἐπιστήμας· μαρτύρων δὲ μνῆμαι ταῖς τῶν δαιμόνων τὰς ψυχὰς ἀνθοπλίζουσι
τέχναις, καὶ πρὸς τὰς κατ' ἐκείνων παιδοτριβοῦσι λαβάς. Τὸν γὰρ ἀθλητικὸν
δημοσιεύουσαι τόνον καὶ τὴν ἀνένδοτον πρὸς τὰς μάστιγας πάλην, ἐγείρουσιν εἰς
50.612 θάρσος τὴν εὐσέβειαν, ὥσπερ ἐν σκάμμασι ταῖς τῶν παθῶν διηγήσεσιν ὑπὸ
μάρτυρος παντὸς τεταμένον προθεῖσαι δίαυλον. Τοιαύτη ἡ τοῦ σήμερον στεφανω
θέντος ἀθλητοῦ μνήμη. Τίς γὰρ οὐκ ἂν τὰ κατὰ τοῦ διαβόλου θαῤῥήσειε σκάμματα
τοῖς τοῦ μάρτυρος τὴν ψυχὴν παιδοτριβήσας παλαίσμασιν, ὃν τοσούτων κιν δύνων
οὐ κατέσεισε πλῆθος; Πολλὴ γάρ τις τῷ κόσμῳ 50.613 τῆς ἀσεβείας τότε τυραννὶς
ἐνωρχεῖτο, καὶ θάλασσαν ἐκ βυθῶν ἐμιμεῖτο σειομένην ὁ βίος, καὶ τοῦ πελάγους εἰς
τὴν γῆν αἱ τρικυμίαι μετέστησαν, καὶ σφοδρὸν τὸ τῆς εὐσεβείας σκάφος δυσσεβείας
περιήντλει κλυ δώνιον, ἐν ᾧ πολλοὶ μὲν νεκροὶ πανταχοῦ κυβερνῆται, ναυτῶν δὲ
οὐκ ὀλίγος ἀριθμὸς ὑποβρύχιος· πάντα δὲ ἦν φόβων πικρῶν καὶ ναυαγίων
ἀνάμεστα.
Ἔπνεον βασιλεῖς καταιγίδος σφοδρότερον, φοβερὰς τύραννοι τρικυμίας
ἐπῆγον, ἀρχόντων συγκεκίνηντο θρόνοι, δικασταὶ τὴν εἰς Χριστὸν ἐκήρυττον
ἄρνησιν, πικρὰς νομοθέται τιμωρίας ἠπείλουν, πρὸς τὰς τῶν δαιμόνων ἄνδρες
θυσίας ἡρπάζοντο, πρὸς τὴν τῶν βω μῶν εἵλκοντο βδελυρίαν γυναῖκες· ἐσύροντο
πρὸς ταύ την καὶ παρθένοι τὴν λύσσαν, φυγαῖς ἱερεῖς καὶ σφα γαῖς παρεδίδοντο, τῶν
ἱερῶν ἠλαύνοντο οἱ πιστοὶ πε ριβόλων. Πρὸς τηλικαύτην ὁ μάρτυς ἐξωπλίζετο μά
χην, καὶ τοσούτοις τὴν γνώμην κινδύνοις ἀνθίστησιν, ὥσπερ τινὰ σκιαμαχίαν γελῶν
τὴν παράταξιν, καὶ καθάπερ ἐν σκάμμασιν ἀντὶ κόνεως τῇ πίστει τοὺς δικαστὰς
καταπάσσων, οὕτω τὴν τοῦ τότε δικάζοντος ἀνεῤῥίπιζε γνώμην, δρόμον αὐτῷ κατὰ
τῆς Ἐκ κλησίας μελετώμενον στήσας. ∆ιὸ δὴ πρὸς κόλασιν ὀξέως ὁ γενναῖος
ἀνήρπαστο, καὶ πολλαὶ μὲν προσ ήγοντο τιμωριῶν ποικιλίαι· ὁ μάρτυς δὲ προσεῴκει
κιθάρᾳ, τῷ τῶν βασάνων πλήκτρῳ πρὸς φθόγγον κι νούμενος· συνέτριβον τὸ σῶμα
περιστάντες οἱ δή μιοι, ὁ δὲ, ὡς χαλκός τις, τῆς εὐσεβείας τὸ μέλος ὑπήχει
κρουόμενος· κρεμῶντες ἐπὶ ξύλου κατέξαινον, ὁ δὲ ὡς δένδρον ζωῆς τὸ ξύλον
ἠσπάζετο· ὡς πλευρὰς τοῦ δικαίου τὰς παρειὰς διεσπάραττον, ὁ δὲ ὡς πλείο να
στόματα λαμβάνων ἐῤῥητόρευεν, καὶ καινῷ κατ ῄσχυνε τὸν ἀντίπαλον πτώματι. Ὡς
γὰρ εἶδε τὸν δικαστὴν πρὸς τὴν τῶν δαιμόνων σπουδὴν αὐτὸν ἐκκαλούμενον, αἰτεῖ
βρέφος ἐξ ἀγορᾶς ἀχθῆναι, ὡς ἐκεῖνο τῶν ζητουμένων παρὰ τοῦ δικα στοῦ κριτὴν
ποιησόμενος· καὶ προσαχθέντι τῷ βρέφει τὴν περὶ τῶν προκειμένων προσῆγεν
ἐρώτησιν. Τέ κνον, φησὶ, δίκαιόν ἐστι τὸν Θεὸν προσκυνεῖν, ἢ τοὺς θεοὺς τοὺς
λεγομένους ὑπὸ τούτων;
Πολλὴ τῆς τοῦ μάρτυρος σοφίας ἡ περιουσία· τοῦ δικαστοῦ δικαστὴν τὸ
παιδίον καθίζει· τὸ δὲ ὑπὲρ τοῦ Χριστοῦ ταχέως ἀπεφήνατο ψῆφον, ἵνα δειχθῇ καὶ
παιδία δυσσεβούν των δικαστῶν συνετώτερα, μᾶλλον δὲ ἵνα φανῇ μὴ μόνον μάρτυς
ὁ μάρτυς, ἀλλὰ καὶ μαρτύρων ἀλεί πτης. Οὐδὲ τοῦτο δὲ ὅμως τὴν δικαστικὴν
ὑπεσκέλισε λύσσαν, ἀλλ' εὐθὺς ἐπὶ ξύλου μετὰ τοῦ βρέφους ὁ μάρτυς ἡρπάζετο, καὶ
τὴν τοῦ ξύλου κόλασιν εἱρκτὴ διεδέχετο, κἀκείνην ψῆφος ποικίλας τοῖς ἀθληταῖς
τὰς τιμωρίας μερίζουσα· τὸ μὲν γὰρ βρέφος θανάτῳ, τὸν δὲ μάρτυρα τῇ τῆς γλώττης
ἐκτομῇ κατεδίκασε. Τίς τρόπον κρίσεως τοιοῦτον ἀκήκοε; μαστίζουσι δι κασταὶ
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

καταδίκους, ἃ συνίσασιν ἑαυτοῖς, ὁμολογεῖν ἀναγκάζοντες, ἐκτέμνει δὲ τὴν
γλῶσσαν ὁ τῆς ἀσε βείας κριτὴς, ἃ σύνοιδεν ἑαυτῷ σιγᾷν βιαζόμενος. Ὢ δεινοῦ
κακοτεχνίας εὑρήματος. Φρονοῦσαν, φησὶ, τὰ τοῦ Χριστοῦ τὴν ψυχὴν οὐκ ἐξέστησα,
κἂν τὴν λαλοῦ σαν ὑπὲρ Χριστοῦ διακόψω γλῶτταν. Ἔκκοψον, ὦ τύραννε,
γλῶσσαν, ἵνα μάθῃς τὴν φύσιν καὶ ἄγλωσσον ὑπὲρ Χριστοῦ ῥητορεύουσαν·
ἐκκόλαψον τὴν γλῶσσαν τοῦ στόματος, ἵνα μάθῃς ἀληθῆ τὸν τὰ χαρίσματα τῶν
γλωσσῶν ὑποσχόμενον. Τὸ μὲν γὰρ τῆς γλώσσης ἐξ έκοψεν ὄργανον, ὁ λόγος δὲ
ἐξώρμα σφοδρότερον, 50.614 ὥσπερ ἐμποδίου τινὸς ἀπηλλαγμένος τῆς γλώττης·
θέαμα καινὸν καὶ παράδοξον· σάρκινος σαρκίνοις ἀσάρκως φθεγγόμενος. Πρέπον
ἄρα τῷ μάρτυρι τοῦ προφήτου τὸ μέλος· Ἐπλήσθη χαρᾶς τὸ στόμα ἡμῶν, καὶ ἡ
γλῶσσα ἡμῶν ἀγαλλιάσεως· ἐπλήσθη τὸ στόμα χαρᾶς, καινὸν θῦμα τῷ Χριστῷ
προσκομί σαν τὴν γλῶσσαν· ἔσχεν ἡ γλῶσσα πολλὴν ἀγαλλίασιν, πρόδρομος μάρτυς
ὀφθεῖσα τοῦ μάρτυρος. Ὢ γλώσ σης πρὸς τοὺς τῶν μαρτύρων δήμους τὴν ψυχὴν προ
λαβούσης· ὢ στόματος κρυπτόμενον γεννήσαντος μάρτυρα· ὢ γλώσσης
θυσιαστήριον κτησαμένης τὸ στόμα· ὢ στόματος ἱερεῖον κτησαμένου τὴν γλῶσσαν·
ναὸν ἔχων ἐλελήθεις, ὦ γενναῖε, τὸ στόμα, ναὸν ἐν ᾧ τὴν σύναιμον γλῶσσαν
παράδοξον ἀμνάδα κατέθυσας.
βʹ. Τίς ἄρα σου τὰς ἀρετὰς ἀξίως στεφανώσειε ῥήτωρ; Ἔλαβες παρὰ τῆς
φύσεως γλῶσσαν, σὺ δὲ αὐτὴν ἐξέθρεψας μάρτυρα· ἔλαβες στόμα φυλακτήριον γλώσ
σης, σὺ δὲ τῇ γλώσσῃ τὸ στόμα βωμὸν κατεσκεύα σας· ἔλαβες εἰς τὸ φθέγγεσθαι
πλῆκτρον, σὺ δὲ αὐτὸ τεμνόμενον ἄσταχυν ἀνέδειξας· ἔλαβες γλῶσσαν ῥη μάτων
διάκονον, σὺ δὲ αὐτὴν ἄμωμον τῷ Χριστῷ κατέθυσας πρόβατον. Τίνι σου τὴν
γλῶσσαν πρεπούσῃ προσηγορίᾳ τιμήσω; ποίῳ σου κοσμήσω τὴν γλῶσ σαν ὀνόματι;
Προσῆγον ταύτῃ τὸν σίδηρον δήμιοι, ἡ δὲ κατὰ τὸν δεδεμένον Ἰσαὰκ οὐκ ἐσκίρτα,
ἀλλ' ὡς ἐν βωμῷ κειμένη τῷ στόματι τὴν πληγὴν σὺν ἡδονῇ προσεδέχετο,
διδάσκουσα τῶν ἀνθρώπων τὰς γλώσ σας, ὡς οὐ φθέγγεσθαι μόνον διὰ Χριστὸν,
ἀλλὰ καὶ σφάττεσθαι δεῖ. Τῆς τοῦ πατριάρχου θυσίας ἥρπασας, γενναῖε, τὸ
φιλότιμον, μονογενὲς βλάστημα γλώττης ἀντὶ τέκνου μονογενοῦς προσκομίσας.
Καλῶς ἄρα σοι δευτέραν ὁ Χριστὸς ἀντεφύτευσε γλῶσσαν· εὗρε γὰρ ἀγαθόν σε τῆς
προτέρας γεωργόν· καλῶς σοι τὴν ἄσαρκον ἐχαρίσατο γλῶσσαν· ἀγγελικῷ γὰρ οὐκ
ἔπρεπε φρονήματι σάρκινος· καλῶς σοι τὴν τῆς γλώσ σης ἀντιμισθίαν ἀπέδωκεν.
Ἔχρησάς σου τῷ ∆εσπότῃ πρὸς θυσίαν τὴν γλῶσσαν, ὁ δὲ αὐτῇ πρὸς ῥητορείαν
φωνὴν ἀντεδάνεισε· καὶ γέγονε τῆς γλώσσης καὶ τοῦ Χριστοῦ πρὸς ἀλλήλους
συνάλλαγμα, τῆς μὲν γλώσ σης ὑπὲρ Χριστοῦ τεμνομένης, τοῦ Χριστοῦ δὲ ὑπὲρ τῆς
γλώσσης λαλοῦντος. Ποῦ νῦν ἡμῖν Μακεδόνιος, ὁ τῷ δεδωκότι τὸ χάρι σμα τῶν
γλωσσῶν Παρακλήτῳ μαχόμενος;
Ὅτι δὲ οὐ ψεύδομαι τῇ τοῦ Παρακλήτου θεότητι τὰς τῶν χαρισμάτων δωρεὰς
περιάπτων, μάρτυς ἡμῖν ὁ μα κάριος Παῦλος ὁ νῦν πρὸς τὴν ὑμετέραν φιληκοΐαν
βοῶν· Πάντα ταῦτα ἐνεργεῖ ἓν καὶ τὸ αὐτὸ Πνεῦ μα, διαιροῦν ἰδίᾳ ἑκάστῳ, καθὼς
βούλεται. Κα θὼς βούλεται, φησὶν, οὐ καθὼς ἐπιτάττεται. Ἀλλ' ἵνα μή τινας πρὸς
τούτοις ἐπιθέντες προσθήκας τῷ πλή θει τὴν ὑμετέραν καταχώσωμεν μνήμην,
ταύτην οὖσαν ἰσχυρὰν τὴν φωνὴν ὑπὲρ τοῦ Παρακλήτου μεμνημένοι οὕτως
ἐντεῦθεν ἐξέλθωμεν, μεγάλα μὲν κατ' ἐκείνων φρονοῦντες, συγγινώσκοντες δὲ
τέως 50.615 αὐτοῖς πλανωμένοις, καὶ τὴν τοῦ Παρακλήτου προσ κυνοῦντες θεότητα.
Ἡ μὲν οὖν προφητικὴ σάλπιγξ τὴν οἰκουμενικὴν εἰς Χριστὸν προσημαίνουσα συμ
φωνίαν ἔλεγεν, Ὅτι εἰδήσουσί με, φησὶν, ἀπὸ μι κροῦ αὐτῶν καὶ ἕως μεγάλου
αὐτῶν, Καὶ πᾶσα γλῶσσα ἐξομολογήσεται τῷ Θεῷ τῷ ἀλη θινῷ. Καὶ ὁ μὲν προφήτης,
ὡς ἔφην, ἅπασαν γλῶσ σαν τῷ τῆς θεογνωσίας περικλείει δικτύῳ· ἡμεῖς δὲ σήμερον
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

καὶ ἀγλώττου ῥήτορος ἀκουσόμεθα τῇ εὐ σεβείᾳ συνηγοροῦντος· οἱονεὶ γάρ τις
κιθάρα πλῆ κτρον οὐκ ἔχουσα τὸν δημιουργὸν εὐφημεῖ. Λεγέτω τοίνυν καὶ ὁ
μακάριος Ῥωμανός· Ἡ γλῶσσά μου κάλαμος γραμματέως ὀξυγράφου. Ποία γλῶσσα;
Οὐχ ἣν ὁ σίδηρος ἀφεῖλεν, ἀλλ' ἣν ἡ τοῦ Πνεύματος χάρις ἐχάλκευσε· τῆς γλώσσης
γὰρ συληθείσης ἡ τοῦ Πνεύματος χάρις ἀντεισήχθη. Εἶχον καὶ οἱ ἀπό στολοι
γλώσσας, ἀλλ' ἵνα δειχθῇ τοῦ ἐνεργοῦντος ἡ δύναμις, ὁ πηλὸς ἤργει, καὶ τὸ πῦρ τὸ
οὐράνιον ἐφθέγγετο. Εἶχε καὶ ἡ Μωσαϊκὴ Γραφὴ τοῦ ὑπὲρ λόγον πράγματος τὴν
εἰκόνα· βάτος γὰρ παρ' ἐκείνῳ καὶ πῦρ. Καὶ τὸ ἀποστολικὸν πῦρ τὰς τοῦ κηρύγμα τος
φωνὰς ἐπὶ τῆς βάτου προετύπου, καὶ φωνὴν τῷ ἀψύχῳ χαρίζεται, ἵνα τῶν ἐμψύχων
ὀργάνων ἁψά μενον πιστευθῇ. Εἰ γὰρ τὸ ἄψυχον ἡ ἁφὴ τοῦ πυρὸς ἔμφωνον
ἀπειργάσατο, πῶς οὐκ ἔμελλεν ἀναπλῆσαί που ψυχὰς λογικὰς δι' ἁφῆς τὸ
παναρμόνιον ἀνακρούε σθαι μέλος;
Ταύτης μετέσχε τῆς χάριτος καὶ Ῥωμα νὸς ὁ ἀοίδιμος, καὶ τὴν γλῶτταν
ἀποτμηθεὶς τρανο τέρῳ φθόγγῳ τὸν τύραννον ἤλεγχεν. Οὐκ ἂν δὲ ὅλως ὁ τύραννος
ἐπὶ τὴν τῆς γλώττης ἔδραμεν ἐκτομὴν, εἰ μὴ τῶν ἐλέγχων ἐφοβήθη τὰ ῥεύματα, εἰ
μὴ τὸν χειμάῤῥουν τοῦ κηρύγματος κατεπλάγη, εἰ μὴ τὰ κύματα τῆς εὐαγγελικῆς
ῥητορείας στορέσαι ὑπέλα βεν. Ἀλλ' ἴδωμεν, ὅθεν ὁ τύραννος εἰς ἀνάγκην ἔρχε ται
τῆς τοιαύτης τόλμης.
γʹ. ∆αίμοσί ποτε θύσας ὁ δυσσεβὴς, καὶ καπνοῦ καὶ κνίσσης ἀνάπλεως
γεγονὼς, καὶ ταῖς σταγόσι τῆς ἀσεβείας μεμολυσμένος, δρομαίως ἐπὶ τὴν ἐκκλη σίαν
ἐχώρει, καὶ τὸν ᾑμαγμένον ἐπιφερόμενος πέ λεκυν, τὸ ἀναίμακτον θυσιαστήριον
πρὸς ἱερουργίαν ἀθέμιτον ἐπεζήτει. Ἀλλ' οὐκ ἔλαθεν ἡ τοῦ τυράννου λύττα τὸν
μάρτυρα. Εὐθέως γοῦν ἐκπηδήσας εἰς τὰ προπύλαια, φερομένην ἤδη τῆς ἀσεβείας
τὴν ἐπί κλυσιν ἀναστέλλει· καὶ καθάπερ τις εὐμήχανος κυ βερνήτης τὴν θάλατταν
βλέπων κατὰ τῆς πρώρας ἐπιφερομένην, ἠρεμεῖν οὐκ ἀνέχεται, ἀλλὰ τὸ σκάφος
ὅλον κούφῳ ποδὶ διατρέχει, καὶ διὰ τοῦ πηδαλίου τὴν πρύμναν ἐγείρας, ἵστησι τὸ
πλοῖον τοῖς κύμασιν ἀντιπρόσωπον, καὶ μετεωρίσας τὸ κινδυνεῦον, μέσην διασχίζει
τὴν τρικυμίαν, καὶ τέχνῃ τινὶ τὸ κυρτωθὲν πέλαγος αὐλακίζει· τοῦτο καὶ ὁ μακάριος
Ῥωμανὸς ἐποίησε.
Τῆς εἰδωλικῆς θαλάττης βλάσφημα μυκωμένης, καὶ κατὰ τοῦ
ἐκκλησιαστικοῦ σκάφους λυττώσης, καὶ ἀφρὸν αἱμάτων κατὰ τῶν θυσιαστηρίων
ἐρευγομένης, μόνος ἀνθοπλίζεται τῇ μαινομένῃ θαλάττῃ, καὶ εἰς ἄκρον ὁρῶν τὸ
σκάφος σφαλλόμενον, ἀφυπνίζει τὸν ἐν τῷ πλοίῳ ∆εσπότην, ἀφυπνίζει δὲ αὐτὸν
τὸν τῆς μακροθυμίας ὕπνον καθεύδοντα. Βλέπει τὸ πέλαγος ταῖς ἀντιπνοίαις
χειμαζόμενον, καὶ τὰ τῶν κινδυνευόντων μαθητῶν φθέγγεται ῥήματα· Ἐπιστάτα,
σῶσον, ἀπολλύμεθα· πειραταὶ τὸ σκάφος περιστοιχίζονται, λύκοι πολιορκοῦσι τὸ
ποίμνιον, λῃσταὶ τὴν παστάδα τὴν σὴν διορύττουσι, μοιχικὰ συρίγματα τὴν σὴν
νύμφην περικτυπεῖ, πάλιν ὁ 50.616 ὄφις τοιχωρυχεῖ τὸν παράδεισον· ὁ τῆς
Ἐκκλησίας θεμέλιος ἡ πέτρα σαλεύεται· ἀλλ' ἐξ οὐρανοῦ τὴν εὐ-αγγελικὴν ἄγκυραν
ῥῖψον, καὶ τὴν πέτραν στήριξον σειομένην· Ἐπιστάτα, σῶσον, ἀπολλύμεθα. Ὁ κοινὸς
κίνδυνος μερίζει τὸν μάρτυρα, καὶ πρὸς τὸν ∆εσπότην παῤῥησιάζεται· ἐπαφίησι
ῥέουσαν κατὰ τοῦ τυράννου τὴν γλῶτταν, στῆσον, λέγων, τὸν ἐμμανῆ τοῦτον
δρόμον, ὦ τύραννε· ἐπίγνωθι τῆς σῆς ἀσθενείας τὰ μέτρα, αἰδέσθητι τοῦ
σταυρωθέντος τοὺς ὅρους· ὅροι δὲ τοῦ σταυρωθέντος οὐ τῆς ἐκκλησίας οἱ τοῖχοι,
ἀλλὰ τῆς οἰκουμένης τὰ πέρατα· ἀποτίναξαι τὴν ἀχλὺν τῆς μανίας· βλέψον εἰς γῆν,
καὶ τὸ τῆς φύσεως τῆς σῆς ἀσθενὲς ἐνθυμήθητι· ἀνάβλεψον εἰς τὸν οὐρανὸν, καὶ τοῦ
πολέμου τὸ μέγεθος λόγισαι· τῶν δαιμόνων τὴν ἀσθενῆ διάπτυσον συμμαχίαν·
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

3

σκόπησον, ὅτι τῷ σταυρῷ πεπληγότες οἱ δαίμονές σε προστάτην τῶν βωμῶν τῶν
οἰκείων προβάλλονται.
Τί διώκεις ἀκατάληπτα; τί τοξεύεις ἀπέραντα; μὴ γὰρ τοίχοις ὁ Θεὸς
περιγράφεται; ἀπερίγραπτον τὸ θεῖον· μὴ γὰρ ὀφθαλμοῖς ὁ ἡμέτερος ∆εσπότης
ὁρᾶται; ἀθεώρητος γάρ ἐστι καὶ ἀνείδεος τῇ οὐσίᾳ, κατὰ δὲ τὸ ἀνθρώπινον γράφεται
καὶ ὁρᾶται. Μὴ γὰρ λίθον καὶ ξύλον οἰκεῖ, πιπράσκων ἀντὶ βοὸς καὶ προβάτου τὴν
πρόνοιαν; μὴ γὰρ βωμὸς τοῖς συναλλάγμασιν αὐτοῦ μεσιτεύει; Αὕτη τῶν σῶν
δαιμόνων ἡ λίχνος προσαίτησις· ὁ ἐμὸς ∆εσπότης, μᾶλλον δὲ ὁ τῶν ὅλων ∆εσπότης,
Χριστὸς οὐρανὸν οἰκεῖ, καὶ κόσμον ἡνιοχεῖ, καὶ θυσία τούτῳ ψυχὴ πρὸς αὐτὸν
ἀνανεύουσα, μία τούτῳ τροφὴ, τῶν πιστευόντων σωτηρία. Παῦσαι κατὰ τῆς
Ἐκκλησίας τὰ ὅπλα κινῶν· ἐπὶ γῆς τὸ ποίμνιον, καὶ ὁ ποιμαίνων ἐν οὐρανῷ· ἐπὶ γῆς
αἱ κληματίδες, καὶ ἐν οὐρανῷ ἡ ἄμπελος· ἂν δὲ ἐκτέμῃς τὰς κληματίδας,
πολυπλασιάζεις τὴν ἄμπελον. Λύθρου γέμουσιν αἱ χεῖρές σου, τὸ ξίφος τὸ σὸν ἀπὸ
θυμάτων ἐστὶν ἀλόγων· φεῖσαι τῶν ἀναιτίων θρεμμάτων, καὶ ἡμῖν τοῖς ἐλέγχουσιν
ἔπαφες τὸν σίδηρον· φεῖσαι τῶν σιωπώντων ἀλόγων, καὶ ἡμᾶς τοὺς κατηγοροῦντας
ἀπόσφαττε. Οὐ γὰρ οὕτω φοβοῦμαι τὸν ἀνδροφόνον σίδηρον, ὡς τὸν ἐπιβώμιον
πέλεκυν· ὁ ἀνδροφόνος σίδηρος τὸ σῶμα διασπαράττει, ὁ ἐπιβώμιος πέλεκυς τὴν
ψυχὴ ἀναιρεῖ· ὁ ἀνδροφόνος σίδηρος τὸ θυόμενον κατασφάττει, ὁ δὲ ἐπιβώμιος
πέλεκυς καὶ τὸ θυόμενον καὶ τὸν θύοντα συναπόλλυσι. Τέμε τὸν αὐχένα τὸν ἐμὸν,
καὶ μὴ μολύνῃς θυσιαστήριον· ἔχεις αὐτοκέλευστον ἱερεῖον, τί τὸν δεσμώτην ταῦρον
ἀνανεύοντα συμποδίζεις; Εἰ θύειν ἐπιθυμεῖς, ἐν τοῖς τῆς ἐκκλησίας προθύροις θύε
λογικὸν ἱερεῖον. Οὐ φέρει τὴν ἄμετρον ὁ τύραννος παῤῥησίαν τοῦ μάρτυρος, ἀλλ'
εὐθὺς ἀπὸ τῆς γλώττης τοῦ θύματος ἄρχεται.
Ἐκτέμνει τοίνυν τὴν γλῶτταν, οὐκ ἀνελεῖν βουλόμενος, ἀλλὰ τῷ κηρύγματι
πολεμῶν· οὐ τοσοῦτον τῷ κήρυκι φθονῶν, ὅσον τῷ κηρυττομένῳ βασκαίνων, Ἀλλ'
Ὁ δρασσόμενος τοὺς σοφοὺς ἐν τῇ πανουργίᾳ αὐτῶν, τὸ ἐκτμηθὲν ὄργανον τῆς
φωνῆς ἐξ οὐρανοῦ ἀποδίδωσι, καὶ ἀοράτῳ γλώττῃ τὸν χωλεύοντα φθόγγον
ὑποστηρίζει, καὶ χαρίζεται τῷ ἀγλώττῳ τὴν φωνὴν, ἔργῳ τὴν ἀνθρωπίνην
δημιουργίαν ἐνδεικνύμενος τῷ τυράννῳ, Καὶ καθάπερ οἱ φρεωρύχοι τὰς 50.617
φλέβας διασκαλεύοντες εὐρυτέραν τῷ ὕδατι τὴν ἐκροὴν ἀπεργάζονται, οὕτω καὶ ὁ
τύραννος σιδήρῳ τῆς γλώττης διορύττων τὴν ῥίζαν, σφοδροτέροις ἐπεκλύζετο τοῖς
τῶν ἐλέγχων κρουνοῖς.
Ἐβουλόμην ἄχρι τέλους ἐγχορεῦσαι τῇ τοῦ μάρτυρος ὑποθέσει, ἀλλ' ὁ τῆς
συμμετρίας ἐπέστη καιρὸς, καί μοι σιωπᾷν 50.618 ἐγκελεύεται· ὑμῖν τε γὰρ πρὸς
ὠφέλειαν ἱκανὰ τὰ ῥηθέντα, καὶ ἀναγκαῖα τοῦ πατρὸς τὰ διδάγματα πρὸς τὸν τῶν
ῥηθέντων ἀπαρτισμόν· ἀλλὰ καὶ τὰ ῥηθέντα τοῖς τῆς μνήμης κόλποις
περιπτυξώμεθα, καὶ τοῖς λέγεσθαι μέλλουσι τοὺς τῆς ψυχῆς αὔλακας ἀναπτύξωμεν,
καὶ ἐπὶ πᾶσι τὸν θαυματοποιὸν Χριστὸν προσκυνήσωμεν, ὅτι αὐτῷ ἡ δόξα σὺν τῷ
Πατρὶ καὶ τῷ παναγίῳ Πνεύματι νῦν καὶ ἀεὶ, καὶ εἰς τοὺς αἰῶνας τῶν αἰώνων. Ἀμήν.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

4

