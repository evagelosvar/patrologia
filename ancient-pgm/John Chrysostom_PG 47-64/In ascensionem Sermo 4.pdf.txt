In ascensionem Sermo 4
ΕΙΣ ΤΗΝ ΑΝΑΛΗΨΙΝ Τοῦ Κυρίου ἡμῶν Ἰησοῦ Χριστοῦ. Λόγος δʹ.
52.799
μίας, καὶ φαιδρύνουσα τῶν πιστῶν τὰς καρδίας· φαιδρὰ δὲ καὶ τῆς
προκειμένης ἑορτῆς ἡ ὑπόθεσις· διὰ τί δὲ φαιδρὰ, ἐν τοῖς καθεξῆς ῥηθησομένοις
ὑποδείξομεν. Ἐν ἓξ τοίνυν ἡμέραις ἐποίησεν ὁ Θεὸς πάντα τὰ ἔργα αὐτοῦ, καθὼς
γέγραπται, τῇ δὲ ἑβδόμῃ κατέπαυσε. ∆ιὸ καὶ ἐπ' ἐσχάτου τῶν ἡμερῶν εὐδοκήσας ὁ
τοῦ Θεοῦ Λόγος ζητῆσαι καὶ σῶσαι τὸ ἀπολωλὸς, καὶ ἐνανθρωπήσας, τὸν αὐτὸν
τρόπον κατὰ τὸν ἀριθμὸν τῶν ἡμερῶν τῆς κοσμοποιίας, τὰς ἑορτὰς τῆς ἑαυτοῦ
οἰκονομίας ἡμῖν παρέδωκε. Πρώτη τοίνυν καὶ ῥίζα τῶν ἑορτῶν Χριστοῦ, ἡ κατὰ
σάρκα ἐκ τῆς ἁγίας Παρθένου καὶ θεοτόκου Μαρίας μετὰ τὴν σύλληψιν παράδοξος
γέννησις, ἐν ᾗ παρεγένετο Χριστὸς ὁ Θεὸς κλίνας οὐρανοὺς, ὅπως σώσῃ κόσμον
ἀπολλύμενον· ἐν ᾗ ἄγγελοι τοῖς ποιμέσιν ὀφθέντες, τὸ παράδοξον τοῦ θαύματος
γνωρίζοντες ἐβόων· ∆όξα ἐν ὑψίστοις Θεῷ, καὶ ἐπὶ γῆς εἰρήνη, ἐν ἀνθρώποις
εὐδοκία. ∆ιὸ καὶ δικαίως πρώτη ἑορτὴ προσηγόρευται. ∆ευτέρα δὲ ἑορτὴ, ἡ
ἐπιφάνεια Χριστοῦ τοῦ Θεοῦ ἡμῶν, καθ' ἣν ἐν τῷ Ἰορδάνῃ παραγινόμενος ἔδειξε
πᾶσιν ἀνθρώποις τῆς ἀφάτου αὐτοῦ εὐσπλαγχνίας τὴν συγκατάβασιν· ἐν ᾗ καὶ φωνὴ
Πατρὸς ἐξ οὐρανοῦ ἦλθε, μαρτυροῦσα περὶ αὐτοῦ· Οὗτός ἐστιν ὁ Υἱός μου ἀγαπητὸς,
ἐν ᾧ ηὐδόκησα.
Ἀλλὰ καὶ τὸ Πνεῦμα ἐν εἴδει περιστερᾶς ἐλθὸν ἔμεινεν ἐπ' αὐτὸν, τῆς
ὁμοουσίου Τριάδος δηλαδὴ ἐπιφανείσης ἐν μιᾷ καιροῦ ῥοπῇ· ἧς αὐτόπτης ἄξιος ὁ τοῦ
Κυρίου πρόδρομος γέγονεν Ἰωάννης. Περὶ ἧς ἐπιφανείας καὶ ὁ μακάριος Παῦλος ὁ
ἀπόστολος ἔλεγεν· Ἐπεφάνη ἡ χάρις τοῦ Θεοῦ ἡ σωτήριος πᾶσιν ἀνθρώποις. Ἐπεὶ
οὖν πάντων ἀνθρώπων σωτηρία πέφυκεν, ἀξίως καὶ ἑορτὴ μεγίστη ὑπὸ τῶν
εὐσεβῶν ὀνομάζεται. Τρίτη τῶν ἑορτῶν ἡ ἁγία καὶ προσκυνητὴ καὶ πάνσεπτος τοῦ
σωτηρίου πάθους Χριστοῦ τοῦ ἀληθινοῦ ἡμῶν Θεοῦ ἡμέρα, ἐν ᾗ ἐν σταυρῷ σαρκὶ
προσηλωθεὶς, τὸ καθ' ἡμῶν τῆς ἁμαρτίας ἐξήλειψε γραμματεῖον· ἐν ᾗ καὶ τὸν διὰ
τῆς παρακοῆς ἐκβληθέντα τοῦ παραδείσου Ἀδὰμ διὰ μιᾶς φωνῆς ἐν αὐτῷ εἰσήγαγε.
Φησὶ γὰρ τῷ λῃστῇ· Σήμερον μετ' ἐμοῦ ἔσῃ ἐν τῷ παραδείσῳ. Ὅθεν καὶ ταύτῃ ἑορτὴ
μεγίστη καθέστηκε, καὶ μάλιστα· Παῦλος γὰρ εἴρηκε· Τὸ Πάσχα ἡμῶν ὑπὲρ ἡμῶν
ἐτύθη Χριστός. Τετάρτη ἑορτὴ, ἡ ὑπερένδοξος καὶ εἰρηνοποιὸς ἡμέρα τῆς ἁγίας
Χριστοῦ τοῦ Θεοῦ ἡμῶν ἀναστάσεως, καθ' ἣν καὶ ἐν τοῖς καταχθονίοις γενόμενος,
τοὺς μοχλοὺς τοὺς αἰωνίους συντρίψας, καὶ τὰ δεσμὰ διαῤῥήξας, ἀνέστη ἐκ τῶν
νεκρῶν, νίκην κατὰ τοῦ ᾅδου ἀράμενος, τοὺς ἐν αὐτῷ κατακειμένους συναναστήσας
δικαίους, καὶ πρὸς τοὺς μαθητὰς εἰσελθὼν, τὴν εἰρήνην αὐτοῖς κατεβράβευσεν.
Ὅθεν πάσης ἀντιλογίας ἐκτὸς, μήτηρ αὕτη ἑορτῶν ἡ ἀοίδιμος ἡμέρα τοῖς πιστοῖς
λελόγισται. Πέμπτη τοιγαροῦν ἑορτὴ ἡ ἁγία τοῦ Κυρίου εἰς οὐρανοὺς ἀνάληψις, περὶ
ἧς νυνὶ καὶ τὴν πραγματείαν ποιούμεθα· πέμπτη δὲ τῶν ἑορτῶν ἐστι, καθότι καὶ
πέμπτῃ ἡμέρᾳ τῆς ἑβδομάδος ἐπράχθη.
∆ιὰ τοῦτο δὲ ἑορτάζειν ὀφείλομεν, ἐπειδὴ σήμερον τὴν ἀπαρχὴν τοῦ
ἡμετέρου φυράματος, τουτέστι τὴν σάρκα, ἐν οὐρανοῖς Χριστὸς ἀνήγαγε. ∆ιὸ καὶ ὁ
Ἀπόστολος ἔλεγε· Συνήγειρε καὶ συνεκάθισεν ἡμᾶς ἐν τοῖς ἐπουρανίοις ἐν Χριστῷ
Ἰησοῦ. Ὅθεν ὁ ἀρχέκακος καὶ τῆς ἁμαρτίας εὑρετὴς διάβολος, διὰ τὸν ὄγκον τῆς
ἐπάρσεως καὶ τῆς ὑπερηφανίας τὸν τῦφον, ἐξέπεσεν, ἐκεῖ τῷ μεγέθει τῆς ἑαυτοῦ
φιλανθρωπίας τὸν διὰ τῆς ἐκείνου κακίστης συμβουλίας ἐκ τοῦ παραδείσου
ἐκβληθέντα ἄνθρωπον Χριστὸς ἀντικατέστησε. Τιμῆς οὖν ἀξία καὶ πνευματικῆς
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

χορείας καὶ ἡ παροῦσα ἑορτὴ καθέστηκεν. Ἀλλ' ἐπειδὴ ἑπτὰ ἑορτῶν ἀριθμὸν κατὰ
τὰς ἡμέρας τῆς κοσμοποιίας ἐν ἀρχῇ τοῦ λόγου ἐμνημονεύσαμεν, πέντε δὲ καὶ μόνον
μέχρι τοῦ παρ 52.800 όντος ἑορτὰς ἐδείξαμεν, δέον ἐστὶ καὶ τὰς ἄλλας δύο ἑορτὰς εἰς
μέσον ἀγαγεῖν, εἶθ' οὕτως ἐπὶ τὴν προκειμένην ἱστορίαν τὸν λόγον
χειραγωγήσωμεν. Ἕκτη τοίνυν ἐστὶ τῶν ἑορτῶν ἡ ἕκτη πανεύφημος ἡμέρα τῆς
ἐπιφοιτήσεως τοῦ ἁγίου Πνεύματος, καθ' ἣν τοῖς ἁγίοις ἀποστόλοις ἡ κατ'
ἐπαγγελίαν προσδοκωμένη δωρεὰ τοῦ ἁγίου Πνεύματος ἐν εἴδει πυρίνων γλωσσῶν
παρὰ Χριστοῦ ἐξ οὐρανῶν ἀπεστάλη, ἥτις καὶ Πεντηκοστὴ διὰ τὴν τῶν ἑπτὰ
ἑβδομάδων συμπλήρωσιν ὠνομάσθη.
Ἀναντιῤῥήτως οὖν καὶ αὕτη ἑορτὴ τοῖς εὐσεβέσι μεγίστη καθέστηκεν.
Ἑβδόμη ἑορτὴ ἡ προσδοκωμένη ἡμέρα τῆς ἀναστάσεως τῶν νεκρῶν (τὴν γὰρ ἐλπίδα
ταύτην οἱ πιστοὶ ἀπεκδεχόμεθα), ἐν ᾗ μέλλει ὁ φιλάνθρωπος καὶ ἀπροσωπόληπτος
δικαστὴς Χριστὸς ὁ Θεὸς ἡμῶν ἀποδιδόναι ἑκάστῳ κατὰ τὰ ἔργα αὐτοῦ. Αὕτη ἐστὶν ἡ
ἀληθὴς κατάπαυσις, δι' ἣν καὶ ὁ ἀπόστολος Παῦλος ἔλεγε· Σπουδάσωμεν εἰσελθεῖν
εἰς ἐκείνην τὴν κατάπαυσιν. Τότε τοίνυν καὶ μάλιστα ἑορτὴ χαρᾶς καὶ εὐφροσύνης
καὶ ἀγαλλιάσεως πεπληρωμένη ὑπάρχει τοῖς μέλλουσι κληρονομεῖν, Ἃ ὀφθαλμὸς
οὐκ εἶδε, καὶ οὖς οὐκ ἤκουσε, καὶ ἐπὶ καρδίαν ἀνθρώπου οὐκ ἀνέβη, ἃ ἡτοίμασεν ὁ
Θεὸς τοῖς ἀγαπῶσιν αὐτόν. Ὥστε, ἀγαπητοὶ, εὐξώμεθα καὶ ἡμεῖς συνεορτάσαι τοῖς
δικαίοις ἐν τῷ νυμφῶνι τῆς βασιλείας τῶν οὐρανῶν, οὐ μόνον πρὸς μίαν ἡμέραν, ἢ
δύο, ἢ τρεῖς, ἀλλ' εἰς ἀτελευτήτους αἰῶνας. Τοιγαροῦν τῶν ἑπτὰ ἑορτῶν ἡμῖν
ἀποδειχθεισῶν, ἐπὶ τὸ προκείμενον ἐπανέλθωμεν. Ἔστι δὲ ἡμῖν περὶ τῆς παρούσης
ἑορτῆς τῆς ἐν οὐρανοῖς, λέγω δὲ τῆς τοῦ Χριστοῦ ἀναλήψεως, τὸ διήγημα. ∆αυῒδ μὲν
γὰρ εἴρηκε· Καὶ ἔκλινεν οὐρανοὺς, καὶ κατέβη, καὶ γνόφος ὑπὸ τοὺς πόδας αὐτοῦ·
τὴν ἐξ οὐρανῶν δηλαδὴ τοῦ Λόγου παρουσίαν ἐπὶ γῆς ἐσήμανε. Γνόφον δὲ εἶπεν ὑπὸ
τοὺς πόδας αὐτοῦ, τὸ διὰ σαρκὸς ἔνδυμα τῆς θεότητος γνωρίζων· ὥστε καὶ ἄγνωστος
ἦν τοῖς πολλοῖς ἡ αὐτοῦ παρουσία διὰ τὸ ταπεινὸν καὶ πρᾶον. Πάλιν γὰρ ὁ αὐτὸς
προφήτης λέγει· Κύριε, ἐν τῇ θαλάσσῃ ἡ ὁδός σου, καὶ αἱ τρίβοι σου ἐν ὕδασι
πολλοῖς, καὶ τὰ ἴχνη σου οὐ γνωσθήσονται. Εἰ γὰρ ἔγνωσαν, φησὶν, οὐκ ἂν τὸν
Κύριον τῆς δόξης ἐσταύρωσαν. Καὶ πάλιν εἶπεν· Ἐπέβη ἐπὶ Χερουβὶμ, ἐπετάσθη ἐπὶ
πτερύγων ἀνέμων. Ἐπὶ Χερουβὶμ εἶπε, καθότι αὐτὸς καὶ ἐν οὐρανοῖς σὺν τῷ Πατρὶ
ὢν ἐπὶ τῶν Χερουβὶμ ἐπωχεῖτο· καὶ σὺν ἀνθρώποις γὰρ ἐπὶ γῆς ἀναστρεφόμενος, τοῦ
Χερουβικοῦ καὶ ἐπουρανίου θρόνου οὐδαμῶς ἐχωρίζετο· πτέρυγας δὲ ἀνέμων τὰς
νεφέλας αἰνίττεται, ἐν αἷς καὶ ἀνελήφθη καθὼς καὶ ἐν ταῖς Πράξεσι τῶν ἀποστόλων
γέγραπται· Καὶ νεφέλη ὑπέλαβεν αὐτὸν ἀπὸ τῶν ὀφθαλμῶν αὐτῶν. Εἶτα καθεξῆς
λέγει· Καὶ ὡς ἀτενίζοντες ἦσαν εἰς τὸν οὐρανὸν πορευομένου αὐτοῦ, θαῦμα τοῖς
ἀποστόλοις καὶ ἔκστασις καὶ θροῦς ἐπεγίνετο· θνητοὶ γὰρ τὴν φύσιν ὄντες, καὶ
ἀσυνήθεις τοιούτων θεωριῶν, ἐζήτησαν τῇ διανοίᾳ.
Ἀλλ' ἐρεῖ τις, ὅτι προειδότες ἦσαν καὶ τὴν μεταμόρφωσιν ὅ τε Πέτρος καὶ
Ἰάκωβος καὶ Ἰωάννης. Προεῖδον μὲν ἐκεῖνοι μεταμορφωθέντα τὸν Κύριον, καὶ
νεφέλην σκιάσασαν αὐτὸν, ἀλλ' οὐκ εἶδον τὴν νεφέλην εἰς ἀέρα ἁρπασθεῖσαν, καὶ
εἰς οὐρανοὺς τὸν ∆εσπότην ἀναλαμβάνουσαν. Θαῦμα ἐκεῖνο, θαῦμα καὶ τοῦτο· ἀλλὰ
θαῦμα θαύματος φοβερώτερον, τοῦτο ἐκείνου ὑψηλότερον. κἂν ἑνὸς Θεοῦ ἡ δύναμις
καὶ τὸ μυστήριον ὑπῆρχε. Τότε Μωϋσῆς καὶ Ἠλίας ὤφθησαν τοῖς περὶ τὸν Πέτρον,
μετ' αὐτοῦ συλλαλοῦντες· νῦν δὲ θρόνος Χερουβικὸς, τουτέστιν, ἀόρατος δύναμις,
τὴν νεφέλην καλυπτομένην ἀθρόον ἐπιστᾶσα, τὸν ∆εσπότην τοῖς δούλοις
συνομιλοῦντα ἀφήρπασε. Τότε Πέτρος μετὰ παῤῥησίας 52.801 ἀπεκρίνατο· Κύριε,
καλόν ἐστιν ἡμᾶς ὧδε εἶναι· ποιήσωμεν ὧδε τρεῖς σκηνὰς, σοὶ μίαν, καὶ Μωϋσῇ μίαν,
καὶ μίαν Ἠλίᾳ. Νυνὶ δὲ οὐδὲ φθέγξασθαι ἠδυνήθη τις τῶν μαθητῶν, οὐδὲ τὸ στόμα
διανοῖξαι, ἀλλὰ φόβῳ μεγίστῳ καταπλαγέντες ἐξέστησαν. Ἐπάγει δὲ καθεξῆς ἡ
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

Γραφὴ λέγουσα· Καὶ ἰδοὺ ἄνδρες δύο παρειστήκεισαν αὐτοῖς ἐν ἐσθῆσι λευκαῖς, οἳ
καὶ εἶπον. Οὐδὲ τοῦτο ἀργόν· τὸ γὰρ λευχείμονας ὑπάρχειν τοὺς ἄνδρας· ἑορτῆς
μεγίστης σημαντικόν ἐστι. Τί δὲ εἶπαν; Ἄνδρες Γαλιλαῖοι, τί ἑστήκατε ἐμβλέποντες
εἰς τὸν οὐρανόν; Τὸ ὑπεράγαν θαῦμα κατέπτηξεν ὑμᾶς· ἐπελάθεσθε τοῦ εἰπόντος·
Πορεύομαι πρὸς τὸν Πατέρα μου, καὶ Πατέρα ὑμῶν, καὶ Θεόν μου, καὶ Θεὸν ὑμῶν, Ἢ
οὐχὶ ἕνεκεν τούτου ἔλεγε πρὸς ὑμᾶς, Οὐκ ἀφήσω ὑμᾶς ὀρφανούς· καὶ πάλιν, Εἰρήνην
τὴν ἐμὴν δίδωμι ὑμῖν, εἰρήνην τὴν ἐμὴν ἀφίημι ὑμῖν; Οὐ τὴν ἐπαγγελίαν τοῦ
Παρακλήτου, τουτέστι τοῦ ἁγίου Πνεύματος, ὑπέσχετο ὑμῖν; Οὗτος ὁ Ἰησοῦς ὁ
ἀναληφθεὶς ἀφ' ὑμῶν εἰς τὸν οὐρανὸν, οὕτως ἐλεύσεται, ὃν τρόπον ἐθεάσασθε
πορευόμενον αὐτὸν εἰς τὸν οὐρανόν.
Οὕτως ἐλεύσεται μετὰ τῶν νεφελῶν τοῦ οὐρανοῦ, μετὰ δόξης καὶ δυνάμεως
πολλῆς· οὕτως ἐλεύσεται ἐν καιρῷ, ὅταν μέλλῃ κρίνειν τὴν οἰκουμένην ἐν
δικαιοσύνῃ. Αὐτῷ γὰρ ὁ Πατὴρ δέδωκε τὴν κρίσιν πᾶσαν. Οὐ μνημονεύετε ὅτι ὢν
σὺν ὑμῖν, ἔλεγεν· Ἐδόθη μοι πᾶσα ἐξουσία ἐν οὐρανῷ καὶ ἐπὶ γῆς; Τότε οἱ ἀπόστολοι
ἐν ἑαυτοῖς γενόμενοι, ὑπέστρεφον, αἰνοῦντες καὶ εὐλογοῦντες τὸν Κύριον, καὶ
ἀπεκδεχόμενοι τῆς θείας δωρεᾶς, τουτέστι τοῦ παναγίου Πνεύματος τὴν
ἐπιφοίτησιν. Καὶ δὴ ἐπληρώθη τὸ ἐν Ψαλμοῖς εἰρημένον· Ἀνέβη ὁ Θεὸς ἐν
ἀλαλαγμῷ, Κύριος ἐν φωνῇ σάλπιγγος. Σαφῶς ἐνταῦθα ὁ προφήτης τὴν δυάδα τῶν
φύσεων τῆς ἐν Χριστῷ οἰκονομίας ἐσήμανε· τὸν αὐτὸν δὲ τρόπον καὶ Θωμᾶς μετὰ
τὴν τοῦ Κυρίου ἀνάστασιν ψηλαφήσας τὴν πλευρὰν αὐτοῦ, ἐβόα λέγων· Ὁ Κύριός
μου καὶ ὁ Θεός μου.
Ἀνέβη ὁ Θεὸς ἐν ἀλαλαγμῷ, Κύριος ἐν φωνῇ σάλπιγγος. Ἐν ἀλαλαγμῷ μὲν,
ὅτι ἀκαταπαύστῳ φωνῇ τὸν τρισάγιον ὕμνον ἀναπέμπουσι τῷ Θεῷ· καὶ ὅπως ἐπὶ
Χερου 52.802 βικοῦ θρόνου ἀνελήφθη, προαπεδείξαμεν· Ἐν φωνῇ δὲ σάλπιγγος,
ἀρχαγγελικῆς δηλαδὴ, προσημαινούσης αὐτοῦ τὴν ἐν οὐρανοῖς ἄνοδον. Ἀλλὰ καὶ τὸ
Πνεῦμα τὸ ἅγιον ταῖς ἄνω δυνάμεσι τῇ προστακτικῇ φωνῇ ἀνεκήρυττεν· Ἄρατε
πύλας, οἱ ἄρχοντες, ὑμῶν, καὶ ἐπάρθητε, πύλαι αἰώνιοι, καὶ εἰσελεύσεται ὁ βασιλεὺς
τῆς δόξης. Αἱ δὲ δυνάμεις ἔλεγον· Τίς ἐστιν οὗτος ὁ βασιλεὺς τῆς δόξης; Εἶτα τὸ
Πνεῦμα· Κύριος κραταιὸς καὶ δυνατὸς, Κύριος δυνατὸς ἐν πολέμῳ. Ἐνίκησε γὰρ τὸν
Πολέμιον, ὡπλίσατο κατὰ τῆς τοῦ διαβόλου τυραννίδος ἐν τῷ ἀνθρωπίνῳ σώματι,
ἔσβεσεν αὐτοῦ τὰ πεπυρωμένα βέλη· καὶ τῷ σταυρῷ προσηλωθεὶς, καὶ θανάτου
γευσάμενος, ἀθάνατος ὑπάρχων, ἐσκύλευσε τὸν ᾅδην, καὶ νικητὴς ἀποδειχθεὶς
ἀνέστη ἐκ τῶν νεκρῶν· καὶ τὸ πλανώμενον πρόβατον ἐπιστρέψας, ἰδοὺ ἀνέρχεται
ἐπ' ὤμων τοῦτο φέρων πρὸς τὰ ἐνενήκοντα ἐννέα τὰ ἀπλανῆ, τὰ ἐν τοῖς ὄρεσι,
τουτέστιν ἐν οὐρανοῖς νεμόμενα. Κύριος κραταιὸς καὶ δυνατὸς, Κύριος δυνατὸς ἐν
πολέμῳ.
Καὶ πάλιν λέγει· Κύριος τῶν δυνάμεων, αὐτός ἐστιν ὁ βασιλεὺς τῆς δόξης. Αἱ
οὖν δυνάμεις ὡς ἤκουσαν, Κύριος τῶν δυνάμεων, ὁμοφώνως τὴν συνήθη
δοξολογίαν ἐκβοῶντες, ἐδέχοντο μετὰ χαρᾶς τὸν Κύριον, καὶ ἔπεμπον ἕως τοῦ
ὑψηλοῦ θρόνου καὶ ἐπηρμένου. Καὶ ἐπληρώθη τὸ ὑπὸ τοῦ ∆αυῒδ εἰρημένον· Εἶπεν ὁ
Κύριος τῷ Κυρίῳ μου· Κάθου ἐκ δεξιῶν μου, ἕως ἂν θῶ τοὺς ἐχθρούς σου ὑποπόδιον
τῶν ποδῶν σου. Ὦ αἱρετικὲ, ἠπάντως πρὸς τὴν ἐξ ἡμῶν ληφθεῖσαν σάρκα,
ἐψυχωμένην ἐκ τῆς ἁγίας Παρθένου.
Αἰσχυνέσθωσαν οὖν οἱ μίαν φύσιν ἐπὶ Χριστοῦ ὁμολογοῦντες. Ἡ γὰρ θεία
φύσις εὔδηλον ὅτι συνῆν ἀϊδίως τῷ θρόνῳ τῆς μεγαλωσύνης. Ἡ δὲ καθολικὴ
Ἐκκλησία δοξαζέσθω. Ἡμᾶς δὲ πάντας ἀξίους ἀναδείξῃ Χριστὸς ὁ Θεὸς τῆς αἰωνίου
αὐτοῦ βασιλείας· αὐτῷ ἡ δόξα καὶ τὸ κράτος σὺν τῷ ἀχράντῳ Πατρὶ, ἅμα τῷ ἁγίῳ
Πνεύματι, εἰς τοὺς αἰῶνας τῶν αἰώνων. Ἀμήν.
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

3

