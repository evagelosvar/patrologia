In decollationem sancti Joannis
ΕΙΣ ΤΗΝ ΑΠΟΤΟΜΗΝ ΤΟΥ ΠΡΟ∆ΡΟΜΟΥ ΚΑΙ ΒΑΠΤΙΣΤΟΥ ΙΩΑΝΝΟΥ, Καὶ
εἰς τὴν Ἡρωδιάδα.
59.485
Πάλιν Ἡρωδιὰς μαίνεται, πάλιν ταράττεται, πάλι ὀρχεῖται, πάλιν ἐπιζητεῖ
τὴν κεφαλὴν Ἰωάννου τοῦ Βαπτιστοῦ ἀνόμως ὑπὸ Ἡρώδου ἀποτμηθῆναι. Πάλιν
Ἰεζάβελ περιέρχεται ζητοῦσα τοῦ Ναβουθαὶ τὸν ἀμπελῶνα ἁρπάσαι, καὶ τὸν ἅγιον
Ἠλίαν καταδιῶξαι ἐπὶ τὰ ὄρη. Οἶμαι δὲ μὴ μόνον ἐμὲ εἰς ἔκστασιν τυγχάνειν, ἀλλὰ
καὶ πάντας ὑμᾶς τοὺς ἀκούοντας τῆς τοῦ Εὐαγγελίου φωνῆς, καὶ θαυμάζειν σὺν ἐμοὶ
τὴν μὲν Ἰωάννου παῤῥησίαν τὴν δὲ Ἡρώδου κουφότητα, καὶ τὴν τῶν ἀθέων
γυναικῶν θηριώδη μανίαν. Τί γὰρ ἠκούομεν; Ὁ γὰρ Ἡρώδης κρατήσας τὸν
Ἰωάννην, ἔθετο ἐν φυλακῇ. ∆ιὰ τί; ∆ιὰ Ἡρωδιάδα γυναῖκα Φιλίππου τοῦ ἀδελφοῦ
αὐτοῦ. Καὶ ψέξειεν ἄν τις τὴν Ἡρώδου κουφότητα ὑπὸ δυστήνων γυναικῶν
παραχθεῖσαν. Τί δ' ἄν τις εἴποι, ἢ πῶ τις ἐκφράσειε τὴν τῶν γυναικῶν ἐκείνων
ἀκόλαστον πονηρίαν; Ἐμοὶ μὲν δοκεῖ μηδὲν εἶναι ἐν κόσμῳ θηρίον ἐφάμιλλον
γυναικὸς πονηρᾶς. Ἀλλὰ νῦν ἐμοὶ περὶ πονηρᾶς ὁ λόγος, οὐ περὶ ἀγαθῆς καὶ
σώφρονος. Οἶδα γὰρ πολλὰς εὐσχήμονας καὶ ἀγαθὰς, ὧν με δεῖ μνημονεῦσαι τὸν
βίον πρὸς οἰκοδομὴν καὶ ἔρωτα τῶν καλῶν.
Οὐ 59.486 δὲν τοίνυν θηρίον ἐν κόσμῳ ἐφάμιλλον γυναικὸς πονηρᾶς. Τί
λέοντος δεινότερον ἐν τετραπόδοις; Ἀλλ' οὐδέν. Τί δὲ ὠμότερον δράκοντος ἐν
ἑρπετοῖς; Ἀλλ' οὐδέν. Πλὴν καὶ λέων καὶ δράκων ἐν τῷ κακῷ ἐλάττω τυγχάνουσι
Καὶ μαρτυρεῖ μου τῷ λόγῳ ὁ σοφώτατος Σολομὼν, λέγων· Συνοικῆσαι λέοντι καὶ
δράκοντι εὐδόκησα, ἢ μετὰ γυναικὸς πονηρᾶς καὶ γλωσσώδους. Καὶ ἵνα μὴ νομίσῃς
τὸν προφήτην εἰρωνείᾳ εἰρηκέναι, ἐξ αὐτῶν τῶν πραγμάτων κατάμαθε ἀκριβῶς.
Τὸν ∆ανιὴλ ἐν τῷ λάκκῳ οἱ λέοντες ᾐδέσθησαν, τὸν δὲ δίκαιον Ναβουθαὶ Ἰεζάβελ
ἐφόνευσε· τὸ κῆτος τὸν Ἰωνᾶν ἐν τῇ κοιλίᾳ ἐφύλαξε, ∆αλιλὰ δὲ τὸν Σαμψὼν
ξυρήσασα καὶ δήσασα, τοῖς ἀλλοφύλοις παρέδωκε· δράκοντες καὶ ἀσπίδες καὶ
κεράσται τὸν Ἰωάννην ἐν τῇ ἐρήμῳ ἐτρόμασαν, Ἡρωδιὰς δὲ αὐτὸν ἐν ἀρίστῳ
ἀπέτεμεν· οἱ κόρακες τὸν Ἠλίαν ἐν τῷ ὄρει διέθρεψαν, Ἰεζάβελ δὲ αὐτὸν μετὰ τὴν
εὐεργεσίαν τοῦ ὑετοῦ πρὸς φόνον ἐδίωκε. Τί γὰρ ἔλεγεν; Εἰ σὺ Ἠλιοὺ, καὶ ἐγὼ
Ἰεζάβελ· Τάδε ποιήσαισάν μοι οἱ θεοὶ, καὶ τάδε προσθείησαν, εἰ μὴ αὔριον ταύτῃ τῇ
ὥρᾳ θήσομαι τὴν ψυχήν σου ὡς ἑνὸς τῶν τεθνηκότων. Καὶ ἐφοβήθη Ἠλίας, καὶ
ἐπορεύθη κατὰ τὴν ψυχὴν αὐτοῦ, καὶ ἀπῆλθεν εἰς τὴν ἔρημον ὁδὸν ἡμερῶν τεσσαρά
59.487 κοντα Καὶ ἦλθεν ὑπὸ Ῥαθμὲν, καὶ ᾐτήσατο τὴν ψυχὴν αὐτοῦ ἀποθανεῖν, καὶ
εἶπε· Κύριε ὁ Θεὸς, ἱκανούσθω μοι νῦν, λάβε τὴν ψυχήν μου ἀπ' ἐμοῦ, ὅτι οὐ
κρείσσων ἐγὼ ὑπὲρ τοὺς πατέρας μου. Οἴμοι! ὁ προφήτης Ἠλίας ἐφοβήθη γυναῖκα; ὁ
τὸν ὑετὸν τῆς οἰκουμένης ἐν τῇ γλώττῃ βαστάζων, ὁ πῦρ οὐρανόθεν κατενέγκας, καὶ
δι' εὐχῆς νεκροὺς ἐγείρας, ἐφοβήθη γυναῖκα; Ναὶ, ἐφοβήθη. Οὐδεμία γὰρ κακία
συγκρίνεται γυναικὶ πονηρᾷ.
Μαρτυρεῖ δέ μου τῷ λόγῳ ἡ Σοφία λέγουσα, ὅτι Οὐκ ἔστι κεφαλὴ ὑπὲρ
κεφαλὴν ὄφεως, καὶ οὐκ ἔστι κακία ὑπὲρ κακίαν γυναικός. Ὢ τὸ κακὸν τοῦ
διαβόλου καὶ ὀξύτατον ὅπλον! διὰ γυναικὸς ἐξ ἀρχῆς τὸν Ἀδὰμ ἐν παραδείσῳ
κατέτρωσε· διὰ γυναικὸς τὸν πραότατον ∆αυῒδ πρὸς τὴν τοῦ Οὐρίου δολοφονίαν
ἐξέμηνε· διὰ γυναικὸς τὸν σοφώτατον Σολομῶντα πρὸς παράβασιν κατέστρωσε· διὰ
γυναικὸς τὸν ἀνδρειότατον Σαμψῶνα ξυρήσας ἐτύφλωσε· διὰ γυναικὸς τοὺς υἱοὺς
Ἠλεὶ τοῦ ἱερέως ἠδάφισε· διὰ γυναικὸς τὸν εὐγενέστατον Ἰωσὴφ ἐν φυλακῇ
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

δεσμεύσας κατέκλεισε· διὰ γυναικὸς τὸν παντὸς κόσμου λύχνον Ἰωάννην ἀπέτεμε.
Τί δὲ λέγω περὶ ἀνθρώπων; ∆ιὰ γυναικὸς τοὺς ἀγγέλους οὐρανόθεν κατέβαλε· διὰ
γυναικὸς πάντας κατασφάζει, πάντας φονεύει, πάντας ἀτιμάζει, πάντας ὑβρίζει.
Γυνὴ γὰρ ἀναιδὴς οὐδενὸς φείδεται· οὐ Λευΐτην τιμᾷ, οὐχ ἱερέα ἐντρέπεται, οὐ
προφήτην αἰδεῖται. Ὢ κακὸν κακοῦ κάκιστον γυνὴ πονηρά! Κἂν μὲν πενιχρὰ ᾖ, τῇ
κακίᾳ πλουτεῖ· ἐὰν δὲ πλοῦτον ἔχῃ τῇ πονηρίᾳ συνεργοῦντα, δισσὸν τὸ κακὸν,
ἀφόρητον τὸ ζῶον, ἀθεράπευτος νόσος, ἀνήμερον θηρίον. Ἐγὼ οἶδα καὶ ἀσπίδας
κολακευομένας ἡμεροῦσθαι, καὶ λέοντας καὶ τίγρεις καὶ παρδάλεις τιθασσευομένας
πραΰνεσθαι· γυνὴ δὲ πονηρὰ καὶ ὑβριζομένη μαίνεται, καὶ κολακευομένη ἐπαίρεται.
Κἂν ἔχῃ ἄνδρα ἄρχοντα, νύκτωρ καὶ μεθ' ἡμέραν τοῖς λόγοις αὐτὸν ἐκμοχλεύουσα
πρὸς δολοφονίαν ὀξύνει, ὡς Ἡρωδιὰς τὸν Ἡρώδην· κἂν πένητα ἔχῃ ἄνδρα, πρὸς
ὀργὰς καὶ μάχας αὐτὸν διεγείρει· κἂν χήρα τυγχάνῃ, αὐτὴ δι' ἑαυτῆς τοὺς πάντας
ἀτιμάζει. Φόβῳ γὰρ Κυρίου οὐ χαλινοῦται τὴν γλῶτταν, οὐκ εἰς τὸ μέλλον κριτήριον
ἀποβλέπει, οὐκ εἰς Θεὸν ἀναβλέπει, οὐ φιλίας οἶδε θεσμοὺς φυλάττειν.
Οὐδέν ἐστι γυναικὶ πονηρᾷ τὸν ἴδιον ἄνδρα παραδοῦναι εἰς θάνατον. Ἀμέλει
γοῦν τὸν δίκαιον Ἰὼβ ἡ ἰδία γυνὴ πρὸς θάνατον τῆς βλασφημίας παρεδίδω, λέγουσα·
Εἰπόν τι ῥῆμα πρὸς Κύριον, καὶ τελεύτα. Ὢ φύσεως πονηρᾶς! ὢ προαιρέσεως
ἀνοσίας! Οὐκ ἠλέησεν ὁρῶσα τοῦ ἰδίου ἀνδρὸς τὰ σπλάγχνα ὑπὸ τῶν ἀναζεουσῶν
φλυκταινῶν, ὥσπερ ὑπὸ ἀνθράκων σπινθηροβόλων καιόμενα, καὶ ὅλας τὰς σάρκας
τοῖς σκώληξι συνειλημμένας· οὐκ ἐκάμφθη πρὸς οἶκτον, ὁρῶσα αὐτὸν ὅλον δι' ὅλου
ἑλισσόμενον, καὶ κάμνοντα καὶ ἀγωνιῶντα, καὶ συνεχῆ ἄσθματα μετὰ πόνου
κεχηνότι τῷ στόματι φέροντα. Οὐκ ἐμειλίχθη πρὸς εὐσπλαγχνίαν ὁρῶσα τόν ποτε ἐν
βασιλικῇ πορφυρίδι προσιόντα, τότε ἐπὶ κοπρίας κείμενον γεγυμνωμένον τῷ
σώματι· οὐκ ἐμνημόνευσε τῆς πρὸς αὐτὸν ἀρχαίας συνηθείας, οὐδ' ὅσα δι' αὐτὸν
ἐπίδοξα καὶ καλὰ ἤνθησεν αὕτη. Ἀλλὰ τί; Εἰπόν τι ῥῆμα πρὸς Κύριον, καὶ τελεύτα. Ὢ
χάρις γυναικός! ὢ μάλαγμα ὀδύνων προμαλακτήριον! ὢ φιλίας ὁμοζύγου θεσμός!
Ἆρα σοῦ ποτε ἀῤῥωστούσης τοιοῦτον ἐφθέγξατο ῥῆμα, καὶ οὐχὶ εὐχαῖς καὶ εὐποιίαις
τὴν νόσον σου ἀπέσμηξεν; οὐκ ἤρκει γὰρ αὐτῷ ἡ πρόσκαιρος παιδεία, ἀλλὰ καὶ
αἰώνιον αὐτῷ προξενεῖς τὴν κόλασιν διὰ τῆς βλασφημίας; ἢ οὐκ οἶδας, ὅτι Πᾶσα
βλασφημία καὶ ἁμαρτία ἀφεθήσεται ἀνθρώποις· ἡ δὲ κατὰ τοῦ Πνεύματος τοῦ ἁγίου
βλασφημία, οὐκ ἀφεθήσεται οὔτε ἐν τῷ αἰῶνι τούτῳ, οὔτε ἐν τῷ μέλλοντι; Θέλεις
ἰδεῖν καὶ ἄλλην τῆς πονηρίας ταύτης ὁμόζυγον; Ἴδε μοι τὴν ∆αλιλάν. Καὶ γὰρ ἐκείνη
τὸν ἀνδρειότατον Σαμψῶνα ξυρήσασα καὶ δήσασα, τοῖς ἀλλοφύλοις παρέδωκε, τὸν
ἴδιον ἄνδρα τὸν ἴδιον σύνευνον, ὃν ἔθαλπεν, ὃν ἐθώπευεν, ὃν ἐκολάκευεν, ὃν ὑπὲρ
ἑαυτὴν ἀγαπᾷν ὑπεκρίνατο· ὃν χθὲς ἠγάπα, σήμερον ἠπάτα· ὂν χθὲς ἔθαλπεν
ἀγαπῶσα, σήμερον ἔθαπτεν ἀπατῶσα. Καὶ μὴν οὐκ ἦν ὡραῖος; Καὶ τίς αὐτοῦ
ὡραιότερος κατ' ἐκεῖνο καιροῦ, ὃς ἑπτὰ βο 59.488 στρύχους ἐπὶ κεφαλῆς ἔφερε, τῆς
ἑπταφώτου χάριτος τὴν εἰκόνα βαστάζων; Καὶ μὴν οὐκ ἦν ἀνδρεῖος; Καὶ τίς αὐτοῦ
ἀνδρειότερος, ὅς γε λέοντα φοβερὸν ἐν ὁδῷ μόνος ἀπέπνιξε, καὶ ἐν μιᾷ σιαγόνι ὄνου
χιλίους ἀλλοφύλους κατέστρωσεν; Ἀλλὰ καὶ οὐχ ἅγιος ἦν;
Τοσοῦτον ἦν ἅγιος, ὡς διψήσαντα αὐτόν ποτε ἐν σπάνει ὕδατος εὔξασθαι, καὶ
ἐκ τῆς κατεχομένης ἐν τῇ χειρὶ αὐτοῦ σιαγόνος νεκρᾶς ὕδωρ πηγάσαι, κἀκεῖθεν τὸ
ἴαμα τῆς δίψης ἀρύσασθαι. Καὶ τὸν οὕτως ὡραῖον, τὸν οὕτως ἀνδρεῖον, τὸν οὕτως
ἅγιον, ἡ ἰδία γυνὴ, ὡς πολέμιον δήσασα, τοῖς ἀλλοφύλοις παρέδωκε. Καὶ πόθεν ἆρα
γυνὴ τοῦ οὕτως ἀνδρείου κατίσχυσεν; Ἐκ τῆς οἰκείας τοῦ ἀνδρὸς ἀγαθότητος. Τὸ
γὰρ μυστήριον αὐτοῦ τῆς ἰσχύος νύκτωρ ἀποσυλήσασα, γυμνὸν αὐτὸν ἱμᾶσιν
ἰσχυροῖς κατέστρεψε. ∆ιὰ τοῦτο παραγγέλλει σοι ἡ Σοφία· Ἀπὸ τῆς συγκοίτου σου
φυλάσσου τοῦ ἀναθέσθαι τι αὐτῇ. Ποῖον, εἰπέ μοι, θηρίον κατὰ τοῦ ἰδίου ἄῤῥενος
τοιαῦτα ἐμελέτησε πώποτε; τίς δράκαινα τὸν ἴδιον ὁμόζυγον ἀπολέσθαι θέλει; ποία
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

δὲ λέαινα τὸν ἴδιον ἄῤῥενα πρὸς σφαγὴν παραδίδωσιν; Ὁρᾷς ὅτι ἐπιτετευγμένως
εἶπεν ἡ Σοφία, ὅτι Οὐκ ἔστι κεφαλὴ ὑπὲρ κεφαλὴν ὄφεως, καὶ οὐκ ἔστι κακία ὑπὲρ
κακίαν γυναικός; Καὶ ἁπαξαπλῶς, ὁ ἔχων γυναῖκα πονηρὰν, γινωσκέτω ἤδη τοὺς
τῶν ἀνομιῶν αὐτοῦ κεκομίσθαι μισθούς. Οὐκ ἀμάρτυρος ὁ λόγος· ἄκουε τῆς Σοφίας
λεγούσης· Γυνὴ πονηρὰ ἀνδρὶ ἀνόμῳ δοθήσεται ἀντὶ ἔργων πονηρῶν. Ἀλλὰ περὶ
πονηρᾶς γυναικὸς ὁ λόγος ἄχρι τούτου ἐχέτω ὅρον. βʹ. ∆εῖ δὲ ἡμᾶς μνημονεῦσαι καὶ
τὰς ἀγαθὰς μάλιστα διὰ τὰς παρούσας. Αἱ γὰρ ἀγαθαὶ τὰς τῶν ἀγαθῶν ἀρετὰς ὡς
ἰδίας ὁρῶσι, καὶ τοὺς ἐκείνων πόνους ὡς ἰδίους στεφάνους εἶναι λογίζονται. Γυνὴ
ἀγαθὴ καὶ φιλόξενος ἦν ἡ μακαρία Σουναμῖτις, ἥτις τὸν ἄνδρα παρακαλέσασα,
δωμάτιον ᾠκοδόμησε τῷ Ἑλισσαίῳ, ἵνα διερχόμενος ἀκωλύτως ἔχῃ τὴν ἀνάπαυσιν,
τεθεικυῖα αὐτῷ κλίνην καὶ λυχνίαν καὶ τράπεζαν· κλίνην οὐ κενὴν ἱματίων, ἀλλ'
ἔχουσαν πρέποντα τῷ προφήτῃ τὰ στρώματα· λυχνίαν οὐκ ἄνευ λύχνου, ἀλλὰ σὺν
τῷ ἄρδοντι ἐλαίῳ τὸ φῶς· τράπεζαν οὐκ ἔρημον ἄρτων, ἀλλ' ἔγκαρπον ἐδεσμάτων.
Τί δ' ἄν τις εἴπῃ περὶ τῆς μακαρίας ἐκείνης χήρας τῆς τὸν προφήτην Ἠλίαν
ὑποδεξαμένης; ᾗ οὐδὲν ἐνεπόδιζεν ἡ πενία τῶν χρημάτων, διὰ τὸ πλούσιον τῆς
προαιρέσεως; ᾗ οὐ παρῆν οὐ σῖτος, οὐκ οἶνος, οὐ προσέψημα, οὐκ ἄλλο τι τῶν
γεηρῶν πρὸς τὴν τῆς πενίας παραμυθίαν. Οὐ χώρα αὐτῇ σιτοφόρος σπέρμα ἄρτου
προσέφερεν· οὐκ ἄμπελος αὐτῇ γλυκόῤῥυτον βότρυν ἐγεώργει· οὐ δένδρον αὐτῇ
ὀπώρας καρπὸν ἥδιστον προσέφερε. Πῶς γὰρ, ᾗ οὔτε σπιθαμὴ γῆς ἀροσίμου τόπος
ὑπῆρχεν, οὔτε πηχυαῖον ἔδαφος πρὸς ἀμπέλου φυτείαν; ἀλλ' ἀεὶ ἐν τῷ καιρῷ τοῦ
θέρους ἐν ταῖς ἀρούραις κυρτοβατοῦσα, τοὺς ἀποπίπτοντας τῶν δρεπανιστῶν
παλάμαις ἀστάχυας ἐρανιζομένη, συμμεμετρημένην τῷ ἐνιαυσιαίῳ κύκλῳ τοῦ
χρόνου τὴν τροφὴν ἀπετίθετο. Πρὸς ταύτην ἀπῆλθεν Ἠλίας ἐν τῷ καιρῷ τοῦ λιμοῦ,
ὅτε πᾶσα ἡ γῆ ἐκ τῆς ἀβροχίας σχεδὸν διεθρύπτετο· ὅτε οὐρανὸς ἐπεπύρωτο, καὶ ὁ
ἀὴρ ἐχαλκοῦτο, καὶ αἱ νεφέλαι ἐχαλινοῦντο· ὅτε οὐ βοτάνη, οὐκ ἄνθος, οὐκ
ἀκροδρύων βλαστὸς, οὐκ αὔρα ἔνδροσος, οὐ στάχυος ἀκμὴ ἐσαλεύετο· ὅτε ποταμοὶ
ἐλεπτύνθησαν, καὶ πηγαὶ τοὺς μαζοὺς τῶν ὑδάτων τῷ καύσωνι ἔσφιγξαν, καὶ
θάλασσα ἐξηλμίσθη, μὴ ἐπεισερχομένων αὐτῇ γλυκέων ὑδάτων καὶ ὄμβρων
ἀνασταλέντων. Τότε ἀπῆλθεν Ἠλίας πρὸς τὴν πενιχρὰν καὶ τὴν χήραν. Οἷα δὲ
πάσχει χήρα καὶ ἐν καιρῷ εὐθηνίας, οἴδατε. Καὶ ἀφῆκε τοὺς πλουσίους ὁ προφήτης,
τοὺς ἔχοντας δαψιλῶς ἄρτους, καὶ καταβὰς ἀπὸ τοῦ ὄρους, ἦλθε πρὸς αὐτήν. ∆ιὰ τί
γὰρ μᾶλλον ὁ πῦρ οὐρανόθεν ῥήμασι κατενέγκας, ἄρτους ἑαυτῷ οὐ κατήνεγκε; μὴ
οὐκ ἠδύνατο; Ναὶ, ἠδύνατο· ἀλλὰ τοῦτο οὐκ ἐποίησε. ∆ιὰ τί; Ἵνα μὴ τῆς φιλοξενίας
τοὺς καρποὺς στερήσῃ τὴν χήραν· ἄλλως δὲ καὶ τὴν δράκα τοῦ ἀλεύρου καὶ τὸ
ὀλιγοστὸν ἔλαιον διὰ τῆς εὐλογίας πληθύνει.
Οὐ γὰρ τοσοῦτον ἀπῆλθεν ὁ προφήτης τραφῆναι, ὅσον θρέψαι τὴν πενιχρὰν,
καὶ ἐλέγξαι κρυπτομένην καρδίαν καὶ εὐπροαίρετον. Τοῦτο ποιεῖ ὁ Θεὸς δυνάμενος
πάντας τοὺς ἁγίους, τοὺς ὄντας ἐν κόσμῳ, δι' ἑαυτοῦ θρέψαι· ἐπέχει τὴν δόσιν, ἵνα
τὰς εὐπροαιρέτους καρδίας ἐν τῷ τῆς φι 59.489 λοξενίας καιρῷ ἀπὸ τῶν καρπῶν
διακρίνῃ. Ἐπειδὰν δὲ μὴ ὦσί τινες οἱ ὀφείλοντες αὐτοὺς ὑποδέξασθαι, ἢ δι' ὀρνέων
τρέφει, ὡς τὸν Ἠλίαν ἐν τῷ ὄρει, ἢ διὰ προφήτου ξένου, ὡς τὸν ∆ανιὴλ ἐν τῷ
λάκκῳ, ἢ διὰ θαλαττίου ζώου, ὡς τὸν Ἰωνᾶν διὰ τοῦ κήτους, ἢ αὐτὸς δι' ἑαυτοῦ τὴν
τροφὴν ἐπώμβρισεν, ὡς τοῖς πατράσιν ἡμῶν ἐν ἐρήμῳ· οὐκ ὄντων γὰρ τῶν
ὀφειλόντων αὐτοὺς ὑποδέξασθαι, οὐρανόθεν τὸ μάννα ἐπώμβρισε, καὶ ἐκ πέτρας
ὕδωρ ἐπήγαζεν· ὅταν δὲ ὦσιν οἱ ἅγιοι ἐν κόσμῳ συναναστρεφόμενοι τοῖς ἄλλοις,
ἀναστέλλει αὐτοῦ τὴν δεξιὰν χεῖρα· κἂν ὁρᾷ αὐτοὺς θλιβομένους, ἀφίησιν, ἵνα δῷ
χάριν διὰ τῆς εἰς αὐτοὺς γινομένης εὐποιίας πολλῆς τοῖς βουλομένοις τὴν σωτηρίαν
καρπώσασθαι. Ἀπῆλθεν οὖν Ἠλίας πρὸς τὴν χήραν, ᾗ οὐδὲν παρῆν, εἰ μὴ δρὰξ
ἀλεύρου, ὅπερ μετὰ καμάτου αὐτῇ καὶ τοῖς τέκνοις αὐτῆς ἐν τῷ καιρῷ ἀρίστου
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

3

ἐπήρκει. Καὶ τί πρὸς αὐτήν; Λάβε μοι ὀλίγον ὕδωρ εἰς ἄγγος, καὶ πίομαι. Αὐτῆς δὲ
πορευομένης, ἐβόησεν ὀπίσω αὐτῆς· Λήψῃ δέ μοι καὶ ψωμὸν ἄρτου ἐν τῇ χειρί σου.
Ἡ δὲ, ὃ μὲν οὐκ εἶχεν, εἶπεν· ὃ δὲ εἶχεν, ὡμολόγησε. Τί γάρ; Ζῇ Κύριος, εἰ ἔσται μοι
ἐγκρυφίας, ἀλλ' ἢ ὅσον δρὰξ ἀλεύρου, καὶ ὀλίγον ἔλαιον ἐν τῷ καμψάκῃ.
Θαυμαστὸν, ὅτι ἐν τοιαύτῃ σπάνει τροφῶν τὸ παρὸν αὐτῇ τῆς πενίας λείψανον οὐκ
ἠρνήσατο. Πόσοι νῦν χρυσὸν καὶ ἄργυρον ὡς πηλὸν ἔχοντες, παρακαλούμενοι τὰς
εὐεργεσίας τῶν φίλων ἀνανεύουσι, καὶ κολακευόμενοι ἀρνοῦνται τὸ ἔχειν; κἂν
παρακλήσει καμφθῶσι πρὸς τὴν ἐπίδοσιν, γράμματα συντάττουσι σιδήρου
ἰσχυρότερα, καὶ τὴν δεχομένην χεῖρα πρῶτον τοῖς γραμματείοις δεσμεύουσιν,
ἱκανοδότας καὶ μεσίτας τοῦ χρυσίου λαμβάνοντες. Ἐκείνη δὲ ἀπὸ μιᾶς φωνῆς οὐκ
ἠρνήσατο τὴν δράκα τοῦ ἀλεύρου. Καὶ τί πρὸς αὐτὴν ὁ προφήτης; Σπεῦσον, καὶ
ποίησόν μοι ἐγκρυφίας, ἐν πρώτοις ἐμοὶ, ἐπ' ἐσχάτῳ δὲ σεαυτῇ καὶ τοῖς τέκνοις σου.
Πειρασμὸς ἦν τὸ ῥῆμα τοῦ προφήτου, δοκιμὴ καρδίας, προαιρέσεως βάσανος· καὶ ἦν
ὥσπερ ὑπὸ ζυγοῦ κειμένη ἡ καρδία τῆς μακαρίας ἐκείνης, καὶ δοκιμαζομένη. Ποῦ
ἆρα ῥέπει; Πρὸς τὴν τῶν τέκνων ἑαυτῆς εὔ 59.490 νοιαν, ἢ πρὸς τὴν τοῦ προφήτου
φιλοξενίαν; Ἡ δὲ εἵλετο μᾶλλον αὐτὴν καὶ τὰ τέκνα αὐτῆς στενοχωρῆσαι, καὶ τὸν
προφήτην ὑποδέξασθαι.
Ἤδει γὰρ, ὅτι Ὁ δεχόμενος προφήτην εἰς ὄνομα προφήτου, μισθὸν προφήτου
λήψεται· καὶ ὁ ποτίζων ποτήριον ψυχροῦ εἰς ὄνομα μαθητοῦ, οὐ μὴ ἀπολέσῃ τὸν
μισθὸν αὐτοῦ. Τί δέ ἐστιν ὃ εἶπεν ὁ προφήτης, Σπεῦσον; ἆρά γε τοσοῦτον ἐπείνα, ὡς
δεῖσθαι σπουδῆς; Οὐ πάντως, ἀλλὰ τὸ τῆς εὐποιίας σπουδαῖον καὶ ἱλαρὸν καὶ θερμὸν,
μὴ μετὰ λύπης, ἢ μετὰ ἀνάγκης· Ἱλαρὸν γὰρ δότην ἀγαπᾷ ὁ Θεός. Σπεῦσον καὶ
ποίησον ἐν πρώτοις ἐμοὶ, ἐπ' ἐσχάτῳ δὲ σεαυτῇ καὶ τοῖς τέκνοις σου. Σπεῦσον, ὡς
Ἀβραὰμ ἐν τῇ τῶν ἀγγέλων ἐπιδημίᾳ ἔσπευσεν ἐπὶ τοὺς βόας, ἐπὶ τὸν μόσχον, ἵνα
λάβῃ τὸν ἀμνόν· καὶ ὡς Σάῤῥα ἔσπευσεν ἐπὶ τοὺς ἐγκρυφίας, ἵνα λάβῃ τὸν ἐν
οὐρανοῖς κρυπτόμενον ἄρτον. Σπεῦσον, καὶ ποίησον ὡς Ἀβραὰμ τὰς θυσίας τῷ Θεῷ·
μὴ πρότερον σαυτῇ, καὶ τότε ἐμοὶ, ὡς Κάϊν, καὶ Ὀφνὶ, καὶ Φινεὲς, υἱοὶ Ἠλὶ τοῦ
ἱερέως, οἵτινες τὸν Θεὸν παρύβριζον, πρῶτοι τὰς ἀπαρχὰς λαμβάνοντες τῶν τῷ Θεῷ
προσφερομένων δώρων. Ἡ δὲ τὸ προστεταγμένον σπουδαίως ἐποίει. Ὁ δὲ τὸν ἄρτον
ἐγκρυφίαν πλούσιον θεασάμενος ἐδέξατο, ἐφθέγξατο, καὶ τὸν οἶκον αὐτῆς
ἐνέπλησεν ἀγαθῶν. Οὐκ ἐκλείψει γὰρ, φησὶν, ἡ δρὰξ τοῦ ἀλεύρου ἐκ τῆς ὑδρίας, καὶ
τὸ ἔλαιον ἐκ τοῦ καμψάκου, ἕως τοῦ δοῦναι τὸν Κύριον ὑετὸν ἐπὶ τὴν γῆν. ∆ιὰ τί
ἕως τότε; Καὶ ἀναγκαίως. Ἔδει γὰρ τῆς καινῆς χάριτος ὡς ὑετοῦ ἐλθόντος, τὸν
παλαιὸν νόμον λαβεῖν τέλος. Καὶ ἠκολούθει τὸ ἔργον τῷ λόγῳ. Ὁρᾷς πῶς τῆς
φιλοξενίας τοὺς καρποὺς ἐτρύγησαν αἱ ἀγαθαί; Ἀγαθῶν γὰρ πόνων καρποὶ εὐκλεεῖς,
καὶ ἀδιάπτωτος ἡ ῥίζα τῆς φρονήσεως.
Ἠκούσατε, γυναῖκες, τὰς τῶν πονηρῶν γυναικῶν πράξεις, καὶ τὰς τῶν
ἀγαθῶν ἀρετάς. Τὰς μὲν οὖν ἀγαπήσατε, τὰς δὲ μὴ ποθήσατε· καὶ τὰς μὲν μιμήσασθε,
τὰς δὲ μισήσατε, ἵνα τὸν αὐτῶν δρόμον, τῶν καλῶν λέγω, ἰχνηλατήσαντες, εἰς τὸν
αὐτὸν τῶν ἁγίων χορὸν ἀριθμηθῆτε ἐν Χριστῷ Ἰησοῦ τῷ Κυρίῳ ἡμῶν, ᾧ ἡ δόξα καὶ
τὸ κράτος εἰς τοὺς αἰῶνας τῶν αἰώνων. Ἀμήν.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

4

