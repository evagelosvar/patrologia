In evangelii dictum et de virginitate
Ιʹ. Τοῦ αὐτοῦ εἰς τὸ τοῦ Εὐαγγελίου ῥητὸν, καὶ περὶ παρθενείας, καὶ
παραινετικὸς εἰς τὰς ἐκπεσούσας.
64.37
Οὐδὲν ὁ θεῖος λόγος ἀσκόπως, οὐδὲ ξένον τῆς ἡμετέρας σωτηρίας παρέδωκεν
αἴνιγμα. Ὅθεν ποῦ μου τὸν, νοῦν ἐκπετάσω, τὸ πυκνὸν τῆς ἐννοίας ἐρευνῆσαι
προετρέψατο, καὶ ἰδεῖν τί ἄρα βούλεται τὸ ἐν τῷ Εὐαγγελίῳ παρὰ τοῦ ∆εσπότου
λεγόμενον. Πολλοῖς μὲν γὰρ πολλὰ διὰ παραβολῆς ἐλάλησε, τοῖς μὲν σαφηνίζων τὰ
δυσχερῆ, τοῖς δὲ ἀποκαλύπτων τὰ εὔδηλα ἡμῖν, τὸν περὶ σωτηρίας λόγον
ποιούμενος, ὡς ἀγαθὸς καὶ φιλάνθρωπος τρανὴν καὶ ἀσύγχυτον τὴν πεῖραν τῶν
αἰνιγμάτων παρέδωκε· πῆ μὲν ἀπὸ τῶν καθ' ἡμέραν πραγμάτων διεξιέναι
δυναμένους τὴν ἔμφασιν, πῆ δὲ καὶ ἀπ' αὐτῆς τῆς ἐννοίας τοῦ λεγομένου τὴν
αἴσθησιν ὑποδέξασθαι.
Ἔλεγεν ὁ ∆εσπότης, ὅτι Στενὴ καὶ τεθλιμμένη ἡ ὁδὸς, ἡ ἀπάγουσα εἰς τὴν
ζωὴν, καὶ ὀλίγοι εἰσὶν οἱ εὑρίσκοντες αὐτήν· πλατεῖα δὲ καὶ εὐρύχωρος ἡ ὁδὸς ἡ
ἀπάγουσα εἰς τὴν ἀπώλειαν, καὶ πολλοὶ εἰσέρχονται δι' αὐτῆς· Ἐκπλήξεως ὁ λόγος,
ἀγνωσίας ὁ τρόπος, τί, Πλατεῖα, φησὶν, ἡ ὁδὸς τῆς ἀπωλείας. Οὐ γνωρίζει τὴν ὁδὸν
ταύτην ὁ Χριστός· οὐ γὰρ δι' αὐτοῦ ἐγένετο· συμπαθείας οὖν ῥῆμα, ἰατρείας τὸ
αἴνιγμα. Ὢ τῆς φιλανθρώπου εὐνοίας! ὢ τῆς ∆εσποτικῆς κηδεμονίας! ὁμοῦ καὶ τὴν
νίκην καὶ τὴν ἧτταν προτίθησιν, ὁμοῦ καὶ τὸν οἶκτον καὶ τὴν ἀπόφασιν· Οἶδα, φησὶ,
πῶς ὀλισθηρὸν τὸ γένος τῶν ἀνθρώπων, πῶς χαμαίζηλον, πῶς ὀφθαλμοῤῥεπὲς, πῶς
εὐολίσθητον, πῶς εὐκαταμάχητον, πῶς ἄνανδρον, πῶς εὐάλωτον, εἰ μὴ πίστει
στηρίζοιτο· ἤκουσε πλατεῖαν ὁδὸν, καὶ ἐπέδραμεν ὡς ἐπὶ παγίδα ὄρνεον· ἤκουσε
στενὴν, καὶ φεύγει τὴν σωτηρίαν, οὐκ ἐρευνήσας ἀμφοτέρων τὸ χρήσιμον· καὶ γὰρ
64.38.40 αἰτεῖται τὴν πάλην, καὶ φεύγει τὸ σκάμμα· καὶ μάλιστα ἀκούσας, ὅτι Πολλαὶ
αἱ θλίψεις τῶν δικαίων, καὶ ἐκ πασῶν αὐτῶν ῥύσεται αὐτοὺς ὁ Κύριος, καὶ τῷ βοηθῷ
οὐ προσέδραμεν, ἀλλὰ φεύγει φυγῇ, μὴ γινώσκων ὅτι τὸ ἀγαθὸν ἐκφεύγει καὶ εἰς τὸ
ἧττον προσκολλᾶται. Ὢ τῆς ἀνοίας! Πολλαὶ αἱ θλίψεις τῶν δικαίων, ἀκούσας,
ὤκλασας, ἄνθρωπε; καὶ Πολλαὶ αἱ μάστιγες τοῦ ἁμαρτωλοῦ ἀκούσας, οὐκ ἐπτόησας;
Μᾶλλον δὲ τὴν μὲν μάστιγα ἐπτόησας, τὸ δὲ πάθος τῆς ἁμαρτίας οὐκ ἔσβεσας. Τί
πλατεῖα καὶ εὐρύχωρος ἡ ὁδὸς τῆς ἀπωλείας; Ὁδὸν ἐνταῦθα τὸν βίον λέγει, ἐπειδὴ ἡ
πλατεῖα τοῖς ἀπατωμένοις δοκεῖ τὸ ἡδὺ τοῦ κόσμου. Τί δὲ ταύτης παρὰ τοῖς
εὐφρονοῦσιν ἀθλιώτερον; Ἡ μὲν γὰρ στενὴ νομιζομένη εἰς οὐρανοὺς ὁδηγεῖ, ἡ δὲ
πλατεῖα δοκοῦσα εἰς ᾅδην καθέλκει τοὺς ἑπομένους αὐτήν. Μὴ γὰρ πλατεῖαν
νομίσῃς εἶναι ἀληθῶς, ἀγαπητὲ, ἢ ἀγαθὴν τὴν ὁδὸν ἐκείνην, ἐπειδὴ τοιαύτῃ λέξει ὁ
∆εσπότης ἐχρήσατο.
Ὥσπερ γὰρ ὁ Σολομὼν λέγει, μέλι ἀποστάζειν ἀπὸ χειλέων γυναικὸς πόρνης,
παρὰ φύσιν δὲ τὸ λεγόμενον· πῶς γὰρ δυνατὸν ἀπὸ χειλέων γυναικὸς μέλι
ἀποστάξαι ποτέ; ἀλλὰ τὸ δοκοῦν τῆς ἀπάτης ἡδὺ καὶ τῆς ματαίας ἡδονῆς τὴν ἀπάτην
μέλι ὁ λόγος ἐκάλεσεν· οὕτω καὶ ἐνταῦθα πλάτος λέγει ὁδοῦ τὰ διάφορα τῶν
ἡδονῶν πάθη, καὶ πᾶσαν τὴν τοῦ βίου ἀπόλαυσιν. Ἀκούσατέ μου οἱ τὸν αὐχένα
ὑποκλίναντες καὶ δεξάμενοι τὸν χρηστὸν καὶ τὸν ἐλαφρὸν καὶ μακάριον ζυγὸν,
μονάζοντες καὶ παρθένοι· ἀκούσατέ μου ὅσαι ἀφιερώσατε ἑαυτὰς καὶ ἐδώκατε τὰ
σώματα ὑμῶν τῷ Κυρίῳ· ἀκούσατέ μου αἱ τοῦ οὐρανίου νυμφίου στέφανοι,
ἀκούσατέ μου αἱ τοῦ ἡλίου λαμπρότεραι, ἀκούσατέ μου αἱ τῆς βασιλείας εἴσοδοι,
ἀκούσατέ μου αἱ τοῦ Θεοῦ φίλαι, 64.39 ἀκούσατέ μου αἱ τοῦ ∆αυῒδ χορεύτριαι,
 
 

1
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

ἀκούσατέ μου τὰ τῆς Ἐλισαβὲτ ἔγγονα, ἀκούσατέ μου αἱ Ἰωάννου σύντροφοι,
ἀκούσατέ μου αἱ κληρονόμοι Θεοῦ, ἀκούσατέ μου αἱ τὸ σκάμμα καὶ τὴν νίκην καὶ
τὸν ἀγωνοθέτην ἐν τῷ σκάμματι περιφέρουσαι, ἀκούσατέ μου αἱ Μαρίας ἀδελφαὶ,
ἀκούσατέ μου αἱ μητέρες καὶ νύμφαι καὶ δουλίδες Χριστοῦ· οὐχὶ ἐμοῦ δὲ ἀκούσατε,
παρακαλῶ, ἀλλὰ τοῦ Ἀποστόλου λέγοντος, Παρακαλῶ οὖν ὑμᾶς, παραστήσατε τὰ
σώματα ἡμῶν θυσίαν ζῶσαν, ἁγίαν, εὐάρεστον τῷ Θεῷ. Ἀξιόπιστος ὁ παρακαλῶν,
καὶ μὴ παραιτήσει τὴν δόσιν. Ἴδε τίς ἐστιν ὁ παρακαλῶν σε, καὶ φρίξον τὴν αἴτησιν·
ὁ ἕως τρίτου οὐρανοῦ ἀναβὰς, ὁ φωνῆς Θεοῦ ἀξιωθεὶς, ὁ ἀκούσας ἄῤῥητα ῥήματα,
παρακαλεῖ, Παραστήσατε τὰ σώματα ὑμῶν ἁγνὰ τῷ Θεῷ. Τί παρακαλεῖς, Παῦλε; τί
γάρ σοι μέλει περὶ τῶν παρθένων; οὐ σὺ εἶπας, ὅτι Περὶ τῶν παρθένων ἐπιταγὴν
Κυρίου οὐκ ἔχω; οὐ σὺ εἶπας, Τίμιος ὁ γάμος; οὐ σὺ εἶπας, Μὴ ἀποστερεῖτε ἀλλήλους
τῆς συμμιξίας; τί νῦν ἁγνείαν κηρύττεις; εἶτα δὲ τί σοι λοιπὸν μέλει περὶ τῶν
παρθένων; ἐκ σαρκικοῦ γάμου καὶ φθορᾶς Θέκλαν ἥρπασας, καὶ ἔσωσας· τί σοι περὶ
τῶν λοιπῶν παρθένων μέλει λοιπόν; Ναὶ, φησὶ, σφόδρα φροντίζω περὶ τοῦ ἁγνοῦ
καὶ ἀπαθοῦς γάμου. Εἰ γὰρ τῷ τῶν ἀνθρώπων ἐμεσίτευσα γάμῳ, πολλῷ μᾶλλον τὸ
τοῦ Χριστοῦ μεσιτεύσω μετὰ πολλῆς τῆς σπουδῆς· νυμφαγωγός εἰμι τοῦ Χριστοῦ·
οὐκ ἀρκεῖταί μου ὁ ∆εσπότης μιᾷ κόρῃ, οὐ θέλει ἕνα γάμον· πολλὴ αὐτοῦ ἡ προὶξ,
πολλὴ ἡ κληρονομία.
∆ιὸ ἀπέστειλέ με πολλὰς αὐτῷ συναγαγεῖν· ὅθεν ἔλεγεν, Ἡρμοσάμην ὑμᾶς
ἑνὶ ἀνδρὶ παρθένον ἁγνὴν παραστῆσαι τῷ Χριστῷ. Ὅρα πόσην ἀκρίβειαν εἶπεν ὁ
Ἀπόστολος, ἵνα μὴ σαρκικὸν τὸν γάμον νομίσῃς εἶναι. Ὢ τῆς τυφλότητος! ὢ τῆς
ἀνοίας! τίς οὐκ ἂν ποθήσειε τὸν ἄφθαρτον καὶ ἀκάματον γάμον; τὴν ἁγνὴν κοίτην,
τὴν ἄσπιλον παστάδα; τὴν ἀμόλυντον λοχείαν; τὴν ἀθάνατον συνάφειαν; τὴν ἀπαθῆ
τεκνογονίαν; τὴν ἀπένθητον φιλανδρίαν; τὴν ἀζήμιον προῖκα; τὴν αἰώνιον
ἀπόλαυσιν; τὴν ἀμέριμνον ζωήν; τὴν μετὰ Χριστοῦ συνοικεσίαν; τὴν μετὰ ἀγγέλων
διαγωγήν; τὴν τῶν θεοπρεπῶν ᾀσμάτων τερπνότητα; τὴν εὐωδίαν τὴν πανάρετον;
τὴν πάμφωτον λυχνίαν; τὴν παγκόσμιον ἐλευθερίαν; τὴν πρὸς Θεὸν πορείαν; τὴν
μετὰ ἀγγέλων χορείαν; τὴν τοῦ σώματος ῥῶσιν; τὴν τῆς ψυχῆς καθαρότητα; Ἡ γὰρ
συναφθεῖσα πρὸς γάμον ἀνδρὶ μεριμνᾷ πῶς ἀρέσει φθειρομένῃ σαρκί· οὔπω τίκτει,
καὶ τὸ πένθος ὀδυρᾶται· θρῆνος πολλάκις διὰ τὸν ἄνδρα, θρῆνος περὶ στειρώσεως
καὶ ἀτεκνίας, θρῆνος περὶ πολυτεκνογονίας, τούτων θνησκόντων· θρῆνος ἀτάκτως
τῶν τέκνων περιπατούντων. Ἡ δὲ τοῦ Κυρίου παρθένος μεριμνᾷ πως ἀρέσει τῷ
Κυρίῳ. Ἀκούσατε μετὰ ἀκριβείας τοῦ Ἀποστόλου βοῶντος, Ἡρμοσάμην ὑμᾶς ἑνὶ
ἀνδρὶ παρθένον ἁγνὴν παραστῆσαι τῷ Χριστῷ. Καὶ οὐκ εἶπε, Σὲ, ἀλλὰ, Ὑμᾶς, ἵνα μὴ
νομίσῃς περὶ σαρκικοῦ γάμου λέγειν αὐτόν. Παρακαλῶ οὖν ὑμᾶς, φησὶ, παραστήσατε
τὰ σώματα ὑμῶν θυσίαν ζῶσαν, ἁγίαν, εὐάρεστον τῷ Θεῷ. Κοινὸν τὸ παράγγελμα,
μάλιστα δὲ ἐπιβάλλει τοῖς ἁγιωσύνην ἀσκοῦσιν. Οἱ γὰρ τῷ ἁγίῳ ἁγίως προσελθεῖν
βουλόμενοι, ἐὰν μὲν κατορθώσωσιν ἐνταῦθα, μεγάλων καὶ λαμπρῶν στεφάνων
τυγχάνουσιν· οἵτινες δὲ προθέμενοι οὐκ ἐτελείωσαν, ἐξαίσιον αὐτοῖς τὸ πτῶμα
γίνεται. Καὶ γὰρ δεινὸν ἀπὸ ὕψους πεσεῖν, καὶ μαρτυρεῖ τὰ πράγματα, ἢ ἀπὸ τοῦ
χαμαὶ πεσεῖν. Πένης μὲν ἐὰν μέχρι τέλους τῇ πενίᾳ συζῇ, κουφοτέρως φέρει τὸν
ὀνειδισμόν· πλουσίου δὲ εἰς πενίαν ἐληλακότος θανάτου χεῖρον τὸ θέλημα γίνεται.
Οὕτως οἶμαι πικρότερον εἶναι τὸ δάκρυον καὶ τὸ πένθος θρηνωδέστερον τῆς
παρθενικῆς ἀπωλείας, τοῦ τῶν καθ' ἡμέραν δίκην χοίρων ἐν βορβόρῳ
κυλινδουμένων καὶ ἁμαρτανόντων· ἐπειδὴ ἐκείνοις τέχνη τῆς εὐπορίας ἡ συνήθεια
τῆς ἁμαρτίας γεγένηται λοιπόν· ἐπὶ δὲ ταῖς τὸν ναὸν τοῦ Θεοῦ φθείρασιν, πενθεῖν
οἶμαι καὶ θρηνεῖν, κόπτεσθαι καὶ κατατίλλεσθαι ἄξιον, τοῦ τοιούτου καὶ τηλικούτου
θησαυροῦ ἀθρόως διωρυγμένου.
 
 

2
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

Τί δέ ἐστιν ἐπὶ σοὶ, 64.40 ὦ παρθένε; πότε ἢ ποία ὄψις πολεμικὴ ἠπάτησέ σε;
ποῖα ξίφη; ποῖα δόρατα· ποῖαι ἀσπίδες; ποῖος στρατιωτῶν θόρυβος οὕτω ταχέως τῆς
ἀσφαλείας ἀποστῆναι ἠνάγκασε· σὺ ὑπῆρχες καὶ πόλις καὶ ναὸς τοῦ Χριστοῦ, καὶ
τεῖχος καὶ φύλαξ τῆς πόλεως καὶ τῆς πύλης. Ἀλλ' ὢ τοῦ θρήνου! πῶς συνεχώρησας
τῷ ἐχθρῷ ἄνευ ἀνάγκης ἀνοῖξαι ταύτην; πῶς βραχείᾳ ὥρᾳ δελεασθεῖσα ἐκείνην
προέδωκας, δι' ἧς ὁ Χριστὸς ἐξελθὼν ἐσφράγισεν; Ὢ τοῦ κακοῦ πολέμου! τὸ τεῖχος
διέῤῥηξας, τὸν ναόν σου τὸν ἅγιον διέφθειρας, τὰς συνθήκας τοῦ γάμου παρέβης, τὰ
γράμματα ἀπήλειψας, τὴν προῖκα ἠθέτησας, τὴν τοῦ Χριστοῦ κοίτην ἐνύβρισας·
ἀλλὰ μὴν καὶ τὸ ἐνέχυρον ἀπώλεσας. Οὐχ ὁρᾷς τοὺς ἐνταῦθα νόμους, ποῖαι τιμωρίαι
μένουσι τὴν μοιχαλίδα; Τί δὲ σὺ ποιήσεις, ἣ οὐχὶ συνδούλου κοίτην ἐνύβρισας, ἀλλὰ
τοῦ ∆εσπότου ἡμῶν Ἰησοῦ Χριστοῦ τοῦ ζῶντος ἀεὶ, πηλῷ φθειρομένῳ καὶ δυσωδίᾳ
συμπλακεῖσα; Πῶς σε πενθήσω οὐκ οἶδα· μηδενὸς γὰρ πολεμήσαντος τέτρωσαι, ἄνευ
ξίφους ἀποτέτμησαι. Τίνα σε κρίνω οὐκ οἶδα· ἄσπλαγχνον, ἢ ἀσυνείδητον, ἢ ὡς
ψυχοφθόρον, ἢ ὡς αὐτοφόνον.
Τοσαύτη γὰρ τῆς μανίας ἐκείνης ἡ ὄψις, οὐχ εὑρίσκουσα ἄξιον τοῦ πένθους
τὸν ὄνειδον. Ἆρα οὐκ ἐμνήσθης τῶν συνθηκῶν τοῦ Χριστοῦ; οὐκ ἐμνήσθης τῆς
ἁγνείας, ἐν ᾗ καθ' ἡμέραν σοι ὁ Χριστὸς πρὸς τῇ πλευρᾷ καὶ τῷ μαστῷ ἀοράτως καὶ
μυστικῶς σκιρτῶν καὶ ἀγαλλόμενος; Καὶ διὰ τί δέ; Ἐπειδὴ καὶ σὺ Μαρία ὑπῆρχες
ἕως οὗ τὴν παρθενείαν διεφύλαττες. Ποῖος οὖν κηρὸς πονηρίας συνέτηξέ σου τοὺς
ὀφθαλμούς; ποία σκοτομήνη ἐσκότισέ σε, ὥστε τὰ μέλη τοῦ Χριστοῦ ποιῆσαι μέλη
πορνείας; Ὢ τοῦ πένθους! τὴν πορνικὴν δυσωδίαν τῆς παρθενικῆς εὐωδίας
προέκρινας; τὸν τῆς ἡδονῆς βόρβορον τῷ μύρῳ κατήλλαξας; Οὐκ ἐμνήσθης τῆς
κατανυκτικῆς πλήρης ὥρας ἐκείνης, ἐν ᾗ συγχαίροντες ἄγγελοι ὁμοῦ τε καὶ
ἄνθρωποι εἰς τὸν νυμφῶνά σε τοῦ Χριστοῦ λαμπάδας κατέχοντες προέπεμπον,
∆αυιτικὰ ᾄσματα ᾄδοντες, καὶ τὴν πτωχὴν καὶ πένητα νύμφην Χριστοῦ καὶ βασιλίδα
πάντες ἐκήρυττον; Ὢ τίς μὴ πενθήσῃ τὴν βασιλέα καταλείπουσαν, σαπρίᾳ
σκωλήκων ἑαυτὴν συνάψασαν, [ἢ] καὶ μεθ' οὗ ἤμελλε συζῇν καὶ συμβασιλεύειν
κατεφρόνησε, καὶ τὸν ὑπὸ τοῦ βασιλέως ἐν γεέννῃ πυρὸς ἐμβάλλεσθαι μέλλοντα
ἐπόθησεν; Ἵνα τί τοιοῦτον βόθρον ἑαυτῇ κατώρυξας; Ἀλλὰ τὸ πλάτος σε τῆς ὁδοῦ
ἠπάτησε. Ποῖον πλάτος, παρακαλῶ; τί σοι ἐπηγγείλατο ὁ ὁδηγὸς τῆς ἀπωλείας, ὁ
πρόξενος τῆς γεέννης; ποίαν ἀπόλαυσιν βίου ὁ ἀνθρωποκτόνος; οὐκ ἤκουσας, ὦ
παρθένε, τοῦ ∆εσπότου λέγοντος ἐν Εὐαγγελίοις, Ἐὰν τὸν κόσμον ὅλον κερδήσῃς,
τὴν δὲ ψυχήν σου ζημιωθῇς, τί ὠφέλησας; Ἀλλ' ὢ τῆς τυφλότητος! ὢ τῆς ἀνοίας! ὢ
τῆς σκοτώσεως! ἐννόησον, ὦ παρθένε, τὴν ἀπώλειαν· ἐννόησον τίνα μὲν προκρίνεις,
τίνα δὲ ἀθετεῖς, τί δέ σοι τὸ χαριζόμενον παρὰ τοῦ ἀπατοῦντος· ῥοπὴν ἡδονῆς ἐν
πηλῷ συμπεφυρμένην, καὶ σταγόνα βορβόρου ἐν παρθενικῇ γαστρὶ ὑπὸ ἁμαρτίας
μιγνύμενον, καὶ γέενναν πυρὸς, καὶ σκώληκα ἀτελεύτητον ἀπάγουσα. Φρίξον τὴν
ἀσχημοσύνην ἐκείνην. Ἆρα γὰρ ταύτην ἡδονὴν ἔστιν εἰπεῖν ἀπόλαυσιν βίου; ἆρα
τοῦτο τρυφὴ, τοῦτο πλάτος; τίς αὕτη τῆς μανίας ἡ συγκατάθεσις; Ἀλλὰ νῆφε ἐν πᾶσι
καὶ λέγε· οὐ γὰρ αὑτοῦ τὰ νοήματα ἀγνοοῦμεν. Χαῖρε οὖν πάντοτε, καθὼς
γέγραπται· Τὸ ἐπιεικὲς ὑμῶν γνωσθήτω πᾶσιν ἀνθρώποις· ὁ φόβος τοῦ Θεοῦ
καταστραπτέτω ἐν τῇ καρδίᾳ σου. Μὴ ῥίψασπις γένῃ στρατιώτης, μὴ δειλὸς καὶ
ὀκνηρὸς ἐργάτης, μὴ φύγῃς τὸν στέφανον.
Ὁ βίος βραχὺς, ἡ δὲ κρίσις μακρά. Πρὸς ταύτην ἀφορῶν, ἀδελφὲ, ὑποφώνει
σου τὴν καρδίαν ἅμα τῷ ἁγίῳ, καὶ λέγε· Ἀνδρίζου, καὶ κραταιούσθω ἡ καρδία σου,
καὶ ὑπόμεινον τὸν Κύριον· μιμοῦ τὸν ∆αυῒδ, καὶ μιᾷ λίθου βολῇ κατάῤῥαξον τὸν
πολέμιον. Ἑστήκασιν ἄγγελοι θεωροὶ τοῦ βίου σου· Θέατρον, γὰρ φησὶν, ἐγενήθημεν
τῷ κόσμῳ καὶ ἀγγέλοις καὶ ἀνθρώποις· ἐάν σε ἴδωσι νικῶντα, χαίρουσιν ἐπὶ τῷ
κατορθώματι· ἐὰν ἡττώμενον, ἀναχωροῦσι στυγνοὶ, γελῶσι δὲ ἐπὶ σοὶ τὰ δαιμόνια.
 
 

3
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

Σπάσαι οὖν ἀντὶ ῥομφαίας τὸν φόβον τοῦ Θεοῦ· ὁ γὰρ φόβος τοῦ Θεοῦ ὡς μάχαιρα
δίστομός ἐστι, συγκόπτων πᾶσαν ἐπιθυμίαν πονηράν. 64.41 Λάμβανε οὖν πάντοτε
τὸν φόβον τοῦ Θεοῦ, μιμνησκόμενος τὴν ἐσχάτην ἡμέραν διαπαντὸς, ὅταν οἱ
οὐρανοὶ πυρούμενοι λυθήσονται, ὅταν ἄστρα ὡς φύλλα πεσοῦνται, ἥλιος δὲ καὶ
σελήνη σκοτισθήσονται, καὶ οὐ δώσουσι τὸ φέγγος αὐτῶν· ὅταν φανερωθῇ ὁ Υἱὸς
τοῦ ἀνθρώπου, καὶ κατέλθῃ ἀπὸ τῶν οὐρανῶν ἐπὶ τὴν γῆν· καὶ αἱ δυνάμεις τῶν
οὐρανῶν σαλευθήσονται· ὅταν ἀγγέλων καταδρομαὶ, αἱ φωναὶ τῶν σαλπίγγων
ἐπερχομέων, πῦρ ἐνώπιον αὐτοῦ καιόμενον καὶ διατρέχον κατακλύζειν τὴν
οἰκουμένην, κύκλῳ αὐτοῦ καταιγὶς σφόδρα, σεισμοὶ φοβεροὶ καὶ κεραυνοὶ, οἳ
οὐδέποτε ἐγένοντο οὐδὲ γίνονται ἕως ἐκείνης τῆς ἡμέρας, ὥστε καὶ αὐτὰς τὰς
δυνάμεις τῶν οὐρανῶν τρόμῳ μεγάλῳ συλληφθῆναι. Οὐκοῦν ποταμοὺς δεῖ εἶναι
ἡμᾶς, ἀδελφοί μου; ποῖος φόβος καὶ ποία φρίκη λήψεται ἡμᾶς; κατανόησον τὸν
Ἰσραὴλ ἐν τῇ ἐρήμῳ, ὅτε οὐκ ἴσχυσαν τὸν γνόφον καὶ τὸν ζόφον καὶ τὴν φωνὴν τοῦ
λαλοῦντος Θεοῦ ἐκ μέσου τοῦ πυρὸς, ἀλλ' ἐσπούδασαν τοῦ μὴ προσθεῖναι αὐτοῖς
λόγον. Οὐδὲ γὰρ κατὰ ἀλήθειαν ἔφερον τὸ διαστελλόμενον, καίπερ οὐ μετὰ θυμοῦ
κατελθόντος, οὐδὲ μετ' ὀργῆς λαλοῦντος πρὸς αὐτοὺς, ἀλλὰ παρακλήσει
πληροφοροῦντος αὐτοὺς, ὅτι αὐτός ἐστι μετ' αὐτῶν.
Ἄκουσον οὖν, ἀδελφέ μου, εἰ μετὰ παρακλήσεως οὐκ ἰσχύσαμεν βαστάσαι
τὴν ἔλευσιν αὐτοῦ, ὅτε οὔτε οἱ οὐρανοὶ πυρούμενοι ἐλύθησαν, οὔτε οἱ σαλπίζοντες
ἤχησαν καθὼς μέλλει ἡ σάλπιγξ ἐκείνη βοᾷν καὶ ἐξυπνίζειν τοὺς ἀπ' αἰῶνος
κεκοιμημένους, οὔτε πῦρ κατακλύζον τὴν οἰκουμένην, οὔτε τι τῶν μελλόντων
γίνεσθαι φοβερῶν ἐγένετο· τί ποιήσομεν, ὅταν κατέλθῃ μετ' ὀργῆς καὶ θυμοῦ
ἀνυποστάτου, καὶ καθίσῃ ἐπὶ θρόνου δόξης αὐτοῦ, καὶ προσκαλέσωνται τὴν γῆν ἀπὸ
ἀνατολῶν ἡλίου μέχρι δυσμῶν, καὶ ἀπὸ πάντων τῶν περάτων τῆς γῆς τοῦ διακρῖναι
τὸν λαὸν αὐτοῦ, καὶ ἀποδοῦναι ἑκάστῳ κατὰ τὰ ἔργα αὐτοῦ; Οἴμοι! ποταποὺς δεῖ
ὑπάρχειν ἡμᾶς, ὅτε παριστάμεθα γυμνοὶ καὶ τετραχηλισμένοι, μέλλοντες εἰσάγεσθαι
εἰς τὸ φρικτὸν βῆμα; οἴμοι, οἴμοι! ποῦ τότε ἡ περπερότης; ποῦ τότε ἡ ἀνδρεία τῆς
σαρκός; ποῦ δὲ τὸ κάλλος τὸ ψευδὲς καὶ ἀνωφελές; ποῦ τότε ἡ ἡδυφωνία τῶν
ἀνθρώπων; ποῦ τότε ἡ παῤῥησία ἡ ἀναιδὴς καὶ ἀναίσχυντος, ποῦ τότε ὁ
καλλωπισμὸς τῶν ἱματίων; ποῦ τότε ἡ ἡδονὴ τῆς ἁμαρτίας ἡ ἀκάθαρτος καὶ μιαρὰ
ἀληθῶς; ποῦ τότε οἱ τὴν κόπρον τῶν ἀῤῥένων ἡδονὴν ἡγούμενοι; ποῦ τότε οἱ μετὰ
τυμπάνων καὶ μουσικῶν τὸν οἶνον πίνοντες; ποῦ τότε ἡ καταφρόνησις τῶν ἐν
ἀφοβίᾳ ζώντων; ποῦ τότε ἡ τρυφὴ καὶ σπατάλη; πάντα γὰρ παρῆλθον καὶ ὡς χαῦνος
ἀὴρ διελύθησαν. Ποῦ τότε ἡ φιλαργυρία καὶ ἡ φιλοκτημοσύνη, καὶ ἡ ἐξ αὐτῶν
ἀσπλαγχνία; ποῦ τότε ἡ ἀπάνθρωπος ὑπερηφανία, ἡ πάντα βδελυσσομένη, καὶ αὐτὴ
λογιζομένη εἶναί τι; ποῦ τότε ἡ καινὴ καὶ ματαία ἀνθρωπίνη προκοπὴ καὶ δόξα; ποῦ
τότε ἡ δυναστεία καὶ ἡ τυραννίς; ποῦ τότε βασιλεὺς, ποῦ ἄρχων, ποῦ ἡγούμενος; ποῦ
οἱ ἐπ' ἐξουσιῶν, οἱ γαυριώμενοι ἐπὶ πλήθει πλούτου, καὶ τοῦ Θεοῦ καταφρονοῦντες;
Τότε ἰδόντες οὕτως ἐθαύμασαν, τρόμος ἐπελάβετο αὐτούς· ἐκεῖ ὠδῖνες ὡς τικτούσης,
ἐν πνεύματι βιαίῳ συντριβήσονται.
Ποῦ τότε ἡ σοφία τῶν σοφῶν; ποῦ τὰ μάταια αὐτῶν πανουργεύματα; Οὐαὶ
αὐτοῖς, Ἐταράχθησαν, ἐσαλεύθησαν ὡς ὁ μεθύων, καὶ πᾶσα ἡ σοφία αὐτῶν
κατεπόθη. Ποῦ τότε σοφός; ποῦ τότε γραμματεύς; ποῦ συνζητητὴς τοῦ αἰῶνος τοῦ
ματαίου τούτου; Ἀδελφέ μου, ἀναλόγισαι ποταποὺς δεῖ ὑπάρχειν ἡμᾶς, διδόντας
λόγον περὶ ὧν ἐπράξαμεν, εἴτε μεγάλων εἴτε μικρῶν· ἕως γὰρ ἀργοῦ λόγου
ἀπολογίαν διδοῦμεν τῷ δικαίῳ κριτῇ. Ποταποὺς οὖν δεῖ εἶναι ἡμᾶς ἐν τῇ ὥρᾳ
ἐκείνῃ, ἐὰν εὕρωμεν χάριν ἐνώπιον αὐτοῦ; ποία δὲ χαρὰ διαδέξεται ἡμᾶς
ἀφοριζομένους ἐκ δεξιῶν τοῦ βασιλέως; ποταποὺς δεῖ εἶναι ἡμᾶς, ὅταν ἀσπάζωνται
ἡμᾶς οἱ ἅγιοι πάντες; Ἐκεῖ ἀσπάζεταί σε Ἀβραὰμ, Ἰσαὰκ, Ἰακὼβ, Νῶε, Ἰὼβ, ∆αυῒδ,
 
 

4
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

καὶ οιἅγιοι προφῆται καὶ ἀπόστολοι καὶ μάρτυρες καὶ πάντες οἱ δίκαιοι οἱ τῷ Θεῷ
εὐαρεστήσαντες ἐν τῇ ζωῇ τῆς σαρκὸς αὐτῶν, καὶ ὅσους ἀκούεις καὶ θαυμάζεις
αὐτῶν τὸν βίον, καὶ ἤθελες ἄρτι θεάσασθαι αὐτοὺς, αὐτοὶ ἐκεῖ ἔρχονται πρὸς σὲ, καὶ
περιπτυσσόμενοι ἁρπάζονταί σε, ἀγαλλιώμενοι ἐπὶ τῇ σῇ σωτηρίᾳ. Ποταποὺς οὖν δεῖ
εἶναι ἡμᾶς τότε; ποταπὴν δὴ χαρὰν ἔχειν ἐκείνην τὴν ἀνεκλάλητον, ὅταν εἴπῃ ὁ
Βασιλεὺς τοῖς ἐκ δεξιῶν μεθ' ἱλαρότητος, ∆εῦτε, οἱ εὐλογημένοι τοῦ Πατρός μου,
64.42 κληρονομήσατε τὴν ἡτοιμασμένην ὑμῖν βασιλείαν ἀπὸ καταβολῆς κόσμου;
Τότε, ἀδελφέ μου, λήψῃ τὸ βασίλειον τῆς εὐπρεπείας, καὶ τὸ διάδημα τοῦ κάλλους
ἐκ χειρὸς Κυρίου, καὶ λοιπὸν σὺν Χριστῷ βασιλεύσεις· τότε κατακληρονομήσεις
ἐκεῖνα τὰ ἀγαθὰ, Ἃ ἡτοίμασεν ὁ Θεὸς τοῖς ἀγαπῶσιν αὐτόν· τότε λοιπὸν ἀμέριμνος
ἔσῃ, μηκέτι πτοούμενος μηδεμίαν πτόησιν. Ἀναλόγισαι, ἀδελφέ μου, οἷόν ἐστι τὸ
βασιλεῦσαι ἐν οὐρανῷ. Καθὼς γὰρ προείπομεν, δέξῃ τὸ διάδημα ἐκ χειρὸς Κυρίου,
καὶ βασιλεύσεις τοῦ λοιποῦ σὺν Χριστῷ. Ἀναλόγισαι, ἀδελφέ μου. οἷόν ἐστι τοῦ Θεοῦ
τὸ πρόσωπον βλέπειν διαπαντὸς, οἷόν ἐστιν ἔχειν αὐτὸν φωστῆρα.
Τότε γὰρ οὐκ ἔσται σοι ὁ ἥλιος ὡς φῶς ἡμέρας, καθώς φησιν Ἡσαΐας, Οὐδὲ
ἀνατολὴ σελήνης φωτιεῖ σοι τὴν νύκτα, ἀλλ' ἔσται σου Κύριος φῶς αἰώνιον, καὶ ὁ
Θεὸς δόξα σοι. Ἰδοὺ, ἀδελφέ μου, οἵα χαρὰ ἀπόκειται τοῖς φοβουμένοις τὸν Κύριον,
καὶ τοῖς φυλάσσουσι τὰς ἐντολὰς αὐτοῦ. Ἀναλόγισαι δὲ μετὰ ταῦτα καὶ τὴν τῶν
ἁμαρτωλῶν ἀπώλειαν, ὅταν εἰσάγωνται εἰς τὸ βῆμα τὸ φοβερὸν, οἵα αἰσχύνη
αὐτοὺς λήψεται ἐνώπιον τοῦ δικαίου κριτοῦ, μὴ ἔχοντες λόγον ἀπολογίας· οἵα
ἐντροπὴ λήψεται αὐτοὺς ἀφοριζομένους ἐξ εὐωνύμων τοῦ Βασιλέως· οἷον σκότος
ἐπιπέσει ἐπ' αὐτοὺς, ὅταν Λαλήσει πρὸς αὐτοὺς ἐν ὀργῇ αὐτοῦ, καὶ ἐν τῷ θυμῷ
αὐτοῦ ταράξει αὐτοὺς λέγων· Πορεύεσθε ἀπ' ἐμοῦ, οἱ κατηραμένοι, εἰς τὸ πῦρ τὸ
αἰώνιον τὸ ἡτοιμασμένον τῷ διαβόλῳ καὶ τοῖς ἀγγέλοις αὐτοῦ· οἴμοι, οἴμοι! οἵαν
θλῖψιν καὶ στενοχωρίαν ἕξει τὸ πνεῦμα αὐτῶν, ὅταν γένηται κραυγὴ πάντων
λεγόντων· Ἀποστραφήτωσαν οἱ ἁμαρτωλοὶ εἰς τὸν ᾅδην, πάντα τὰ ἔθνη τὰ
ἐπιλανθανόμενα τοῦ Θεοῦ! Οἴμοι! οἷός ἐστιν ὁ τόπος, ὅπου ἐστὶν Ὁ κλαυθμὸς καὶ ὁ
βρυγμὸς τῶν ὀδόντων, ὁ καλούμενος Τάρταρος, ὃν καὶ αὐτὸς ὁ Σατανᾶς φρίσσει!
οἴμοι! οἵα ἐστὶν ἡ γέεννα τοῦ πυρὸς τοῦ ἀσβέστου! οἴμοι! οἷός ἐστιν ὁ ἀκοίμητος καὶ
ἰοβόλος σκώληξ! οἴμοι! οἷοί εἰσιν οἱ ἄγγελοι οἱ ἐπὶ τῶν κολάσεων, ἄσπλαγχνοι καὶ
ἀνελεήμονες! ὀνειδίζουσι γὰρ καὶ ἐπιπλήττουσι δεινῶς. Τότε οἱ κολαζόμενοι
κεκράξονται πρὸς Κύριον, καὶ οὐκ εἰσακούσεται αὐτῶν· τότε γνώσονται ὅτι μάταια
αὐτοῖς ἀπέβη πάντα τὰ τοῦ βίου, καὶ ἃ ἐνόμιζον ἐντεῦθεν ἡδέα εἶναι, χολῆς καὶ ἰοῦ
πικρότερα εὑρέθησαν. Ποῦ τότε ἡ ψευδώνυμος ἡδονὴ τῆς ἁμαρτίας; ἄλλη γὰρ ἡδονὴ
οὐκ ἔστιν εἰ μὴ τὸ φοβεῖσθαι τὸν Κύριον. Ἀληθῶς τοῦτο ἡδονή ἐστιν εἰ μὴ τὸ
φοβεῖσθαι τὸν Κύριον. Ἀληθῶς τοῦτο ἐμπιπλᾷ τὴν ψυχήν.
Τότε καταγνώσουσιν ἑαυτῶν, καὶ τῶν ἔργων ὧν ἔπραξαν· τότε
ὁμολογήσουσι λέγοντες ὅτι δικαία ἡ κρίσις τοῦ Θεοῦ. Ἠκούσαμεν γὰρ ταῦτα, καὶ οὐκ
ἠθελήσαμεν ἐπιστρέψαι ἀπὸ τῶν πονηρῶν ἡμῶν πράξεων· καὶ τότε ἀνοίσουσι ταῦτα
λέγοντες· Οἴμοι τῷ ἐν ἁμαρτίαις ἀνεικάστοις συλληφθέντι· ὑπὲρ ἀριθμῶν ψάμμου
θαλάσσης ἥμαρτον, καὶ κατακάμπτομαι ἐξ αὐτῶν ὡσεὶ ἀπὸ πολλῶν δεσμῶν σιδήρου·
οὐ γὰρ ἔστι μοι παῤῥησία ἀτενίσαι εἰς τὸ ὕψος τοῦ οὐρανοῦ. Πρὸς τίνα οὖν
καταφύγω, εἰ μὴ πρὸς σὲ, φιλάνθρωπε, εἰ μὴ πρὸς σὲ, ἀμνησίκακε; Ἐλέησόν με, ὁ
Θεὸς, κατὰ τὸ μέγα σου ἔλεος, καὶ κατὰ τὸ πλῆθος τῶν οἰκτιρμῶν σου ἐξάλειψον τὸ
ἀνόμημά μου. Ἐπὶ πλεῖον πλῦνόν με ἀπὸ τῆς ἀνομίας μου, καὶ ἀπὸ τῆς ἁμαρτίας μου
καθάρισόν με· ὅτι τὴν ἀνομίαν μου ἐγὼ γινώσκω· καὶ ἡ ἁμαρτία μου ἐνώπιόν μού
ἐστι διαπαντός. Σοὶ μόνῳ ἥμαρτον, καὶ τὸ πονηρὸν ἐνώπιόν σου ἐποίησα. Πρὸς σὲ
καταφεύγω διὰ τὴν πολλήν σου ἀγαθότητα καὶ εὐσπλαχνίαν· σὲ παρώργισα, καὶ
πρὸς σὲ καταφεύγω διὰ τὴν πολλήν σου ἀρνησικακίαν· σὲ ἠθέτησα, καὶ πρὸς σὲ
 
 

5
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

καταφεύγω διὰ τὴν πολλήν σου ἀγαθότητα καὶ φιλανθρωπίαν, καὶ δεόμενος βοῶ·
Ἀπόστρεψον τὸ πρόσωπόν σου ἀπὸ τῶν ἁμαρτιῶν μου, καὶ πάσας τὰς ἀνομίας μου
ἐξάλειψον. Καρδίαν καθαρὰν κτίσον ἐν ἐμοὶ, ὁ Θεὸς, καὶ πνεῦμα εὐθὲς ἐγκαίνισον ἐν
τοῖς ἐγκάτοις μου, διὰ τὸ ὄνομά σου, καὶ μόνον. Οὐδὲν γὰρ ἔχω προσενέγκαι σοι, οὐ
πρᾶξιν ἀγαθὴν, οὐ καρδίαν καθαρὰν, ἀλλὰ θαῤῥῶν εἰς τοὺς οἰκτιρμούς σου
ἐπιῤῥίπτω ἐμαυτὸν, ὅπως κτίσῃς ἐν ἐμοὶ κατάνυξιν, ἵνα μὴ πάλιν καταπίπτω
εὐκόλως εἰς ἁμαρτίαν, ἀλλ' ἐκ τοῦ νῦν λατρεύσω σοι ἐν ὁσιότητι καὶ δικαιοσύνῃ
πάσας τὰς ἡμέρας τῆς ζωῆς μου, ὅτι σοῦ ἡ βασιλεία καὶ τὸ κράτος εἰς τοὺς αἰῶνας.
Ἀμήν. 64.43 Παρακαλῶ οὖν ὑμᾶς ταῦτα προσδοκοῦντας, ἀδελφοί μου, σπουδάζειν
ἀσπίλους καὶ ἀμώμους αὐτῷ εὑρεθῆναι ὑμᾶς ἐν εἰρήνῃ. Ὅταν ἐπέλθῃ σοι πονηρὰ
ἐνθύμησις, σπάσαι τὴν μάχαιραν ταύτην, τουτέστι τὸν φόβον τοῦ Θεοῦ ἀναλόγισαι,
καὶ συγκόπτεις πᾶσαν τὴν δύναμιν τοῦ ἐχθροῦ. Ἀντὶ δὲ σάλπιγγος ἔχε τὰς θείας
Γραφάς· καθ' ὃν γὰρ τρόπον ἡ σάλπιγξ βοῶσα ἐπισυνάγει τοὺς στρατιώτας, οὕτω καὶ
αἱ θεῖαι Γραφαὶ βοῶσαι πρὸς ἡμᾶς ἐπισυνάγουσιν ἡμῶν τοὺς λογισμοὺς εἰς τὸν
φόβον τοῦ Θεοῦ. Καὶ γάρ εἰσιν οἱ λογισμοὶ ἡμῶν δίκην στρατιωτῶν πολεμοῦντες
πρὸς τοὺς ἐχθροὺς τοῦ βασιλέως. Πάλιν ὃν τρόπον ἡ σάλπιγξ βοῶσα ἐν καιρῷ
πολέμου διεγείρει τῶν νέων καὶ ἀγωνιστῶν τὴν προθυμίαν κατὰ τῶν ἀντιδίκων,
οὕτω καὶ αἱ θεῖαι Γραφαὶ διεγείρουσί σου τὴν προθυμίαν εἰς τὸ ἀγαθὸν, καὶ
ἀνδρίζουσί σε κατὰ τῶν παθῶν.
∆ιὸ, ἀδελφέ μου, ὅσον δύνασαι ἀνάγκαζε σεαυτὸν πυκνότερον ἐντυγχάνειν
αὐταῖς, ὅπως ἐπισυνάξουσί σου τοὺς λογισμοὺς, οὓς διασκορπίζει ὁ ἐχθρὸς τῇ αὐτοῦ
κακομηχανίᾳ, ἢ πονηρὰς ἐνθυμήσεις, ἢ καὶ θλίψεις πολλάκις ἐπιφέρων, ἢ εὐημερίας
καὶ εὐρυχωρίας πολλὰς παρέχων. Ταῦτα γὰρ κατεργάζεται τῇ αὐτοῦ πανουργίᾳ καὶ
δολιότητι, ὅπως ἀπαλλοτριώσῃ ἀπὸ τοῦ Θεοῦ 64.44 τὸν ἄνθρωπον. Ὅταν γὰρ
πολλάκις μὴ δυνηθῇ διὰ τῶν ἐνθυμήσεων ἐπηρεάσαι καὶ καταβαλεῖν, τότε λοιπὸν
ἐπιφέρει αὐτῷ θλίψεις, ὅπως σκοτίσῃ τὴν διάνοιαν, καὶ εὑρήσῃ λοιπὸν ἐπισπεῖραι, ἃ
βούλεται. Καὶ ἄρχεται ὑποβάλλειν τότε τῷ ἀνθρώπῳ τὰ τοιαῦτα ἐνθυμήματα, καὶ
λέγειν μεθ' ὅρκων, ὅτι Ἀφ' οὗ ἐργάζομαι τὸ ἀγαθὸν, κακὰς ἡμέρας εἶδον· Ποιήσωμεν
οὖν τὰ κακὰ, ἵνα ἔλθῃ τὰ ἀγαθά. Τότε ἐὰν μὴ εὑρεθῇ τις νήφων, καταπίνει αὐτὸν,
ὥσπερ ᾅδης, ζῶντα. Εἰ δὲ καὶ ἐν τούτοις μὴ δυνηθῇ αὐτὸν ἐπηρεάσαι, τότε ἐπιφέρει
αὐτῷ τὴν εὐρυχωρίαν, καὶ ὑψώνει αὐτὸν καὶ ἀπάτην πολλὴν παρέχει αὐτῷ τὴν
δεινοτέραν καὶ χείρονα πάντων τῶν παθῶν.
Αὕτη γάρ ἐστιν ἡ ὑπερήφανον καὶ ἄφοβον ἐργαζομένη τὸν ἄνθρωπον· αὕτη
εἰς τὸν βυθὸν τῶν ἡδονῶν κατασπᾷ τὸν νοῦν· αὕτη ποιεῖ τὸν ἄνθρωπον μὴ
γινώσκειν Θεὸν, μηδὲ γινώσκειν τὴν ἰδίαν ἀσθένειαν, μηδὲ ἐννοεῖσθαι τὴν ἡμέραν
τοῦ θανάτου καὶ τῆς κρίσεως· αὕτη γάρ ἐστιν ἡ ὁδὸς πάντων τῶν κακῶν. Ταύτην
τὴν ὁδὸν ὁ ἀγαπῶν βαδίζειν τὴν τῆς εὐρυχωρίας καὶ ἀνέσεως, εἰς τὰ ταμεῖα τοῦ
θανάτου εὑρίσκεται φθάνων· αὕτη ἐστὶν ἡ ὁδὸς, ἣν ὁ Κύριος εἶπε· Πλατεῖα καὶ
εὐρύχωρος ἡ ὁδὸς, ἡ ἀπάγουσα εἰς τὴν ἀπώλειαν.

 
 

6
Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

