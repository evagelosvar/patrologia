In resurrectionem domini B
Τοῦ ἐν ἁγίοις πατρὸς ἡμῶν Ἰωάννου ἀρχιεπισκόπου
Κωνσταντινουπόλεως τοῦ Χρυσοστόμου λόγος εἰς τὸ ἅγιον πάσχα καὶ
εἰς τὰς μυροφόρους καὶ εἰς τοὺς νεοφωτίστους καὶ εἰς τὸ Ἐν ἀρχῇ ἦν ὁ
λόγος
Πάντοτε μὲν χαίρειν πάρεστι τῇ καθ' ἡμᾶς τοῦ Χριστοῦ ἐκκλησίᾳ, πολλῷ δὲ
μᾶλλον ἐν τῇ παρούσῃ τῆς ἡμέρας μυριοφώτῳ λαμπαδουχίᾳ, ὅτε καὶ αὐτὸς ὁ τῶν
ὅλων δεσπότης Χριστός, ὁ τῆς δικαιοσύνης ἥλιος, ἐκ τῶν ταφιαίων νυμφικῶν
θαλάμων ἀνατείλας τῇ ἀναστάσει πρὸ πάντων τὸ τῶν γυναικῶν φῦλον
ἀκτινοβόλησε φήσας πρὸς αὐτάς· Χαίρετε. Πρώτη φωνὴ τῆς ἀναστάσεως χαίρετε· ὁ
φόβος ἐχώσθη καὶ ἡ χάρις ἐδόθη.
Πρώτη φωνὴ τῆς ἀναστάσεως χαίρετε· καὶ πρὸ πάντων γυναιξὶ συνήντησε
καὶ πρὸς αὐτὰς ὁ κύριος εἶπεν· Ταχὺ πορευθεῖσαι εἴπατε τοῖς μαθηταῖς μου ὅτι
ἀνέστην ἀπὸ τῶν νεκρῶν καὶ προάγω ὑμᾶς εἰς τὴν Γαλιλαίαν, ἐκεῖ με ὄψεσθε.
Γυναιξὶ συνήντησεν, ὅπου γὰρ ἐπλεόνασεν ἡ ἁμαρτία, ὑπερεπερίσσευσεν ἡ χάρις. Ὢ
τῆς τοῦ κυρίου σοφῆς πραγματείας. Οὐκ ἀπέστειλε πρὸς τοὺς μαθητὰς οὐκ ἄγγελον,
οὐκ αὐτὸς παραγέγονεν, ἀλλὰ γυναῖκας ἀπέστειλεν· καὶ οὐχ ἁπλῶς γυναῖκας, ἀλλὰ
πιστὰς γυναῖκας, τὰς αὐτοῦ στρατιώτιδας. Ταύτας ἀπέστειλε πρὸς τοὺς μαθητὰς
ὁμοῦ καὶ χαροποιῶν καὶ ὀνειδίζων· χαροποιῶν μὲν ὅτι ἀνέστη ἐκ νεκρῶν ὁ ἐν
νεκροῖς ἐλεύθερος, ὀνειδίζων δὲ ὅτι ἄνδρες ὄντες οἱ μαθηταὶ τῇ δειλίᾳ
κατεκρύπτοντο καὶ γυναῖκες τῇ παρρησίᾳ φαιδρυνθεῖσαι τὴν ἀνάστασιν
εὐηγγελίζοντο. Ὄντως οὖν εὔκαιρον εἰπεῖν καὶ τὴν παροῦσαν ἐκκλησίαν διὰ τὴν
τῶν ἀναγεννηθέντων πνεύματι πολυτοκίαν· Ἰδοὺ ἐγὼ καὶ τὰ παιδία, ἅ μοι ἔδωκεν ὁ
θεός. Παιδία τοὺς νεοφωτίστους καλῶ διὰ τὸ ἄκακον λοιπὸν τῆς διαθέσεως, παιδία
ὡς γαλακτιζόμενα τὸ τοῦ κυρίου γάλα.
Οὐ μόνον δὲ παιδία, ἀλλὰ καὶ νεφέλαι καὶ περιστεραὶ τὸ τῶν νεοφωτίστων
τάγμα προσαγορεύεται. Καὶ τίς τούτου μάρτυς; Ἠσαΐας ὁ προφήτης. Ἄκουσον αὐτοῦ
λέγοντος· Τίνες οἵδε ὡς νεφέλαι πέτανται καὶ ὡς περιστεραὶ σὺν νεοσσοῖς; Καὶ διὰ τί
νεφέλαι καὶ περιστεραὶ οἱ νεοφώτιστοι; Νεφέλαι διὰ τὸ τῆς γῆς χωρισθῆναι, διὰ τὸ
περιπολεῖν τὸν ἀέρα, διὰ τὸ τὰ ἐπουράνια ζητεῖν, διὰ τὸ φέρειν τὸν νοητὸν ὑετὸν
τὸν κατελθόντα ἐπὶ τὸν νοητὸν πόκον. Περιστεραὶ ὡς ἀπωθούμεναι τὸν διαβολικὸν
κόρακα, περιστεραὶ ὡς τὸ κάρφος τῆς εὐσεβείας ἐν τῷ στόματι φέροντες. Οὕτως καὶ
ἕτερος προφήτης κέκραγε λέγων· Εὐφράνθητι, στεῖρα ἡ οὐ τίκτουσα, ῥῆξον καὶ
βόησον, ἡ οὐκ ὠδίνουσα, ὅτι πολλὰ τὰ τέκνα τῆς ἐρήμου μᾶλλον ἢ τῆς ἐχούσης τὸν
ἄνδρα. Ἔρημος ἤκουσεν ἡ ἐξ ἐθνῶν ἐκκλησία, πρὸ τῆς συναφείας τοῦ δεσπότου
Χριστοῦ ἔρημος ἦν ὡς μὴ ἔχουσα τὸν φοίνικα τῆς δικαιοσύνης, ὡς μὴ κεκτημένη τὴν
σταυρικὴν ἀναδενδράδα. Ἀλλ' ἡ ἔρημος γέγονεν ἔγκαρπος οὐ φυσικῶς προκόψασα,
ἀλλὰ θεϊκῶς καρποφορήσασα, ὡς δύνασθαι λέγειν· Ἐγὼ δὲ ὡς ἐλαία κατάκαρπος ἐν
τῷ οἴκῳ τοῦ θεοῦ. Καθολικὴν ἑορτὴν πανηγυρίζομεν σήμερον, ὁμοῦ καὶ τὸν νόμον
πληροῦντες καὶ τὴν χάριν κυροῦντες, ὅτι ὁ νόμος διὰ Μωσέως ἐδόθη, ἡ χάρις καὶ ἡ
ἀλήθεια διὰ Ἰησοῦ Χριστοῦ ἐγένετο.
Ὅπου δοῦλος ἐκεῖ τὸ ἐδόθη, ὅπου ἐλεύθερος ἐκεῖ τὸ ἐγένετο, ὅτι ὁ δοῦλος
μὲν διακονεῖ, ὁ δεσπότης δὲ αὐθεντεῖ. Καὶ πάλιν· Πλήρωμα νόμου Χριστὸς εἰς
δικαιοσύνην παντὶ τῷ πιστεύοντι. Ὁ νόμος ἐπληρώθη, ἡ χάρις ἀντεισήχθη· ὁ
ἰουδαϊκὸς λαὸς ἐπαλαιώθη, ὁ χριστιανικὸς λαὸς ἀνεκαινίσθη. ∆ιὸ περὶ μὲν ἐκείνου
εἴρηται· Τὸ παλαιούμενον καὶ γηράσκον ἐγγὺς ἀφανισμοῦ, περὶ δὲ τούτου·
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

Ἀνακαινισθήσεται ὡς ἀετοῦ ἡ νεότης σου. Ἐκεῖ ἡ σκιὰ ὧδε ἡ ἀλήθεια, ἐκεῖ τὰ ἄζυμα
ὧδε ὁ ἄρτος, ἐκεῖ ἡ περιτομὴ ὧδε ἡ σφραγίς, ἐκεῖ ἡ κιβωτὸς ὧδε ἡ παρθένος, ἐκεῖ αἱ
πλάκες ὧδε τὰ εὐαγγέλια, ἐκεῖ ἡ στάμνος ὧδε τὸ θυσιαστήριον, ἐκεῖ ἡ ῥάβδος Ἀαρὼν
ἀνθήσασα κάρυα ὧδε ἡ σταυρικὴ βακτηρία λῃστὴν εἰς παράδεισον καρποφορήσασα,
ἐκεῖ ἡ ἔρημος ὧδε ὁ παράδεισος, ἐκεῖ ἡ γῆ τῆς ἐπαγγελίας ὧδε ἡ κληρονομία τῆς τῶν
οὐρανῶν βασιλείας. ∆ιὸ καὶ οὐρανοῦ τάξιν τὰ παρόντα μιμεῖται τῇ τῶν
νεοφωτίστων ἀστρολαμψίᾳ· καὶ ἄνω ἀστέρες καὶ κάτω ἀστέρες. Οὐρανὸς ἡμῖν
σήμερον ἡ γῆ, οὐ τῆς τοποθεσίας ἀμειφθείσης ἀλλὰ τῆς προτέρας εὐλογίας
διαδοθείσης. Οὐκέτι τὸ τῆς ἔχθρας μεσότοιχον κρατεῖ, ἀλλὰ τὸ τῆς φιλίας ἐνέχυρον
δεξιοδεκτεῖ· οὐκέτι ἀκούομεν· Γῆ εἶ καὶ εἰς γῆν ἀπελεύσῃ, ἀλλ' Ἡμῶν τὸ πολίτευμα
ἐν οὐρανοῖς ὑπάρχει.
Ὄντως οὖν εὔκαιρον εἰπεῖν ἀρτίως ὑποψάλλοντας· Αὕτη ἡ ἡμέρα ἣν
ἐποίησεν ὁ κύριος, ἀγαλλιασώμεθα καὶ εὐφρανθῶμεν ἐν αὐτῇ. Ἀγαλλιασώμεθα ὅτι
τὰ ἀγάλματα τῶν εἰδώλων συνετρίβη καὶ τὰ ἅγια τῶν ἁγίων ἐδόθη· εὐφρανθῶμεν
ὅτι ἡ λύπη τῆς Εὔας ἐσβέσθη καὶ ἡ χαρὰ τῆς παρθένου ἀνήφθη. Αὕτη ἡ ἡμέρα ἣν
ἐποίησεν ὁ κύριος. Τί αὐτὴν ἐποίησεν; Βασίλισσαν τῶν ἄλλων ἡμερῶν. ∆ιὸ καὶ ὁ
∆αβὶδ περὶ αὐτῆς πρὸς τὸν βασιλέα Χριστὸν κέκραγε λέγων· Παρέστη ἡ βασίλισσα ἐκ
δεξιῶν σου ἐν ἱματισμῷ διαχρύσῳ περιβεβλημένη πεποικιλμένη. Καὶ τί διάχρυσον
ἱμάτιον; Τὸ τῆς πίστεως ἔνδυμα, χρυσὸς γὰρ ἡ πίστις διὰ τὸ ἀπαραχάρακτον τοῦ
βαπτίσματος. Παρέστη ἡ βασίλισσα ἐκ δεξιῶν σου περι-βεβλημένη πεποικιλμένη. Τί
περιβεβλημένη; Τὸ τῆς εὐσεβείας ἀπαλαίωτον θέριστρον. Τί πεποικιλμένη; Τὴν τῶν
δογμάτων ποικιλόγλωττον εὐπρέπειαν. Αὕτη ἡ ἡμέρα ἣν ἐποίησεν ὁ κύριος,
ἀγαλλιασώμεθα καὶ εὐφρανθῶμεν ἐν αὐτῇ· οὐκ οἴνῳ μεθύσκοντι ἀλλὰ λόγῳ πρὸς
κατάνυξιν ἄγοντι. Λογικοὶ τυγχάνοντες λόγον εὐφροσύνης προσδεξώμεθα. Καὶ τίς ὁ
εὐφραντικὸς λόγος;
Αὐτὸς ὁ θεὸς λόγος, ὁ ἡμᾶς μὲν εὐφραίνων αἱρετικοὺς δὲ μαστίζων.
Ἤκουσας ἀρτίως Ἰωάννου βοῶντος· Ἐν ἀρχῇ ἦν ὁ λόγος καὶ ὁ λόγος ἦν πρὸς τὸν
θεὸν καὶ θεὸς ἦν ὁ λόγος. Τί ἐροῦσιν αἱρετικῶν παῖδες, οἱ ἄλογοι τὴν θρησκείαν; Ἐν
ἀρχῇ ἦν ὁ λόγος· μή που κτίσμα, μή που ποίημα, μή που ὑπεξούσιον, μή που ἄτονον,
μή που τὸ οὐκ ἦν. Ἰωάννης τὸ ἦν καὶ Ἄρειος τὸ οὐκ ἦν. Τίνι πεισθῶ; Ἰωάννῃ τῷ
ἀνακλιθέντι ἐπὶ τὸ στῆθος τοῦ κυρίου ἢ Ἀρείῳ τῷ ἀθρῶον εἰς κοπρώδεις
ἀποψύξαντι τόπους διὰ τὴν εἰς τὸν υἱὸν τοῦ θεοῦ βλασφημίαν. Ἐν ἀρχῇ ἦν ὁ λόγος·
οὐκ εἶπεν ἐγένετο, ἵνα μὴ γένεσιν θεότητος εἰσαγάγῃ· ἀγενεσιούργητος γὰρ τῆς
θεότητος ἡ ὕπαρξις οὐδὲ ἀρχὴν ἡμερῶν ἔχουσα οὐδὲ ζωῆς τέλος, μένει δὲ κατὰ
ταὐτὰ καὶ ὡσαύτως ἔχουσα· μεταβολὴν γὰρ ἡ θεία φύσις οὐκ ἐπιδέχεται. Τί οὖν ὁ
∆αβίδ; Ποθῶ μεθ' ὑμῶν πάλιν εἰπεῖν· Αὕτη ἡ ἡμέρα ἣν ἐποίησεν ὁ κύριος,
ἀγαλλιασώμεθα καὶ εὐφρανθῶμεν ἐν αὐτῇ. Ὄντως εὐφροσύνης ἀνάμεστος ἡ
παροῦσα ἡμέρα. ∆ιὰ τί εὐφροσύνης ἀνάμεστος; Ὅτι ἐν ταύτῃ τῇ ἡμέρᾳ τὸν ᾅδην
ἔφραξε καὶ τὸν οὐρανὸν ἀνέῳξεν, τὸν ᾅδην ἐσκύλευσε καὶ ζωὴν ἀνέθαλεν, τὸν
διάβολον ἔδησε καὶ τὸν Ἀδὰμ ἀπέλυσεν, τὴν φλογίνην ῥομφαίαν μετέστησε καὶ τοῦ
πιστεύσαντος λῃστοῦ τὴν ψυχὴν τῷ ζωοποιῷ ξύλῳ εἰς τὸν παράδεισον εἰσήγαγεν,
τοὺς κριοὺς καὶ τοὺς θεοὺς ἐφυγάδευσε καὶ τὸν λογικὸν ἀμνὸν μελίζεσθαι
παρεσκεύασεν. Καὶ τίς τούτου μάρτυς; Ὁ Παῦλος βοῶν· Τὸ πάσχα ἡμῶν ὑπὲρ ἡμῶν
ἐτύθη Χριστός. Ὥστε ἑορτάζομεν μὴ ἐν ζύμῃ παλαιᾷ μηδὲ ἐν ζύμῃ κακίας καὶ
πονηρίας, ἀλλ' ἐν ἀζύμοις εἰλικρινείας καὶ ἀληθείας. Τὸ χριστόγευστον πάθος
ἑορτάζειν προσετάχθημεν, οὐχὶ δὲ ἐν ἀζύμοις ἰουδαϊκῆς πονηρίας.
Ἐπειδὴ ἄλλα μὲν Ἰουδαῖοι μετέρχονται, ἄλλα δὲ ἡμεῖς· ἐκεῖνοι τὴν πονηρίαν
ἡμεῖς τὴν εὐλογίαν. Ἄλλα ἐκεῖνοι περὶ τὸν δεσπότην Χριστόν· ἐκεῖνοι ἔσφαξαν
ἡμεῖς ὀψωνήσαμεν, ἐκεῖνοι τὴν πλευρὰν διώρυξαν ἡμεῖς τὸ μυστικὸν γάλα
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

ἠντλήσαμεν, ἐκεῖνοι τὸ ὄξος ἡμεῖς τὸ αἷμα, ἐκεῖνοι τὴν χολὴν ἡμεῖς δὲ τὴν
γλυκύτητα, ἐκεῖνοι τὸν τάφον ἡμεῖς δὲ τὸν οὐρανόν, ἐκεῖνοι τὸν λίθον ἡμεῖς τὸν
θρόνον, ἐκεῖνοι τὴν σφραγίδα ἡμεῖς τὸν θησαυρόν, ἐκεῖνοι τὴν στρατιωτικὴν
κουστωδίαν ἡμεῖς δὲ τὴν ἀγγελικὴν προεδρίαν, ἐκεῖνοι τὸν Πιλᾶτον ἡμεῖς τὸν
Παῦλον, ἐκεῖνοι τὸν Βαραββᾶν ἡμεῖς τὸν Ἀβραάμ. Αὕτη ἡ ἡμέρα ἣν ἐποίησεν ὁ
κύριος, ἀγαλλιασώμεθα καὶ εὐφρανθῶμεν ἐν αὐτῇ, ὅτι οἱ λογικοὶ ἄρνες τῇ
δεσποτικῇ φάτνῃ παριστάμενοι τὸ τοῦ κυρίου σῶμα χλοάζονται. Οἱ ἄνθρωποι
γεγόνασιν ἄγγελοι, οἱ δοῦλοι ἐλεύθεροι, οἱ στρατιῶται βασιλεῖς, οἱ τὴν τοῦ Ἀδὰμ
μηλωτὴν περιφέροντες τὴν βασιλικὴν χλαῖναν περιβέβληνται. Γένοιτο δὲ πάντας
ἡμᾶς τῆς τοῦ Χριστοῦ ἀναστάσεως ἀξίους εὑρεθῆναι καὶ ἐν τῷ νῦν αἰῶνι καὶ ἐν τῷ
μέλλοντι, νῦν καὶ ἀεὶ καὶ εἰς τοὺς αἰῶνας τῶν αἰώνων. Ἀμήν.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

3

