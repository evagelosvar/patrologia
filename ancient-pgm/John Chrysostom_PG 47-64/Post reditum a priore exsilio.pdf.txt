Post reditum a priore exsilio
ΤΟΥ ΑΥΤΟΥ ΕΠΑΝΕΛΘΟΝΤΟΣ Ἀπὸ τῆς προτέρας ἐξορίας ὁμιλία.
52.443
αʹ. Ὅτε τὴν Σάῤῥαν ἀπὸ τοῦ Ἀβραὰμ ἥρπασεν ὁ Φαραὼ, τὴν καλὴν καὶ εὐειδῆ
γυναῖκα ὁ πονηρὸς καὶ βάρβαρος καὶ Αἰγύπτιος, ἀδίκοις ὀφθαλμοῖς ἰδὼν αὐτῆς τὸ
κάλλος, καὶ μοιχείας ἐργάσασθαι δρᾶμα βουλόμενος· τότε δὴ, τότε παρὰ μὲν τὴν
ἀρχὴν οὐκ ἐκόλασεν ὁ Θεὸς, ἵνα δειχθῇ καὶ τοῦ δικαίου ἡ ἀνδρεία, καὶ τῆς γυναικὸς
ἡ σωφροσύνη, καὶ τοῦ βαρβάρου ἡ ἀκολασία, καὶ ἡ τοῦ Θεοῦ φιλανθρωπία· τοῦ
δικαίου ἡ ἀνδρεία, ὅτι ἔφερεν εὐχαρίστως τὸ γενόμενον· τῆς γυναικὸς ἡ σωφροσύνη,
ὅτι ἐνέπεσεν εἰς τὰς βαρβαρικὰς χεῖρας, καὶ τὴν σεμνότητα διετήρησε· τοῦ βαρβάρου
ἡ ἀκολασία, ὅτι ἀλλοτρίᾳ ἐπῆλθεν εὐνῇ· τοῦ Θεοῦ ἡ φιλανθρωπία, ὅτι μετὰ
ἀπόγνωσιν ἀνθρώπων, τότε τὸν στέφανον ἤνεγκε τῷ δικαίῳ. Ταῦτα ἐγίνετο τότε ἐπὶ
τοῦ Ἀβραὰμ, ἐγένετο δὲ σήμερον ἐπὶ τῆς Ἐκκλησίας. Αἰγύπτιος οὗτος, ὡς ἐκεῖνος
Αἰγύπτιος· οὗτος δορυφόρους εἶχεν, ἐκεῖνος ὑπασπιστάς· ἐκεῖνος τὴν Σάῤῥαν, οὗτος
τὴν Ἐκκλησίαν· μίαν νύκτα ἐκεῖνος συνέσχε, μίαν ἡμέραν οὗτος εἰσῆλθεν· οὐδ' ἂν
τὴν μίαν συνεχωρήθη, ἀλλ' ἵνα δειχθῇ τῆς νύμφης ἡ σωφροσύνη, ὅτι εἰσέρχεται, καὶ
οὐ διεφθάρη αὐτῆς τὸ κάλλος τῆς σωφροσύνης, καίτοι μοιχὸν ἡτοίμασε, καὶ τὰ
γραμματεῖα συνετελεῖτο, καὶ πολλοὶ τῶν τῆς οἰκίας ὑπέγραφον. Ἀπηρτίσθη ἡ
μηχανὴ, καὶ τὸ τέλος οὐκ ἐγένετο.
Ἐφάνη ἐκείνου ἡ πονηρία, καὶ τοῦ Θεοῦ ἡ φιλανθρωπία. Ἀλλ' ὁ μὲν
βάρβαρος τότε ἐκεῖνος ἐπιγνοὺς τὸ ἁμάρτημα, ὡμολόγησε τὸ παρανόμημα· λέγει γὰρ
τῷ Ἀβραάμ· Τί ἐποίησας τοῦτο; εἰς τί εἶπας, ὅτι ἀδελφή μού ἐστι; καὶ μικροῦ ἂν
ἥμαρτον· οὗτος δὲ μετὰ τὴν παρανομίαν ἐπηγωνίσατο. Ἄθλιε καὶ ταλαίπωρε,
Ἥμαρτες, ἡσύχασον, μὴ πρόσθες ἁμαρτίαν ἐφ' ἁμαρτίαν. Κἀκείνη μὲν ἐπανῆλθε,
πλοῦτον ἔχουσα τὸν Αἰγυπτιακόν· ἡ δὲ Ἐκκλησία ἐπανῆλθε, πλοῦτον ἔχουσα τὸν
ἀπὸ τῆς γνώμης, καὶ σωφρονεστέρα ἐφάνη. Ὄρα δὲ τὴν μανίαν τοῦ βαρβάρου.
Ἐξέβαλες τὸν ποιμένα· τί τὴν ἀγέλην διέσπασας; Ἀπέστησας τὸν κυβερνήτην· τί τοὺς
οἴακας κατέκλασας; Τὸν ἀμπελουργὸν ἐξέβαλες· τί τὰς ἀμπέλους ἀνέσπασας; τί τὰ
μοναστήρια διέφθειρας; Βαρβάρων ἔφοδον ἐμιμήσω.
βʹ. Ἐποίησεν ἅπαντα, ἵνα δειχθῇ ὑμῶν ἡ ἀνδρεία· ἐποίησεν ἅπαντα, ἵνα μάθῃ
ὅτι ποίμνη ἐστὶν ἐνταῦθα ὑπὸ Χριστοῦ ποιμαινομένη. Ἔξω ὁ ποιμὴν, καὶ ἡ ἀγέλη
συνεκροτεῖτο, καὶ τὸ ἀποστολικὸν ἐπληροῦτο ῥῆμα· Οὐκ ἐν τῇ παρουσίᾳ μου μόνον,
ἀλλὰ καὶ ἐν τῇ ἀπουσίᾳ μου, μετὰ φόβου καὶ τρόμου τὴν ἑαυτῶν σωτηρίαν
κατεργάζεσθε. Καὶ τοιαῦτα ἠπείλουν δεδοικότες ὑμῶν τὴν ἀνδρείαν καὶ τῆς ἀγάπης
τὸ φίλτρον, καὶ τὸν πόθον τὸν περὶ ἐμέ. Οὐδὲν τολμῶμεν, φησὶν, ἐν τῇ πόλει· δότε
ἡμῖν αὐτὸν ἔξω. Λάβετέ με ἔξω, ἵνα μάθητε τὸν πόθον τῆς Ἐκκλησίας, μάθητε τῶν
ἐμῶν τέκνων τὴν εὐγένειαν, τῶν στρατιωτῶν τὴν ἰσχὺν, τῶν ὁπλιτῶν τὴν δύναμιν,
τῶν διαδημάτων τὴν περιφάνειαν, τοῦ πλούτου τὴν περιουσίαν, τῆς ἀγάπης τὸ
μέγεθος, τῆς καρτερίας τὴν ὑπομονὴν, τῆς ἐλευθερίας τὸ ἄνθος, τῆς νίκης τὸ
περιφανὲς, τῆς ἥττης σου τὸν γέλωτα. Ὢ καινῶν καὶ παραδόξων πραγμάτων· ὁ
ποιμὴν ἔξω, καὶ ἡ ἀγέλη σκιρτᾷ· ὁ στρατηγὸς πόῤῥω, καὶ οἱ 52.444 στρατιῶται
ὡπλίζοντο. Οὐκέτι ἡ ἐκκλησία εἶχε τὸ στρατόπεδον μόνον, ἀλλὰ καὶ ἡ πόλις
ἐκκλησία ἐγένετο. Αἱ ῥῦμαι, αἱ ἀγοραὶ, ὁ ἀὴρ ἡγιάζετο· αἱρετικοὶ ἐπεστρέφοντο, οἱ
Ἰουδαῖοι βελτίους ἐγένοντο· οἱ ἱερεῖς κατεδικάζοντο, καὶ οἱ Ἰουδαῖοι τὸν Θεὸν
εὐφήμουν, καὶ ἡμῖν προσέτρεχον. Οὕτω καὶ ἐπὶ τοῦ Χριστοῦ ἐγένετο. Καΐφας
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

ἐσταύρωσε, καὶ λῃστὴς ὡμολόγησεν. Ὢ καινῶν καὶ παραδόξων πραγμάτων· ἱερεῖς
ἀπέκτειναν, καὶ μάγοι προσεκύνησαν. Μὴ ξενιζέτω ταῦτα τὴν Ἐκκλησίαν. Εἰ μὴ
ταῦτα ἐγένετο, ὁ πλοῦτος ἡμῶν οὐκ ἂν ἐφάνη· ἦν μὲν, οὐκ ἂν δὲ ἐφάνη. Ὥσπερ γὰρ
ὁ Ἰὼβ δίκαιος μὲν ἦν, οὐκ ἂν δὲ ἐφάνη, εἰ μὴ τὰ τραύματα, καὶ οἱ σκώληκες· οὕτω
καὶ ὁ ὑμέτερος πλοῦτος, εἰ μὴ αἱ ἐπιβουλαὶ, οὐκ ἂν ἐφάνη. Ἀπολογούμενος δὲ τῷ
Ἰὼβ ὁ Θεός φησιν, ὅτι Οἴει με ἄλλως σοι κεχρηματικέναι, ἢ ἵνα δίκαιος ἀναφανῇς;
Ἐπεβούλευσαν ἐκεῖνοι, ἐπολέμησαν, καὶ ἡττήθησαν. Πῶς ἐπολέμησαν; Ῥοπάλοις.
Πῶς ἡττήθησαν; Εὐχαῖς. Ἐάν τίς σε ῥαπίσῃ εἰς τὴν δεξιὰν σιαγόνα, στρέψον αὐτῷ καὶ
τὴν ἄλλην. Σὺ ῥόπαλα εἰσφέρεις εἰς τὴν ἐκκλησίαν καὶ πολεμεῖς· ὅπου εἰρήνη πᾶσι,
πολέμου ἄρχῃ· οὐδὲ τὸν τόπον ᾐδέσθης, ἄθλιε καὶ ταλαίπωρε, οὐδὲ τῆς ἱερωσύνης τὸ
ἀξίωμα, οὐδὲ τῆς ἀρχῆς τὸ μέγεθος. Τὸ φωτιστήριον αἱμάτων ἐμπέπλησται· ὅπου
ἁμαρτημάτων ἅφεσις, αἱμάτων ἕκχυσις.
Ἐν ποίᾳ παρατάξει ταῦτα γίνεται; Βασιλεὺς εἰσέρχεται καὶ ῥίπτει ἀσπίδα καὶ
διάδημα· σὺ εἰσῆλθες, καὶ ῥόπαλα ἥρπασας. Ἐκεῖνος καὶ τὰ συνθήματα τῆς βασιλείας
ἔξω ἀφίησι· σὺ τὰ συνθήματα τοῦ πολέμου ἐνταῦθα εἰσήνεγκας. Ἀλλὰ τὴν νύμφην
μου οὐδὲν ἔβλαψας, ἀλλὰ μένει τὸ κάλλος αὐτῆς ἐπιδεικνυμένη.
γʹ. ∆ιὰ τοῦτο χαίρω, οὐχ ὅτι ἐνικήσατε. Εἰ παρήμην, ἐμεριζόμην μεθ' ὑμῶν
τὴν νίκην· ἐπειδὴ δὲ ἀνεχώρησα, γυμνὸν ὑμῶν τὸ τρόπαιον ἐφάνη. Ἀλλὰ καὶ τοῦτο
ἐμὸν ἐγκώμιον, καὶ πάλιν μερίζομαι ἐγὼ μεθ' ὑμῶν τὴν νίκην, ὅτι οὕτως ὑμᾶς
ἀνέθρεψα, ὡς καὶ ἀπόντος τοῦ πατρὸς τὴν οἰκείαν εὐγένειαν ἐπιδείκνυσθαι. Ὥσπερ
γὰρ οἱ γενναῖοι τῶν ἀθλητῶν καὶ ἀπόντος τοῦ παιδοτρίβου τὴν ἑαυτῶν ῥώμην
ἐπιδείκνυνται· οὕτω καὶ ἡ εὐγένεια τῆς ὑμετέρας πίστεως καὶ ἀπόντος τοῦ
διδασκάλου τὴν οἰκείαν εὐμορφίαν ἐπεδείξατο. Τίς χρεία λόγων; Οἱ λίθοι βοῶσιν· οἱ
τοῖχοι φωνὴν ἀφιᾶσιν. Ἄπελθε εἰς βασιλικὰς αὐλὰς, καὶ ἀκούεις εὐθέως· Οἱ λαοὶ
Κωνσταντινουπόλεως. Ἄπελθε εἰς τὴν θάλατταν, εἰς τὴν ἔρημον, εἰς τὰ ὄρη, εἰς τὰς
οἰκίας, τὸ ἐγκώμιον ὑμῶν ἀναγέγραπται. Ἐν τίνι ἐνικήσατε; Οὐ χρήμασιν, ἀλλὰ
πίστει. Ὢ λαὸς φιλοδιδάσκαλος, ὢ λαὸς φιλοπάτωρ, μακαρία ἡ πόλις, οὐ διὰ κίονας
καὶ χρυσοῦν ὄροφον, ἀλλὰ διὰ τὴν ὑμετέραν ἀρετήν. Τοσαῦται αἱ ἐπιβουλαὶ, καὶ αἱ
εὐχαὶ ὑμῶν ἐνίκησαν· καὶ μάλα εἰκότως· καὶ γὰρ ἐκτενεῖς ἦσαν αἱ εὐχαὶ, καὶ αἱ πηγαὶ
τῶν δακρύων ἐπέῤῥεον. Ἐκεῖνοι βέλη, ὑμεῖς δὲ δάκρυα· ἐκεῖνοι θυμὸν, ὑμεῖς δὲ
πραΰτητα. Ὃ βούλει ποίησον· ὑμεῖς εὔχεσθε. Κἀκεῖνοι, οἳ ἀντέλεγον, ποῦ νῦν 52.445
εἰσιν; Ὅπλα ἐκινήσαμεν; μὴ τόξα ἐτείναμεν; μὴ βέλη ἀφήκαμεν; Εὐχόμεθα, κἀκεῖνοι
ἔφυγον· ὡς γὰρ ἀράχνη διεσπάσθησαν, καὶ ὑμεῖς ὡς πέτρα ἑστήκατε. Μακάριος ἐγὼ
δι' ὑμᾶς. Ἤδειν μὲν καὶ πρὸ τούτου ἡλίκον ἔχω πλοῦτον, ἐθαύμασα δὲ καὶ νῦν.
Πόῤῥω ἤμην, καὶ πόλις μετῳκίζετο. ∆ι' ἕνα ἄνθρωπον τὸ πέλαγος πόλις ἐγίνετο.
Γυναῖκες, ἄνδρες, παιδία ἄωρα τὴν ἡλικίαν, γυναῖκες βαστάζουσαι παιδία,
κατετόλμων πελάγους, κατεφρόνησαν κυμάτων. Οὐ δοῦλος ἐδεδοίκει δεσπότην, οὐ
γυνὴ τῆς φυσικῆς ἀσθενείας ἐμέμνητο. Γέγονεν ἡ ἀγορὰ ἐκκλησία, τὰ πανταχοῦ δι'
ἡμᾶς. Τίνα γὰρ οὐκ ἐπαιδεύσατε; Βασιλίδα συγχορεύουσαν ἐλάβετε· οὐ γὰρ
ἀποκρύψομαι τὸν ζῆλον αὐτῆς. Οὐ βασιλίδα κολακεύων ταῦτα λέγω, ἀλλ' εὐσέβειαν
θεραπεύων· οὐ γὰρ ἀποκρύψομαι αὐτῆς τὸν ζῆλον. Οὐ γὰρ ὅπλα ἔλαβεν, ἀλλὰ
κατορθώματα ἀρετῆς. Ἀπηγόμην τότε, ἴστε πῶς. ∆εῖ γὰρ καὶ λυπηρὰ εἰπεῖν, ἵνα
μάθητε τὰ χρηστά· ἀλλὰ μάθητε πῶς ἀπηγόμην, καὶ πῶς ἐπανῆλθον. Οἱ σπείροντες
ἐν δάκρυσιν, ἐν ἀγαλλιάσει θεριοῦσι. Πορευόμενοι ἐπορεύοντο καὶ ἔκλαιον,
βάλλοντες τὰ σπέρματα αὐτῶν· ἐρχόμενοι δὲ ἥξουσιν ἐν ἀγαλλιάσει, αἴροντες τὰ
δράγματα αὐτῶν. Ταῦτα τὰ ῥήματα ἐγένετο πράγματα. Μετ' εὐχαριστίας ὑπεδέξασθε
ὃν ὀδυνώμενοι προεπέμψατε. Καὶ οὐδὲ ἐν μακρῷ χρόνῳ· μετὰ μίαν ἡμέραν πάντα
ἐλύθη. Καὶ γὰρ ἡ ἀναβολὴ ἐγένετο δι' ὑμᾶς, ἐπεὶ ὁ Θεὸς ἐξ ἀρχῆς ἔλυσε. δʹ. Λέγω
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

ὑμῖν τὸ ἀπόῤῥητον. Ἐπεραιώθην τὸ πέλαγος μόνος τὴν Ἐκκλησίαν βαστάζων· ἡ γὰρ
ἀγάπη οὐ στενοῦται. Οὐκ ἐστενοχωρεῖτο τὸ πλοῖον· Οὐ στενοχωρεῖσθε γὰρ ἐν ἡμῖν.
Ἀπῄειν τὰ ὑμέτερα μεριμνῶν, κεχωρισμένος μὲν τῷ σώματι, συνημμένος δὲ τῇ
γνώμῃ· ἀπῄειν τὸν Θεὸν παρακαλῶν, καὶ παρακατατιθέμενος ὑμῶν τὴν ἀγάπην·
ἀπῄειν, ἐκαθεζόμην μόνος τὰ ὑμέτερα μεριμνῶν, βουλευόμενος περὶ ἀποδημίας
μόνος. Ἀθρόον ἀωρίας γενομένης γράμματα ἔπεμψεν ἡ θεοφιλεστάτη αὕτη ἐν τῇ
πρώτῃ ἡμέρᾳ, ταῦτα λέγουσα τὰ ῥήματα (δεῖ γὰρ αὐτῆς καὶ τὰ ῥήματα εἰπεῖν)· Μὴ
νομίσῃ σου ἡ ἁγιωσύνη ὅτι ἔγνων τὰ γεγενημένα. Ἀθῶος ἐγὼ ἀπὸ τοῦ αἵματός σου.
Ἄνθρωποι πονηροὶ καὶ διεφθαρμένοι ταύτην τὴν μηχανὴν διεσκεύασαν· τῶν δὲ
ἐμῶν δακρύων μάρτυς ὁ Θεὸς, ᾧ ἱερεύω. Οἵαν σπονδὴν ἐξέχεε; τὰ γὰρ δάκρυα αὐτῆς
σπονδὴ ἐγένετο. Ὧ ἱερεύω. Ἡ ἱέρεια, αὐτοχειροτόνητος θύουσα τῷ Θεῷ καὶ
σπένδουσα δάκρυα καὶ ἐξομολόγησιν καὶ μετάνοιαν, οὐχ ὑπὲρ ἱερέως, ἀλλ' ὑπὲρ
Ἐκκλησίας, ὑπὲρ δήμου διεσπαρμένου. Ἐμέμνητο, ἐμέμνητο καὶ τῶν παιδίων καὶ
τοῦ βαπτίσματος. Μέμνημαι ὅτι διὰ τῶν χειρῶν τῶν σῶν τὰ παιδία τὰ ἐμὰ
ἐβαπτίσθη. Ταῦτα ἡ βασίλισσα· οἱ δὲ ἱερεῖς περὶ φθόνου πάντες ἠγνόουν τὸ χωρίον,
ἔνθα κατέλυον. Καὶ τὸ δὴ θαυμαστὸν εἰπεῖν, ἐκείνη μὲν ὡς ὑπὲρ τέκνου τρέμουσα
περιῄει πανταχοῦ, οὐ τῷ σώματι, ἀλλὰ τῇ ἰδίᾳ πομπῇ τῶν στρατιωτῶν. Οὐ γὰρ
κατείληφε τὸ χωρίον, ἔνθα διῆγον. Πανταχοῦ ἔπεμπε μεριμνῶσα μὴ δολοφονηθῇ,
μὴ ἀναιρεθῇ, καὶ ἀπολέσωμεν τὸ θήραμα. Τοῦτο μόνον, καὶ τὰ παρ' ἐμαυτῆς
ἐπιδείκνυμι. Ζητῶ μόνον, καὶ οὐ περιγίνονται, 52.446 Οἱ ἐχθροὶ πανταχοῦ περιῄεσαν
δίκτυα ἁπλοῦντες, ἵνα λάβωσι καὶ ἐπαναγάγωσιν εἰς τὰς ἐκείνων χεῖρας. Εἶτα καὶ
παρεκάλει, καὶ τῶν γονάτων ἥπτετο τῶν βασιλικῶν, κοινωνὸν τὸν ἄνδρα ποιοῦσα
τοῦ θηράματος· καθάπερ ὁ Ἀβραὰμ τὴν Σάῤῥαν, οὕτως αὐτὴ τὸν ἄνδρα.
Ἀπωλέσαμεν, φησὶ, τὸν ἱερέα, ἀλλ' ἐπαναγάγωμεν. Οὐκ ἔστιν ἡμῖν οὐδεμία ἐλπὶς
τῆς βασιλείας, ἐὰν μὴ ἐκεῖνον ἐπαναγάγωμεν. Ἀμήχανον ἐμὲ κοινωνῆσαί τινι τῶν
ταῦτα ἐργασαμένων· δάκρυα ἐξαφιεῖσα, τὸν Θεὸν ἱκετεύουσα, πᾶσαν μηχανὴν
ἐπιδεικνυμένη. Ἴστε καὶ ὑμεῖς μεθ' ὅσης εὐνοίας ἡμᾶς ὑπεδέξατο, πῶς ἐνηγκαλίσατο
ὡς οἰκεῖα μέλη, πῶς μεθ' ὑμῶν ἔλεγε καὶ αὐτὴ σπουδάζειν.
Οὐδὲ γὰρ ταῦτα τὰ ῥήματα ἔλαθε τὴν εὐγνωμοσύνην ὑμῶν, ὅτι ἀπεδέξασθε
τὴν μητέρα τῶν Ἐκκλησιῶν, τὴν τροφὸν τῶν μοναζόντων, καὶ προστάτιν τῶν
ἁγίων, τῶν πτωχῶν τὴν βακτηρίαν. Ὁ ἔπαινος ἐκείνης δόξα εἰς Θεὸν γίνεται,
στέφανος τῶν Ἐκκλησιῶν. Εἴπω θερμὸν αὐτῆς πόθον; εἴπω φιλοτιμίαν τὴν περὶ ἐμέ;
Ἐν ἑσπέρᾳ βαθείᾳ χθὲς ἀπέστειλε ταῦτα λέγουσα τὰ ῥήματα· Εἰπὲ πρὸς αὐτόν· ἡ εὐχή
μου πεπλήρωται· ἀπῄτησα τὸ κατόρθωμα· ἐστεφανώθην μᾶλλον τοῦ διαδήματος·
ἀπέλαβον τὸν ἱερέα, ἀπέδωκα τὴν κεφαλὴν τῷ σώματι, τὸν κυβερνήτην τῇ νηῒ, τὸν
ποιμένα τῇ ποίμνῃ, τὸν νυμφίον τῇ παστάδι. εʹ. Κατῃσχύνθησαν οἱ μοιχοί. Ἐὰν ζήσω,
ἐὰν ἀποθάνω, οὐκέτι μοι μέλει. Ἴδετε τοῦ πειρασμοῦ τὰ κατορθώματα. Τί ποιήσω,
ἵνα ὑμῖν ἀξίαν ἀποδῶ τῆς ἀγάπης τὴν ἀμοιβήν; Ἀξίαν μὲν οὐ δύναμαι, ἣν δὲ ἔχω,
δίδωμι. Ἀγαπῶ ἑτοίμως τὸ αἷμά μου ἐκχέειν ὑπὲρ τῆς ὑμετέρας σωτηρίας. Οὐδεὶς
ἔχει τέκνα τοιαῦτα, οὐδεὶς ἀγέλην τοιαύτην, οὐδεὶς ἄρουραν οὕτως εὐθαλῆ. Οὐ
χρεία μοι γεωργίας· ἐγὼ καθεύδω, καὶ οἱ στάχυες κομῶσιν. Οὐ χρεία μοι πόνου· ἐγὼ
ἡσυχάζω, καὶ τὰ πρόβατα τῶν λύκων περιγίνονται. Τί ὑμᾶς καλέσω; πρόβατα ἢ
ποιμένας, ἢ κυβερνήτας, ἢ στρατιώτας καὶ στρατηγούς; Πάντα ὑμῖν ἐπαληθεύω τὰ
ῥήματα. Ὅταν ἴδω τὴν εὐταξίαν, πρόβατα καλῶ· ὅταν ἴδω τὴν πρόνοιαν, ποιμένας
προσαγορεύω· ὅταν ἴδω τὴν σοφίαν, κυβερνήτας ὀνομάζω· ὅταν ἴδω τὴν ἀνδρείαν
καὶ τὴν εὐτονίαν, στρατιώτας καὶ στρατηγοὺς ὑμᾶς ἅπαντας λέγω. Ὢ πόνος, ὢ
πρόνοια λαοῦ· ἠλάσατε τοὺς λύκους, καὶ οὐκ ἀμεριμνήσετε. Οἱ ναῦται οἱ μεθ' ὑμῶν
καθ' ὑμῶν γεγόνασιν, οἵτινες τὸν πόλεμον τῷ πλοίῳ κατεσκεύασαν. Βοᾶτε ἔξω τὸν
κλῆρον, καὶ ἄλλον κλῆρον τῇ Ἐκκλησίᾳ. Τίς χρεία βοῆς; Ἀπῆλθον, καὶ ἀπηλάθησαν,
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

3

μηδενὸς διώκοντος ἐφυγαδεύθησαν. Οὐ κατηγορεῖ αὐτῶν ἄνθρωπος, ἀλλὰ τὸ
συνειδός. Εἰ ἐχθρὸς ὠνείδισέ με, ὑπήνεγκα ἄν. Οἱ μεθ' ἡμῶν καθ' ἡμῶν γεγόνασιν· οἱ
μεθ' ἡμῶν τὸ πλοῖον κυβερνῶντες, τὸ πλοῖον καταποντίσαι ἠθέλησαν. Ἐθαύμασα
ὑμῶν τὴν σύνεσιν. Ταῦτα λέγω, οὐκ εἰς στάσιν ὑμᾶς ἀλείφων. Στάσις γὰρ τὰ
ἐκείνων, τὰ δὲ ὑμέτερα ζῆλος. Οὐ γὰρ ἠξιώσατε αὐτοὺς ἀναιρεθῆναι, ἀλλὰ
κωλυθῆναι τοῦτο καὶ ὑπὲρ ὑμῶν, καὶ ὑπὲρ τῆς Ἐκκλησίας, ἵνα μὴ πάλιν ὑποβρύχιος
γένηται. Ἡ γὰρ ἀνδρεία ὑμῶν οὐκ ἀφῆκε γενέσθαι τὸν χειμῶνα, ἀλλ' ἡ γνώμη
ἐκείνων τὸ κλυδώνιον εἰργάσατο. Ἐγὼ δὲ, οὐ τῷ τέλει, ἀλλὰ τῇ γνώμῃ ἐκείνων
λογίζομαι. Ἄνθρωπος θυσιαστηρίῳ παρεστηκὼς, δήμου τοσούτου 52.447
ἐγκεχειρισμένος πρόνοιαν, ὀφείλων καταστέλλειν τὰ λυπηρὰ, ηὔξησας τὸν χειμῶνα,
κατὰ σαυτοῦ τὸ ξίφος ἤλασας, τὰ τέκνα τὰ σὰ ἀναλώσας τῇ γνώμῃ, εἰ καὶ μὴ τῇ
πείρᾳ. Ἀλλ' ὁ Θεὸς ἐκώλυσεν. Ὥστε θαυμάζω ὑμᾶς καὶ ἐπαινῶ, ὅτι μετὰ τὸν πόλεμον
καὶ τῆς εἰρήνης γενομένης σκοπεῖτε, ὅπως ἂν τελεία γένηται εἰρήνη. ∆εῖ γὰρ τὸν
κυβερνήτην μετὰ τῶν ναυτῶν ὁμόνοιαν ἔχειν· ἐὰν γὰρ διαστασιάζωσι,
καταποντίζεται τὸ σκάφος. Ὑμεῖς κατορθώσατε τὴν εἰρήνην μετὰ τὴν τοῦ Θεοῦ
χάριν· ὑμᾶς κοινωνοὺς ποιήσομαι τῆς ἀσφαλείας. Χωρὶς ὑμῶν οὐδὲν ἐργάσομαι, εἶτα
καὶ τῆς θεοφιλεστάτης Αὐγούστης. Καὶ γὰρ κἀκείνη φροντίζει καὶ μεριμνᾷ καὶ
πάμπολλα ποιεῖ, ὥστε τὸ φυτευθὲν μεῖναι βέβαιον, ὥστε τὴν Ἐκκλησίαν
ἀκλυδώνιστον μεῖναι. Ἐπῄνεσα οὖν καὶ ὑμῶν τὴν 52.448 σύνεσιν, καὶ τῶν βασιλέων
τὴν πρόνοιαν. Οὐ γὰρ οὕτως αὐτοῖς μέλει περὶ πολέμου, ὡς περὶ Ἐκκλησίας, οὐχ
οὕτω περὶ πόλεως, ὡς περὶ Ἐκκλησίας.
Παρακαλέσωμεν τὸν Θεὸν, ἀξιώσωμεν ἐκείνην, παραμείνωμεν ταῖς εὐχαῖς·
καὶ μὴ, ἐπειδὴ ἐλύθη τὰ δεινὰ, χαυνότεροι γενώμεθα. ∆ιὰ τοῦτο ἕως τῆς σήμερον
εὐχόμεθα λυθῆναι τὰ δεινά. Εὐχαριστήσωμεν τῷ Θεῷ, ὥσπερ τότε ἀνδρεῖοι, οὕτω
καὶ σήμερον σπουδαῖοι. Ὑπὲρ δὲ τούτων ἁπάντων εὐχαριστήσωμεν τῷ Θεῷ, ᾧ ἡ
δόξα καὶ τὸ κράτος ἅμα τῷ Υἱῷ σὺν τῷ ἀγαθῷ καὶ ζωοποιῷ Πνεύματι νῦν καὶ ἀεὶ,
καὶ εἰς τοὺς αἰῶνας τῶν αἰώνων. Ἀμήν.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

4

