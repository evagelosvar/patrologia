De partibus animae
Τῆς δὲ ψυχῆς μέρη γʹ. Ἐπιθυμητικὸν, θυμικὸν, λογιστικόν. Ἀρεταῖς δὲ τέσσαρσι
τεθρόνισται ὁ ἱερώτατος νοῦς· φρονήσεως, ἀνδρείας, δικαιοσύνης, σωφροσύνης.
95.232 Τρισσὸς ὁ νόμος. Φυσικὸς, γραπτὸς, καὶ ὁ ἐν χάριτι πνευματικός. Τετραμερές
ἐστι τὸ σῶμα, ὥσπερ τις κόσμος συνιστάμενον· τριμερὴς δὲ ἐν αὐτῷ ἡ ψυχὴ, προτυποῦσα
τὴν ἁγίαν Τριάδα, καὶ τοῦτο ταῖς ἐνεργείαις ζωοποιοῦσα. Τριμερῆ δὲ λέγομεν τὴν
ψυχὴν, ἔχουσαν λογικὸν, θυμικὸν, καὶ ἐπιθυμητικόν· ἵνα διὰ μὲν τοῦ λογικοῦ
λογίζηται· διὰ δὲ τοῦ θυμικοῦ ὀργίζηται τοῖς δαίμοσιν, ἀνδριζομένη κατ' αὐτῶν· διὰ δὲ
τοῦ ἐπιθυμητικοῦ ἀγαπᾷ τὸν Θεόν. Τοῦτο δὲ τὸ ἐπιθυμητικὸν διαιρεῖται εἰς τρία· εἰς
θεϊκὸν, φυσικὸν, καὶ μέσον τὸ σαρκικὸν, ὅπερ ἐστὶ τὸ διαβολικόν. Θεϊκὸν μὲν, τὸ
ἀγαπᾷν τὸν Θεόν· φυσικὸν δὲ, τὸ ἀγαπᾷν τὰ κατὰ φύσιν· σαρκικὸν δὲ τὸ ἀγαπᾷν τὰ
παρὰ φύσιν· οἷον, κατὰ φύσιν μὲν ὁ γάμος· παρὰ φύσιν δὲ, ἡ πορνεία, καὶ τὰ τοιαῦτα.
Ἐν ἄλλῳ εὗρον. Τῆς ψυχῆς εἰσι δυνάμεις πέντε· νοῦς, διάνοια, δόξα, φαντασία,
αἴσθησις. Νοῦς ἐστι νοερὰ αἴσθησις τῆς ψυχῆς, καθ' ἣν δύναται αὐτοπτικῶς ὁρᾷν τὰ
πράγματα, καὶ δίχα ζητήσεων. ∆ιάνοιά ἐστι δύναμις τῆς ψυχῆς, καθ' ἣν μετὰ
συλλογισμοῦ δύναται γινώσκειν τὰ πράγματα· διὸ καὶ λέγεται διάνοια, παρὰ τὸ ὁδόν
τινα διανοίγειν. ∆όξα ἐστὶ δύναμις τῆς ψυχῆς, καθ' ἣν ἄνευ αἰτίας γινώσκει τὸ
πράγματα. Φαντασία δύναμίς ἐστι τῆς ἀλόγου ψυχῆς. Αἴσθησίς ἐστι δύναμις τῆς ψυχῆς,
ἀντιληπτικὴ τῶν ὑλῶν.

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

